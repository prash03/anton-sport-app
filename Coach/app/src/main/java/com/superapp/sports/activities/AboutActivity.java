package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.fragments.SettingFragment;
import com.superapp.sports.model.AboutResponse;
import com.superapp.sports.utils.SharedHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class AboutActivity extends AppCompatActivity {
    ImageView imgBack;
    TextView tvDescription;
    String authkey="",descript="";
    private ProgressDialog progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        imgBack=findViewById(R.id.imgBack);
        tvDescription=findViewById(R.id.tvDescription);

        authkey= SharedHelper.getKey(this,AUTH_TOKEN);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

        callaboutApi(authkey);
    }

    private void callaboutApi(String authToken) {
        progressBar = new ProgressDialog(this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<AboutResponse> responseCall = api.aboutApi(authToken);
        responseCall.enqueue(new Callback<AboutResponse>() {
            @Override
            public void onResponse(Call<AboutResponse> call, Response<AboutResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        descript=response.body().getDescription();
                        tvDescription.setText(Html.fromHtml(descript));

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(AboutActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AboutActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<AboutResponse> call, Throwable t) {
                progressBar.dismiss();
                Log.d("errorResponse:",t.getMessage());
            }
        });
    }
}