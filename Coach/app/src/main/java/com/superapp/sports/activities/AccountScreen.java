package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.superapp.sports.R;
import com.superapp.sports.fragments.SettingFragment;

public class AccountScreen extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack;
    RelativeLayout rlLiked,rlhide,rlBlocked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_screen);

        rlBlocked=findViewById(R.id.rlBlocked);
        rlhide=findViewById(R.id.rlhide);
        rlLiked=findViewById(R.id.rlLiked);
        imgBack=findViewById(R.id.imgBack);

        imgBack.setOnClickListener(this);
        rlBlocked.setOnClickListener(this);
        rlhide.setOnClickListener(this);
        rlLiked.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.rlBlocked:
                Intent ib=new Intent(this,PostLiked.class);
                ib.putExtra("posttype","blocked");
                startActivity(ib);
                break;

            case R.id.rlLiked:
                Intent in=new Intent(this,PostLiked.class);
                in.putExtra("posttype","liked");
                startActivity(in);
                break;

            case R.id.rlhide:
                Intent i=new Intent(this,PostLiked.class);
                i.putExtra("posttype","hide");
                startActivity(i);
                break;

        }
    }
}