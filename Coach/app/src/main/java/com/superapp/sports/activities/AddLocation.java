
package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.superapp.sports.R;
import com.superapp.sports.adapters.AddLocationAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.LocationListResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.activities.CreateNewPost.TAG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class AddLocation extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recycleLocation;
    AddLocationAdapter addLocationAdapter;
    ImageView imgBack,imgnext;
    String authKey="",searchKey="",countryCode="",countryName="", selectedAddress="";
    private List<LocationListResponse.Datum> dataList;
    RelativeLayout errorLayout;
    TextView ed_search;
    FusedLocationProviderClient mFusedLocationClient;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    String lat = "";
    String lng = "";
    FrameLayout search_frm_lyt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);

        imgnext=findViewById(R.id.imgnext);
        search_frm_lyt=findViewById(R.id.search_frm_lyt);
        ed_search=findViewById(R.id.ed_search);
        imgBack=findViewById(R.id.imgBack);
        errorLayout=findViewById(R.id.errorLayout);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        imgBack.setOnClickListener(this);
        imgnext.setOnClickListener(this);

        authKey= SharedHelper.getKey(AddLocation.this,AUTH_TOKEN);
        searchKey=ed_search.getText().toString();

        recycleLocation=findViewById(R.id.recycleLocation);
        recycleLocation.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(AddLocation.this);
        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 200);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getResources().getString(R.string.map_api));
        }
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        //setting place api field to arralist
        autocompleteFragment.setPlaceFields(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.LAT_LNG, com.google.android.libraries.places.api.model.Place.Field.ADDRESS));


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(com.google.android.libraries.places.api.model.Place place) {
                // TODO: Get info about the selected place.
                //Log.i(TAG, "Place: " + place.getLatLng() + ", " + place.getId());
                String data =place.getLatLng().toString();
                String [] tempArray = data.substring(data.indexOf("(")+1, data.lastIndexOf(")")).split(",");
                //mMap.clear();
                lat=tempArray[0];
                lng=tempArray[1];

                Log.i(TAG, "latitudeVal: " + tempArray[0]);
                Log.i(TAG, "longitudeVal: " + tempArray[1]);

                ed_search.setText(place.getAddress());
                selectedAddress=place.getAddress();

                if(search_frm_lyt.getVisibility() == View.GONE){
                    search_frm_lyt.setVisibility(View.VISIBLE);

                }

                Log.i(TAG, "addressVal: " + selectedAddress);
                Log.i(TAG, "FulladdressVal: " + place.getAddress());

                search_frm_lyt.setVisibility(View.GONE);
                ed_search.setVisibility(View.VISIBLE);

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

        ed_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_frm_lyt.setVisibility(View.VISIBLE);
                ed_search.setVisibility(View.GONE);
                recycleLocation.setVisibility(View.GONE);
            }
        });

        /*ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchKey=ed_search.getText().toString();
                callLocationApi();
            }
        });*/

        callLocationApi();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                onBackPressed();
                break;

                case R.id.imgnext:
                    Intent returIntent = new Intent();
                    returIntent.putExtra("selectedAddress",selectedAddress);
                    setResult(Activity.RESULT_OK, returIntent);
                    AddLocation.this.finish();
                break;
        }
    }

    private void callLocationApi() {
        /*progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<LocationListResponse> responseCall = api.locationListApi(authKey,countryName,countryCode,searchKey);
        responseCall.enqueue(new Callback<LocationListResponse>() {
            @Override
            public void onResponse(Call<LocationListResponse> call, Response<LocationListResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData();

                        if(dataList.size()>0){
                            errorLayout.setVisibility(View.GONE);
                            addLocationAdapter = new AddLocationAdapter(AddLocation.this, dataList,searchKey);
                            recycleLocation.setAdapter(addLocationAdapter);
                            addLocationAdapter.onItemClick(new AddLocationAdapter.OnClickDataCart() {
                                @Override
                                public void addNewClass(String address) {
                                    Log.i(TAG, "addNewClass address: " + address);
                                    selectedAddress=address;
                                    Intent intent = new Intent();
                                    intent.putExtra("selectedAddress",selectedAddress);
                                    setResult(RESULT_OK,intent);
                                    AddLocation.this.finish();


                                }
                            });
                        }
                        else {
                            recycleLocation.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(AddLocation.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddLocation.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<LocationListResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }
}