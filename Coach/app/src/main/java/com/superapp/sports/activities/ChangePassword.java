package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.ChangePasswordResponse;
import com.superapp.sports.utils.SharedHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack;
    String authkey="";
    Button btSubmit;
    EditText ed_current,ed_new,ed_confirm;
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        btSubmit=findViewById(R.id.btSubmit);
        imgBack=findViewById(R.id.imgBack);
        ed_current=findViewById(R.id.ed_current);
        ed_new=findViewById(R.id.ed_new);
        ed_confirm=findViewById(R.id.ed_confirm);

        imgBack.setOnClickListener(this);
        btSubmit.setOnClickListener(this);

        authkey= SharedHelper.getKey(this,AUTH_TOKEN);
    }

    private void submitpasswordApi(String authToken,String current,String newPass) {
        progressBar = new ProgressDialog(ChangePassword.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<ChangePasswordResponse> responseCall = api.changePassword(authToken,current,newPass);
        responseCall.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(ChangePassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(ChangePassword.this,SecurityScreen.class);
                        startActivity(i);
                        //logout();

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(ChangePassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChangePassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                progressBar.dismiss();
            }
        });
    }
    public void logout() {

        FacebookSdk.setAdvertiserIDCollectionEnabled(false);
        SharedHelper.putKey(this, AUTH_TOKEN, "");
        SharedPreferences preferences = getSharedPreferences("Cache", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        Intent goToLogin = new Intent(this, LoginActivity.class);
        goToLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(goToLogin);
        finish();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.btSubmit:
                String newPass=ed_new.getText().toString();
                String confPass=ed_confirm.getText().toString();
                if (ed_current.getText().toString().equals("")){
                    Toast.makeText(ChangePassword.this, "Enter Current Password", Toast.LENGTH_SHORT).show();

                }
                else if(ed_new.getText().toString().equals("")){
                    Toast.makeText(ChangePassword.this, "Enter New Password", Toast.LENGTH_SHORT).show();
                }else if(ed_confirm.getText().toString().equals("")){
                    Toast.makeText(ChangePassword.this, "Enter Confirm Password", Toast.LENGTH_SHORT).show();
                }else if(!newPass.equals(confPass)){
                    Toast.makeText(ChangePassword.this, "New Password and Confirm Password Is Not Equal", Toast.LENGTH_SHORT).show();
                }
                else {
                    submitpasswordApi(authkey,ed_current.getText().toString(),ed_new.getText().toString());
                }
                break;
        }
    }
}