package com.superapp.sports.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.superapp.sports.R;
import com.superapp.sports.adapters.InboxAdapter;
import com.superapp.sports.agorafiles.ChatManager;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.ChatListResponse;
import com.superapp.sports.model.InboxResponse;
import com.superapp.sports.utils.Constants;
import com.superapp.sports.utils.DeliverItToApplication;
import com.superapp.sports.utils.SharedHelper;
import com.superapp.sports.utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.USERID;

public class ChatInbox extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = ChatInbox.class.getName();
    RecyclerView chat_list;
    InboxAdapter inboxAdapter;
    private List<InboxResponse> Cdata;
    ImageView imgBack, send_btn, cam_icon;
    FirebaseUser firebaseUser;
    DatabaseReference reference;
    String taguserId = "", myId = "", authKey = "", MSG = "", currentTIme = "", heading = "", Uimg = "";
    private List<ChatListResponse.Datum> dataList;
    EditText userMsgEt_ids;
    SimpleDateFormat simpleDateFormat;
    Calendar calander;
    Bitmap image_bitmap;
    private static final int REQUEST_CAMERA = 211;
    private static final int SELECT_FILE = 121;
    TextView tv_heading;

    RelativeLayout rlaudiolayout, rlvideolayout;

    RtmClient mRtmClient;
    Timer timer;
    public static int repeatTime = 5000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_inbox);

        tv_heading = findViewById(R.id.tv_heading);
        chat_list = findViewById(R.id.chat_list);
        imgBack = findViewById(R.id.imgBack);
        send_btn = findViewById(R.id.send_btn);
        userMsgEt_ids = findViewById(R.id.userMsgEt_ids);
        cam_icon = findViewById(R.id.cam_icon);

        rlaudiolayout = findViewById(R.id.rl_audio_layout);
        rlvideolayout = findViewById(R.id.rl_video_layout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        chat_list.setLayoutManager(linearLayoutManager);
        chat_list.setHasFixedSize(true);
        taguserId = getIntent().getStringExtra("uid");
        heading = getIntent().getStringExtra("uname");
        Uimg = getIntent().getStringExtra("uImg");


        myId = SharedHelper.getKey(ChatInbox.this, USERID);
        authKey = SharedHelper.getKey(ChatInbox.this, AUTH_TOKEN);

        Log.i(TAG, "onCreatetaguserId: " + taguserId + heading);

        tv_heading.setText(heading);

        ChatManager mChatManager = DeliverItToApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();

        imgBack.setOnClickListener(this);
        send_btn.setOnClickListener(this);
        cam_icon.setOnClickListener(this);
        rlaudiolayout.setOnClickListener(this);
        rlvideolayout.setOnClickListener(this);

        calander = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("HH:mm");

        currentTIme = simpleDateFormat.format(calander.getTime());
        System.out.println("Current time => " + currentTIme);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(taguserId);


        mRtmClient.login(getString(R.string.agora_access_token), "1", new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                System.out.println("login success");
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                System.out.println("login failed: " + errorInfo.getErrorCode());
                System.out.println("login failed: " + errorInfo.getErrorDescription());
            }
        });

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                readMessage(myId, taguserId);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void selectPicture() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // tvEdit.setEnabled(true);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
        //final CharSequence[] items = {"Camera", "Gallery", "Cancel"};
       /* final CharSequence[] items = {"Camera","Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ChatInbox.this);
        builder.setTitle("Click Image");
        builder.setItems(items, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                if (items[which].equals("Camera"))
                {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
                *//*else if (items[which].equals("Gallery"))
                {
                    Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intentGallery.setType("image/*");
                    startActivityForResult(intentGallery.createChooser(intentGallery, "Select File"), SELECT_FILE);
                }*//*
                else if (items[which].equals("Cancel"))
                {
                    dialog.dismiss();
                }
            }
        });

        builder.show();//*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.rl_audio_layout:
                Intent vvIn = new Intent(getApplicationContext(), VoiceCallAct.class);
                vvIn.putExtra("usern", heading);
                vvIn.putExtra("userID", taguserId);
                vvIn.putExtra("uImg", Uimg);
                startActivity(vvIn);

                break;

            case R.id.rl_video_layout:
                Intent vioIn = new Intent(getApplicationContext(), VideoCallAct.class);
                vioIn.putExtra("usern", heading);
                vioIn.putExtra("userID", taguserId);
                vioIn.putExtra("uImg", Uimg);
                startActivity(vioIn);
                break;

            case R.id.send_btn:
                MSG = userMsgEt_ids.getText().toString().trim();

                if (MSG.equalsIgnoreCase("")) {
                    Toast.makeText(ChatInbox.this, "type a message", Toast.LENGTH_SHORT).show();
                } else {
                    //CallSendApi();
                    updateMessageApi(0);
                    //sendMessage("3", userId, userMsgEt_ids.getText().toString());
                    sendMessage(myId, taguserId, MSG);
                }
                break;

            case R.id.cam_icon:
                final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

                AlertDialog.Builder builder = new AlertDialog.Builder(ChatInbox.this);
                builder.setTitle("Add Photo!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Take Photo")) {
                            /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);*/
                            selectPicture();
                        } else if (items[item].equals("Choose from Gallery")) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        timer = new Timer();
        TimerTask mytimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getChatList();
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(mytimerTask, 0, repeatTime);
    }

    private void sendMessage(String sender, String receiver, String message) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("message", message);
        databaseReference.child("Chats").push().setValue(hashMap);

    }


    private void readMessage(final String myID, final String userID) {
        dataList = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataList.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    //ChatListDataResponse chatModal = snapshot.getValue(ChatListDataResponse.class);
                    // getChatList();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static byte[] getFileDataFromDrawable(Bitmap bitmap) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            return byteArrayOutputStream.toByteArray();
        }
    }

    private void updateMessageApi(final int check_value) {
        final ProgressDialog progressDialog = new ProgressDialog(ChatInbox.this);
        progressDialog.setTitle("Loading...");
        progressDialog.show();
        VolleyMultipartRequest stringRequest = new VolleyMultipartRequest(Request.Method.POST, Constants.SENDMSG,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        progressDialog.dismiss();
                        String res = new String(response.data);
                        try {
                            JSONObject responseData = new JSONObject(res);

                            String status = responseData.getString("status");
                            //String msg = responseData.getString("message");
                            if (Integer.parseInt(status) == 1) {
                                //JSONObject obj=jsonObject.getJSONObject("data");
                                userMsgEt_ids.setText("");
                                getChatList();
                            } else {
                                Toast.makeText(ChatInbox.this, "No record found", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse response = error.networkResponse;
                try {
                    if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500 || response.statusCode == 422 || response.statusCode == 503) {

                        Toast.makeText(ChatInbox.this, getResources().getString(R.string.oops_connect_your_internet), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("message", MSG);
                params.put("reciver_user_id", taguserId);
                params.put("time", currentTIme);
                Log.i(TAG, "getParams: " + params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                params.clear();
                if (check_value == 1) {
                    Log.i(TAG, "getByteData: " + "IFF");
                    params.put("image", new DataPart("image.jpg", getFileDataFromDrawable(image_bitmap), "image/jpeg"));

                } else {
                    Log.i(TAG, "getByteData: " + "Else");
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", authKey);
                System.out.println(headers + "..........head");
                return headers;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(Request<Object> request) {
                requestQueue.getCache().clear();
            }
        });
    }

    private void getChatList() {
        Api api = RestManager.instanceOf();
        Call<ChatListResponse> responseCall = api.chatlist(authKey, taguserId);
        responseCall.enqueue(new Callback<ChatListResponse>() {
            @Override
            public void onResponse(Call<ChatListResponse> call, Response<ChatListResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        dataList = response.body().getData();

                        //Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        inboxAdapter = new InboxAdapter(ChatInbox.this, dataList);
                        chat_list.setAdapter(inboxAdapter);
                        chat_list.scrollToPosition(inboxAdapter.getItemCount() -1);
                    } else if (response.body().getStatus() == 0) {

                        Toast.makeText(ChatInbox.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ChatInbox.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<ChatListResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
          /*  if (requestCode == REQUEST_CAMERA) {
                image_bitmap = (Bitmap) data.getExtras().get("data");
                Log.i(TAG, "onActivityResultimage_bitmap: " + image_bitmap);
                //imageView.setImageBitmap(srcBmp)
                updateMessageApi(1);

            } else */
            if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                try {
                    image_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    Log.i(TAG, "onActivityResultimage_bitmap: " + image_bitmap);
                    updateMessageApi(1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            image_bitmap = (Bitmap) data.getExtras().get("data");
            Log.i(TAG, "onActivityResultimage_bitmap: " + image_bitmap);
            //imageView.setImageBitmap(srcBmp)
            updateMessageApi(1);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}