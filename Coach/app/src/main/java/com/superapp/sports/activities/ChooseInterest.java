package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.ChooseAdapter;
import com.superapp.sports.adapters.ImageViewPagerAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.InterestResponse;
import com.superapp.sports.model.SubmitInterestResponse;
import com.superapp.sports.utils.SharedHelper;
import com.viewpagerindicator.LinePageIndicator;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.activities.CreateNewPost.TAG;
import static com.superapp.sports.utils.Constants.ACCOUNTTYPE;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class ChooseInterest extends AppCompatActivity implements View.OnClickListener{
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    LinePageIndicator indicator;
    protected Context mContext;
    RecyclerView recycleChoose;
    ChooseAdapter chooseAdapter;
    RelativeLayout rl_bottom;
    String accountType="",authKey="" , isfrom="";
    TextView tv_heading,tvSelected;
    private ProgressDialog progressBar;
    private List<InterestResponse.InterestDatum> dataList;
    private List<InterestResponse.SliderImage> sliderList;
    ArrayList<String> imageArr = new ArrayList<>();
    ArrayList<String> titleArr = new ArrayList<>();
    ArrayList<String> SubtitleArr = new ArrayList<>();
    ArrayList<Integer> selectedItemid = new ArrayList<>();
    ArrayList<Integer> sortedItemid = new ArrayList<>();
    int selectedPlay=0;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_interest);

        rl_bottom=findViewById(R.id.rl_bottom);
        tv_heading=findViewById(R.id.tv_heading);
        tvSelected=findViewById(R.id.tvSelected);
        imgBack=findViewById(R.id.imgBack);

        recycleChoose=findViewById(R.id.recycleChoose);
        recycleChoose.setLayoutManager(new GridLayoutManager(this, 2));
        recycleChoose.setHasFixedSize(true);

        accountType= SharedHelper.getKey(this,ACCOUNTTYPE);
        authKey= SharedHelper.getKey(this,AUTH_TOKEN);

        if(accountType.equals("Coach")){
            tv_heading.setText("Choose Coaching your Interest");
        }
        else if(accountType.equals("Player")){
            tv_heading.setText("Choose your Interest");
        }
        else if(accountType.equals("Sponser")){
            tv_heading.setText("Choose your Interest");
        }

        rl_bottom.setOnClickListener(this);

        isfrom = getIntent().getStringExtra("ISFROM");
        Log.i(TAG, "onCreateisfrom: " + isfrom);

        imgBack.setOnClickListener(this);

    }

    private void init() {

        mPager = findViewById(R.id.pager);
        mPager.setAdapter(new ImageViewPagerAdapter(ChooseInterest.this,imageArr,titleArr,SubtitleArr));

        LinePageIndicator indicator = findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        indicator.setLineWidth(50);
        indicator.setSelectedColor(Color.parseColor("#E6683A"));
        NUM_PAGES =imageArr.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        callinterestApi(authKey);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_bottom:
                Log.i(TAG, "onClick: "+selectedItemid.toString());
                submitinterestApi(authKey, String.valueOf(selectedItemid));
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
        }

    }


    private void callinterestApi(String authToken) {
        progressBar = new ProgressDialog(ChooseInterest.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<InterestResponse> responseCall = api.interestApi(authToken);
        responseCall.enqueue(new Callback<InterestResponse>() {
            @Override
            public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {
                progressBar.dismiss();

                if (response != null) {
                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData().getInterestData();
                        sliderList = response.body().getData().getSliderImages();
                       // Toast.makeText(ChooseInterest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        chooseAdapter = new ChooseAdapter(ChooseInterest.this, dataList,isfrom);
                        recycleChoose.setAdapter(chooseAdapter);

                        for (int i = 0; i < sliderList.size(); i++) {
                            imageArr.add(sliderList.get(i).getSliderImage());
                            titleArr.add(sliderList.get(i).getSliderTitle());
                            SubtitleArr.add(sliderList.get(i).getSlider_sub_title());
                        }

                        init();

                        chooseAdapter.onItemClick(new ChooseAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int numSelected, ArrayList<Integer> playIdSelected) {
                                //selectedPlay=numSelected;
                               selectedItemid=playIdSelected;
                               Set<Integer> set = new LinkedHashSet<>();
                                set.addAll(selectedItemid);
                                selectedItemid.clear();
                                selectedItemid.addAll(set);
                                selectedPlay=selectedItemid.size();
                                tvSelected.setText(String.valueOf(selectedPlay));
                                Log.d("selectedItemid:", String.valueOf(selectedItemid));
                                /*if(isfrom.equalsIgnoreCase("MYPROFILE")){
                                    Intent intent = new Intent();
                                    setResult(RESULT_OK,intent);
                                    finish();
                                }*/

                            }
                        });

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(ChooseInterest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChooseInterest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<InterestResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    private void submitinterestApi(String authToken,String sid) {
        progressBar = new ProgressDialog(ChooseInterest.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<SubmitInterestResponse> responseCall = api.submitInterest(authToken,sid);
        responseCall.enqueue(new Callback<SubmitInterestResponse>() {
            @Override
            public void onResponse(Call<SubmitInterestResponse> call, Response<SubmitInterestResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(ChooseInterest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                        if(isfrom.equalsIgnoreCase("MYPROFILE")){
                            Intent intent = new Intent();
                            setResult(RESULT_OK,intent);
                            finish();
                        }else {
                            Intent i=new Intent(ChooseInterest.this,FindFriend.class);
                            i.putExtra("ISFROM","interestList");
                            startActivity(i);
                        }

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(ChooseInterest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChooseInterest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<SubmitInterestResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

}