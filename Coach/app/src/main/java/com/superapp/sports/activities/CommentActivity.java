package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.adapters.CommentsAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.CommentListResponse;
import com.superapp.sports.model.CommentPostResponse;
import com.superapp.sports.model.DummyModal;
import com.superapp.sports.model.LikePostResponse;
import com.superapp.sports.utils.Constants;
import com.superapp.sports.utils.DeliverItToApplication;
import com.superapp.sports.utils.SharedHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.DeliverItToApplication.trimMessage;

public class CommentActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recycle_comment;
    CommentsAdapter commentsAdapter;
    ArrayList<DummyModal> dummyModals;
    ImageView backbtn, send_btn, imgDelete, imgCross;
    private static final Integer[] commentArr = {1, 2, 3};
    private ProgressDialog progressBar;
    private List<CommentListResponse.CommentDatum> dataList;
    String selectedText = "", selectedCommentId = "", CommentUserId = "", Commentid = "", authKey = "", replyMsg = "", commentId = "";
    int postId;
    EditText ed_message;
    JSONArray commentArrayJson, replyArrayJson;
    RelativeLayout rlhide;
    TextView tvCount;
    LinearLayout ll_top;
    CircleImageView imgProfilePic;
    String profilePic = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        //dummyModals = getCommentData();

        authKey = SharedHelper.getKey(this, AUTH_TOKEN);

        ll_top = findViewById(R.id.ll_top);
        tvCount = findViewById(R.id.tvCount);
        imgCross = findViewById(R.id.imgCross);
        imgDelete = findViewById(R.id.imgDelete);
        rlhide = findViewById(R.id.rlhide);
        ed_message = findViewById(R.id.ed_message);
        backbtn = findViewById(R.id.imgBack);
        send_btn = findViewById(R.id.send_btn);
        imgProfilePic = findViewById(R.id.imgProfilePic);
        recycle_comment = findViewById(R.id.recycle_comment);
        recycle_comment.setLayoutManager(new LinearLayoutManager(
                this, RecyclerView.VERTICAL, false));


        postId = getIntent().getIntExtra("postId", 0);
        Log.i(TAG, "onCreate postId: " + postId);


        imgCross.setOnClickListener(this);
        backbtn.setOnClickListener(this);
        send_btn.setOnClickListener(this);
        imgDelete.setOnClickListener(this);

        profilePic = SharedHelper.getKey(CommentActivity.this, Key_profile_image);
        Log.i(TAG, "onCreate profilePic: " + profilePic);

        if (!profilePic.equalsIgnoreCase("")) {
            Picasso.get().load(profilePic).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(imgProfilePic);
        }


        getmyCommentApi();


    }


    public void getmyCommentApi() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("post_id", postId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.RV_COMMENTLIST_URL, jsonObject,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null && response.length() > 0) {

                            // progressDialog.dismiss();
                            if (response.optString("status").equalsIgnoreCase("1")) {
                                System.out.println(response.toString() + "....mycommentListdata");
                                JSONObject jsonObject = response.optJSONObject("data");


                                commentArrayJson = jsonObject.optJSONArray("comment_data");
                                commentsAdapter = new CommentsAdapter(CommentActivity.this, commentArrayJson, new CommentsAdapter.ClickItem() {
                                    @Override
                                    public void clickLikebtn(String id, String userID) {
                                        Commentid = id;
                                        CommentUserId = userID;
                                        likeCommentApi(authKey, Integer.parseInt(Commentid), CommentUserId);
                                    }

                                    @Override
                                    public void unLikeClickbtn(String id, String userID) {
                                        Commentid = id;
                                        CommentUserId = userID;
                                        UnlikeCommentApi(authKey, Integer.parseInt(Commentid), CommentUserId);
                                    }
                                });
                                recycle_comment.setAdapter(commentsAdapter);


                                commentsAdapter.onItemClick(new CommentsAdapter.OnClickDataCart() {
                                    @Override
                                    public void addNewClass(String message, String commentid) {
                                        replyMsg = message;
                                        commentId = commentid;
                                        ReplypostComment(authKey, replyMsg, postId, Integer.parseInt(commentId));
                                    }
                                });


                                commentsAdapter.onItemLike(new CommentsAdapter.OnClickDataReplyComment() {
                                    @Override
                                    public void addNewClass(String msg, String commentid) {
                                        String Message = msg;
                                        String commentId = commentid;
                                        ReplypostComment(authKey, Message, postId, Integer.parseInt(commentId));
                                    }
                                });


                                commentsAdapter.onItemClick(new CommentsAdapter.OnClickDataReplyLike() {
                                    @Override
                                    public void addNewClass(String replyid, String userid, String likeStat, String commid) {
                                        String replyId = replyid;
                                        String userId = userid;
                                        String likeStatus = likeStat;
                                        String commentId = commid;

                                        if (likeStatus.equals("0")) {
                                            likeReplyApi(authKey, replyId, userId, "1", commentId);
                                        } else {
                                            likeReplyApi(authKey, replyId, userId, "0", commentId);
                                        }
                                    }
                                });

                                commentsAdapter.onItemClick(new CommentsAdapter.OnLongClickComment() {
                                    @Override
                                    public void addNewClass(String comment, String commentid) {
                                        selectedText = comment;
                                        selectedCommentId = commentid;
                                        rlhide.setVisibility(View.VISIBLE);
                                        ll_top.setVisibility(View.GONE);
                                    }
                                });

                                commentsAdapter.onReplyLongClick(new CommentsAdapter.OnLongClickReply() {
                                    @Override
                                    public void addNewClass(String reply, String replyid) {
                                        selectedText = reply;
                                        selectedCommentId = replyid;
                                        rlhide.setVisibility(View.VISIBLE);
                                        ll_top.setVisibility(View.GONE);
                                    }
                                });

                            } else {
                                Toast.makeText(CommentActivity.this, response.optString("message"), Toast.LENGTH_SHORT).show();

                                //progressDialog.dismiss();
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            Toast.makeText(CommentActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                            // progressDialog.dismiss();
                        } else if (response.statusCode == 401) {

                        } else if (response.statusCode == 422) {

                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                Toast.makeText(CommentActivity.this, json, Toast.LENGTH_SHORT).show();

                                //progressDialog.dismiss();
                            } else {

                                Toast.makeText(CommentActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                                //progressDialog.dismiss();
                            }

                        } else {
                            Toast.makeText(CommentActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();

                            //progressDialog.dismiss();
                        }

                    } catch (Exception e) {

                        Toast.makeText(CommentActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                        //progressDialog.dismiss();
                    }

                } else {
                    //progressDialog.dismiss();
                    if (error instanceof NoConnectionError) {

                        Toast.makeText(CommentActivity.this, getString(R.string.oops_connect_your_internet), Toast.LENGTH_SHORT).show();

                    } else if (error instanceof NetworkError) {
                        Toast.makeText(CommentActivity.this, getString(R.string.oops_connect_your_internet), Toast.LENGTH_SHORT).show();

                    } else if (error instanceof TimeoutError) {

                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", authKey);

                System.out.println(headers + "...headers");
                return headers;
            }
        };

        DeliverItToApplication.getInstance().addToRequestQueue(jsonObjectRequest);
        Log.i(TAG, "my comment list api url : " + jsonObjectRequest.getUrl());

    }

    /*private void callCommentApi() {
        progressBar = new ProgressDialog(CommentActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<CommentListResponse> responseCall = api.CommentList(authKey);
        responseCall.enqueue(new Callback<CommentListResponse>() {
            @Override
            public void onResponse(Call<CommentListResponse> call, Response<CommentListResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getCommentData();
                        commentsAdapter = new CommentsAdapter(getApplicationContext(), dataList);
                        recycle_comment.setAdapter(commentsAdapter);

                        commentsAdapter.onItemClick(new CommentsAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(String message) {
                                replyMsg=message;
                                ReplypostComment(authKey,replyMsg,postId);
                            }
                        });


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CommentListResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }*/
    private void likeReplyApi(String authToken, String replyid, String userid, String stat, String commentid) {

        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.likeReply(authToken, replyid, userid, stat, commentid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        getmyCommentApi();

                    } else if (response.body().getStatus() == 0) {

                        //Toast.makeText(CommentActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void likeCommentApi(String authToken, int commentid, String userid) {
        progressBar = new ProgressDialog(CommentActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.likeComment(authToken, commentid, userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                progressBar.dismiss();
                if (response != null) {
                    if (response.body().getStatus() == 1) {
                        getmyCommentApi();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                progressBar.dismiss();
            }
        });
    }

    private void UnlikeCommentApi(String authToken, int commentid, String userid) {
        progressBar = new ProgressDialog(CommentActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.UnlikeComment(authToken, commentid, userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                progressBar.dismiss();

                if (response != null) {
                    if (response.body().getStatus() == 1) {
                        getmyCommentApi();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                progressBar.dismiss();
            }
        });
    }


    private void postComment() {
        Api api = RestManager.instanceOf();
        Call<CommentPostResponse> responseCall = api.commentPost(authKey, ed_message.getText().toString(), postId);
        responseCall.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        ed_message.setText("");
                        getmyCommentApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void ReplypostComment(String authKey, String msg, int postid, int commentid) {

        Api api = RestManager.instanceOf();
        Call<CommentPostResponse> responseCall = api.commentPostReply(authKey, msg, postid, commentid);
        responseCall.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getmyCommentApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void DeleteComment(String authKey, String commentid) {

        Api api = RestManager.instanceOf();
        Call<CommentPostResponse> responseCall = api.deleteComment(authKey, commentid);
        responseCall.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        rlhide.setVisibility(View.GONE);
                        ll_top.setVisibility(View.VISIBLE);
                        getmyCommentApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void DeleteReply(String authKey, String replyid) {

        Api api = RestManager.instanceOf();
        Call<CommentPostResponse> responseCall = api.deletereply(authKey, replyid);
        responseCall.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        rlhide.setVisibility(View.GONE);
                        ll_top.setVisibility(View.VISIBLE);
                        getmyCommentApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack: {
                finish();
            }

            case R.id.imgDelete: {
                if (selectedText.equals("comment")) {
                    DeleteComment(authKey, selectedCommentId);
                } else {
                    DeleteReply(authKey, selectedCommentId);
                }

            }
            case R.id.imgCross: {
                rlhide.setVisibility(View.GONE);
                ll_top.setVisibility(View.VISIBLE);
            }
            case R.id.send_btn: {
                if (ed_message.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(CommentActivity.this, "Enter Your Comment", Toast.LENGTH_SHORT).show();
                } else {
                    postComment();
                }

            }
            break;
        }
    }
}