package com.superapp.sports.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;

import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.adapters.CreateNewAdapter;
import com.superapp.sports.adapters.SelectedTagUserAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.utils.SharedHelper;
import com.superapp.sports.utils.UpdateImageInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.FNAME;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.LNAME;
import static com.superapp.sports.utils.Constants.USERID;
import static com.superapp.sports.utils.PermissionClass.hasPermissions;

public class CreateNewPost extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = CreateNewPost.class.getName();
    private static final String LOG_TAG = "dsfsf";
    RecyclerView recycleCreateNew, recycleTaglist;
    CreateNewAdapter createNewAdapter;
    SelectedTagUserAdapter selectedTagUserAdapter;
    ImageView imgBanner, imgBackCreate, imgFirst, imgPic;
    LinearLayout llNext, image_banner_lyt;
    CardView add_image_btn;
    private VideoView videoPlayer_ids, videoView2;
    TextView tvpost, tvtag, tvlocation, tv_name, tvAddress;
    String strJson = "", fname = "", lname = "", userId = "", mAuth_Token = "", selected_address = "";
    String latString = "", longString = "", tag = "";
    EditText ed_caption;
    RelativeLayout rlCreatePost, video_layout;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    ArrayList<String> selectedItemdetail = new ArrayList<>();
    private static final int TAGPAGE = 131;
    private static final int TAGLOACTIONPAGE = 121;
    private static final int CAMERA_PIC = 31;
    private static final int GALLERY_PIC = 35;

    private static final int CAMERA_VIDEO = 71;
    private static final int GALLERY_VIDEO = 75;

    ArrayList<Uri> mArrayUri = new ArrayList<>();
    ArrayList<Uri> mVideoArrayUri = new ArrayList<>();
    Uri imageUri;
    Uri videoUri;
    Bitmap pictureBitmap;
    MultipartBody.Builder m_builder;
    ArrayList<Integer> selectedItemid = new ArrayList<>();
    ArrayList<String> selectedName = new ArrayList<>();
    ArrayList<String> selectedImage = new ArrayList<>();
    JSONObject jsonObject;
    //JSONArray jsonArray;


    String videopath = "";

    // currentTimestamp
    long timeMilli;
    private static Retrofit retrofit = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);


        rlCreatePost = findViewById(R.id.rlCreatePost);
        imgPic = findViewById(R.id.imgPic);
        video_layout = findViewById(R.id.video_layout);

        add_image_btn = findViewById(R.id.add_image_btn);

        imgFirst = findViewById(R.id.imgFirst);
        videoPlayer_ids = findViewById(R.id.videoPlayer_ids);
        image_banner_lyt = findViewById(R.id.image_banner_lyt);


        imgBanner = findViewById(R.id.imgBanner);
        imgBackCreate = findViewById(R.id.imgBackCreate);
        llNext = findViewById(R.id.llNext);
        tv_name = findViewById(R.id.tv_name);
        tvAddress = findViewById(R.id.tvAddress);

        imgFirst = findViewById(R.id.imgFirst);
        ed_caption = findViewById(R.id.ed_caption);

        tvpost = findViewById(R.id.tvpost);
        tvtag = findViewById(R.id.tvtag);
        tvlocation = findViewById(R.id.tvlocation);


        recycleTaglist = findViewById(R.id.recycletaglist);
        recycleCreateNew = findViewById(R.id.recycleCreateNew);
        recycleCreateNew.setLayoutManager(new LinearLayoutManager(CreateNewPost.this, RecyclerView.HORIZONTAL, false));
        recycleCreateNew.setHasFixedSize(true);
        recycleCreateNew.setNestedScrollingEnabled(true);

        recycleTaglist.setLayoutManager(new LinearLayoutManager(CreateNewPost.this, RecyclerView.VERTICAL, false));
        recycleTaglist.setHasFixedSize(true);
        recycleTaglist.setNestedScrollingEnabled(true);

        mAuth_Token = SharedHelper.getKey(this, AUTH_TOKEN);
        userId = SharedHelper.getKey(this, USERID);
        fname = SharedHelper.getKey(this, FNAME);
        lname = SharedHelper.getKey(this, LNAME);

        tv_name.setText(fname + " " + lname);
        String profile_img = String.valueOf(SharedHelper.getKey(this, Key_profile_image));
        if (!profile_img.equalsIgnoreCase("")) {
            Picasso.get().load(profile_img).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(imgPic);
        }


        imgBackCreate.setOnClickListener(this);
        add_image_btn.setOnClickListener(this);

        tvpost.setOnClickListener(this);
        tvtag.setOnClickListener(this);
        tvlocation.setOnClickListener(this);


       // GiraffeCompressor.init(CreateNewPost.this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBackCreate:
                onBackPressed();
                break;

            case R.id.tvpost:


                if (mArrayUri.size() > 0 || mVideoArrayUri.size() > 0) {

                    m_builder = new MultipartBody.Builder();
                    m_builder.setType(MultipartBody.FORM);
                    m_builder.addFormDataPart("userid", userId);
                    m_builder.addFormDataPart("description", ed_caption.getText().toString());
                    m_builder.addFormDataPart("location", selected_address);
                    m_builder.addFormDataPart("tag", tag);
                    m_builder.addFormDataPart("latitude", latString);
                    m_builder.addFormDataPart("longitude", longString);

                    Log.d("uiyui", "put data: " + userId);
                    Log.d("14", "decp: " + ed_caption.getText().toString());
                    Log.d("15", "mlocation: " + selected_address);
                    Log.d("16", "tag: " + tag);
                    Log.d("18", "mLatitude: " + latString);
                    Log.d("19", "mLongitude: " + longString);
                    Log.d("20", "mVideoArrayUri: " + mVideoArrayUri.size());
                    Log.d("21", "mArrayUri: " + mArrayUri.size());
                    Log.e("SizeOFUri", String.valueOf(mVideoArrayUri.size()));


                    if (mVideoArrayUri.size() > 0) {
                        m_builder.addFormDataPart("type", "video");
                        calltoapiaddListData(m_builder, mVideoArrayUri);
                        Log.d("19", "mVideoArrayUri: " + mVideoArrayUri.size());
                        Log.d("242", "mVideoArrayUri: " + mVideoArrayUri.get(0).getPath());
                    } else {
                        m_builder.addFormDataPart("type", "image");
                        calltoapiaddListData(m_builder, mArrayUri);
                        Log.d("24", "mArrayUri: " + mArrayUri.size());
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please select at least one image !", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.tvlocation:
                Intent inn = new Intent(CreateNewPost.this, AddLocation.class);
                startActivityForResult(inn, TAGLOACTIONPAGE);
                break;

            case R.id.tvtag:

                Intent in = new Intent(CreateNewPost.this, TagActivity.class);
                startActivityForResult(in, TAGPAGE);
                break;

            case R.id.add_image_btn:

                if (!hasPermissions(this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    getPictureDialog();

                }
                break;


        }
    }


    private void getPictureDialog() {
        android.app.AlertDialog.Builder pictureDialog = new AlertDialog.Builder(CreateNewPost.this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera", "Select video from gallery"};
        // String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                PicGallery();
                                break;
                            case 1:
                                PicCamera();
                                break;
                            case 2:
                                videoGallery();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    public void PicGallery() {
        Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        // galleryIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY_PIC);
    }

    public void PicCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_PIC);
    }

    public void videoGallery() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        startActivityForResult(intent, GALLERY_VIDEO);


       /* Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                    try {

                        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                        takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                        Uri capturedUri = Uri.fromFile(createMediaFile(TYPE_VIDEO));
                        takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedUri);
                        Log.d(LOG_TAG, "VideoUri: " + capturedUri.toString());
                        startActivityForResult(takeVideoIntent, GALLERY_VIDEO);
                    } catch (IOException e) {
                        e.printStackTrace();
            }

        }*/
    }


    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 111 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startMediaCompression();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PIC && resultCode == RESULT_OK) {

            image_banner_lyt.setVisibility(View.VISIBLE);
            video_layout.setVisibility(View.GONE);
            if (data.getClipData() != null) {
                ClipData mClipData = data.getClipData();

                for (int i = 0; i < mClipData.getItemCount(); i++) {
                    ClipData.Item item = mClipData.getItemAt(i);

                    imageUri = item.getUri();
                    Log.i("23543", "Image Uri: " + imageUri.toString());
                    mArrayUri.add(imageUri);
                    setPriviewImage(mArrayUri);

                    Log.i("23543", "mArrayUri: " + mArrayUri.size());

                    if (mArrayUri.size() > 0) {

                        setImagesinList(mArrayUri);

                    }


                }

            } else if (data.getData() != null) {

                Uri uri = data.getData();
                Log.i(TAG, "onActivityResult Single click: " + uri);
                mArrayUri.add(uri);
                setPriviewImage(mArrayUri);
                if (mArrayUri.size() > 0) {
                    setImagesinList(mArrayUri);
                }
            }

        } else if (requestCode == CAMERA_PIC && resultCode == RESULT_OK) {

            image_banner_lyt.setVisibility(View.VISIBLE);
            video_layout.setVisibility(View.GONE);
            if (data != null) {
                pictureBitmap = (Bitmap) data.getExtras().get("data");
                Uri tempUri = getImageUri(getApplicationContext(), pictureBitmap);
                mArrayUri.add(tempUri);
                setPriviewImage(mArrayUri);

                if (mArrayUri.size() > 0) {
                    setImagesinList(mArrayUri);
                }

            }
        } else if (requestCode == GALLERY_VIDEO && resultCode == RESULT_OK) {


            image_banner_lyt.setVisibility(View.VISIBLE);
            imgBanner.setVisibility(View.GONE);
            video_layout.setVisibility(View.VISIBLE);

            videoUri = data.getData();
            mVideoArrayUri.add(videoUri);
            setVideoLayout(videoUri);
            Log.i("98098", "videoUri path: " + videoUri.getPath());
            Log.i("2434", "videoUri: " + videoUri.toString());
            Log.i("198098", "videoUrisize: " + mVideoArrayUri.size());

            videopath = getPath(this, data.getData());
            Log.i("198098", "inputPath: " + videopath);

//            Date date = new Date();
//            timeMilli = date.getTime();
//            uploadvideoAWS(videopath, timeMilli);

            requestPermission();

        } else if (requestCode == CAMERA_VIDEO && resultCode == RESULT_OK) {

            image_banner_lyt.setVisibility(View.VISIBLE);
            imgBanner.setVisibility(View.GONE);
            video_layout.setVisibility(View.VISIBLE);
            videoUri = data.getData();
            mVideoArrayUri.add(videoUri);
            setVideoLayout(videoUri);

        } else if (requestCode == TAGPAGE && resultCode == RESULT_OK) {

            selectedItemid = data.getIntegerArrayListExtra("selectedId");
            selectedName = data.getStringArrayListExtra("selectedname");
            selectedImage = data.getStringArrayListExtra("selectedimage");
            tag = selectedItemid.toString().replace("]", "").replace("[", "");
            Log.i(TAG, "onActivityResult: " + selectedItemid);

            setTagImagesinList(selectedName, selectedImage);

        } else if (requestCode == TAGLOACTIONPAGE && resultCode == RESULT_OK) {
            selected_address = data.getStringExtra("selectedAddress");
            Log.i(TAG, "onActivityResult selected_address: " + selected_address);

            if (!selected_address.equalsIgnoreCase("")) {
                tvAddress.setText(selected_address);
                tvAddress.setVisibility(View.VISIBLE);
            } else {
                tvAddress.setVisibility(View.GONE);
            }

        }
    }


   /* private void uploadvideoAWS(String path, long timemile) {
        ClientConfiguration configuration = new ClientConfiguration();
        configuration.setMaxErrorRetry(3);
        configuration.setConnectionTimeout(501000);
        configuration.setSocketTimeout(501000);
        configuration.setProtocol(Protocol.HTTP);

//        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(
//                NK_AWS_ACCESS_KEY, NK_AWS_SECRET_KEY), configuration);
//        s3Client.setRegion(Region.getRegion(Regions.US_EAST_2));
        //  s3Client.setBucketAccelerateConfiguration(NK_AWS_BUCKET_NAME,"");


        AWSCredentials myCredentials = new BasicAWSCredentials(
                NK_AWS_ACCESS_KEY, NK_AWS_SECRET_KEY);
        AmazonS3 s3client = new AmazonS3Client(myCredentials,
                Region.getRegion(Regions.US_EAST_2));
        s3client.setEndpoint("https://supersportselite.s3-accelerate.amazonaws.com/");
        s3client.setS3ClientOptions(S3ClientOptions.builder().setAccelerateModeEnabled(true).build());



        TransferUtility transferUtility = new TransferUtility(s3client, CreateNewPost.this);

        //You have to pass your file path here.
        File file = new File(path);
        if (!file.exists()) {
            Toast.makeText(getApplicationContext(), "File Not Found!", Toast.LENGTH_SHORT).show();
            return;
        }

        TransferObserver observer = transferUtility.upload(
                NK_AWS_BUCKET_NAME,
                "uploads/" + timemile + "mp.4",
                file
        );

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.COMPLETED.equals(observer.getState())) {
                    System.out.println(observer.getKey() + "........File Upload Complete");
                    String url = "https://" + NK_AWS_BUCKET_NAME + ".s3.amazonaws.com/" + observer.getKey();
                    System.out.println(url + "........File Upload Complete");
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int total = (int) bytesTotal;
                int current = (int) bytesCurrent;
                int progress = current * 100 / total;


                System.out.println(bytesTotal + "........bytesTotal");
                System.out.println(bytesCurrent + "........bytesCurrent");
                System.out.println(id + "........id");
                System.out.println(progress + ".....progress");
            }

            @Override
            public void onError(int id, Exception ex) {
                System.out.println(ex.getMessage() + "........message");
                Toast.makeText(getApplicationContext(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }*/


    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 111);
        } else {
            startMediaCompression();
        }
    }

    private void setVideoLayout(Uri videoUri) {
        try {
            videoPlayer_ids.setVideoURI(videoUri);
            // videoPlayer_ids.seekTo(500);
            MediaController mediaController = new MediaController(CreateNewPost.this);
            mediaController.setAnchorView(videoPlayer_ids);
            videoPlayer_ids.setMediaController(mediaController);
            videoPlayer_ids.requestFocus();
            videoPlayer_ids.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                    //videoPlayer_ids.start();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setImagesinList(ArrayList<Uri> mArrayUri) {

        if (mArrayUri.size() > 0) {

            createNewAdapter = new CreateNewAdapter(getApplicationContext(), mArrayUri);
            recycleCreateNew.setAdapter(createNewAdapter);
            createNewAdapter.clickItem(new CreateNewAdapter.addImagePerItem() {
                @Override
                public void addImageOnClicked(String media_pic) {
                    Log.i("809", "mArrayUri: " + media_pic);
                    Glide.with(getApplicationContext()).load(media_pic).placeholder(R.drawable.dummy_user).centerCrop().into(imgBanner);

                }

                @Override
                public void deletImageOnClicked(int position) {

                    mArrayUri.remove(position);
                    createNewAdapter.notifyDataSetChanged();
                    setPriviewImage(mArrayUri);
                }
            });

        }
    }

    private void setTagImagesinList(ArrayList<String> arraynameList, ArrayList<String> arrayImageList) {

        selectedTagUserAdapter = new SelectedTagUserAdapter(getApplicationContext(), arraynameList, arrayImageList);
        recycleTaglist.setAdapter(selectedTagUserAdapter);
/*
        selectedTagUserAdapter.clickItem(new SelectedTagUserAdapter.addImagePerItem() {
            @Override
            public void addImageOnClicked(String media_pic) {
                Log.i("809", "mArrayUri: " + media_pic);
                Glide.with(getApplicationContext()).load(media_pic).placeholder(R.drawable.dummy_user).centerCrop().into(imgBanner);

            }

            @Override
            public void deletImageOnClicked(int position) {

                selectedTagUserAdapter.notifyDataSetChanged();
                //setPriviewImage(mArrayUri);

            }
        });
*/
    }


    private void setPriviewImage(ArrayList<Uri> mArrayUri) {
        if (mArrayUri.size() == 0) {
            Log.i("23", "mArrayUri: " + mArrayUri.size());
            image_banner_lyt.setVisibility(View.GONE);
            video_layout.setVisibility(View.GONE);

        } else {

            Uri media_pic = (mArrayUri.get(0));
            Log.i("809", "media_pic: " + media_pic);
            Log.i("809", "mArrayUri: " + mArrayUri.size());

            // news_image.setImageURI(media_pic);
            if (media_pic != null) {
                Picasso.get().load(media_pic).placeholder(R.mipmap.ic_logos).resize(450, 450).into(imgBanner);

                imgBanner.setImageResource(R.mipmap.ic_logos);
            } else {

            }
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private void calltoapiaddListData(MultipartBody.Builder m_builder, ArrayList<Uri> mArrayUri) {

        ProgressDialog loading = ProgressDialog.show(CreateNewPost.this, "File Upload Processing", "Please wait...", false, false);
        if (mArrayUri.size() > 0) {
            for (int m = 0; m < mArrayUri.size(); m++) {

                Uri uriStr = mArrayUri.get(m);
                if (mVideoArrayUri.size() > 0) {

                    File file = new File(getPath(getApplicationContext(), uriStr));
                    Log.d("19", "mVideoArrayUri: " + mVideoArrayUri.size());
                    Log.d("191", "mArrayUri: " + file.getPath());
                    m_builder.addFormDataPart("image[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
                   // m_builder.addFormDataPart("post_image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

                } else {
                    File file = new File(getPath(getApplicationContext(), uriStr));
                    //  File file = new File(getRealPathFromURI(uriStr));
                    Log.d("19", "mArrayUri: " + mArrayUri.size());
                    Log.d("191", "mArrayUri: " + file.getPath());
                    m_builder.addFormDataPart("image[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

                }
            }
        }

        MultipartBody requestBody = m_builder.build();
        Api api = RestManager.instanceOf();
        Call<ResponseBody> call = api.createPost(mAuth_Token, requestBody);
        //Call<ResponseBody> call = api.createPost( requestBody);
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetJavaScriptEnabled")
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                boolean isSuccess = response.isSuccessful();
                if (isSuccess) {
                    try {
                        try {
                            JSONObject json = new JSONObject(response.body().string());
                            Log.e("AddListing Response : ", json.toString());
                            loading.dismiss();
                            //JSONObject dataObj = json.optJSONObject("data");
                            if (response != null) {
                                if (json.optInt("status") == 1) {

                                    String messag = json.optString("message");
                                    Toast.makeText(CreateNewPost.this, messag, Toast.LENGTH_SHORT).show();
                                    Log.d("response====>", response.body().toString());

                                    Intent intent = new Intent(CreateNewPost.this, Dashboard.class);
                                    startActivity(intent);
                                } else if (json.optInt("status") == 0) {
                                    Toast.makeText(CreateNewPost.this, json.optString("message"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(CreateNewPost.this, json.optString("message"), Toast.LENGTH_SHORT).show();
                                }

                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                            loading.dismiss();
                            Toast.makeText(getApplicationContext(), "Check your internet connection !", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i(TAG, "onResponse exception: " + e.getMessage());
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Try again !", Toast.LENGTH_LONG).show();
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Log.i("kjkhjkh", "onFailure: " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Try again !", Toast.LENGTH_LONG).show();
            }
        });


    }


    private void uploadVideo(String path) {
        ProgressDialog loading = ProgressDialog.show(CreateNewPost.this, "Processing", "Please wait...", false, false);


        File file = new File(path);

        // Parsing any Media type file
        MultipartBody requestBody = m_builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://supersportselite.s3-accelerate.amazonaws.com/userFolder/")
                .build();


        UpdateImageInterface imageInterface = retrofit.create(UpdateImageInterface.class);
        // imageUrl is "xxfooxx..."
        Call<Void> call = imageInterface.updateProfilePhoto(requestBody);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });

    }


    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    private void startMediaCompression() {

        Log.i(TAG, "startMediaCompression videoUri : " + videopath);
        
/*
        mVideoCompressor.startCompressing(videopath, new VideoCompressor.CompressionListener() {
            @Override
            public void compressionFinished(int status, boolean isVideo, String fileOutputPath) {
                if (mVideoCompressor.isDone()) {
                    File outputFile = new File(fileOutputPath);
                    long outputCompressVideosize = outputFile.length();
                    long fileSizeInKB = outputCompressVideosize / 1024;
                    long fileSizeInMB = fileSizeInKB / 1024;

                    String s = "Output video path : " + fileOutputPath + "\n" + "Output video size : " + fileSizeInMB + "mb";
                    Log.i(TAG, "compressionFinished: " + s);
                }

            }

            @Override
            public void onFailure(String message) {

                Log.i(TAG, "onFailure message: " + message);
            }

            @Override
            public void onProgress(final int progress) {
                *//*progress_bar.setProgress(progress);
                tv_progress.post(new Runnable() {
                    @Override
                    public void run() {
                        tv_progress.setText(progress + "%");
                    }
                });*//*
            }
        });
        */


    }

}