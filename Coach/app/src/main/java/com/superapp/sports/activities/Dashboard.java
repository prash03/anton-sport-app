package com.superapp.sports.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.superapp.sports.R;
import com.superapp.sports.fragments.HomeFragment;
import com.superapp.sports.fragments.NewHomeFragment;
import com.superapp.sports.fragments.SearchFragment;
import com.superapp.sports.fragments.SettingFragment;

import de.hdodenhof.circleimageview.CircleImageView;


public class Dashboard extends AppCompatActivity {

    private final static int ID_SEARCH = 1;
    private final static int ID_MESSAGE = 2;
    private final static int ID_HOME = 3;
    private final static int ID_NOTIF = 4;
    private final static int ID_MENU = 5;
    private static final String TAG_HOME = "home";
    private static final String TAG_SEARCH = "search";
    private static final String TAG = "Main Activity";
    // index to identify current nav menu item
    public int navItemIndex = 0;
    public String CURRENT_TAG = TAG_SEARCH;
    private NavigationView navigationView;
    //private DrawerLayout drawer;
    //private View navHeader;
    private CircleImageView profile_img;
    private ImageView img_cross;
    LinearLayout profile_lyt;
    //    private TextView txtWebsite;
    private TextView user_name,user_specialization;
    private Toolbar toolbar;
    private final String UserData="";
    private final String fname="";
    private final String lname="";
    private final String profile_image="";
    Fragment selectedFragment;
    private Handler mHandler;
    BottomNavigationView bottomNavigationView;
    //MeowBottomNavigation bottomNavigationView;
    public DrawerLayout drawerLayout;
    //public ActionBarDrawerToggle actionBarDrawerToggle;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
*/

        setContentView(R.layout.activity_dashboard);

        bottomNavigationView = findViewById(R.id.nav_bottomview);
        mHandler = new Handler();

        //setUpBottomNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }

        setUpBottomNavigationView();

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.navigation_search, R.id.navigation_home,R.id.navigation_notification,
                R.id.navigation_chat, R.id.navigation_notification, R.id.navigation_menu)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.main_frame);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);



    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void loadNavHeader( String userData) { }



    private void loadHomeFragment() {
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            //drawer.closeDrawers();
            // show or hide the fab button
            return;
        }
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = HomeFragment.newInstance();
             //   Fragment fragment = NewHomeFragment.newInstance();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        invalidateOptionsMenu();

    }
    private void loadSearchFragment() {
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            //drawer.closeDrawers();
            // show or hide the fab button
            return;
        }
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = SearchFragment.newInstance();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        invalidateOptionsMenu();

    }


    /*public void curved(){
        bottomNavigationView.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                switch (item.getId()) {

                    case 0:
                        selectedFragment = SearchFragment.newInstance();
                        loadSearchFragment();
                        //loadSearchFragment();
                        break;
                    case 1:
                        selectedFragment = SearchFragment.newInstance();
                        loadSearchFragment();
                        break;



                    case 2:
                        selectedFragment = HomeFragment.newInstance();
                        FragmentTransaction transaction3 = getSupportFragmentManager().beginTransaction();
                        transaction3.replace(R.id.main_frame, selectedFragment);
                        transaction3.addToBackStack(null);
                        transaction3.commit();
                        break;


                    case 3:
                        selectedFragment = NotificationFragment.newInstance();
                        FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
                        transaction4.replace(R.id.main_frame, selectedFragment);
                        transaction4.addToBackStack(null);
                        transaction4.commit();
                        break;

                    case 4:
                        selectedFragment = SettingFragment.newInstance();
                        FragmentTransaction transaction5 = getSupportFragmentManager().beginTransaction();
                        transaction5.replace(R.id.main_frame, selectedFragment);
                        transaction5.addToBackStack(null);
                        transaction5.commit();
                        break;

                    default:
                        navItemIndex = 0;
                        loadHomeFragment();
                }
            }
        });




        *//*bottomNavigationView.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {
                Toast.makeText(Dashboard.this, "showing item : " + item.getId(), Toast.LENGTH_SHORT).show();

                *//**//*if(item.getId()==1){
                    loadSearchFragment();
                }*//**//*
                switch (item.getId()) {

                    case ID_MENU:
                        selectedFragment = SettingFragment.newInstance();
                        break;


                    case ID_HOME:
                        selectedFragment = HomeFragment.newInstance();

                        break;
                    case ID_SEARCH:
                        //selectedFragment = SearchFragment.newInstance();
                        loadSearchFragment();
                        break;

                    case ID_NOTIF:
                        selectedFragment = NotificationFragment.newInstance();
                        break;

                    default:
                        navItemIndex = 0;
                        loadHomeFragment();
                }

            }
        });*//*
    }*/

    /*public void curvdBottomNavigation(){
        bottomNavigationView.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                // your codes
                switch (item.getId()) {

                    case ID_MENU:
                        selectedFragment = SettingFragment.newInstance();
                        break;


                    case ID_HOME:
                        selectedFragment = HomeFragment.newInstance();

                        break;
                    case ID_SEARCH:
                        selectedFragment = SearchFragment.newInstance();
                        break;

                        case ID_NOTIF:
                        selectedFragment = NotificationFragment.newInstance();
                        break;

                    default:
                        navItemIndex = 0;
                        loadHomeFragment();
                }
            }
        });



        *//*bottomNavigationView.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
                // your codes
            }
        });*//*
    }*/
    private void setUpBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @SuppressLint("WrongConstant")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                switch (item.getItemId()) {

                    case R.id.navigation_menu:
                        selectedFragment = SettingFragment.newInstance();

                        //bottomNavigationView.setItemIconTintList(ColorStateList.valueOf(R.drawable.ic_path));
                        break;


                    case R.id.navigation_home:
                        selectedFragment = HomeFragment.newInstance();
                       // selectedFragment = NewHomeFragment.newInstance();

                        break;
                    case R.id.navigation_search:
                        selectedFragment = SearchFragment.newInstance();
                        break;

                    default:
                        navItemIndex = 0;
                        loadHomeFragment();
                }
                return false;
            }
        });
    }



    public void displayMessage(String toastString) {
        Log.e("displayMessage", "" + toastString);
        Snackbar.make(this.findViewById(android.R.id.content), toastString, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }




    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int which) {
                            finishAffinity();
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else {
            super.onBackPressed();

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}

