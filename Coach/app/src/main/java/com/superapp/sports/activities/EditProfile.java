package com.superapp.sports.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.EditProfileResponse;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.ACCOUNTTYPE;
import static com.superapp.sports.utils.Constants.ACHIEVEMENT;
import static com.superapp.sports.utils.Constants.APG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.BIO;
import static com.superapp.sports.utils.Constants.CLASSIFICATION;
import static com.superapp.sports.utils.Constants.COMPANYNAME;
import static com.superapp.sports.utils.Constants.DOB;
import static com.superapp.sports.utils.Constants.EXPERIENCE;
import static com.superapp.sports.utils.Constants.FNAME;
import static com.superapp.sports.utils.Constants.GENDER;
import static com.superapp.sports.utils.Constants.GPA;
import static com.superapp.sports.utils.Constants.HEIGHT;
import static com.superapp.sports.utils.Constants.HOME;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.LNAME;
import static com.superapp.sports.utils.Constants.MARITAL;
import static com.superapp.sports.utils.Constants.POSITION;
import static com.superapp.sports.utils.Constants.PPG;
import static com.superapp.sports.utils.Constants.RANK;
import static com.superapp.sports.utils.Constants.RPG;
import static com.superapp.sports.utils.Constants.SCHOOLNAME;
import static com.superapp.sports.utils.Constants.SPECIALITY;
import static com.superapp.sports.utils.Constants.SPEED;
import static com.superapp.sports.utils.Constants.SPONSERNAME;
import static com.superapp.sports.utils.Constants.SPORTNAME;
import static com.superapp.sports.utils.Constants.USEREMAIL;
import static com.superapp.sports.utils.Constants.USERMOBILE;
import static com.superapp.sports.utils.Constants.VERTICLE;
import static com.superapp.sports.utils.Constants.WEBSITE;
import static com.superapp.sports.utils.Constants.WEIGHT;

public class EditProfile extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    public static final String TAG = EditProfile.class.getName();
    ImageView imgBack,imupload;
    Button bt_save;
    private ProgressDialog progressBar;
    String mAuth_Token="",accountType="";
    Uri uri1;
    Bitmap bitmap;
    CircleImageView imProfile;
    LinearLayout llupload;
    EditText ed_fname,ed_lname,ed_email,ed_mob,ed_web,ed_bio,ed_marital,edgender,ed_spc,ed_company,ed_sponser,
            ed_achieve,ed_sport,ed_exp,ed_school,ed_position,ed_coachpost,ed_class,ed_home,ed_gpa,ed_vert,ed_speed,
            ed_rank,ed_weight,ed_height,ed_ppg,ed_rpg,ed_apg;
    TextView tvDob,tv_subhead;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    Spinner gender_spinner;
    MultipartBody.Part body ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        tvDob=findViewById(R.id.tvDob);
        tv_subhead=findViewById(R.id.tv_subhead);
        edgender=findViewById(R.id.edgender);
        gender_spinner=findViewById(R.id.gender_spinner);
        ed_spc=findViewById(R.id.ed_spc);
        ed_company=findViewById(R.id.ed_company);
        ed_sponser=findViewById(R.id.ed_sponser);
        ed_achieve=findViewById(R.id.ed_achieve);
        ed_sport=findViewById(R.id.ed_sport);
        ed_exp=findViewById(R.id.ed_exp);
        ed_school=findViewById(R.id.ed_school);
        ed_position=findViewById(R.id.ed_position);
        ed_coachpost=findViewById(R.id.ed_coachpost);
        ed_fname=findViewById(R.id.ed_fname);
        ed_lname=findViewById(R.id.ed_lname);
        ed_email=findViewById(R.id.ed_email);
        ed_mob=findViewById(R.id.ed_mob);
        ed_web=findViewById(R.id.ed_web);
        ed_bio=findViewById(R.id.ed_bio);
        ed_marital=findViewById(R.id.ed_marital);
        ed_class=findViewById(R.id.ed_class);
        ed_home=findViewById(R.id.ed_home);
        ed_gpa=findViewById(R.id.ed_gpa);
        ed_vert=findViewById(R.id.ed_vert);
        ed_speed=findViewById(R.id.ed_speed);
        ed_rank=findViewById(R.id.ed_rank);
        ed_weight=findViewById(R.id.ed_weight);
        ed_height=findViewById(R.id.ed_height);
        imProfile=findViewById(R.id.imProfile);
        llupload=findViewById(R.id.llupload);
        imgBack=findViewById(R.id.imgBack);
        imupload=findViewById(R.id.imupload);
        bt_save=findViewById(R.id.bt_save);
        ed_ppg=findViewById(R.id.ed_ppg);
        ed_rpg=findViewById(R.id.ed_rpg);
        ed_apg=findViewById(R.id.ed_apg);

        mAuth_Token = SharedHelper.getKey(this, AUTH_TOKEN);
        accountType= SharedHelper.getKey(this,ACCOUNTTYPE);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        setDateTimeField();

        if(accountType.equals("Coach")){
            ed_spc.setVisibility(View.VISIBLE);
            ed_company.setVisibility(View.VISIBLE);
            ed_sponser.setVisibility(View.VISIBLE);
            ed_achieve.setVisibility(View.VISIBLE);
            ed_sport.setVisibility(View.VISIBLE);
            ed_exp.setVisibility(View.VISIBLE);
            ed_school.setVisibility(View.VISIBLE);
            ed_position.setVisibility(View.VISIBLE);
            tvDob.setVisibility(View.VISIBLE);
            ed_lname.setVisibility(View.VISIBLE);
            ed_fname.setVisibility(View.VISIBLE);

            ed_position.setVisibility(View.GONE);
            ed_class.setVisibility(View.GONE);
            ed_home.setVisibility(View.GONE);
            ed_gpa.setVisibility(View.GONE);
            ed_vert.setVisibility(View.GONE);
            ed_speed.setVisibility(View.GONE);
            ed_rank.setVisibility(View.GONE);
            ed_weight.setVisibility(View.GONE);
            ed_height.setVisibility(View.GONE);
        }
        else if(accountType.equals("Player")){
            ed_spc.setVisibility(View.GONE);
            ed_company.setVisibility(View.GONE);
            ed_sponser.setVisibility(View.GONE);
            ed_achieve.setVisibility(View.GONE);
            ed_sport.setVisibility(View.GONE);
            ed_exp.setVisibility(View.GONE);
            ed_school.setVisibility(View.GONE);
            ed_position.setVisibility(View.GONE);

            ed_position.setVisibility(View.VISIBLE);
            ed_class.setVisibility(View.VISIBLE);
            ed_home.setVisibility(View.VISIBLE);
            ed_gpa.setVisibility(View.VISIBLE);
            ed_vert.setVisibility(View.VISIBLE);
            ed_speed.setVisibility(View.VISIBLE);
            ed_rank.setVisibility(View.VISIBLE);
            ed_weight.setVisibility(View.VISIBLE);
            ed_height.setVisibility(View.VISIBLE);
            tvDob.setVisibility(View.VISIBLE);
            ed_lname.setVisibility(View.VISIBLE);
            ed_fname.setVisibility(View.VISIBLE);
        }
        else if(accountType.equals("Sponser")){
            ed_spc.setVisibility(View.GONE);
            ed_company.setVisibility(View.GONE);
            ed_sponser.setVisibility(View.GONE);
            ed_achieve.setVisibility(View.GONE);
            ed_sport.setVisibility(View.GONE);
            ed_exp.setVisibility(View.GONE);
            ed_school.setVisibility(View.GONE);
            ed_position.setVisibility(View.GONE);

            ed_position.setVisibility(View.VISIBLE);
            ed_class.setVisibility(View.VISIBLE);
            ed_home.setVisibility(View.VISIBLE);
            ed_gpa.setVisibility(View.VISIBLE);
            ed_vert.setVisibility(View.VISIBLE);
            ed_speed.setVisibility(View.VISIBLE);
            ed_rank.setVisibility(View.VISIBLE);
            ed_weight.setVisibility(View.VISIBLE);
            ed_height.setVisibility(View.VISIBLE);
            tvDob.setVisibility(View.VISIBLE);
            ed_lname.setVisibility(View.VISIBLE);
            ed_fname.setVisibility(View.VISIBLE);
        }
        else{}

        String profile_img = String.valueOf(SharedHelper.getKey(this,Key_profile_image));
        if (!profile_img.equalsIgnoreCase("")) {
            Picasso.get().load(profile_img).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(imProfile);
        }

        List<String> categories = new ArrayList<String>();
        categories.add("Item 1");
        categories.add("Item 2");
        categories.add("Item 3");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        gender_spinner.setAdapter(dataAdapter);

        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(EditProfile.this,SecondActivity.class);
                intent.putExtra("data",String.valueOf(spinner.getSelectedItem()));
                startActivity(intent);
            }
        });*/



        String firstname=SharedHelper.getKey(this,FNAME);
        String lastname=SharedHelper.getKey(this,LNAME);
        String mobile=SharedHelper.getKey(this,USERMOBILE);
        String web=SharedHelper.getKey(this,WEBSITE);
        String bio=SharedHelper.getKey(this,BIO);
        String dob=SharedHelper.getKey(this,DOB);
        String gender=SharedHelper.getKey(this,GENDER);
        String maritalStatus=SharedHelper.getKey(this,MARITAL);
        String Height=SharedHelper.getKey(this,HEIGHT);
        String Weight=SharedHelper.getKey(this,WEIGHT);
        String Position=SharedHelper.getKey(this,POSITION);
        String Ranking=SharedHelper.getKey(this,RANK);
        String Speed=SharedHelper.getKey(this,SPEED);
        String Vertical=SharedHelper.getKey(this,VERTICLE);
        String Gpa=SharedHelper.getKey(this,GPA);
        String HomeTown=SharedHelper.getKey(this,HOME);
        String Classification=SharedHelper.getKey(this,CLASSIFICATION);
        String SchoolName=SharedHelper.getKey(this,SCHOOLNAME);
        String Experience=SharedHelper.getKey(this,EXPERIENCE);
        String SportsName=SharedHelper.getKey(this,SPORTNAME);
        String Achievement=SharedHelper.getKey(this,ACHIEVEMENT);
        String SponserName=SharedHelper.getKey(this,SPONSERNAME);
        String CompanyName=SharedHelper.getKey(this,COMPANYNAME);
        String Speciality=SharedHelper.getKey(this,SPECIALITY);

        tv_subhead.setText(firstname+" "+lastname);
        ed_fname.setText(firstname);
        ed_lname.setText(lastname);
        if(Speciality.equals("null")){
            ed_spc.setText("");
        }
        else{
            ed_spc.setText(Speciality);
        }
        if(CompanyName.equals("null")){
            ed_company.setText("");
        }
        else{
            ed_company.setText(CompanyName);
        }
        if(SponserName.equals("null")){
            ed_sponser.setText("");
        }
        else{
            ed_sponser.setText(SponserName);
        }
        if(Achievement.equals("null")){
            ed_achieve.setText("");
        }
        else{
            ed_achieve.setText(Achievement);
        }
        if(SportsName.equals("null")){
            ed_sport.setText("");
        }
        else{
            ed_sport.setText(SportsName);
        }
        if(Experience.equals("null")){
            ed_exp.setText("");
        }
        else{
            ed_exp.setText(Experience);
        }
        if(SchoolName.equals("null")){
            ed_school.setText("");
        }
        else{
            ed_school.setText(SchoolName);
        }
        if(Classification.equals("null")){
            ed_class.setText("");
        }
        else{
            ed_class.setText(Classification);
        }
        if(HomeTown.equals("null")){
            ed_home.setText("");
        }
        else{
            ed_home.setText(HomeTown);
        }
        if(Gpa.equals("null")){
            ed_gpa.setText("");
        }
        else{
            ed_gpa.setText(Gpa);
        }
        if(Vertical.equals("null")){
            ed_vert.setText("");
        }
        else{
            ed_vert.setText(Vertical);
        }
        if(Speed.equals("null")){
            ed_speed.setText("");
        }
        else{
            ed_speed.setText(Speed);
        }
        if(Ranking.equals("null")){
            ed_rank.setText("");
        }
        else{
            ed_rank.setText(Ranking);
        }
        if(Position.equals("null")){
            ed_position.setText("");
        }
        else{
            ed_position.setText(Position);
        }
        if(Weight.equals("null")){
            ed_weight.setText("");
        }
        else{
            ed_weight.setText(Weight);
        }
        if(mobile.equals("null")){
            ed_mob.setText("");
        }
        else{
            ed_mob.setText(mobile);
        }
        if(Height.equals("null")){
            ed_height.setText("");
        }
        else{
            ed_height.setText(Height);
        }
        if(web.equals("null")){
            ed_web.setText("");
        }
        else{
            ed_web.setText(web);
        }
        if(bio.equals("null")){
            ed_bio.setText("");
        }
        else{
            ed_bio.setText(bio);
        }
        if(dob.equals("null")){
            tvDob.setText("");
        }
        else{
            tvDob.setText(dob);
        }
        if(gender.equals("null")){
            edgender.setText("");
        }
        else{
            edgender.setText(gender);
        }
        if(maritalStatus.equals("null")){
            ed_marital.setText("");
        }
        else{
            ed_marital.setText(maritalStatus);
        }



        ed_email.setText(SharedHelper.getKey(this,USEREMAIL));
        ed_ppg.setText(SharedHelper.getKey(this,PPG));
        ed_rpg.setText(SharedHelper.getKey(this,RPG));
        ed_apg.setText(SharedHelper.getKey(this,APG));


        imupload.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        bt_save.setOnClickListener(this);
        tvDob.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        gender_spinner.setSelection(position);
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }
    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tvDob.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }


    private void callEditProfileApi() {
        progressBar = new ProgressDialog(EditProfile.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Uri photoUri =uri1 ;
        RequestBody fname = RequestBody.create(MediaType.parse("multipart/form-data"), ed_fname.getText().toString());
        RequestBody lname = RequestBody.create(MediaType.parse("multipart/form-data"), ed_lname.getText().toString());
        RequestBody mobile = RequestBody.create(MediaType.parse("multipart/form-data"), ed_mob.getText().toString());
        RequestBody dob = RequestBody.create(MediaType.parse("multipart/form-data"), tvDob.getText().toString());
        RequestBody height = RequestBody.create(MediaType.parse("multipart/form-data"), ed_height.getText().toString());
        RequestBody weight = RequestBody.create(MediaType.parse("multipart/form-data"), ed_weight.getText().toString());
        RequestBody position = RequestBody.create(MediaType.parse("multipart/form-data"), ed_position.getText().toString());
        RequestBody rank = RequestBody.create(MediaType.parse("multipart/form-data"), ed_rank.getText().toString());
        RequestBody speed = RequestBody.create(MediaType.parse("multipart/form-data"), ed_speed.getText().toString());
        RequestBody vertical = RequestBody.create(MediaType.parse("multipart/form-data"), ed_vert.getText().toString());
        RequestBody gpa = RequestBody.create(MediaType.parse("multipart/form-data"), ed_gpa.getText().toString());
        RequestBody hometown = RequestBody.create(MediaType.parse("multipart/form-data"), ed_home.getText().toString());
        RequestBody classification = RequestBody.create(MediaType.parse("multipart/form-data"), ed_class.getText().toString());
        RequestBody schoolname = RequestBody.create(MediaType.parse("multipart/form-data"), ed_school.getText().toString());
        RequestBody experience = RequestBody.create(MediaType.parse("multipart/form-data"), ed_exp.getText().toString());
        RequestBody sportname = RequestBody.create(MediaType.parse("multipart/form-data"), ed_sport.getText().toString());
        RequestBody achievement = RequestBody.create(MediaType.parse("multipart/form-data"), ed_achieve.getText().toString());
        RequestBody sponser = RequestBody.create(MediaType.parse("multipart/form-data"), ed_sponser.getText().toString());
        RequestBody company = RequestBody.create(MediaType.parse("multipart/form-data"), ed_company.getText().toString());
        RequestBody speciality = RequestBody.create(MediaType.parse("multipart/form-data"), ed_spc.getText().toString());
        RequestBody ppg = RequestBody.create(MediaType.parse("multipart/form-data"), ed_ppg.getText().toString());
        RequestBody rpg = RequestBody.create(MediaType.parse("multipart/form-data"), ed_rpg.getText().toString());
        RequestBody apg = RequestBody.create(MediaType.parse("multipart/form-data"), ed_apg.getText().toString());

        try {
            File imageFile = new File(photoUri.getPath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageFile);
            body = MultipartBody.Part.createFormData("user_image", imageFile.getName(), requestFile);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.i(TAG, "calltoapiaddListData: " + e.getMessage());
        }

            Call<EditProfileResponse> responseCall = api.editprofile(mAuth_Token, fname, lname,dob,height,weight,position,rank,speed,vertical,gpa,hometown,classification,schoolname,experience,sportname,achievement,sponser,company,speciality,ppg,rpg,apg, body);

            responseCall.enqueue(new Callback<EditProfileResponse>() {
                @Override
                public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                    progressBar.dismiss();
                    if (response != null) {
                        if (response.body().getStatus() == 1) {

                            Toast.makeText(EditProfile.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            Log.d("response====>", response.body().toString());
                            Intent intent = new Intent(EditProfile.this, Dashboard.class);
                            startActivity(intent);
                        }
                        else if (response.body().getStatus() == 0) {
                            Toast.makeText(EditProfile.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditProfile.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }

                @Override
                public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                    Log.d("errorMsg", t.getMessage());
                    progressBar.dismiss();
                }
            });


    }

    private void openGallery() {
        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.bt_save:

                    callEditProfileApi();

                    break;

            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.imupload:
                openGallery();
                break;

            case R.id.tvDob:
                datePickerDialog.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                uri1 = result.getUri();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "onActivityResult: " + uri1);
                imProfile.setImageURI(uri1);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.i(TAG, "onActivityResult: " + error);
            } else {
                Log.i(TAG, "onActivityResult: " + "Else condition Error ");
            }
        }
    }
}