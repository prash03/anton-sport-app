package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.FindFriendAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.FollowUserListResponse;
import com.superapp.sports.model.FollowUserResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class FindFriend extends AppCompatActivity {
    RecyclerView recycleFind;
    FindFriendAdapter findFriendAdapter;
    Button btProceed;
    EditText edSearch;
    private ProgressDialog progressBar;
    String searchKey="",pageNum="",authKey="",isFrom="";
    int userId=0;
    boolean followStatus;
    private List<FollowUserListResponse.UserListData> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friend);

        isFrom = getIntent().getStringExtra("ISFROM");
        authKey= SharedHelper.getKey(this,AUTH_TOKEN);
        Log.d("authkey",authKey);

        edSearch=findViewById(R.id.edSearch);
        btProceed=findViewById(R.id.btProceed);
        recycleFind=findViewById(R.id.recycleFind);
        recycleFind.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycleFind.setHasFixedSize(true);


        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //searchUserAdapter.getFilter().filter(editable);
                searchKey= edSearch.getText().toString().trim();
                if(!searchKey.equalsIgnoreCase("")){
                    if(editable.length()>=3){
                       // callSearchUserApi(authKey,currentPage,search_query);
                        callUserListApi(searchKey);
                    }
                }else {
                    Toast.makeText(FindFriend.this, "Type a name", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i=new Intent(FindFriend.this,Dashboard.class);
                //startActivity(i);
                if(isFrom.equalsIgnoreCase("home")){
                    Intent intent = new Intent();
                    setResult(RESULT_OK,intent);
                    finish();
                }else {
                    Intent i=new Intent(FindFriend.this,Dashboard.class);
                    //i.putExtra("ISFROM","interestList");
                    startActivity(i);
                }
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        callUserListApi(searchKey);
    }

    private void callUserListApi(String search_type) {
        /*progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<FollowUserListResponse> responseCall = api.followingUserList(authKey, search_type,pageNum);
        responseCall.enqueue(new Callback<FollowUserListResponse>() {
            @Override
            public void onResponse(Call<FollowUserListResponse> call, Response<FollowUserListResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getUserData();

                        //Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        findFriendAdapter = new FindFriendAdapter(FindFriend.this, dataList);
                        recycleFind.setAdapter(findFriendAdapter);

                        findFriendAdapter.onItemClick(new FindFriendAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int userid,boolean followstat) {
                                userId=userid;
                                followStatus=followstat;

                                if(followStatus==false){
                                    callFollowApi();
                                }
                                else {
                                    callUnFollowApi();
                                }

                            }
                        });


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserListResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }

    private void callFollowApi() {
        progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<FollowUserResponse> responseCall = api.followUser(authKey,userId);
        responseCall.enqueue(new Callback<FollowUserResponse>() {
            @Override
            public void onResponse(Call<FollowUserResponse> call, Response<FollowUserResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        callUserListApi(searchKey);
                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }
    private void callUnFollowApi() {
        progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<FollowUserResponse> responseCall = api.UnfollowUser(authKey,userId);
        responseCall.enqueue(new Callback<FollowUserResponse>() {
            @Override
            public void onResponse(Call<FollowUserResponse> call, Response<FollowUserResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        callUserListApi(searchKey);
                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }
}