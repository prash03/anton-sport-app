package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.FollowingAdapter;
import com.superapp.sports.adapters.FollwerAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.FollowUserResponse;
import com.superapp.sports.model.FollowerUserResponse;
import com.superapp.sports.model.FollowingUserResponse;
import com.superapp.sports.model.PostStatusResponse;
import com.superapp.sports.model.RemoveFollowerResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class FollwerActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_follower,tv_following,tv_heading;
    RecyclerView recycleFollower,recycleFollowing;
    EditText ed_search,ed_searchFollowing;
    FollwerAdapter follwerAdapter;
    FollowingAdapter followingAdapter;
    String openTag="",authKey="",userId="",m_fullName,UserIdtodelete="";
    ImageView imgBack;
    private List<FollowerUserResponse.UserDatum> followerdataList;
    private List<FollowingUserResponse.UserDatum> followingdataList;
    RelativeLayout errorLayout;
    LinearLayout ll_searchFollowing,ll_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follwer);

        authKey= SharedHelper.getKey(FollwerActivity.this,AUTH_TOKEN);

        errorLayout=findViewById(R.id.errorLayout);
        ll_search=findViewById(R.id.ll_search);
        ll_searchFollowing=findViewById(R.id.ll_searchFollowing);
        ed_searchFollowing=findViewById(R.id.ed_searchFollowing);
        imgBack=findViewById(R.id.imgBack);
        tv_follower=findViewById(R.id.tv_follower);
        tv_following=findViewById(R.id.tv_following);
        ed_search=findViewById(R.id.ed_search);
        recycleFollower=findViewById(R.id.recycleFollower);
        recycleFollowing=findViewById(R.id.recycleFollowing);
        tv_heading=findViewById(R.id.tv_heading);

        openTag=getIntent().getStringExtra("openTag");
        userId=getIntent().getStringExtra("userid");
        m_fullName=getIntent().getStringExtra("fullName");


        tv_heading.setText(m_fullName);
        recycleFollower.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        recycleFollowing.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));


        if(openTag.equals("follower")){
            recycleFollower.setVisibility(View.VISIBLE);
            ll_search.setVisibility(View.VISIBLE);
            recycleFollowing.setVisibility(View.GONE);
            ll_searchFollowing.setVisibility(View.GONE);

            tv_follower.setTextColor(Color.parseColor("#1C1D46"));
            tv_follower.setBackgroundResource(R.drawable.white_rect_filled);

            tv_following.setTextColor(Color.parseColor("#ffffff"));
            tv_following.setBackgroundResource(R.drawable.blue_rect_filled);

            //follower User Api:--------

            followerUserApi();
        }
        else {
            recycleFollower.setVisibility(View.GONE);
            ll_search.setVisibility(View.GONE);
            recycleFollowing.setVisibility(View.VISIBLE);
            ll_searchFollowing.setVisibility(View.VISIBLE);

            tv_following.setTextColor(Color.parseColor("#1C1D46"));
            tv_following.setBackgroundResource(R.drawable.white_rect_filled);

            tv_follower.setTextColor(Color.parseColor("#ffffff"));
            tv_follower.setBackgroundResource(R.drawable.blue_rect_filled);

            //following User Api :-----

            followingUserApi();
        }

        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()>=3){
                    follwerAdapter.getFilter().filter(s);
                }

            }
        });
        ed_searchFollowing.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                if(s.length()>=3){
                    followingAdapter.getFilter().filter(s);
                }


            }
        });



        tv_follower.setOnClickListener(this);
        tv_following.setOnClickListener(this);
        imgBack.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_follower:
                ed_searchFollowing.setText("");
                ll_search.setVisibility(View.VISIBLE);
                recycleFollower.setVisibility(View.VISIBLE);
                recycleFollowing.setVisibility(View.GONE);
                ll_searchFollowing.setVisibility(View.GONE);

                tv_follower.setTextColor(Color.parseColor("#1C1D46"));
                tv_follower.setBackgroundResource(R.drawable.white_rect_filled);

                tv_following.setTextColor(Color.parseColor("#ffffff"));
                tv_following.setBackgroundResource(R.drawable.blue_rect_filled);
                break;

            case R.id.tv_following:
                ed_search.setText("");
                recycleFollower.setVisibility(View.GONE);
                ll_search.setVisibility(View.GONE);
                recycleFollowing.setVisibility(View.VISIBLE);
                ll_searchFollowing.setVisibility(View.VISIBLE);

                tv_following.setTextColor(Color.parseColor("#1C1D46"));
                tv_following.setBackgroundResource(R.drawable.white_rect_filled);

                tv_follower.setTextColor(Color.parseColor("#ffffff"));
                tv_follower.setBackgroundResource(R.drawable.blue_rect_filled);
                break;

            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    private void followerUserApi() {

        Api api = RestManager.instanceOf();
        Call<FollowerUserResponse> responseCall = api.followerUserListApi(authKey,userId);
        responseCall.enqueue(new Callback<FollowerUserResponse>() {
            @Override
            public void onResponse(Call<FollowerUserResponse> call, Response<FollowerUserResponse> response) {
                if (response != null) {

                    if (response.body().getStatus()==1) {
                        followerdataList = response.body().getUserData();
                        tv_follower.setText(followerdataList.size()+" Followers");

                        /*
                        notificationAdapter = new NotificationAdapter(getActivity(), dataList);
                        recycleNotification.setAdapter(notificationAdapter);*/

                        if(followerdataList.size()>0){
                            follwerAdapter = new FollwerAdapter(FollwerActivity.this, followerdataList);
                            recycleFollower.setAdapter(follwerAdapter);

                            errorLayout.setVisibility(View.GONE);
                            recycleFollower.setVisibility(View.VISIBLE);

                            follwerAdapter.onItemClick(new FollwerAdapter.OnClickDataCart() {
                                @Override
                                public void addNewClass(int userid) {
                                    int userId=userid;
                                    callRemoveFollower(authKey,userId);
                                }
                            });
                            follwerAdapter.onClickBlock(new FollwerAdapter.OnClickBlock() {
                                @Override
                                public void addNewClass(int userid, String blockStat) {
                                    UserIdtodelete= String.valueOf(userid);
                                    String BlockStatus=blockStat;
                                    if(BlockStatus.equals("yes")){
                                        blockUnblockApi(authKey,"unblock","",UserIdtodelete);
                                    }
                                    else {
                                        blockUnblockApi(authKey,"block","",UserIdtodelete);
                                    }
                                }
                            });
                        }
                        else {
                            recycleFollower.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowerUserResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }

    private void blockUnblockApi(String authKey,String postType, String postid,String userid) {

        Api api = RestManager.instanceOf();
        Call<PostStatusResponse> responseCall = api.statusPost(authKey, postType,postid,userid);
        responseCall.enqueue(new Callback<PostStatusResponse>() {
            @Override
            public void onResponse(Call<PostStatusResponse> call, Response<PostStatusResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        followerUserApi();
                        followingUserApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<PostStatusResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }
    private void followingUserApi() {
        errorLayout.setVisibility(View.GONE);
        Api api = RestManager.instanceOf();
        Call<FollowingUserResponse> responseCall = api.followingUserListApi(authKey,userId);
        responseCall.enqueue(new Callback<FollowingUserResponse>() {
            @Override
            public void onResponse(Call<FollowingUserResponse> call, Response<FollowingUserResponse> response) {
                if (response != null) {

                    if (response.body().getStatus()==1) {
                        followingdataList = response.body().getUserData();
                        tv_following.setText(followingdataList.size()+" Following");
                        /*
                        notificationAdapter = new NotificationAdapter(getActivity(), dataList);
                        recycleNotification.setAdapter(notificationAdapter);*/

                        if(followingdataList.size()>0){
                            errorLayout.setVisibility(View.GONE);
                            recycleFollowing.setVisibility(View.VISIBLE);

                            followingAdapter = new FollowingAdapter(FollwerActivity.this, followingdataList);
                            recycleFollowing.setAdapter(followingAdapter);

                            followingAdapter.onItemClick(new FollowingAdapter.OnClickDataCart() {
                                @Override
                                public void addNewClass(int userid) {
                                    int userId=userid;

                                    callUnfollowApi(authKey,userId);
                                }
                            });
                            followingAdapter.onClickBlock(new FollowingAdapter.OnClickBlock() {
                                @Override
                                public void addNewClass(int userid,String blockstatus) {
                                    UserIdtodelete= String.valueOf(userid);
                                    String BlockStatus=blockstatus;
                                    if(BlockStatus.equals("yes")){
                                        blockUnblockApi(authKey,"unblock","",UserIdtodelete);
                                    }
                                    else {
                                        blockUnblockApi(authKey,"block","",UserIdtodelete);
                                    }

                                }
                            });
                        }
                        else {
                            recycleFollowing.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowingUserResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }

    private void callRemoveFollower(String authToken,int userid) {

        Api api = RestManager.instanceOf();
        Call<RemoveFollowerResponse> responseCall = api.removeFollowerApi(authToken,userid);
        responseCall.enqueue(new Callback<RemoveFollowerResponse>() {
            @Override
            public void onResponse(Call<RemoveFollowerResponse> call, Response<RemoveFollowerResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        followerUserApi();
                        /*Intent i=new Intent(getActivity(), SecurityScreen.class);
                        startActivity(i);*/

                    } else if (response.body().getStatus()==0) {
                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FollwerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<RemoveFollowerResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void callUnfollowApi(String authToken,int userid) {

        Api api = RestManager.instanceOf();
        Call<FollowUserResponse> responseCall = api.UnfollowUser(authToken,userid);
        responseCall.enqueue(new Callback<FollowUserResponse>() {
            @Override
            public void onResponse(Call<FollowUserResponse> call, Response<FollowUserResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        followingUserApi();
                        /*Intent i=new Intent(getActivity(), SecurityScreen.class);
                        startActivity(i);*/

                    } else if (response.body().getStatus()==0) {
                        Toast.makeText(FollwerActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(FollwerActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                //progressBar.dismiss();
            }
        });
    }
}