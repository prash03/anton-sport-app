package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.ForgotResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {
    ImageView imgBack;
    Button btSend;
    private ProgressDialog progressBar;
    EditText edEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        edEmail = findViewById(R.id.edEmail);
        imgBack = findViewById(R.id.imgBack);
        btSend = findViewById(R.id.btSend);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callForgetApi(edEmail.getText().toString(),"android");
            }
        });
    }

    private void callForgetApi(String email,String device) {
        progressBar = new ProgressDialog(ForgotPassword.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<ForgotResponse> responseCall = api.forgotPasswordApi(email,device);
        responseCall.enqueue(new Callback<ForgotResponse>() {
            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                progressBar.dismiss();
                if (response != null) {
                    if (response.body().getStatus()==1) {

                        Toast.makeText(ForgotPassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ForgotPassword.this, ResetPassword.class);
                        startActivity(i);
                    }
                    else if (response.body().getStatus()==400) {
                        Toast.makeText(ForgotPassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ForgotPassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }
}