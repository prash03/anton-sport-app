package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.HelpAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.HelpResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class HelpActivity extends AppCompatActivity {
    RecyclerView recycl_help;
    ImageView imgBack;
    HelpAdapter helpAdapter;
    String authkey="";
    private ProgressDialog progressBar;
    private List<HelpResponse.DataReview.Reviews> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        recycl_help=findViewById(R.id.recycl_help);
        imgBack=findViewById(R.id.imgBack);
        authkey= SharedHelper.getKey(this,AUTH_TOKEN);

        recycl_help.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        callhelpApi(authkey);
    }

    private void callhelpApi(String authToken) {
        progressBar = new ProgressDialog(HelpActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<HelpResponse> responseCall = api.helpApi(authToken);
        responseCall.enqueue(new Callback<HelpResponse>() {
            @Override
            public void onResponse(Call<HelpResponse> call, Response<HelpResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData().getReviewsList();


                        helpAdapter = new HelpAdapter(HelpActivity.this, dataList);
                        recycl_help.setAdapter(helpAdapter);

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(HelpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(HelpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<HelpResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }
}