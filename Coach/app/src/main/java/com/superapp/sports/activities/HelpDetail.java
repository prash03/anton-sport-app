package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.SubmitProblemResponse;
import com.superapp.sports.utils.SharedHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class HelpDetail extends AppCompatActivity {
    ImageView imgBack;
    String heading="",authkey="" ,report_ID="",sub_report_ID="",decp="";
    TextView tvHeading,tv_header_name;
    private ProgressDialog progressBar;
    EditText edDescription;
    Button btSubmit;
    Call<SubmitProblemResponse> responseCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_detail);

        imgBack=findViewById(R.id.imgBack);
        btSubmit=findViewById(R.id.btSubmit);
        edDescription=findViewById(R.id.edDescription);
        tvHeading=findViewById(R.id.tvHeading);
        tv_header_name=findViewById(R.id.tv_header_name);

        heading = getIntent().getStringExtra("headStr");
        report_ID = getIntent().getStringExtra("ReportID");
        sub_report_ID = getIntent().getStringExtra("SubreportID");
        decp = getIntent().getStringExtra("DECP");

        if(!report_ID.equalsIgnoreCase("")){
            edDescription.setText(Html.fromHtml(decp));
            edDescription.setFocusableInTouchMode(false);
            tv_header_name.setText("Report Issue");
        }


        authkey= SharedHelper.getKey(this,AUTH_TOKEN);


        tvHeading.setText(heading);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edDescription.getText().toString().equals("")){
                    Toast.makeText(HelpDetail.this, "Please enter description", Toast.LENGTH_SHORT).show();
                }
                else {callsubmitApi();}
            }
        });


    }

    private void callsubmitApi() {
        progressBar = new ProgressDialog(HelpDetail.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        String descript=edDescription.getText().toString();
        Api api = RestManager.instanceOf();

        if(!report_ID.equalsIgnoreCase("")){
            responseCall = api.submitReport(authkey,report_ID,sub_report_ID);

        }else {
            responseCall = api.submitProblem(authkey,descript,heading);
        }

        responseCall.enqueue(new Callback<SubmitProblemResponse>() {
            @Override
            public void onResponse(Call<SubmitProblemResponse> call, Response<SubmitProblemResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(HelpDetail.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(HelpDetail.this,Dashboard.class);
                        startActivity(i);
                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(HelpDetail.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(HelpDetail.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<SubmitProblemResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }
}