package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.LoggedinAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.LoginActivityResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class LoggedinActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rv_logged;
    LoggedinAdapter loggedinAdapter;
    ImageView imgBack;
    String authKey="";
    private List<LoginActivityResponse.Datum> dataList;
    RelativeLayout errorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loggedin);

        authKey= SharedHelper.getKey(LoggedinActivity.this,AUTH_TOKEN);

        errorLayout=findViewById(R.id.errorLayout);
        imgBack=findViewById(R.id.imgBack);
        rv_logged=findViewById(R.id.rv_logged);
        rv_logged.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        imgBack.setOnClickListener(this::onClick);

        /*LoggedinResponse[] myListData = new LoggedinResponse[] {
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris),
                new LoggedinResponse("Delhi, India", "October 15,2020 "," This vivo 1724",R.drawable.chris)

        };

        loggedinAdapter=new LoggedinAdapter(myListData);
        rv_logged.setAdapter(loggedinAdapter);*/
        callLoginAcivityApi();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }
    private void callLoginAcivityApi() {
        /*progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<LoginActivityResponse> responseCall = api.loggedinListApi(authKey);
        responseCall.enqueue(new Callback<LoginActivityResponse>() {
            @Override
            public void onResponse(Call<LoginActivityResponse> call, Response<LoginActivityResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData();

                        /*
                        notificationAdapter = new NotificationAdapter(getActivity(), dataList);
                        recycleNotification.setAdapter(notificationAdapter);*/

                        if(dataList.size()>0){
                            loggedinAdapter = new LoggedinAdapter(LoggedinActivity.this, dataList);
                            rv_logged.setAdapter(loggedinAdapter);

                            errorLayout.setVisibility(View.GONE);
                        }
                        else {
                            rv_logged.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(LoggedinActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoggedinActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<LoginActivityResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }
}