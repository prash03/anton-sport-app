package com.superapp.sports.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hbb20.CountryCodePicker;
import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.LoginResponse;
import com.superapp.sports.model.RegisterResponse;
import com.superapp.sports.utils.CommonUtils;
import com.superapp.sports.utils.InternetConnectionCheck;
import com.superapp.sports.utils.MyGPSTracker;
import com.superapp.sports.utils.SharedHelper;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.agora.rtc.IRtcEngineEventHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.superapp.sports.utils.CommonUtils.showToast;
import static com.superapp.sports.utils.Constants.ACCOUNTTYPE;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.FCMTOKEN;
import static com.superapp.sports.utils.Constants.FNAME;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.LNAME;
import static com.superapp.sports.utils.Constants.LOGINTYPE;
import static com.superapp.sports.utils.Constants.USEREMAIL;
import static com.superapp.sports.utils.Constants.USERID;
import static com.superapp.sports.utils.Constants.USERMOBILE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int RC_SIGN_IN = 007;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int REQUEST_LOCATION = 1;
    CardView card_login, card_signup;
    CheckBox checkboxSponser, checkboxCoach, checkboxPlayer;
    Button bt_login, bt_signup;
    String profileImage="",dateTime = "", city = "", state = "", zip = "", country = "", country_code = "", latString = "", longString = "", location = "", selectedCountry = "", accountType = "", fcmToken = "", loginType = "", socialEmail = "", googleFname = "", googleLname = "", googleEmail = "", Fbfname = "", Fblname = "", Fbemail = "", FbUserID = "", type = "", FbBday = "";
    RadioGroup radioGroup, radioGroupLog;
    AppCompatEditText edFname, edLname, edEmail, edMobile, edPassword, edCpassword;
    EditText edLoginId, edLoginPass;
    ImageView imgGoogle, imgFacebook;
    AccessToken fbAccessToken;
    CountryCodePicker cCpicker;
    AccessTokenTracker accessTokenTracker;
    LocationManager locationManager;
    MyGPSTracker gps;
    private TextView tv_heading, tv_subhead, tv_forgot, tvBottomSinup, tvBottom;
    private ProgressDialog progressBar;
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private GoogleSignInClient googleApiClient;

    //method to generate Hash Key
    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }

    private static final int PERMISSION_REQ_ID = 22;


    private static final String[] REQUESTED_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    private boolean checkSelfPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode);
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this);
        // Facebook
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);


        // If all the permissions are granted, initialize the RtcEngine object and join a channel.
        if (checkSelfPermission(REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID)) {
        }


        imgGoogle = findViewById(R.id.imgGoogle);
        imgFacebook = findViewById(R.id.imgFacebook);
        radioGroup = findViewById(R.id.radioGroup);
        bt_login = findViewById(R.id.bt_login);
        bt_signup = findViewById(R.id.bt_signup);
        checkboxPlayer = findViewById(R.id.checkboxPlayer);
        checkboxSponser = findViewById(R.id.checkboxSponser);
        checkboxCoach = findViewById(R.id.checkboxCoach);
        tv_forgot = findViewById(R.id.tv_forgot);
        tvBottomSinup = findViewById(R.id.tvBottomSinup);
        tv_heading = findViewById(R.id.tv_heading);
        tv_subhead = findViewById(R.id.tv_subhead);
        tvBottom = findViewById(R.id.tvBottom);
        card_login = findViewById(R.id.card_login);
        card_signup = findViewById(R.id.card_signup);
        edFname = findViewById(R.id.edFname);
        edLname = findViewById(R.id.edLname);
        edEmail = findViewById(R.id.edEmail);
        edMobile = findViewById(R.id.edMobile);
        edPassword = findViewById(R.id.edPassword);
        edCpassword = findViewById(R.id.edCpassword);
        edLoginId = findViewById(R.id.edLoginId);
        edLoginPass = findViewById(R.id.edLoginPass);
        cCpicker = findViewById(R.id.country_code_picker);

        selectedCountry = cCpicker.getSelectedCountryCodeWithPlus();
        //cCpicker.setCountryForPhoneCode(+1);
        cCpicker.setCountryForNameCode("US");


        cCpicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                selectedCountry = cCpicker.getSelectedCountryCodeWithPlus();
                Log.d("codePic:", selectedCountry);
            }
        });



        // Location
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getAddress();
        imgGoogle.setOnClickListener(this);
        imgFacebook.setOnClickListener(this);
        checkboxPlayer.setOnClickListener(this);
        checkboxSponser.setOnClickListener(this);
        checkboxCoach.setOnClickListener(this);
        bt_signup.setOnClickListener(this);
        bt_login.setOnClickListener(this);
        tvBottom.setOnClickListener(this);
        tvBottomSinup.setOnClickListener(this);
        tv_forgot.setOnClickListener(this);
        tvBottom.setText(Html.fromHtml(getResources().getString(R.string.signuptext)));
        tvBottomSinup.setText(Html.fromHtml(getResources().getString(R.string.logintext)));


        printHashKey(this);
        getFirebaseMessagingToken();

        //Date c = Calendar.getInstance().getTime();
        SimpleDateFormat dsf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
        Calendar dc = Calendar.getInstance();
        dateTime = dsf.format(dc.getTime());
        System.out.println("Current time => " + dateTime);
        Log.d("tokenfcm", fcmToken);


        // Google login
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        googleApiClient = GoogleSignIn.getClient(this, gso);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checkedRadioButton = findViewById(checkedId);
                String text = checkedRadioButton.getText().toString();
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                accountType = text;
                SharedHelper.putKey(LoginActivity.this, ACCOUNTTYPE, accountType);
                Log.d("fbfnamelname:", Fbfname + Fblname);
            }
        });

    }



    public void getAddress() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if (null != locations && null != providerList && providerList.size() > 0) {
            double longitude = locations.getLongitude();
            double latitude = locations.getLatitude();
            latString = String.valueOf(latitude);
            longString = String.valueOf(longitude);
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    String _Location = listAddresses.get(0).getAddressLine(0);
                    String _city = listAddresses.get(0).getLocality();
                    String _state = listAddresses.get(0).getAdminArea();
                    String _zip = listAddresses.get(0).getPostalCode();
                    String _country = listAddresses.get(0).getCountryName();
                    String _country_code = listAddresses.get(0).getCountryCode();

                    location = _Location;
                    city = _city;
                    state = _state;
                    zip = _zip;
                    country = _country;
                    country_code = _country_code;

                    Log.d("fetchedLocation:", location + "; " + country + "; " + country_code + "; " + city + "; " + state + "; " + zip);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void signIn() {

        Intent signInIntent = googleApiClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    // get facebook details /
    private void getFacebookDetails() {
        progressBar = new ProgressDialog(LoginActivity.this);
        progressBar.setProgressStyle(0);
        progressBar.setMessage("Fetching Facebook Details!");
        progressBar.show();
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                progressBar.isShowing();
                AccessToken current_token = AccessToken.getCurrentAccessToken();
                Log.e("fb current_token", current_token.toString());
                GraphRequest request = GraphRequest.newMeRequest(current_token, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        JSONObject facebookResponse = response.getJSONObject();
                        //Log.e("fbResponse", facebookResponse.toString());
                        Log.i("fbResponse", object.toString());
                        try {
                            if (facebookResponse.has("id") && facebookResponse.getInt("id") > 0) {

                                progressBar.dismiss();
                                try {
                                    String name = object.optString("name");
                                    String[] parts = name.split(" ");
                                    Fbfname = parts[0];
                                    Fblname = parts[1];
                                    Fbemail = object.optString("email");
                                    FbUserID = object.getString("id");
                                    String phone = object.optString("phone_number");
                                    Log.d("phone",phone);
                                    //  FbBday=object.getString("birthday");
                                    String image_url = "https://graph.facebook.com/" + FbUserID + "/picture?type=normal";
                                    profileImage=image_url;
                                    SharedHelper.putKey(getApplicationContext(), Key_profile_image, image_url);
                                    Log.i(TAG, " nameonCompleted: " + Fbfname + Fblname);

                                    openLoginTypeDialog();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            LoginManager.getInstance().logOut();
                            e.printStackTrace();
                            progressBar.dismiss();
                            Log.e("Exception", e.getMessage());
                            if (e.getMessage() != null && e.getMessage().contains("webview")) {
                            }
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
                progressBar.dismiss();
            }

            @Override
            public void onError(FacebookException error) {
                LoginManager.getInstance().logOut();
                progressBar.dismiss();
            }
        });
    }

    private void openLoginTypeDialog() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
        final LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialogView1;
        dialogView1 = inflater1.inflate(R.layout.role_dialog, null);
        final RadioGroup radioGrLog1 = dialogView1.findViewById(R.id.radioGroupLog);

        builder1.setView(dialogView1);
        builder1.setCancelable(false);
        final AlertDialog dialog1 = builder1.create();
        TextView ok1 = dialogView1.findViewById(R.id.ok_bt);

        dialog1.show();

        radioGrLog1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton cRadioButton = dialogView1.findViewById(i);
                String textStr = cRadioButton.getText().toString();
                Toast.makeText(getApplicationContext(), textStr, Toast.LENGTH_SHORT).show();
                Log.d("textStr:", textStr);
                accountType = textStr;
            }
        });
        ok1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (accountType.equals("")) {
                    showToast(LoginActivity.this, "Please Select Valid Account Type");
                } else {
                    callSignupApi(accountType, "social", Fbfname, Fblname, Fbemail, "", "", fcmToken, "android",profileImage);
                }
                dialog1.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgGoogle:
                loginType = "social";
                signIn();
                break;

            case R.id.bt_signup:

                String pass = edPassword.getText().toString().trim();
                String cnfpass = edCpassword.getText().toString().trim();
                if (edFname.getText().toString().trim().equals("")) {
                    showToast(this, getString(R.string.name));
                } else if (edLname.getText().toString().trim().equalsIgnoreCase("")) {
                    showToast(this, getString(R.string.last_name));
                } else if (!CommonUtils.isValidEmail(edEmail.getText().toString().trim()) || edEmail.getText().toString().trim().equalsIgnoreCase("")) {
                    showToast(this, getString(R.string.not_valid_email));
                } else if (edMobile.getText().toString().trim().equals("")) {
                    showToast(this, getString(R.string.phone_number));
                } else if (pass.equals("") || pass.equalsIgnoreCase(getString(R.string.password_txt))) {
                    showToast(this, getString(R.string.password_validation));
                } else if (edPassword.length() < 6) {
                    showToast(this, getString(R.string.password_size));
                } else if (!pass.equals(cnfpass)) {
                    showToast(this, getString(R.string.password_match));
                } else if (accountType.equals("")) {
                    showToast(LoginActivity.this, "Please Select Valid Account Type");
                } else {
                    if (InternetConnectionCheck.haveNetworkConnection(LoginActivity.this)) {
                        callSignupApi(accountType, "manual", edFname.getText().toString(), edLname.getText().toString(), edEmail.getText().toString(), edMobile.getText().toString(), edPassword.getText().toString(), fcmToken, "android","");
                    } else {
                        showToast(this, getString(R.string.something_went_wrong_net));
                    }
                }
                break;

            case R.id.bt_login:
                String txtpass = edLoginPass.getText().toString().trim();
                String txtmail = edLoginId.getText().toString().trim();

                if (!CommonUtils.isValidEmail(txtmail) || txtmail.trim().equalsIgnoreCase("")) {
                    //if (edLoginId.getText().toString().trim().equals("")) {
                    showToast(this, getString(R.string.not_valid_email));
                } else if (txtpass.equals("") || txtpass.equalsIgnoreCase(getString(R.string.password_txt))) {
                    showToast(this, getString(R.string.password_validation));
                } else if (txtpass.equals("") || txtmail.equals("")) {
                    showToast(this, getString(R.string.password_email));
                } else {
                    if (InternetConnectionCheck.haveNetworkConnection(LoginActivity.this)) {

                        callLoginApi(edLoginId.getText().toString(), edLoginPass.getText().toString(), fcmToken, "android");
                    } else {
                        showToast(this, getString(R.string.something_went_wrong_net));
                    }
                }
                break;

            case R.id.imgFacebook:
               /* fbAccessToken = AccessToken.getCurrentAccessToken();
                Log.d("tokenvalue", String.valueOf(fbAccessToken));
*/
                loginType = "social";
                Log.d("valueOfType", loginType);
                getFacebookDetails();

                break;

            case R.id.tvBottom:
                card_login.setVisibility(View.GONE);
                tv_forgot.setVisibility(View.GONE);
                tvBottomSinup.setVisibility(View.VISIBLE);
                tvBottom.setVisibility(View.GONE);
                card_signup.setVisibility(View.VISIBLE);
                tv_heading.setText("Sign Up");
                tv_subhead.setText("Enter your details to \ncreate account");
                //Log.d("lat-lng", latString + " , " + longString + "," + location);
                Log.d("fetchedLocation:", location + "; " + country + "; " + country_code + "; " + city + "; " + state + "; " + zip);
                break;

            case R.id.tvBottomSinup:
                tvBottomSinup.setVisibility(View.GONE);
                tvBottom.setVisibility(View.VISIBLE);
                tv_forgot.setVisibility(View.VISIBLE);
                card_login.setVisibility(View.VISIBLE);
                card_signup.setVisibility(View.GONE);
                card_signup.setVisibility(View.GONE);
                tv_heading.setText("Sign In");
                tv_subhead.setText("Enter your login credentials");
                break;
            case R.id.tv_forgot:
                Intent i = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(i);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
                Log.d(TAG, "firebaseAuthWithGoogleLastName:" + account.getFamilyName());
                Log.d(TAG, "firebaseAuthWithName:" + account.getGivenName());
                Log.d(TAG, "firebaseAuthEmail:" + account.getEmail());
                Log.d(TAG, "firebaseAuthIamge:" + account.getPhotoUrl());
                String image_url = String.valueOf(account.getPhotoUrl());
                profileImage=image_url;
                SharedHelper.putKey(getApplicationContext(), Key_profile_image, image_url);
                Fbfname=account.getGivenName();
                Fblname=account.getFamilyName();
                Fbemail=account.getEmail();
                openLoginTypeDialog();
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.i(TAG, "Google sign in failed: " + e.getMessage());
            }

        }
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    private void callSignupApi(String uType, String logType, String fname, String lname, String email, String mobile, String password, String fcmToken, String deviceType,String profilepic) {
        progressBar = new ProgressDialog(LoginActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<RegisterResponse> responseCall = api.signupapi(uType, logType, fname, lname, email, mobile, password, fcmToken, deviceType, country, country_code, zip, location, state, city,profilepic);
        responseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {

                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        SharedHelper.putKey(LoginActivity.this, AUTH_TOKEN, response.body().getRq_detail().getAuthToken());
                        SharedHelper.putKey(LoginActivity.this, ACCOUNTTYPE, response.body().getRq_detail().getUserType());
                        SharedHelper.putKey(LoginActivity.this, FNAME, response.body().getRq_detail().getFname());
                        SharedHelper.putKey(LoginActivity.this, LNAME, response.body().getRq_detail().getLname());
                        SharedHelper.putKey(LoginActivity.this, USEREMAIL, response.body().getRq_detail().getEmail());
                        SharedHelper.putKey(LoginActivity.this, USERMOBILE, response.body().getRq_detail().getMob());
                        SharedHelper.putKey(LoginActivity.this, USERID, response.body().getRq_detail().getUserId());

                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.permission_dialog, null);
                        builder.setView(view);
                        builder.setCancelable(false);
                        final AlertDialog dialog = builder.create();
                        TextView ok_btn = view.findViewById(R.id.ok_btn);

                        TextView tvHead = view.findViewById(R.id.tvHead);
                        TextView tvDeny = view.findViewById(R.id.tvDeny);

                        tvHead.setText(Html.fromHtml(getResources().getString(R.string.permission_loc)));
                        dialog.show();
                        ok_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(LoginActivity.this, ChooseInterest.class);
                                i.putExtra("ISFROM","LogIN");
                                startActivity(i);
                                dialog.dismiss();

                            }
                        });

                        tvDeny.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                                System.exit(0);
                            }
                        });

                    } else if (response.body().getStatus() == 0) {

                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    private void callLoginApi(String email, String password, String fcmToken, String deviceType) {
        progressBar = new ProgressDialog(LoginActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        //Call<RegisterResponse> responseCall = api.loginapi(email,password,fcmToken,deviceType,country,country_code,zip,location,state,city);
        Call<LoginResponse> responseCall = api.loginapi(email, password, fcmToken, deviceType, location, latString, longString, dateTime);
        responseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {

                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        SharedHelper.putKey(LoginActivity.this, AUTH_TOKEN, response.body().getAuthKey());
                        SharedHelper.putKey(LoginActivity.this, ACCOUNTTYPE, response.body().getRq_detail().getUserType());
                        SharedHelper.putKey(LoginActivity.this, FNAME, response.body().getRq_detail().getFname());
                        SharedHelper.putKey(LoginActivity.this, LNAME, response.body().getRq_detail().getLname());
                        SharedHelper.putKey(LoginActivity.this, USEREMAIL, response.body().getRq_detail().getEmail());
                        SharedHelper.putKey(LoginActivity.this, USERMOBILE, response.body().getRq_detail().getMob());
                        SharedHelper.putKey(LoginActivity.this, USERID, response.body().getRq_detail().getUserId());
                        SharedHelper.putKey(LoginActivity.this, LOGINTYPE, response.body().getRq_detail().getLoginType());

                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.permission_dialog, null);
                        builder.setView(view);
                        builder.setCancelable(false);
                        final AlertDialog dialog = builder.create();
                        TextView ok_btn = view.findViewById(R.id.ok_btn);

                        TextView tvHead = view.findViewById(R.id.tvHead);
                        TextView tvDeny = view.findViewById(R.id.tvDeny);

                        tvHead.setText(Html.fromHtml(getResources().getString(R.string.permission_loc)));
                        dialog.show();
                        ok_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                /*Intent i=new Intent(LoginActivity.this,FindFriend.class);
                                startActivity(i);*/
                                Intent i = new Intent(LoginActivity.this, Dashboard.class);
                                startActivity(i);
                                dialog.dismiss();

                            }
                        });

                        tvDeny.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                                System.exit(0);
                            }
                        });

                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (response.body().getStatus() == 400) {

                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    public void getFirebaseMessagingToken ( ) {
        FirebaseMessaging.getInstance ().getToken ().addOnCompleteListener ( task -> {
                    if (!task.isSuccessful ()) {
                        //Could not get FirebaseMessagingToken
                        Log.i(TAG, "getFirebaseMessagingToken: "+ "token Not Received");
                        return;
                    }
                    if (null != task.getResult ()) {
                        //Got FirebaseMessagingToken
                        fcmToken = Objects.requireNonNull ( task.getResult () );
                        Log.i(TAG, "getFirebaseMessagingToken: "+ fcmToken);
                        //Use firebaseMessagingToken further
                    }
                } );
    }
}