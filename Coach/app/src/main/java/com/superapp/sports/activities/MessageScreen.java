package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.superapp.sports.R;


public class MessageScreen extends AppCompatActivity {
    RecyclerView recycleChat,recycleCall;
    TextView tv_chat,tv_call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_screen);

        tv_chat=findViewById(R.id.tv_chat);
        tv_call=findViewById(R.id.tv_call);
        recycleChat=findViewById(R.id.recycleChat);
        recycleCall=findViewById(R.id.recycleCall);

        recycleChat.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycleCall.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        /*ChooseInterestResponse[] myListData = new ChooseInterestResponse[] {
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top)

        };
        chatAdapter = new ChatAdapter(this,myListData);
        recycleChat.setAdapter(chatAdapter);

        callAdapter = new CallAdapter(myListData,this);
        recycleCall.setAdapter(callAdapter);*/

        tv_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recycleChat.setVisibility(View.VISIBLE);
                recycleCall.setVisibility(View.GONE);
                tv_chat.setTextColor(Color.parseColor("#1C1D46"));
                tv_chat.setBackgroundResource(R.drawable.white_rect_filled);

                tv_call.setTextColor(Color.parseColor("#ffffff"));
                tv_call.setBackgroundResource(R.drawable.blue_rect_filled);

            }
        });
        tv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recycleChat.setVisibility(View.GONE);
                recycleCall.setVisibility(View.VISIBLE);
                tv_call.setTextColor(Color.parseColor("#1C1D46"));
                tv_call.setBackgroundResource(R.drawable.white_rect_filled);

                tv_chat.setTextColor(Color.parseColor("#ffffff"));
                tv_chat.setBackgroundResource(R.drawable.blue_rect_filled);

            }
        });
    }
}