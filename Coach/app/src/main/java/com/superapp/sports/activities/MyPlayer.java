package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.PlayersAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.MyPlayerResponse;
import com.superapp.sports.model.MySponserResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class MyPlayer extends AppCompatActivity implements View.OnClickListener {
    ImageView imBack;
    RecyclerView recyclePlayers;
    PlayersAdapter playersAdapter;
    String authkey="";
    private ProgressDialog progressBar;
    private List<MySponserResponse.Datum> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_player);
        imBack=findViewById(R.id.imgBack);
        recyclePlayers=findViewById(R.id.recyclePlayers);

        authkey= SharedHelper.getKey(this,AUTH_TOKEN);
        recyclePlayers.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        /*PlayersResponse[] myListData = new PlayersResponse[] {
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic),
                new PlayersResponse("Bess Wilkerson", "500 $","Feb 09",R.drawable.tagpic)

        };

        playersAdapter=new PlayersAdapter(this,myListData);
        recyclePlayers.setAdapter(playersAdapter);*/
        imBack.setOnClickListener(this::onClick);

        callmyplayerApi(authkey);
    }

    private void callmyplayerApi(String authToken) {
        progressBar = new ProgressDialog(MyPlayer.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<MySponserResponse> responseCall = api.mysponserPlayerApi(authToken);
        responseCall.enqueue(new Callback<MySponserResponse>() {
            @Override
            public void onResponse(Call<MySponserResponse> call, Response<MySponserResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData();

                        playersAdapter = new PlayersAdapter(MyPlayer.this, dataList);
                        recyclePlayers.setAdapter(playersAdapter);



                        /*chooseAdapter.onItemClick(new ChooseAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int numSelected, ArrayList<Integer> playIdSelected) {
                                selectedPlay=numSelected;
                                selectedItemid=playIdSelected;
                                tvSelected.setText(String.valueOf(selectedPlay));
                                Log.d("selectedItemid:",selectedItemid.toString());
                            }
                        });*/

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(MyPlayer.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyPlayer.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<MySponserResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }
}