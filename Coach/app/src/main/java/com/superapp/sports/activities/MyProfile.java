package com.superapp.sports.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.MyInterestAdapter;
import com.superapp.sports.adapters.ProfileAdapter;
import com.superapp.sports.adapters.ProfileTagAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.MyProfileResponse;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.ACCOUNTTYPE;
import static com.superapp.sports.utils.Constants.ACHIEVEMENT;
import static com.superapp.sports.utils.Constants.APG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.BIO;
import static com.superapp.sports.utils.Constants.CLASSIFICATION;
import static com.superapp.sports.utils.Constants.COMPANYNAME;
import static com.superapp.sports.utils.Constants.DOB;
import static com.superapp.sports.utils.Constants.EXPERIENCE;
import static com.superapp.sports.utils.Constants.FNAME;
import static com.superapp.sports.utils.Constants.GENDER;
import static com.superapp.sports.utils.Constants.GPA;
import static com.superapp.sports.utils.Constants.HEIGHT;
import static com.superapp.sports.utils.Constants.HOME;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.LNAME;
import static com.superapp.sports.utils.Constants.MARITAL;
import static com.superapp.sports.utils.Constants.POSITION;
import static com.superapp.sports.utils.Constants.PPG;
import static com.superapp.sports.utils.Constants.RANK;
import static com.superapp.sports.utils.Constants.RPG;
import static com.superapp.sports.utils.Constants.SCHOOLNAME;
import static com.superapp.sports.utils.Constants.SPECIALITY;
import static com.superapp.sports.utils.Constants.SPEED;
import static com.superapp.sports.utils.Constants.SPONSERNAME;
import static com.superapp.sports.utils.Constants.SPORTNAME;
import static com.superapp.sports.utils.Constants.USEREMAIL;
import static com.superapp.sports.utils.Constants.USERMOBILE;
import static com.superapp.sports.utils.Constants.VERTICLE;
import static com.superapp.sports.utils.Constants.WEBSITE;
import static com.superapp.sports.utils.Constants.WEIGHT;

public class MyProfile extends AppCompatActivity implements View.OnClickListener {
    private static final int CHOOSETAG = 233;
    RecyclerView recycleProfile,recycleTag,recycleInterest;
    ProfileAdapter profileAdapter;
    ProfileTagAdapter profileTagAdapter;
    MyInterestAdapter myInterestAdapter;
    LinearLayout ll_self, ll_tag, llFollower, llFollowing;
    ImageView img_self, img_tag;
    TextView tvself, tvtag, tv_upgrade, tvsponsors,tvpostCount,tvFollowerCount,tvFollowingCount,tvName,tv_ppg,tv_rpg,tv_apg;
    View view_self, view_tag;
    CircleImageView imgAdd,imgProfile;
    String accountType="",authkey="",userId="",full_name="";
    String profile_img="";
    private ProgressDialog progressBar;
    private List<MyProfileResponse.UserData.SelfPost> dataListPost;
    private List<MyProfileResponse.UserData.TagPost> dataListTag;
    private List<MyProfileResponse.UserData.MyInterest> interestList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        llFollower = findViewById(R.id.llFollower);
        llFollowing = findViewById(R.id.llFollowing);
        view_tag = findViewById(R.id.view_tag);
        view_self = findViewById(R.id.view_self);
        tvtag = findViewById(R.id.tvtag);
        tvself = findViewById(R.id.tvself);
        img_tag = findViewById(R.id.img_tag);
        img_self = findViewById(R.id.img_self);
        ll_self = findViewById(R.id.ll_self);
        tv_upgrade = findViewById(R.id.tv_upgrade);
        ll_tag = findViewById(R.id.ll_tag);
        imgAdd = findViewById(R.id.imgAdd);
        imgProfile = findViewById(R.id.imgProfile);
        tvpostCount = findViewById(R.id.tvpostCount);
        tvFollowerCount = findViewById(R.id.tvFollowerCount);
        tvFollowingCount = findViewById(R.id.tvFollowingCount);
        tvName = findViewById(R.id.tvName);
        recycleProfile = findViewById(R.id.recycleProfile);
        recycleInterest = findViewById(R.id.recycleInterest);
        recycleTag = findViewById(R.id.recycleTag);
        tvsponsors = findViewById(R.id.tv_sponsors);
        tv_ppg = findViewById(R.id.tv_ppg);
        tv_rpg = findViewById(R.id.tv_rpg);
        tv_apg = findViewById(R.id.tv_apg);

        authkey=SharedHelper.getKey(this,AUTH_TOKEN);
        llFollower.setOnClickListener(this);
        llFollowing.setOnClickListener(this);
        tv_upgrade.setOnClickListener(this);


        imgAdd.setOnClickListener(this);
        tvsponsors.setOnClickListener(this);

        accountType= SharedHelper.getKey(this,ACCOUNTTYPE);

        if(accountType.equals("Coach")){
            tvsponsors.setVisibility(View.GONE);
        }
        else if(accountType.equals("Player")){
            tvsponsors.setVisibility(View.VISIBLE);
        }
        else if(accountType.equals("Sponsor")){
            tvsponsors.setVisibility(View.VISIBLE);
        }

        profile_img = String.valueOf(SharedHelper.getKey(this,Key_profile_image));
        if (!profile_img.equalsIgnoreCase("")) {
            Picasso.get().load(profile_img).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(imgProfile);
        }


        recycleProfile.setLayoutManager(new GridLayoutManager(this, 3));
        recycleProfile.setHasFixedSize(true);

        recycleTag.setLayoutManager(new GridLayoutManager(this, 3));
        recycleTag.setHasFixedSize(true);

        recycleInterest.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recycleInterest.setHasFixedSize(true);


        ll_self.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvself.setTextColor(Color.parseColor("#1C1D46"));
                view_self.setBackgroundColor(Color.parseColor("#1C1D46"));
                img_self.setColorFilter(Color.parseColor("#1C1D46"));

                tvtag.setTextColor(Color.parseColor("#818181"));
                view_tag.setBackgroundColor(Color.parseColor("#908D8D"));
                img_tag.setColorFilter(Color.parseColor("#908D8D"));

                recycleProfile.setVisibility(View.VISIBLE);
                recycleTag.setVisibility(View.GONE);

            }
        });
        ll_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvself.setTextColor(Color.parseColor("#818181"));
                view_self.setBackgroundColor(Color.parseColor("#908D8D"));
                img_self.setColorFilter(Color.parseColor("#908D8D"));

                tvtag.setTextColor(Color.parseColor("#1C1D46"));
                view_tag.setBackgroundColor(Color.parseColor("#1C1D46"));
                img_tag.setColorFilter(Color.parseColor("#1C1D46"));

                recycleProfile.setVisibility(View.GONE);
                recycleTag.setVisibility(View.VISIBLE);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        callmyprofileApi(authkey);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void callmyprofileApi(String authToken) {
        progressBar = new ProgressDialog(MyProfile.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<MyProfileResponse> responseCall = api.myprofileApi(authToken);
        responseCall.enqueue(new Callback<MyProfileResponse>() {
            @Override
            public void onResponse(Call<MyProfileResponse> call, Response<MyProfileResponse> response) {
                progressBar.dismiss();

                if (response != null) {
                    System.out.println(response.toString() + "....My PRofile");
                    if (response.body().getStatus()==1) {
                        dataListTag = response.body().getUserData().getTagPost();
                        dataListPost = response.body().getUserData().getSelfPost();
                        interestList = response.body().getUserData().getMyInterest();
                        String fname=response.body().getUserData().getFirstName();
                        String lname=response.body().getUserData().getLastName();
                        full_name = fname+" "+lname;
                        userId= String.valueOf(response.body().getUserId());
                        profile_img = String.valueOf(response.body().getUserData().getUserImage());
                        if (!profile_img.equalsIgnoreCase("")) {
                            Picasso.get().load(profile_img).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(imgProfile);
                        }

                        tv_ppg.setText(response.body().getUserData().getPpg());
                        tv_rpg.setText(response.body().getUserData().getRpg());
                        tv_apg.setText(response.body().getUserData().getApg());

                        SharedHelper.putKey(MyProfile.this,Key_profile_image, String.valueOf(response.body().getUserData().getUserImage()));
                        SharedHelper.putKey(MyProfile.this,FNAME,String.valueOf(response.body().getUserData().getFirstName()));
                        SharedHelper.putKey(MyProfile.this,LNAME,String.valueOf(response.body().getUserData().getLastName()));
                        SharedHelper.putKey(MyProfile.this,USERMOBILE,String.valueOf(response.body().getUserData().getUserMobileNum()));
                        SharedHelper.putKey(MyProfile.this,WEBSITE,String.valueOf(response.body().getUserData().getUserWebsite()));
                        SharedHelper.putKey(MyProfile.this,BIO,String.valueOf(response.body().getUserData().getUserBio()));
                        SharedHelper.putKey(MyProfile.this,DOB,String.valueOf(response.body().getUserData().getUserDob()));
                        SharedHelper.putKey(MyProfile.this,GENDER,String.valueOf(response.body().getUserData().getUserGender()));
                        SharedHelper.putKey(MyProfile.this,MARITAL, String.valueOf(response.body().getUserData().getUserMaitalStatus()));
                        SharedHelper.putKey(MyProfile.this,USEREMAIL,response.body().getUserData().getUserEmail());
                        SharedHelper.putKey(MyProfile.this,HEIGHT,response.body().getUserData().getHeight());
                        SharedHelper.putKey(MyProfile.this,WEIGHT,response.body().getUserData().getWeight());
                        SharedHelper.putKey(MyProfile.this,POSITION,response.body().getUserData().getPosition());
                        SharedHelper.putKey(MyProfile.this,RANK,response.body().getUserData().getRanking());
                        SharedHelper.putKey(MyProfile.this,SPEED,response.body().getUserData().getSpeed());
                        SharedHelper.putKey(MyProfile.this,VERTICLE,response.body().getUserData().getVertical());
                        SharedHelper.putKey(MyProfile.this,GPA,response.body().getUserData().getGpa());
                        SharedHelper.putKey(MyProfile.this,HOME,response.body().getUserData().getHome_town());
                        SharedHelper.putKey(MyProfile.this,CLASSIFICATION,response.body().getUserData().getClassification());
                        SharedHelper.putKey(MyProfile.this,SCHOOLNAME,response.body().getUserData().getSchool());
                        SharedHelper.putKey(MyProfile.this,EXPERIENCE,response.body().getUserData().getSchool());
                        SharedHelper.putKey(MyProfile.this,SPORTNAME,response.body().getUserData().getSports_name());
                        SharedHelper.putKey(MyProfile.this,ACHIEVEMENT,response.body().getUserData().getAchievement());
                        SharedHelper.putKey(MyProfile.this,SPONSERNAME,response.body().getUserData().getSports_name());
                        SharedHelper.putKey(MyProfile.this,COMPANYNAME,response.body().getUserData().getCompany_work());
                        SharedHelper.putKey(MyProfile.this,SPECIALITY,response.body().getUserData().getSpeciality());
                        SharedHelper.putKey(MyProfile.this,PPG,response.body().getUserData().getPpg());
                        SharedHelper.putKey(MyProfile.this,RPG,response.body().getUserData().getRpg());
                        SharedHelper.putKey(MyProfile.this,APG,response.body().getUserData().getApg());

                        tvpostCount.setText(String.valueOf(response.body().getUserData().getPostsCount()));
                        tvFollowerCount.setText(String.valueOf(response.body().getUserData().getFollowersCount()));
                        tvFollowingCount.setText(String.valueOf(response.body().getUserData().getFollowingCount()));
                        tvName.setText(fname+" "+lname + " ( "+response.body().getUserData().getUser_type()+" )");

                        profileAdapter = new ProfileAdapter(MyProfile.this, dataListPost, response.body().getUserId());
                        recycleProfile.setAdapter(profileAdapter);

                        profileTagAdapter = new ProfileTagAdapter(MyProfile.this, dataListTag,response.body().getUserId());
                        recycleTag.setAdapter(profileTagAdapter);

                        myInterestAdapter = new MyInterestAdapter(MyProfile.this, interestList);
                        recycleInterest.setAdapter(myInterestAdapter);



                        /*chooseAdapter.onItemClick(new ChooseAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int numSelected, ArrayList<Integer> playIdSelected) {
                                selectedPlay=numSelected;
                                selectedItemid=playIdSelected;
                                tvSelected.setText(String.valueOf(selectedPlay));
                                Log.d("selectedItemid:",selectedItemid.toString());
                            }
                        });*/

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(MyProfile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyProfile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<MyProfileResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_view_profile:
            case R.id.imgProfile:

                Intent i = new Intent(MyProfile.this, ViewMyProfileDetail.class);
                i.putExtra("ProIMAGE",profile_img);
                i.putExtra("UserType",accountType);
                i.putExtra("ISFROM","MyProfile");
                i.putExtra("UserID",userId);
                startActivity(i);
                break;

                case R.id.img_edit:
                Intent i_edit = new Intent(MyProfile.this, EditProfile.class);
                startActivity(i_edit);
                break;

            case R.id.tv_upgrade:
                Intent in = new Intent(MyProfile.this, UpgradeScreen.class);
                startActivity(in);
                break;
            case R.id.imgAdd:
                Intent intent = new Intent(this, ChooseInterest.class);
                intent.putExtra("ISFROM","MYPROFILE");
                startActivityForResult(intent,CHOOSETAG);
               // startActivity(intent);
                break;

            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.llFollower:
                Intent inn = new Intent(this, FollwerActivity.class);
                inn.putExtra("openTag", "follower");
                inn.putExtra("fullName", full_name);
                inn.putExtra("userid", userId);
                startActivity(inn);
                break;

            case R.id.llFollowing:
                Intent innn = new Intent(this, FollwerActivity.class);
                innn.putExtra("openTag", "following");
                innn.putExtra("userid", userId);
                innn.putExtra("fullName", full_name);
                startActivity(innn);
                break;

            case R.id.tv_sponsors:
                Intent sponsors = new Intent(this, SponsorsAct.class);
                startActivity(sponsors);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==CHOOSETAG){

        }
    }
}