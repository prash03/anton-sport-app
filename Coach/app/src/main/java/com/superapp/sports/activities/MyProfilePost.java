
package com.superapp.sports.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.adapters.MyPostAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.CommentPostResponse;
import com.superapp.sports.model.LikePostResponse;
import com.superapp.sports.model.MyPostResponse;
import com.superapp.sports.model.PostStatusResponse;
import com.superapp.sports.model.RemoveFollowerResponse;
import com.superapp.sports.utils.Constants;
import com.superapp.sports.utils.DeliverItToApplication;
import com.superapp.sports.utils.SharedHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.NK_PLAYSTORE_URL;
import static com.superapp.sports.utils.Constants.USERID;
import static com.superapp.sports.utils.DeliverItToApplication.trimMessage;

public class MyProfilePost extends AppCompatActivity implements View.OnClickListener {
    MyPostAdapter myPostAdapter;
    RecyclerView recycl_mypost;
    ImageView imgBack;
    private ProgressDialog progressBar;
    String UpdatedId="",posttype="",userid="",profilePic = "",postid="",location="",longString="",latString="",pageNum="1",authKey="",mPlayerID;
    private List<MyPostResponse.MyPost> dataList;
    private List<MyPostResponse.MyPost.MultiplePostImage> sliderdataList;
    JSONArray mainCatArray;
    RelativeLayout errorLayout,create_post_lyt;
    CircleImageView imgProfilePic;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_post);

        authKey= SharedHelper.getKey(this,AUTH_TOKEN);
        userid= SharedHelper.getKey(this,USERID);
        posttype=getIntent().getStringExtra("post_type");

        recycl_mypost=findViewById(R.id.recycl_mypost);
        errorLayout=findViewById(R.id.errorLayout);
        imgBack=findViewById(R.id.imgBack);
        create_post_lyt=findViewById(R.id.create_post_lyt);
        imgProfilePic=findViewById(R.id.imgProfilePic);

        imgBack.setOnClickListener(this);
        errorLayout.setOnClickListener(this);
        create_post_lyt.setOnClickListener(this);

        recycl_mypost.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        mPlayerID = getIntent().getStringExtra("PlayerID");

        Log.i(TAG, "onCreate mPlayerID:"+mPlayerID);
        Log.i(TAG, "onCreate userid:"+userid);

        profilePic = SharedHelper.getKey(getApplicationContext(), Key_profile_image);
        if (!profilePic.equalsIgnoreCase("")) {
            Picasso.get().load(profilePic).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(imgProfilePic);
        }
        getAddress();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getmyPostApi();
    }

    public void getAddress() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String>  providerList = locationManager.getAllProviders();
        if(null!=locations && null!=providerList && providerList.size()>0){
            double longitude = locations.getLongitude();
            double latitude = locations.getLatitude();
            latString=String.valueOf(latitude);
            longString=String.valueOf(longitude);
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if(null!=listAddresses&&listAddresses.size()>0){
                    String _Location = listAddresses.get(0).getAddressLine(0);
                    String _city = listAddresses.get(0).getLocality();
                    String _state = listAddresses.get(0).getAdminArea();
                    String _country_code = listAddresses.get(0).getCountryName();
                    String _zip = listAddresses.get(0).getPostalCode();
                    location=_city+" "+_state+" "+_country_code+" "+_zip;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void getmyPostApi() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading ........");
        progressDialog.show();
        Map<String, String> params = new HashMap<String, String>();
        params.put("page_number",pageNum);
        params.put("user_id",mPlayerID);
        params.put("post_type",posttype);

        Log.d("parameters:",params.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.RV_POSTLIST_URL, new JSONObject(params),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null && response.length() > 0) {

                            progressDialog.dismiss();
                            if (response.optString("status").equalsIgnoreCase("1")) {
                                System.out.println(response.toString() + "....mypostdata");
                                JSONObject jsonObject = response.optJSONObject("data");

                                mainCatArray = jsonObject.optJSONArray("my_post");

                                if(mainCatArray.length()>0){
                                    errorLayout.setVisibility(View.GONE);
                                    myPostAdapter = new MyPostAdapter(MyProfilePost.this, mainCatArray,jsonObject,location);
                                    myPostAdapter.onItemClick(new MyPostAdapter.OnClickDataCart() {
                                        @Override
                                        public void addNewClass(String postId) {
                                            likepostApi(authKey,postId,userid);
                                        }

                                        @Override
                                        public void addDislike(String postId) {

                                            unlikepostApi(authKey,postId,userid);
                                        }
                                    });
                                    recycl_mypost.setAdapter(myPostAdapter);
                                    myPostAdapter.notifyDataSetChanged();

                                    myPostAdapter.onClickShare(new MyPostAdapter.OnClickShare() {
                                        @Override
                                        public void addNewClass(String username, String profilePic, String postImage) {
                                            String uName=username;
                                            String profilImage=profilePic;
                                            String imageOfPost=postImage;
                                            try {
                                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                                shareIntent.setType("text/plain");
                                                //shareIntent.putExtra(Intent.EXTRA_TEXT, "Super Sports is recognized as the growing startup in India. We are a mobile marketplace for local services. " + "\n" + "https://play.google.com/store/apps/details?id=com.anbshopping" + "\n");
                                                shareIntent.putExtra(Intent.EXTRA_TEXT, uName+","+profilImage+","+imageOfPost);
                                                startActivity(Intent.createChooser(shareIntent, "Share via"));
                                            } catch (Exception e) {
                                                //e.toString();
                                            }
                                        }
                                    });

                                    myPostAdapter.onClick(new MyPostAdapter.OnClickMore() {
                                        @Override
                                        public void addNewClass(String userId, String postId ,String type) {

                                            Log.d("valueOfUserId:=>",userId+","+postId);

                                            if(type.equalsIgnoreCase("Delete")){
                                                DeletePost(authKey, postId);
                                            }else if(type.equalsIgnoreCase("Hide")){
                                                ChangePostStatus(authKey, "hide",postId,userId);
                                            }else if(type.equalsIgnoreCase("Unfollow")){
                                                callRemoveFollower(Integer.parseInt(userId));
                                            }else if(type.equalsIgnoreCase("Report")){
                                                Intent intent = new Intent(getApplicationContext(), ReportActivity.class);
                                                startActivity(intent);
                                            }else if(type.equalsIgnoreCase("Block")){
                                                ChangePostStatus(authKey, "block",postId,UpdatedId);
                                            }else if(type.equalsIgnoreCase("Share")){
                                                try {
                                                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                                    shareIntent.setType("text/plain");
                                                    //shareIntent.putExtra(Intent.EXTRA_TEXT, "Super Sports is recognized as the growing startup in India. We are a mobile marketplace for local services. " + "\n" + "https://play.google.com/store/apps/details?id=com.anbshopping" + "\n");
                                                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Super Sports is recognized as the growing startup . We are a Sport college organization +\n"+ NK_PLAYSTORE_URL+"\n\n");
                                                    startActivity(Intent.createChooser(shareIntent, "Share via"));
                                                } catch (Exception e) {
                                                    //e.toString();
                                                }
                                            }
                                            
                                        }
                                    });
                                }
                                else {
                                    recycl_mypost.setVisibility(View.GONE);
                                    errorLayout.setVisibility(View.VISIBLE);
                                }

                            } else {
                                displayMessage(response.optString("message"));
                                progressDialog.dismiss();
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            displayMessage(getString(R.string.something_went_wrong));
                            progressDialog.dismiss();
                        } else if (response.statusCode == 401) {

                        } else if (response.statusCode == 422) {

                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                displayMessage(json);
                                progressDialog.dismiss();
                            } else {
                                displayMessage(getString(R.string.please_try_again));
                                progressDialog.dismiss();
                            }

                        } else {
                            displayMessage(getString(R.string.please_try_again));
                            progressDialog.dismiss();
                        }

                    } catch (Exception e) {
                        displayMessage(getString(R.string.something_went_wrong));
                        progressDialog.dismiss();
                    }

                } else {
                    progressDialog.dismiss();
                    if (error instanceof NoConnectionError) {
                        displayMessage(getString(R.string.oops_connect_your_internet));
                    } else if (error instanceof NetworkError) {
                        displayMessage(getString(R.string.oops_connect_your_internet));
                    } else if (error instanceof TimeoutError) {

                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", authKey);

                System.out.println(headers + "...headers");
                return headers;
            }
        };

        DeliverItToApplication.getInstance().addToRequestQueue(jsonObjectRequest);
        Log.i(TAG, "my post api url : " + jsonObjectRequest.getUrl());

    }


    private void DeletePost(String authKey, String postid) {

        Api api = RestManager.instanceOf();
        Call<CommentPostResponse> responseCall = api.deletePost(authKey, postid);
        responseCall.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        getmyPostApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(MyProfilePost.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyProfilePost.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void ChangePostStatus(String authKey,String postType, String postid,String userid) {

        Api api = RestManager.instanceOf();
        Call<PostStatusResponse> responseCall = api.statusPost(authKey, postType,postid,userid);
        responseCall.enqueue(new Callback<PostStatusResponse>() {
            @Override
            public void onResponse(Call<PostStatusResponse> call, Response<PostStatusResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        getmyPostApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(MyProfilePost.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyProfilePost.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<PostStatusResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }
    private void likepostApi(String authToken,String postid,String userid) {
        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.likePost(authToken,postid,userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        //Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        getmyPostApi();
                        /*Intent i=new Intent(getActivity(), SecurityScreen.class);
                        startActivity(i);*/

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MyProfilePost.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                //progressBar.dismiss();
            }
        });
    }
    private void unlikepostApi(String authToken,String postid,String userid) {
       /* progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.unlikePost(authToken,postid,userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        //Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        getmyPostApi();
                        /*Intent i=new Intent(getActivity(), SecurityScreen.class);
                        startActivity(i);*/

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(MyProfilePost.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyProfilePost.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                progressBar.dismiss();
            }
        });
    }
    public void displayMessage(String toastString) {
        Snackbar.make(this.findViewById(android.R.id.content), toastString, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }

    private void callRemoveFollower(int userid) {
        progressBar = new ProgressDialog(MyProfilePost.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<RemoveFollowerResponse> responseCall = api.removeFollowerApi(authKey,userid);
        responseCall.enqueue(new Callback<RemoveFollowerResponse>() {
            @Override
            public void onResponse(Call<RemoveFollowerResponse> call, Response<RemoveFollowerResponse> response) {
                progressBar.dismiss();

                if (response != null) {
                    if (response.body().getStatus()==1) {
                        getmyPostApi();
                    } else if (response.body().getStatus()==0) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<RemoveFollowerResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                progressBar.dismiss();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

                case R.id.create_post_lyt:
                    Intent i = new Intent(getApplicationContext(), CreateNewPost.class);
                    startActivity(i);
                break;
        }
    }
}