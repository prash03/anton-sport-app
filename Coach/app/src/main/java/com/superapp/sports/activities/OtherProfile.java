package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.adapters.MyInterestAdapter;
import com.superapp.sports.adapters.ProfileAdapter;
import com.superapp.sports.adapters.ProfileTagAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.MyProfileResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.ACCOUNTTYPE;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.USERID;

public class OtherProfile extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recycleProfile,recycleTag,recycleInterest;
    ProfileAdapter profileAdapter;
    ProfileTagAdapter profileTagAdapter;
    MyInterestAdapter myInterestAdapter;
    LinearLayout ll_self, ll_tag, llFollower, llFollowing;
    ImageView img_self, img_tag, imgBack;
    TextView tv_upgrade,tv_f_name,tvself, tvtag, tv_last_name, tv_following, tv_Message,tv_sponsors,tvpostCount,tvFollowerCount,tvFollowingCount,tv_view_profile;
    View view_self, view_tag;
    String accountType="",authkey="",userId="",savedUserId="",fullName="";
    private List<MyProfileResponse.UserData.SelfPost> dataListPost;
    private List<MyProfileResponse.UserData.TagPost> dataListTag;
    private List<MyProfileResponse.UserData.MyInterest> interestList;
    int playerId;
    ImageView imgProfile;
    String profile_Image="";
    String p_User_type="";
    private ProgressDialog progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_profile);

        tv_upgrade = findViewById(R.id.tv_upgrade);
        tvpostCount = findViewById(R.id.tvpostCount);
        tvFollowerCount = findViewById(R.id.tvFollowerCount);
        tvFollowingCount = findViewById(R.id.tvFollowingCount);
        llFollower = findViewById(R.id.llFollower);
        llFollowing = findViewById(R.id.llFollowing);
        view_tag = findViewById(R.id.view_tag);
        view_self = findViewById(R.id.view_self);
        tv_f_name = findViewById(R.id.tv_f_name);
        tv_last_name = findViewById(R.id.tv_last_name);
        tvtag = findViewById(R.id.tvtag);
        tvself = findViewById(R.id.tvself);
        img_tag = findViewById(R.id.img_tag);
        img_self = findViewById(R.id.img_self);
        ll_self = findViewById(R.id.ll_self);
        tv_Message = findViewById(R.id.tv_Message);
        tv_sponsors = findViewById(R.id.tv_sponsors);
        tv_following = findViewById(R.id.tv_following);
        ll_tag = findViewById(R.id.ll_tag);
        imgBack = findViewById(R.id.imgBack);
        recycleProfile = findViewById(R.id.recycleProfile);
        recycleInterest = findViewById(R.id.recycleInterest);
        recycleTag = findViewById(R.id.recycleTag);
        imgProfile = findViewById(R.id.imgProfile);
        tv_view_profile = findViewById(R.id.tv_view_profile);


        authkey=SharedHelper.getKey(this,AUTH_TOKEN);
        savedUserId=SharedHelper.getKey(this,USERID);
        accountType= SharedHelper.getKey(this,ACCOUNTTYPE);
        userId=getIntent().getStringExtra("userid");

        if(accountType.equals("Coach")){
            tv_sponsors.setVisibility(View.GONE);
        }
        else if(accountType.equals("Player")){
            tv_sponsors.setVisibility(View.GONE);
        }
        else {
            tv_sponsors.setVisibility(View.VISIBLE);
        }

        if (userId.equals(savedUserId)) {
            tv_Message.setVisibility(View.GONE);
            tv_upgrade.setVisibility(View.VISIBLE);
        }
        else if (userId.equals("6")){
            tv_Message.setVisibility(View.INVISIBLE);
        }
        else {
            tv_Message.setVisibility(View.VISIBLE);
            tv_upgrade.setVisibility(View.GONE);
        }

        llFollower.setOnClickListener(this);
        llFollowing.setOnClickListener(this);
        tv_Message.setOnClickListener(this);
        tv_following.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        tv_sponsors.setOnClickListener(this);
        tv_upgrade.setOnClickListener(this);

        recycleProfile.setLayoutManager(new GridLayoutManager(this, 3));
        recycleProfile.setHasFixedSize(true);

        recycleTag.setLayoutManager(new GridLayoutManager(this, 3));
        recycleTag.setHasFixedSize(true);

        recycleInterest.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recycleInterest.setHasFixedSize(true);


        ll_self.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvself.setTextColor(Color.parseColor("#1C1D46"));
                view_self.setBackgroundColor(Color.parseColor("#1C1D46"));
                img_self.setColorFilter(Color.parseColor("#1C1D46"));

                tvtag.setTextColor(Color.parseColor("#818181"));
                view_tag.setBackgroundColor(Color.parseColor("#908D8D"));
                img_tag.setColorFilter(Color.parseColor("#908D8D"));

                recycleProfile.setVisibility(View.VISIBLE);
                recycleTag.setVisibility(View.GONE);

            }
        });
        ll_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvself.setTextColor(Color.parseColor("#818181"));
                view_self.setBackgroundColor(Color.parseColor("#908D8D"));
                img_self.setColorFilter(Color.parseColor("#908D8D"));

                tvtag.setTextColor(Color.parseColor("#1C1D46"));
                view_tag.setBackgroundColor(Color.parseColor("#1C1D46"));
                img_tag.setColorFilter(Color.parseColor("#1C1D46"));

                recycleProfile.setVisibility(View.GONE);
                recycleTag.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.tv_Message:
                Intent in = new Intent(OtherProfile.this, ChatInbox.class);
                in.putExtra("uid",userId);
                in.putExtra("uname",fullName);
                startActivity(in);
                break;

            case R.id.tv_view_profile:

                Intent i = new Intent(OtherProfile.this, ViewMyProfileDetail.class);
                i.putExtra("ProIMAGE",profile_Image);
                i.putExtra("UserType",accountType);
                i.putExtra("ISFROM","OtherProfile");
                i.putExtra("UserID",userId);
                startActivity(i);
                break;

            case R.id.tv_sponsors:
                Intent ins = new Intent(OtherProfile.this, RequestSponser.class);
                ins.putExtra("playerId",String.valueOf(playerId));
                startActivity(ins);
                break;


            case R.id.imgBack:
                finish();
                break;

            case R.id.llFollower:
                Intent inn = new Intent(this, FollwerActivity.class);
                inn.putExtra("openTag", "follower");
                inn.putExtra("userid", userId);
                inn.putExtra("fullName", fullName);
                startActivity(inn);
                break;

            case R.id.llFollowing:
                Intent innn = new Intent(this, FollwerActivity.class);
                innn.putExtra("openTag", "following");
                innn.putExtra("userid", userId);
                innn.putExtra("fullName", fullName);
                startActivity(innn);
                break;

            case R.id.tv_upgrade:
                Intent ip = new Intent(OtherProfile.this, UpgradeScreen.class);
                startActivity(ip);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        callprofileApi(authkey, userId);
    }

    private void callprofileApi(String authToken, String userid) {
        progressBar = new ProgressDialog(OtherProfile.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<MyProfileResponse> responseCall = api.otherProfile(authToken, Integer.parseInt(userid));
        responseCall.enqueue(new Callback<MyProfileResponse>() {
            @Override
            public void onResponse(Call<MyProfileResponse> call, Response<MyProfileResponse> response) {

                if (response != null) {
                    if (response.body().getStatus()==1) {
                        progressBar.dismiss();
                        dataListTag = response.body().getUserData().getTagPost();
                        dataListPost = response.body().getUserData().getSelfPost();
                        interestList = response.body().getUserData().getMyInterest();
                        String fname=response.body().getUserData().getFirstName();
                        String lname=response.body().getUserData().getLastName();
                        playerId=response.body().getUserId();

                        tvpostCount.setText(String.valueOf(response.body().getUserData().getPostsCount()));
                        tvFollowerCount.setText(String.valueOf(response.body().getUserData().getFollowersCount()));
                        tvFollowingCount.setText(String.valueOf(response.body().getUserData().getFollowingCount()));
                        tv_f_name.setText(fname);
                        tv_last_name.setText(lname);

                        fullName=(fname+" "+lname);
                        profile_Image = String.valueOf(response.body().getUserData().getUserImage());
                        if (!profile_Image.equalsIgnoreCase("")) {
                            Picasso.get().load(profile_Image).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(imgProfile);
                        }
                        p_User_type = response.body().getUserData().getUser_type();
                        profileAdapter = new ProfileAdapter(OtherProfile.this, dataListPost,playerId);
                        recycleProfile.setAdapter(profileAdapter);
                        profileTagAdapter = new ProfileTagAdapter(OtherProfile.this, dataListTag,response.body().getUserId());
                        recycleTag.setAdapter(profileTagAdapter);

                        myInterestAdapter = new MyInterestAdapter(OtherProfile.this, interestList);
                        recycleInterest.setAdapter(myInterestAdapter);


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(OtherProfile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();
                    } else {
                        Toast.makeText(OtherProfile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();

                    }

                }
            }

            @Override
            public void onFailure(Call<MyProfileResponse> call, Throwable t) {
                Log.d("errorRespons:",t.getMessage());
                progressBar.dismiss();
            }
        });
    }

}