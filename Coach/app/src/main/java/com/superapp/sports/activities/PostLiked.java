package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.PostLikedAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.PostYouLikeResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class PostLiked extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recyclePost;
    PostLikedAdapter postLikedAdapter;
    ImageView imgBack;
    String authKey="",posttype="";
    private List<PostYouLikeResponse.Datum> datumList;
    TextView tv_heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_liked);

        authKey=SharedHelper.getKey(PostLiked.this,AUTH_TOKEN);
        posttype=getIntent().getStringExtra("posttype");

        tv_heading=findViewById(R.id.tv_heading);
        imgBack=findViewById(R.id.imgBack);
        recyclePost=findViewById(R.id.recyclePost);
        recyclePost.setLayoutManager(new GridLayoutManager(this,3));
        recyclePost.setHasFixedSize(true);

        if(posttype.equals("liked")){
            tv_heading.setText("Likes");
        }
        else if (posttype.equals("hide")){
            tv_heading.setText("Hidden Post");
        }
        else {
            tv_heading.setText("BLocked Post");
        }

        imgBack.setOnClickListener(this);

        callLikedPostApi(authKey,posttype);
    }

    private void callLikedPostApi(String authToken,String posttype) {
        ProgressDialog progressDialog = new ProgressDialog(PostLiked.this);
        progressDialog.setTitle("Loading ........");
        progressDialog.show();
        Api api = RestManager.instanceOf();
        Call<PostYouLikeResponse> responseCall = api.postYouLikeApi(authToken,posttype);
        responseCall.enqueue(new Callback<PostYouLikeResponse>() {
            @Override
            public void onResponse(Call<PostYouLikeResponse> call, Response<PostYouLikeResponse> response) {
                progressDialog.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        //Toast.makeText(PostLiked.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        datumList=response.body().getData();
                        postLikedAdapter=new PostLikedAdapter(PostLiked.this,datumList);
                        recyclePost.setAdapter(postLikedAdapter);

                    } else if (response.body().getStatus()==0) {
                        Toast.makeText(PostLiked.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PostLiked.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<PostYouLikeResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                progressDialog.dismiss();
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }
}