package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.PrivacyResponse;
import com.superapp.sports.utils.SharedHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class PrivacyPolicy extends AppCompatActivity {
    ImageView imgBack;
    private ProgressDialog progressBar;
    TextView tvDescription;
    String descript="",authkey="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        imgBack=findViewById(R.id.imgBack);
        tvDescription=findViewById(R.id.tvDescription);

        authkey= SharedHelper.getKey(this,AUTH_TOKEN);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        callprivacyApi(authkey);
    }

    private void callprivacyApi(String authToken) {
        progressBar = new ProgressDialog(this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<PrivacyResponse> responseCall = api.privacypolicyApi(authToken);
        responseCall.enqueue(new Callback<PrivacyResponse>() {
            @Override
            public void onResponse(Call<PrivacyResponse> call, Response<PrivacyResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        descript=response.body().getDescription();
                        tvDescription.setText(Html.fromHtml(descript));

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(PrivacyPolicy.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PrivacyPolicy.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<PrivacyResponse> call, Throwable t) {
                progressBar.dismiss();
                Log.d("errorResponse:",t.getMessage());
            }
        });
    }
}