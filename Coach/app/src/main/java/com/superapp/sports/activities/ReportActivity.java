package com.superapp.sports.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.adapters.HelpAdapter;
import com.superapp.sports.adapters.ReportAdapter;
import com.superapp.sports.adapters.ReportSubCatAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.HelpResponse;
import com.superapp.sports.model.ReportResponse;
import com.superapp.sports.model.ReportSubCatResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.activities.ChatInbox.TAG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class ReportActivity extends AppCompatActivity {
    RecyclerView recycl_help , recycl_sub_cat_list;
    ImageView imgBack,imgCross;
    ReportAdapter reportAdapter;
    ReportSubCatAdapter reportSubCatAdapter;
    String authkey="";
    LinearLayout main_lly,ll_sub_cat;
    TextView tv_heading,tv_head_title , tv_head_sub_title;
    private ProgressDialog progressBar;
    private List<ReportSubCatResponse.SubCatData> sub_dataList;
    private List<ReportResponse.DataReview.Report> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        recycl_help=findViewById(R.id.recycl_help);
        tv_heading=findViewById(R.id.tv_heading);
        tv_head_title=findViewById(R.id.tv_head_title);
        imgBack=findViewById(R.id.imgBack);
        tv_head_sub_title=findViewById(R.id.tv_head_sub_title);
        imgCross=findViewById(R.id.imgCross);
        main_lly=findViewById(R.id.main_lly);
        ll_sub_cat=findViewById(R.id.ll_sub_cat);
        recycl_sub_cat_list=findViewById(R.id.recycl_sub_cat_list);
        authkey= SharedHelper.getKey(this,AUTH_TOKEN);

        recycl_help.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        recycl_sub_cat_list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycl_sub_cat_list.setHasFixedSize(true);

        tv_heading.setText("Report");
        tv_head_title.setVisibility(View.VISIBLE);


        tv_head_title.setText(Html.fromHtml(getResources().getString(R.string.report_note)+"<br />"+"\n\nIf someone is in immediate danger, get help before reporting to Super Sports Elite. Don't wait."));

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imgCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        callhelpApi(authkey);


    }

    private void callhelpApi(String authToken) {
        progressBar = new ProgressDialog(ReportActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<ReportResponse> responseCall = api.getReportCatApi(authToken);
        responseCall.enqueue(new Callback<ReportResponse>() {
            @Override
            public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData().getReportList();

                        reportAdapter = new ReportAdapter(ReportActivity.this, dataList);
                        recycl_help.setAdapter(reportAdapter);
                        reportAdapter.onItemClick(new ReportAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int id , String ttl_name) {

                                Log.i(TAG, "addNewClass id: " + id);
                                main_lly.setVisibility(View.GONE);
                                ll_sub_cat.setVisibility(View.VISIBLE);
                                callSubCatApi(id,ttl_name);

                            }
                        });


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(ReportActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ReportActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<ReportResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    private void callSubCatApi(int id , String ttl_Name) {

        progressBar = new ProgressDialog(ReportActivity.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<ReportSubCatResponse> responseCall = api.getsubCatReport(authkey,id);
        responseCall.enqueue(new Callback<ReportSubCatResponse>() {
            @Override
            public void onResponse(Call<ReportSubCatResponse> call, Response<ReportSubCatResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {

                        main_lly.setVisibility(View.GONE);
                        ll_sub_cat.setVisibility(View.VISIBLE);
                        sub_dataList = response.body().getData();
                        reportSubCatAdapter = new ReportSubCatAdapter(ReportActivity.this, sub_dataList);
                        recycl_sub_cat_list.setAdapter(reportSubCatAdapter);
                        tv_head_sub_title.setText(Html.fromHtml(getResources().getString(R.string.report_sub_cat_note)+ttl_Name+"?"+"<br />"+"Please select the most relevant option."));


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(ReportActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ReportActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<ReportSubCatResponse> call, Throwable t) {
                progressBar.dismiss();
                Log.i(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}