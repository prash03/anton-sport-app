package com.superapp.sports.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.superapp.sports.R;
import com.superapp.sports.appController.network.PaypalClientIDConfigClass;

import org.json.JSONException;

import java.math.BigDecimal;

public class RequestPay extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBack;
    TextView tvNote,tvReqest,tvPay;
    LinearLayout llrequest,llapproved;
    private final int PAYPAL_REQ_CODE=12;
    private static final PayPalConfiguration payPalConfiguration=new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PaypalClientIDConfigClass.PaypalClientId);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_pay);

        llrequest=findViewById(R.id.llrequest);
        llapproved=findViewById(R.id.llapproved);
        imgBack=findViewById(R.id.imgBack);
        tvReqest=findViewById(R.id.tvReqest);
        tvPay=findViewById(R.id.tvPay);
        tvNote=findViewById(R.id.tvNote);

        imgBack.setOnClickListener(this);
        tvReqest.setOnClickListener(this);
        tvPay.setOnClickListener(this);
        llrequest.setOnClickListener(this);
        llapproved.setOnClickListener(this);

        tvNote.setText(Html.fromHtml(getResources().getString(R.string.dummynote)));

        Intent in=new Intent(this,PayPalService.class);
        in.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,payPalConfiguration);
        startService(in);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.tvReqest:
                llrequest.setVisibility(View.GONE);
                llapproved.setVisibility(View.VISIBLE);
            break;

            case R.id.tvPay:
                /*Intent i=new Intent(this,PaymentScreen.class);
                startActivity(i);*/
                PaypalPaymenMethod();
                break;
        }
    }

    private void PaypalPaymenMethod() {
        PayPalPayment palPayment=new PayPalPayment(new BigDecimal(100),"EUR","Test Payment",PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent=new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,payPalConfiguration);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT,palPayment);
        startActivityForResult(intent,PAYPAL_REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==PAYPAL_REQ_CODE){
            if (requestCode== Activity.RESULT_OK){
                Toast.makeText(RequestPay.this,"Payment Successfull",Toast.LENGTH_LONG).show();
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                Intent i=new Intent(RequestPay.this,PaymentScreen.class);
                startActivity(i);
                /*if (confirm != null) {
                    try {
                        Log.i("paymentExample", confirm.toJSONObject().toString(4));

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }*/
            }
            else {
                Toast.makeText(RequestPay.this,"Payment is UnSuccessfull",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this,PayPalService.class));
        super.onDestroy();
    }
}