package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.RequestSponserResponse;
import com.superapp.sports.utils.SharedHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class RequestSponser extends AppCompatActivity implements View.OnClickListener{
    ImageView imgBack;
    Button btSubmit;
    EditText ed_description,ed_amount,ed_subject;
    String authKey="",playerId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_sponser);

        authKey= SharedHelper.getKey(RequestSponser.this,AUTH_TOKEN);
        playerId=getIntent().getStringExtra("playerId");

        ed_description=findViewById(R.id.ed_description);
        ed_amount=findViewById(R.id.ed_amount);
        ed_subject=findViewById(R.id.ed_subject);
        imgBack=findViewById(R.id.imgBack);
        btSubmit=findViewById(R.id.btSubmit);
        imgBack.setOnClickListener(this);
        btSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.btSubmit:

                if(ed_subject.getText().toString().equals("")){
                    Toast.makeText(RequestSponser.this,"Please Enter Subject",Toast.LENGTH_LONG);
                }
                else if(ed_amount.getText().toString().equals("")){
                    Toast.makeText(RequestSponser.this,"Please Enter Amount",Toast.LENGTH_LONG);
                }else if(ed_description.getText().toString().equals("")){
                    Toast.makeText(RequestSponser.this,"Please Enter Description",Toast.LENGTH_LONG);
                }
                else {
                    int amount= Integer.parseInt(ed_amount.getText().toString());
                    requestSponserApi(authKey, Integer.parseInt(playerId),amount,ed_subject.getText().toString(),ed_description.getText().toString());
                }
                break;
        }
    }
    private void requestSponserApi(String authKey,int playerid,int amount,String title,String description) {

        Api api = RestManager.instanceOf();
        Call<RequestSponserResponse> responseCall = api.sponserRequest(authKey,playerid, amount,title,description);
        responseCall.enqueue(new Callback<RequestSponserResponse>() {
            @Override
            public void onResponse(Call<RequestSponserResponse> call, Response<RequestSponserResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(RequestSponser.this,SponsorsAct.class);
                        startActivity(i);
                    } else if (response.body().getStatus()==0) {
                        Toast.makeText(RequestSponser.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RequestSponser.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<RequestSponserResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                //progressBar.dismiss();
            }
        });
    }
}