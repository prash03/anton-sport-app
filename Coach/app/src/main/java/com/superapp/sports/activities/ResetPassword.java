package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.ResetPasswordResponse;
import com.superapp.sports.utils.InternetConnectionCheck;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.CommonUtils.showToast;

public class ResetPassword extends AppCompatActivity implements View.OnClickListener{
    Button btReset;
    private ProgressDialog progressBar;
    EditText ed_otp,ed_newpass,ed_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        btReset=findViewById(R.id.btReset);
        ed_otp=findViewById(R.id.ed_otp);
        ed_newpass=findViewById(R.id.ed_newpass);
        ed_confirm=findViewById(R.id.ed_confirm);

        btReset.setOnClickListener(this);
    }

    private void callResetApi(String otp,String newPassword,String confirmPass) {
        progressBar = new ProgressDialog(ResetPassword.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<ResetPasswordResponse> responseCall = api.resetPasswordApi(otp,newPassword,confirmPass);
        responseCall.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                progressBar.dismiss();
                if (response != null) {
                    if (response.body().getStatus()==1) {

                        Toast.makeText(ResetPassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ResetPassword.this, LoginActivity.class);
                        startActivity(i);
                    }
                    else if (response.body().getStatus()==400) {
                        Toast.makeText(ResetPassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ResetPassword.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btReset:
                String code = ed_otp.getText().toString().trim();
                String txtpass = ed_newpass.getText().toString().trim();
                String txtconfirm = ed_confirm.getText().toString().trim();
                if (code.equals("")) {
                    showToast(this,getString(R.string.otp_validation));
                }
                else if(txtpass.equals("")){
                    showToast(this,getString(R.string.password_validation));
                }else if(txtconfirm.equals("")){
                    showToast(this,getString(R.string.confirmpass));
                }
                else if (!txtpass.equals(txtconfirm)) {
                    showToast(this,getString(R.string.password_match));
                }
                else {
                    if (InternetConnectionCheck.haveNetworkConnection(ResetPassword.this)) {
                        callResetApi(ed_otp.getText().toString(),ed_newpass.getText().toString(),ed_confirm.getText().toString());
                    } else {
                        showToast(this,getString(R.string.something_went_wrong_net));
                    }
                }
                break;
        }
    }
}