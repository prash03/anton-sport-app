package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.facebook.FacebookSdk;
import com.superapp.sports.R;
import com.superapp.sports.utils.SharedHelper;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class SavedInfo extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack,imgSave;
    boolean saveClick=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_info);
        imgBack=findViewById(R.id.imgBack);
        imgSave=findViewById(R.id.imgSave);
        imgBack.setOnClickListener(this::onClick);
        imgSave.setOnClickListener(this::onClick);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.imgSave:
                if(saveClick==true){
                    saveClick=false;
                    imgSave.setImageResource(R.drawable.ic_notification_button);
                    new androidx.appcompat.app.AlertDialog.Builder(SavedInfo.this)
                            .setTitle("Exit")
                            .setMessage("Are you sure you want to logout and delete login data?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int which) {
                                    logout();
                                /*Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);*/
                                }
                            })
                            //.setNegativeButton("No", null)
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    saveClick=true;
                                    imgSave.setImageResource(R.drawable.ic_notif_on);
                                }
                            })
                            .show();
                }
                else {
                    saveClick=true;
                    imgSave.setImageResource(R.drawable.ic_notif_on);
                }
                break;
        }
    }
    public void logout() {

        FacebookSdk.setAdvertiserIDCollectionEnabled(false);
        SharedHelper.putKey(SavedInfo.this, AUTH_TOKEN, "");
        SharedPreferences preferences = SavedInfo.this.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        Intent goToLogin = new Intent(SavedInfo.this, LoginActivity.class);
        goToLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(goToLogin);
        finish();
    }
}