package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.superapp.sports.R;
import com.superapp.sports.adapters.HistoryAdapter;
import com.superapp.sports.utils.SharedHelper;

import org.json.JSONArray;
import org.json.JSONException;

public class SearchHistory extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = SearchHistory.class.getName();
    ImageView imgBack;
    RecyclerView recycleHistory;
    HistoryAdapter historyAdapter;
    String user_search;
    TextView tvClear;
    JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_history);

        tvClear=findViewById(R.id.tvClear);
        imgBack=findViewById(R.id.imgBack);
        recycleHistory=findViewById(R.id.recycleHistory);

        imgBack.setOnClickListener(this::onClick);
        tvClear.setOnClickListener(this::onClick);

        user_search = SharedHelper.getKey(this, "search_Data");
        recycleHistory.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        try {
            jsonArray = new JSONArray(user_search);
            Log.i(TAG, "user_data: " + jsonArray.toString());
           // String pr_image = jsonArray.getJSONObject(i).getString("user_image");

            for (int i=0;i<jsonArray.length();i++){
               /* FIndFriendResponse[] myListData = new FIndFriendResponse[] {
                        new FIndFriendResponse(jsonArray.getJSONObject(i).getString("user_name"), "Followed by "+jsonArray.getJSONArray(i).getJSONObject(1).getString("user_name"),jsonArray.getJSONObject(i).getString("user_image"))

                };*/

            }
            historyAdapter=new HistoryAdapter(SearchHistory.this,jsonArray);

            recycleHistory.setAdapter(historyAdapter);

            historyAdapter.onItemClick(new HistoryAdapter.OnClickDataCart() {
                @Override
                public void addNewClass(int pos) {
                    int position=pos;
                    historyAdapter.removeItem(position);
                }
            });

        } catch (JSONException e) {


        }

    }

    public void deletedData(){
        recycleHistory.setVisibility(View.GONE);
        String arrayString;
        SharedHelper.putKey(SearchHistory.this,"search_Data","");
        arrayString=SharedHelper.getKey(SearchHistory.this,"search_Data");
        try {
            JSONArray jArray=new JSONArray(arrayString);
            historyAdapter=new HistoryAdapter(SearchHistory.this,jArray);
            recycleHistory.setAdapter(historyAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.tvClear:
                //recycleHistory.notifyAll();
                deletedData();

                break;
        }
    }
}