package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.superapp.sports.R;
import com.superapp.sports.utils.SharedHelper;

import static com.superapp.sports.utils.Constants.LOGINTYPE;

public class SecurityScreen extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack;
    RelativeLayout rlpassword,rlLogact,rlSaved,rlTfa,rlhistory;
    String loginType="";
    View viewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_screen);

        viewPassword=findViewById(R.id.viewPassword);
        imgBack=findViewById(R.id.imgBack);
        rlpassword=findViewById(R.id.rlpassword);
        rlSaved=findViewById(R.id.rlSaved);
        rlLogact=findViewById(R.id.rlLogact);
        rlTfa=findViewById(R.id.rlTfa);
        rlhistory=findViewById(R.id.rlhistory);
        imgBack.setOnClickListener(this);
        rlpassword.setOnClickListener(this);
        rlLogact.setOnClickListener(this);
        rlSaved.setOnClickListener(this);
        rlTfa.setOnClickListener(this);
        rlhistory.setOnClickListener(this);

        loginType= SharedHelper.getKey(this,LOGINTYPE);

        if(loginType.equals("manual")){
            viewPassword.setVisibility(View.VISIBLE);
            rlpassword.setVisibility(View.VISIBLE);
        }
        else {
            viewPassword.setVisibility(View.GONE);
            rlpassword.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.rlpassword:
                Intent i=new Intent(this,ChangePassword.class);
                startActivity(i);
                break;

            case R.id.rlLogact:
                Intent in=new Intent(this,LoggedinActivity.class);
                startActivity(in);
                break;

            case R.id.rlSaved:
                Intent inn=new Intent(this,SavedInfo.class);
                startActivity(inn);
                break;

            case R.id.rlTfa:
                Intent in_tfa=new Intent(this,TwoFactorScreen.class);
                startActivity(in_tfa);
                break;

            case R.id.rlhistory:
                Intent in_hs=new Intent(this,SearchHistory.class);
                startActivity(in_hs);
                break;
        }
    }
}