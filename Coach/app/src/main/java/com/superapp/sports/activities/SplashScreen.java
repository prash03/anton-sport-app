package com.superapp.sports.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.utils.SharedHelper;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class SplashScreen extends AppCompatActivity {

    private static final int REQUEST_LOCATION = 1;
    String latString="",longString="",authKey="";
    LocationManager locationManager;
    boolean locationPermission=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        authKey=SharedHelper.getKey(SplashScreen.this, AUTH_TOKEN);

        Log.d("authKey",authKey);

        launchScreen();

    }

    public void launchScreen(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedHelper.getKey(SplashScreen.this, AUTH_TOKEN).equals("")) {

                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                } else {

                    Intent i = new Intent(SplashScreen.this, Dashboard.class);
                    startActivity(i);
                    //SplashScreen.this.finish();
                }
            }
        }, 3000);
    }





}