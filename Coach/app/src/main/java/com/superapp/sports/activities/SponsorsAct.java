package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.MySponsorsAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.MySponserResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class SponsorsAct extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rvsponsorrecyer;
    MySponsorsAdapter mySponsorsAdapter;
    ImageView backbtn;
    private List<MySponserResponse.Datum> dataList;
    String authkey="";
    /*ArrayList<DummyModal> dummyModals;

    private static Integer[] commentArr = {1, 2, 3};*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);

        authkey= SharedHelper.getKey(SponsorsAct.this,AUTH_TOKEN);

        backbtn = findViewById(R.id.imgBack);
        rvsponsorrecyer = findViewById(R.id.rv_sponsor_);
        rvsponsorrecyer.setLayoutManager(new LinearLayoutManager(
                this, RecyclerView.VERTICAL, false));

        backbtn.setOnClickListener(this);


    }


    @Override
    protected void onResume() {
        super.onResume();
        callmyplayerApi(authkey);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack: {
                finish();
            }
            break;
        }
    }
    private void callmyplayerApi(String authToken) {
        /*progressBar = new ProgressDialog(SponsorsAct.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<MySponserResponse> responseCall = api.mysponserPlayerApi(authToken);
        responseCall.enqueue(new Callback<MySponserResponse>() {
            @Override
            public void onResponse(Call<MySponserResponse> call, Response<MySponserResponse> response) {
               // progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                       // dataList = response.body().getData();
                        dataList=response.body().getData();
                        mySponsorsAdapter = new MySponsorsAdapter(SponsorsAct.this, dataList);
                        rvsponsorrecyer.setAdapter(mySponsorsAdapter);


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(SponsorsAct.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SponsorsAct.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<MySponserResponse> call, Throwable t) {
               // progressBar.dismiss();
                Log.d("errorMsg:",t.getMessage());
            }
        });
    }

}