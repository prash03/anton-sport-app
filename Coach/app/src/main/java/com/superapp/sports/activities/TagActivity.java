package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.superapp.sports.R;
import com.superapp.sports.adapters.TagAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.TagSuggestionResponse;
import com.superapp.sports.utils.SharedHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class TagActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recycleTag;
    TagAdapter tagAdapter;
    ImageView imgBack,imgnext;
    String searchKey="",pageNum="",authKey="";
    private List<TagSuggestionResponse.TagSuggestion> dataList;
    ArrayList<Integer> selectedItemid = new ArrayList<>();
    ArrayList<String> selectedName = new ArrayList<>();
    ArrayList<String> selectedImage = new ArrayList<>();
    JSONObject jsObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag);

        recycleTag=findViewById(R.id.recycleTag);
        imgBack=findViewById(R.id.imgBack);
        imgnext=findViewById(R.id.imgnext);

        authKey= SharedHelper.getKey(this,AUTH_TOKEN);

        imgBack.setOnClickListener(this);
        imgnext.setOnClickListener(this);

        recycleTag.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));


        calltagListApi();
    }

    private void calltagListApi() {
        /*progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<TagSuggestionResponse> responseCall = api.tagSuggestion(authKey,pageNum,searchKey);
        responseCall.enqueue(new Callback<TagSuggestionResponse>() {
            @Override
            public void onResponse(Call<TagSuggestionResponse> call, Response<TagSuggestionResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getTagSuggestion();

                        //Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        tagAdapter = new TagAdapter(TagActivity.this, dataList);
                        recycleTag.setAdapter(tagAdapter);

                        tagAdapter.onItemClick(new TagAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int numSelected, ArrayList<Integer> playIdSelected, ArrayList<String> selectname,ArrayList<String> selectedImg) {
                                selectedItemid=playIdSelected;
                                selectedName=selectname;
                                selectedImage=selectedImg;
                                Log.d("selectedItemid:",selectedItemid.toString());
                                Log.d("selectedList:",selectedName.toString());

                            }
                        });
                        /*tagAdapter.onItemClick(new TagAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int numSelected, ArrayList<Integer> playIdSelected) {
                                selectedItemid=playIdSelected;
                                Log.d("selectedItemid:",selectedItemid.toString());
                            }
                        });*/

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(TagActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(TagActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<TagSuggestionResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                onBackPressed();
                break;

            case R.id.imgnext:
                Intent returIntent = new Intent();
                returIntent.putExtra("selectedId",selectedItemid);
                returIntent.putExtra("selectedname", selectedName);
                returIntent.putExtra("selectedimage", selectedImage);
                setResult(Activity.RESULT_OK, returIntent);
                finish();
                break;
        }
    }
}