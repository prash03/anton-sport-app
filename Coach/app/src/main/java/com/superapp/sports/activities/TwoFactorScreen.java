package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.superapp.sports.R;

public class TwoFactorScreen extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack,imgAuth,imgText;
    boolean authClick=false;
    boolean textClick=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_factor_screen);
        imgBack=findViewById(R.id.imgBack);
        imgAuth=findViewById(R.id.imgAuth);
        imgText=findViewById(R.id.imgText);
        imgBack.setOnClickListener(this);
        imgText.setOnClickListener(this);
        imgAuth.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.imgAuth:
               if(authClick==false){
                   authClick=true;
                   imgAuth.setImageResource(R.drawable.ic_notif_on);
               }
               else {
                   authClick=false;
                   imgAuth.setImageResource(R.drawable.ic_notification_button);
               }

                break;

            case R.id.imgText:
                if(textClick==false){
                    textClick=true;
                    imgText.setImageResource(R.drawable.ic_notif_on);
                }
                else {
                    textClick=false;
                    imgText.setImageResource(R.drawable.ic_notification_button);
                }
                break;
        }
    }
}