package com.superapp.sports.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.superapp.sports.R;
import com.superapp.sports.adapters.UpgradeAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.UpgradeResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class UpgradeScreen extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack;
    String authKey="";
    private List<UpgradeResponse.Datum> dataList;
    UpgradeAdapter upgradeAdapter;
    RecyclerView recycleUpgrade;
    RelativeLayout errorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_screen);

        authKey= SharedHelper.getKey(UpgradeScreen.this,AUTH_TOKEN);

        imgBack=findViewById(R.id.imgBack);
        recycleUpgrade=findViewById(R.id.recycleUpgrade);
        errorLayout=findViewById(R.id.errorLayout);
        recycleUpgrade.setLayoutManager(new LinearLayoutManager(UpgradeScreen.this, RecyclerView.VERTICAL, false));

        imgBack.setOnClickListener(this);

        callUpgradeApi();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    private void callUpgradeApi() {

        Api api = RestManager.instanceOf();
        Call<UpgradeResponse> responseCall = api.upgradeListApi(authKey);
        responseCall.enqueue(new Callback<UpgradeResponse>() {
            @Override
            public void onResponse(Call<UpgradeResponse> call, Response<UpgradeResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData();




                        if(dataList.size()>0){
                            upgradeAdapter = new UpgradeAdapter(UpgradeScreen.this, dataList);
                            recycleUpgrade.setAdapter(upgradeAdapter);

                            errorLayout.setVisibility(View.GONE);
                        }
                        else {
                            recycleUpgrade.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(UpgradeScreen.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(UpgradeScreen.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<UpgradeResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }
}