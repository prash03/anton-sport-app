package com.superapp.sports.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.superapp.sports.R;
import com.superapp.sports.utils.SharedHelper;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;

import static com.superapp.sports.activities.ChatInbox.TAG;
import static com.superapp.sports.utils.Constants.USERID;

public class VideoCallAct extends AppCompatActivity implements View.OnClickListener {
    ImageView backimg, endbtn;
    private RtcEngine mRtcEngine;
    private static final int PERMISSION_REQ_ID = 22;
    private VideoCanvas mLocalVideo;
    private VideoCanvas mRemoteVideo;
    String rocaluserName = "", rUserID = "", rUserImg = "", luserID = "";


    // Handle SDK Events
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {
        @Override
        public void onUserJoined(final int uid, int elapsed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("onUserJoined" +"..................uID"+uid);
                    System.out.println("onUserJoined" +"..................uID"+rUserID);
                    if (rUserID.equals(String.valueOf(uid))){
                        setupRemoteVideo(uid);
                    }
                }
            });
        }

        @Override
        public void onJoinChannelSuccess(String channel, final int uid, int elapsed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("onJoinChannelSuccess" +"..................uID"+uid);

                    // mLogView.logI("Join channel success, uid: " + (uid & 0xFFFFFFFFL));
                    Log.i(TAG, "run: " + "Join channel success, uid: " + (uid & 0xFFFFFFFFL));
                }
            });
        }

        @Override
        public void onUserOffline(final int uid, int reason) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("onUserOffline" +"..................uID"+uid);

                    //mLogView.logI("User offline, uid: " + (uid & 0xFFFFFFFFL));
                    Log.i(TAG, "run: " + "User offline, uid: " + (uid & 0xFFFFFFFFL));
                    onRemoteUserLeft(uid);
                }
            });
        }

        // remote user has toggled their video
        @Override
        public void onRemoteVideoStateChanged(final int uid, final int state, int reason, int elapsed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    System.out.println("onRemoteVideoStateChanged" +"..................state"+state);
                    System.out.println("reason" +"..................reason"+reason);

                    //onRemoteUserVideoToggle(uid, state);
                }
            });
        }
    };

    private static final String[] REQUESTED_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA
    };

    private boolean checkSelfPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode);
            return false;
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_call);

        backimg = findViewById(R.id.imgBack);
        endbtn = findViewById(R.id.end_bbtn);

        Intent extras = getIntent();
        if (extras != null) {
            rocaluserName = extras.getStringExtra("usern");
            rUserID = extras.getStringExtra("userID");
            rUserImg = extras.getStringExtra("uImg");
        }
        luserID = SharedHelper.getKey(getApplicationContext(), USERID);

        System.out.println(rUserID +"........rUserID");
        System.out.println(luserID +"........luserID");

        // If all the permissions are granted, initialize the RtcEngine object and join a channel.
        if (checkSelfPermission(REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID)) {
            initializeAndJoinChannel();
        }

        backimg.setOnClickListener(this);
        endbtn.setOnClickListener(this);
    }


    // Java
    private void initializeAndJoinChannel() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
        } catch (Exception e) {
            throw new RuntimeException("Check the error.");
        }

        setupSession();

        // By default, video is disabled, and you need to call enableVideo to start a video stream.
        mRtcEngine.enableAudio();

        FrameLayout container = findViewById(R.id.local_video_view_container);
        // Call CreateRendererView to create a SurfaceView object and add it as a child to the FrameLayout.
        SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
        container.addView(surfaceView);
        // Pass the SurfaceView object to Agora so that it renders the local video.
        mRtcEngine.setupLocalVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_FIT, Integer.parseInt(rUserID)));

        // Join the channel with a token.
        //mRtcEngine.joinChannelWithUserAccount()
        mRtcEngine.joinChannel(getString(R.string.agora_access_token), getString(R.string.chanel_name), "", Integer.parseInt(luserID));
    }


    private void setupSession() {
        mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
        mRtcEngine.enableVideo();
        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(VideoEncoderConfiguration.VD_1280x720, VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_30,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));
    }

    // Java
    private void setupRemoteVideo(int uid) {
        FrameLayout container = findViewById(R.id.remote_video_view_container);
        SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
        surfaceView.setZOrderMediaOverlay(true);
        container.addView(surfaceView);
        mRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_FIT, uid));
    }

    // Java
    protected void onDestroy() {
        super.onDestroy();
        mRtcEngine.leaveChannel();
        RtcEngine.destroy();
    }


    private void onRemoteUserLeft(int uid) {
        if (mRemoteVideo != null && mRemoteVideo.uid == uid) {
            removeFromParent(mRemoteVideo);
            // Destroys remote view
            mRemoteVideo = null;
        }
    }

    private ViewGroup removeFromParent(VideoCanvas canvas) {
        if (canvas != null) {
            ViewParent parent = canvas.view.getParent();
            if (parent != null) {
                ViewGroup group = (ViewGroup) parent;
                group.removeView(canvas.view);
                return group;
            }
        }
        return null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack: {
                finish();
            }
            break;

            case R.id.end_bbtn: {
                mRtcEngine.disableVideo();
                mRtcEngine.disableAudio();
                RtcEngine.destroy();
                mRtcEngine.leaveChannel();
                finish();
            }
            break;
        }
    }


    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.i("LOG_TAG", "onRequestPermissionsResult " + grantResults[0] + " " + requestCode);

        switch (requestCode) {
            case PERMISSION_REQ_ID: {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("LOG_TAG", "Need permissions " + Manifest.permission.RECORD_AUDIO + "/" + Manifest.permission.CAMERA);
                    break;
                }
                // if permission granted, initialize the engine
                initializeAndJoinChannel();
                break;
            }
        }
    }


}