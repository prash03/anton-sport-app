package com.superapp.sports.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.adapters.MyInterestAdapter;
import com.superapp.sports.adapters.ProfileAdapter;
import com.superapp.sports.adapters.ProfileTagAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.MyProfileResponse;
import com.superapp.sports.utils.SharedHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.activities.EditProfile.TAG;
import static com.superapp.sports.utils.Constants.ACHIEVEMENT;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.BIO;
import static com.superapp.sports.utils.Constants.CLASSIFICATION;
import static com.superapp.sports.utils.Constants.COMPANYNAME;
import static com.superapp.sports.utils.Constants.DOB;
import static com.superapp.sports.utils.Constants.EXPERIENCE;
import static com.superapp.sports.utils.Constants.FNAME;
import static com.superapp.sports.utils.Constants.GENDER;
import static com.superapp.sports.utils.Constants.GPA;
import static com.superapp.sports.utils.Constants.HEIGHT;
import static com.superapp.sports.utils.Constants.HOME;
import static com.superapp.sports.utils.Constants.LNAME;
import static com.superapp.sports.utils.Constants.MARITAL;
import static com.superapp.sports.utils.Constants.POSITION;
import static com.superapp.sports.utils.Constants.RANK;
import static com.superapp.sports.utils.Constants.SCHOOLNAME;
import static com.superapp.sports.utils.Constants.SPECIALITY;
import static com.superapp.sports.utils.Constants.SPEED;
import static com.superapp.sports.utils.Constants.SPONSERNAME;
import static com.superapp.sports.utils.Constants.SPORTNAME;
import static com.superapp.sports.utils.Constants.USERMOBILE;
import static com.superapp.sports.utils.Constants.VERTICLE;
import static com.superapp.sports.utils.Constants.WEBSITE;
import static com.superapp.sports.utils.Constants.WEIGHT;

public class ViewMyProfileDetail extends AppCompatActivity implements View.OnClickListener {

    String p_Image = "";
    String p_User_type = "";
    String is_From = "";
    String m_UserID = "";
    String authkey = "";
    ImageView img_profile;
    TextView tv_heading,tv_dob,tv_gpa,tv_hight,tv_weight,tv_position,tv_ranking,tv_home_town,tv_speed,tv_verticle,tv_clasification;
    TextView tv_name,tv_user_type;
    private ProgressDialog progressBar;
    String firstname="null",lastname="null",dob="null",Height="null",Weight="null",Position="null",Ranking="null",Speed="null",Vertical="null",Gpa="null",HomeTown="null",
            Classification="null", SchoolName="null", Experience="null", SportsName="null", Achievement="null",SponserName="null",CompanyName="null",Speciality="null";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_my);

        img_profile = findViewById(R.id.img_profile);
        tv_heading = findViewById(R.id.tv_heading);
        tv_dob = findViewById(R.id.tv_dob);
        tv_gpa = findViewById(R.id.tv_gpa);
        tv_hight = findViewById(R.id.tv_hight);
        tv_weight = findViewById(R.id.tv_weight);
        tv_position = findViewById(R.id.tv_position);
        tv_ranking = findViewById(R.id.tv_ranking);
        tv_home_town = findViewById(R.id.tv_home_town);
        tv_speed = findViewById(R.id.tv_speed);
        tv_verticle = findViewById(R.id.tv_verticle);
        tv_clasification = findViewById(R.id.tv_clasification);
        tv_name = findViewById(R.id.tv_name);
        tv_user_type = findViewById(R.id.tv_user_type);
        p_Image = getIntent().getStringExtra("ProIMAGE");
        p_User_type = getIntent().getStringExtra("UserType");
        is_From = getIntent().getStringExtra("ISFROM");
        m_UserID = getIntent().getStringExtra("UserID");
        Log.i(TAG, "onCreate p_User_type: " + p_User_type);
        authkey=SharedHelper.getKey(this,AUTH_TOKEN);
        Log.i(TAG, "onCreate authkey: " + authkey);


         firstname= SharedHelper.getKey(this,FNAME);
         lastname=SharedHelper.getKey(this,LNAME);
         dob=SharedHelper.getKey(this,DOB);
         Height=SharedHelper.getKey(this,HEIGHT);
         Weight=SharedHelper.getKey(this,WEIGHT);
         Position=SharedHelper.getKey(this,POSITION);
         Ranking=SharedHelper.getKey(this,RANK);
         Speed=SharedHelper.getKey(this,SPEED);
         Vertical=SharedHelper.getKey(this,VERTICLE);
         Gpa=SharedHelper.getKey(this,GPA);
         HomeTown=SharedHelper.getKey(this,HOME);
         Classification=SharedHelper.getKey(this,CLASSIFICATION);
         SchoolName=SharedHelper.getKey(this,SCHOOLNAME);
         Experience=SharedHelper.getKey(this,EXPERIENCE);
         SportsName=SharedHelper.getKey(this,SPORTNAME);
         Achievement=SharedHelper.getKey(this,ACHIEVEMENT);
         SponserName=SharedHelper.getKey(this,SPONSERNAME);
         CompanyName=SharedHelper.getKey(this,COMPANYNAME);
         Speciality=SharedHelper.getKey(this,SPECIALITY);

        tv_heading.setText(firstname + " " + lastname);
        tv_name.setText(firstname + " " + lastname);
        tv_user_type.setText(p_User_type);
        if (!p_Image.equalsIgnoreCase("")) {
            Picasso.get().load(p_Image).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(img_profile);
        }else {
            Picasso.get().load(R.drawable.splash_image).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(img_profile);
        }
        Log.i(TAG, "onCreate dob: " +dob );
        Log.i(TAG, "onCreate tv_user_type: " +tv_user_type );


        if(is_From.equalsIgnoreCase("OtherProfile")){

            callprofileApi(authkey,m_UserID);
        }else {
            setDataView(p_User_type,dob,Height,Weight,Position,Ranking,Speed,Vertical,Gpa,HomeTown,
                    Classification, SchoolName, Experience, SportsName, Achievement,SponserName,CompanyName,Speciality);

        }


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

    private void callprofileApi(String authToken, String userid) {
        progressBar = new ProgressDialog(ViewMyProfileDetail.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<MyProfileResponse> responseCall = api.otherProfile(authToken, Integer.parseInt(userid));
        responseCall.enqueue(new Callback<MyProfileResponse>() {
            @Override
            public void onResponse(Call<MyProfileResponse> call, Response<MyProfileResponse> response) {

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        progressBar.dismiss();
                        String fname=response.body().getUserData().getFirstName();
                        String lname=response.body().getUserData().getLastName();
                        tv_heading.setText(fname+" "+lname);
                        p_User_type = response.body().getUserData().getUser_type();
                        tv_name.setText(fname + " " + lname);
                        tv_user_type.setText(p_User_type);
                        p_Image = String.valueOf(response.body().getUserData().getUserImage());
                        if (!p_Image.equalsIgnoreCase("")) {
                            Picasso.get().load(p_Image).placeholder(R.drawable.profile_white).error(R.drawable.profile_white).into(img_profile);
                        }

                        setDataView(p_User_type,String.valueOf(response.body().getUserData().getUserDob()),response.body().getUserData().getHeight(),response.body().getUserData().getWeight(),response.body().getUserData().getPosition()
                                ,response.body().getUserData().getRanking(),response.body().getUserData().getSpeed(),response.body().getUserData().getVertical(),response.body().getUserData().getGpa(),response.body().getUserData().getHome_town(),
                                response.body().getUserData().getClassification(), response.body().getUserData().getSchool(), response.body().getUserData().getExperience(), response.body().getUserData().getSports_name(),
                                response.body().getUserData().getAchievement(),response.body().getUserData().getSponser(),response.body().getUserData().getCompany_work(),response.body().getUserData().getSpeciality());

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(ViewMyProfileDetail.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();
                    } else {
                        Toast.makeText(ViewMyProfileDetail.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();

                    }

                }
            }

            @Override
            public void onFailure(Call<MyProfileResponse> call, Throwable t) {
                Log.d("errorRespons:",t.getMessage());
                progressBar.dismiss();
            }
        });
    }


    private void setDataView(String p_user_type, String dob, String height, String weight, String position, String ranking, String speed, String vertical,
                             String gpa, String homeTown, String classification, String schoolName, String experience, String sportsName, String achievement,
                             String sponserName, String companyName, String speciality) {


        if(p_user_type.equalsIgnoreCase("Coach")){


            if(schoolName.equals("null")){
                tv_gpa.setText("");
            }
            else{
                tv_gpa.setText("School: "+schoolName);
            }
            if(dob.equalsIgnoreCase("null")){
                tv_dob.setText("");
            } else{
                tv_dob.setText("BORN: " + dob);
            }
            if(experience.equals("null")){
                tv_hight.setText("");
            }
            else{
                tv_hight.setText("Experience: "+experience);
            }
            if(sportsName.equals("null")){
                tv_weight.setText("");
            }
            else{
                tv_weight.setText("Sports: "+sportsName);
            }

            if(achievement.equals("null")){
                tv_clasification.setText("");
            }
            else{
                tv_clasification.setText("Achievement: "+achievement);
            }
            if(sponserName.equals("null")){
                tv_home_town.setText("");
            }
            else{
                tv_home_town.setText("Sponsor: "+sponserName);
            }
            if(companyName.equals("null")){
                tv_ranking.setText("");
            }
            else{
                tv_ranking.setText("Company: "+companyName);
            }
            if(speciality.equals("null")){
                tv_position.setText("");
            }
            else{
                tv_position.setText("Speciality: "+speciality);
            }

        }
        else if(p_user_type.equals("Player")){
            if(gpa.equals("null")){
                tv_gpa.setText("");
            }
            else{
                tv_gpa.setText("GPA: "+gpa);
            }
            if(dob.equals("null")){
                tv_dob.setText("");
            }
            else{
                tv_dob.setText("BORN: " + dob);
            }
            if(speed.equals("null")){
                tv_speed.setText("");
            }
            else{
                tv_speed.setText("Speed: "+speed);
            }
            if(vertical.equals("null")){
                tv_verticle.setText("");
            }
            else{
                tv_verticle.setText("Vertical: "+vertical);
            }

            if(classification.equals("null")){
                tv_clasification.setText("");
            }
            else{
                tv_clasification.setText("Classification: "+classification);
            }
            if(homeTown.equals("null")){
                tv_home_town.setText("");
            }
            else{
                tv_home_town.setText("Home: "+homeTown);
            }
            if(ranking.equals("null")){
                tv_ranking.setText("");
            }
            else{
                tv_ranking.setText("Rank: "+ranking);
            }
            if(position.equals("null")){
                tv_position.setText("");
            }
            else{
                tv_position.setText("Pos: "+position);
            }
            if(weight.equals("null")){
                tv_weight.setText("");
            }
            else{
                tv_weight.setText("WT: "+weight);
            }

            if(height.equals("null")){
                tv_hight.setText("");
            }
            else{
                tv_hight.setText("HT: "+height);
            }

        }
        else if(p_user_type.equals("Sponsor")){
            if(gpa.equals("null")){
                tv_gpa.setText("");
            }
            else{
                tv_gpa.setText("GPA: "+gpa);
            }
            if(dob.equals("null")){
                tv_dob.setText("");
            }
            else{
                tv_dob.setText("BORN: " + dob);
            }
            if(speed.equals("null")){
                tv_speed.setText("");
            }
            else{
                tv_speed.setText("Speed: "+speed);
            }
            if(vertical.equals("null")){
                tv_verticle.setText("");
            }
            else{
                tv_verticle.setText("Vertical: "+vertical);
            }

            if(classification.equals("null")){
                tv_clasification.setText("");
            }
            else{
                tv_clasification.setText("Classification: "+classification);
            }
            if(homeTown.equals("null")){
                tv_home_town.setText("");
            }
            else{
                tv_home_town.setText("Home: "+homeTown);
            }
            if(ranking.equals("null")){
                tv_ranking.setText("");
            }
            else{
                tv_ranking.setText("Rank: "+ranking);
            }
            if(position.equals("null")){
                tv_position.setText("");
            }
            else{
                tv_position.setText("Pos: "+position);
            }
            if(weight.equals("null")){
                tv_weight.setText("");
            }
            else{
                tv_weight.setText("WT: "+weight);
            }

            if(height.equals("null")){
                tv_hight.setText("");
            }
            else{
                tv_hight.setText("HT: "+height);
            }

        }


    }

}