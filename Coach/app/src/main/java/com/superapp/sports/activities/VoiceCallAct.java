package com.superapp.sports.activities;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.superapp.sports.R;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;

public class VoiceCallAct extends AppCompatActivity implements View.OnClickListener {
    ImageView backimg, endbtn, icSpeaker, icbluthoth, ic_video, icmute;
    TextView usrnametxt, collectingtxt;
    String userNm = "", userID = "", userImg = "";
    int speakerI = 0,muteI=0;

    private RtcEngine mRtcEngine;
    private static final int PERMISSION_REQ_ID = 22;

    private static final String[] REQUESTED_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_call);

        backimg = findViewById(R.id.imgBack);
        endbtn = findViewById(R.id.end_bbtn);
        usrnametxt = findViewById(R.id.user_name_txt);
        collectingtxt = findViewById(R.id.collectinon_txt);

        icSpeaker = findViewById(R.id.ic_speaker);
        icbluthoth = findViewById(R.id.ic_bluethoth);
        ic_video = findViewById(R.id.ic_video);
        icmute = findViewById(R.id.ic_mute);

        Intent extras = getIntent();
        if (extras != null) {
            userNm = extras.getStringExtra("usern");
            userID = extras.getStringExtra("userID");
            userImg = extras.getStringExtra("uImg");
        }

        if (!userNm.equals("")) {
            usrnametxt.setText(userNm);
        }

        collectingtxt.setText("Calling..");

        // If all the permissions are granted, initialize the RtcEngine object and join a channel.
        if (checkSelfPermission(REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID)) {
            initializeAndJoinChannel();
        }

        backimg.setOnClickListener(this);
        endbtn.setOnClickListener(this);
        icSpeaker.setOnClickListener(this);
        ic_video.setOnClickListener(this);
        icmute.setOnClickListener(this);
        icbluthoth.setOnClickListener(this);
    }

    // Handle SDK Events
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {

        @Override
        public void onUserJoined(final int uid, int elapsed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("......uID......" + uid);
                }
            });
        }
    };


    private boolean checkSelfPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode);
            return false;
        }
        return true;
    }


    private void initializeAndJoinChannel() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
        } catch (Exception e) {
            throw new RuntimeException("Check the error.");
        }
        mRtcEngine.enableAudio();
        mRtcEngine.joinChannel(getString(R.string.agora_access_token), getString(R.string.chanel_name), "", 0);
    }


    // Java
    protected void onDestroy() {
        super.onDestroy();
        mRtcEngine.leaveChannel();
        RtcEngine.destroy();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.imgBack) {
            finish();
        }

        if (id == R.id.end_bbtn) {
            mRtcEngine.disableVideo();
            mRtcEngine.disableAudio();
            RtcEngine.destroy();
            mRtcEngine.leaveChannel();
            finish();
        }

        if (id == R.id.ic_speaker) {
            if (speakerI == 0) {
                speakerI = 1;
                mRtcEngine.setEnableSpeakerphone(true);
                icSpeaker.setColorFilter(getResources().getColor(R.color.white));
            } else {
                speakerI = 0;
                mRtcEngine.setEnableSpeakerphone(false);
                icSpeaker.setColorFilter(getResources().getColor(R.color.ic_color_gray));
            }
        }


        if (id == R.id.ic_bluethoth) {

        }

        if (id == R.id.ic_video) {

        }

        if (id == R.id.ic_mute) {
            if (muteI == 0) {
                muteI = 1;
                mRtcEngine.muteLocalAudioStream(true);
                icmute.setColorFilter(getResources().getColor(R.color.white));
            } else {
                muteI = 0;
                mRtcEngine.muteLocalAudioStream(false);
                icmute.setColorFilter(getResources().getColor(R.color.ic_color_gray));
            }
        }
    }
}