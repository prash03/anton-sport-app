package com.superapp.sports.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.LocationListResponse;

import java.util.List;

public class AddLocationAdapter extends RecyclerView.Adapter<AddLocationAdapter.ViewHolder>{
    Context context;
    String locationName="";
    private final List<LocationListResponse.Datum> listdata;
    private OnClickDataCart onClickDataCart;

    public AddLocationAdapter(Context context,List<LocationListResponse.Datum> listdata,String locationName) {
        this.listdata = listdata;
        this.context = context;
        this.locationName = locationName;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_location, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        if(!listdata.get(position).getLocationTitle().equalsIgnoreCase(""))
          holder.tvAddress.setText(listdata.get(position).getLocationTitle());

       holder.address_lyt.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               onClickDataCart.addNewClass(listdata.get(position).getLocationTitle());
           }
       });


    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public interface OnClickDataCart {

        void  addNewClass(String address);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvAddress;
        LinearLayout address_lyt;

        public ViewHolder(View itemView) {
            super(itemView);

            this.tvAddress = itemView.findViewById(R.id.tvAddress);
            address_lyt = itemView.findViewById(R.id.address_lyt);


        }
    }
}
