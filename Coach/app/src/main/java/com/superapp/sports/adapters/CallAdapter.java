package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.VideoCallAct;
import com.superapp.sports.activities.VoiceCallAct;
import com.superapp.sports.model.TagSuggestionResponse;

//import org.linphone.core.Core;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CallAdapter extends RecyclerView.Adapter<CallAdapter.ViewHolder> {
    //private ChooseInterestResponse[] listdata;
    private final List<TagSuggestionResponse.TagSuggestion> dataList;
    private final Context mcontext;
    // RecyclerView recyclerView;
    public CallAdapter(List<TagSuggestionResponse.TagSuggestion> listdata, Context context) {
        this.dataList = listdata;
        this.mcontext = context;
    }

    @Override
    public CallAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_call, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(CallAdapter.ViewHolder holder, int position) {

        /* holder.tvCat.setText(listdata[position].getPlayName());
        holder.imgCat.setImageResource(listdata[position].getPlayImage());*/

        int pos = position;

        if (pos == 0) {
            holder.imgOnline.setVisibility(View.VISIBLE);
        } else {
            holder.imgOnline.setVisibility(View.GONE);
        }

        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mcontext, VoiceCallAct.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(i);

            }
        });

        holder.imgVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mcontext, VideoCallAct.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgCall, imgVideo, imgOnline;
        CircleImageView imgProfilePic;
        public TextView tvHead, tvSubHead, tvlast;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgProfilePic = itemView.findViewById(R.id.imgProfilePic);
            this.imgOnline = itemView.findViewById(R.id.imgOnline);
            this.imgVideo = itemView.findViewById(R.id.imgVideo);
            this.imgCall = itemView.findViewById(R.id.imgCall);
            this.tvHead = itemView.findViewById(R.id.tvHead);
            this.tvSubHead = itemView.findViewById(R.id.tvSubHead);
            this.tvlast = itemView.findViewById(R.id.tvlast);

        }
    }
}
