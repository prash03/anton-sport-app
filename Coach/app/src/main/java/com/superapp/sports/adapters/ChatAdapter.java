package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.ChatInbox;
import com.superapp.sports.model.TagSuggestionResponse;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>{
    //private ChooseInterestResponse[] listdata;
    private final List<TagSuggestionResponse.TagSuggestion> dataList;
    private final Context context;

    // RecyclerView recyclerView;
    public ChatAdapter(Context context,List<TagSuggestionResponse.TagSuggestion> listdata) {
        this.dataList = listdata;
        this.context=context;
    }
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_chat, parent, false);
        ChatAdapter.ViewHolder viewHolder = new ChatAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChatAdapter.ViewHolder holder, int position) {
        /*holder.tvCat.setText(listdata[position].getPlayName());
        holder.imgCat.setImageResource(listdata[position].getPlayImage());*/
        int pos=position;

        /*if(pos==0){
            holder.tvCount.setVisibility(View.VISIBLE);
        }
        else {
            holder.tvCount.setVisibility(View.GONE);
        }*/

        holder.rlmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ChatInbox.class);
                i.putExtra("uid",dataList.get(pos).getTagUserId());
                i.putExtra("uname",dataList.get(position).getTagUserName());
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgProfilePic;
        public TextView tvHead,tvSubHead,tvDate,tvCount;
        RelativeLayout rlmain;


        public ViewHolder(View itemView) {
            super(itemView);
            this.imgProfilePic = itemView.findViewById(R.id.imgProfilePic);
            this.tvHead = itemView.findViewById(R.id.tvHead);
            this.tvSubHead = itemView.findViewById(R.id.tvSubHead);
            this.tvDate = itemView.findViewById(R.id.tvDate);
            this.tvCount = itemView.findViewById(R.id.tvCount);
            this.rlmain = itemView.findViewById(R.id.rlmain);

        }
    }
}