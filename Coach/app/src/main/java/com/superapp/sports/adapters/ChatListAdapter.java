package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.ChatInbox;
import com.superapp.sports.model.TagSuggestionResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {
    List<TagSuggestionResponse.TagSuggestion> orderDataList;
    Context context;
    private OnClickDataCart onClickDataCart;

    int check = 0,selectedId=0,deletedId=0;
    boolean ifselected=false;
    ArrayList<Integer> selectedItemid = new ArrayList<>();
    ArrayList<Integer> deletedItemid = new ArrayList<>();


    // RecyclerView recyclerView;
    public ChatListAdapter(Context context, List<TagSuggestionResponse.TagSuggestion> listdata) {

        this.orderDataList = listdata;
        this.context=context;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_chatlist, parent, false);
        return new ViewHolder(listItem);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String user_img = String.valueOf(orderDataList.get(position).getTagUserImage());
        if (user_img.equals(null)) {
            holder.imgPic.setImageResource(R.mipmap.ic_logos_round);
        }
        else {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgPic);
        }

        holder.tvName.setText(orderDataList.get(position).getTagUserName());

        Log.d("userId", String.valueOf(orderDataList.get(position).getTagUserId()));


        holder.imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context, ChatInbox.class);
                i.putExtra("uid",String.valueOf(orderDataList.get(position).getTagUserId()));
                i.putExtra("uname",orderDataList.get(position).getTagUserName());
                i.putExtra("uImg",String.valueOf(orderDataList.get(position).getTagUserImage()));
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public interface OnClickDataCart {

        // public  void  addNewClass(ArrayList<Integer> userIdSelected);
        void  addNewClass(int numSelected, ArrayList<Integer> playIdSelected);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgPic;
        public TextView tvName;
        ImageView imgChat;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imgPic = itemView.findViewById(R.id.imgPic);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.imgChat = itemView.findViewById(R.id.imgChat);
        }
    }
}
