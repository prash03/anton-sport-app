package com.superapp.sports.adapters;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.InterestResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.superapp.sports.utils.DeliverItToApplication.TAG;

public class ChooseAdapter extends RecyclerView.Adapter<ChooseAdapter.ViewHolder> {
    List<InterestResponse.InterestDatum> orderDataList;
    int check = 0,selectedId=0,deletedId=0;
    boolean ifselected=false;
    String idofSelectedPlay="",isfrom="";
    Context context;
    private OnClickDataCart onClickDataCart;
    //ArrayList<String> selectedItemid = new ArrayList<>();
    ArrayList<Integer> selectedItemid = new ArrayList<>();
    ArrayList<Integer> deletedItemid = new ArrayList<>();
    ArrayList<Integer> AllItemid = new ArrayList<>();
    ArrayList<Integer> AllItemidUpdated = new ArrayList<>();
    ArrayList<Integer> AllItemidDeleted = new ArrayList<>();


    // RecyclerView recyclerView;
    public ChooseAdapter(Context context, List<InterestResponse.InterestDatum> listdata,String isfrom) {

        this.orderDataList = listdata;
        this.context=context;
        this.isfrom=isfrom;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_choose_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);

        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //final InterestResponse myListData = listdata[position];
        String banner_img = orderDataList.get(position).getImage();
        if (!banner_img.equalsIgnoreCase("")) {
            Picasso.get().load(banner_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.img);
        }

        holder.tvPlay.setText(orderDataList.get(position).getTitle());

        // If already interest choose
        if(orderDataList.get(position).getIs_intrest()==1) {
            holder.imgheart.setChecked(true);
            AllItemid.add(orderDataList.get(position).getItemId());
            check = orderDataList.size();
        }
        else{
            holder.imgheart.setChecked(false);}

        Log.i(TAG, "onBindViewHolder AllItemid: " + AllItemid);

        holder.imgheart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ifselected=true;
                if(holder.imgheart.isChecked()){
                    if(isfrom.equals("MYPROFILE")){
                        selectedId=orderDataList.get(position).getItemId();
                        check++;
                        selectedItemid.addAll(AllItemid);
                        selectedItemid.add(selectedId);
                        AllItemidUpdated.addAll(selectedItemid);
                        onClickDataCart.addNewClass(check,AllItemidUpdated);
                    }
                    else {
                        selectedId=orderDataList.get(position).getItemId();
                        check++;
                        selectedItemid.add(selectedId);
                        onClickDataCart.addNewClass(check,selectedItemid);
                    }

                }
                else{

                    if(isfrom.equals("MYPROFILE")){
                        check--;
                        deletedId=holder.imgheart.getImeActionId();
                        AllItemidUpdated.remove(deletedId);
                        deletedItemid.addAll(AllItemidUpdated);
                        onClickDataCart.addNewClass(check,deletedItemid);
                    }

                    else {
                        check--;
                        deletedId=holder.imgheart.getImeActionId();
                        selectedItemid.remove(deletedId);
                        deletedItemid.addAll(selectedItemid);
                        onClickDataCart.addNewClass(check,deletedItemid);
                    }

                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public interface OnClickDataCart {

        void  addNewClass(int numSelected, ArrayList<Integer> playIdSelected);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlblacklayout;
        RelativeLayout rlitemlayout;
        ImageView img;
        CheckBox imgheart;
        TextView tvPlay;

        public ViewHolder(View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.img);
            imgheart = itemView.findViewById(R.id.imgheart);
            tvPlay = itemView.findViewById(R.id.tvPlay);
            rlitemlayout = itemView.findViewById(R.id.rl_itemlayout);
            rlblacklayout = itemView.findViewById(R.id.rlv_black_view);
        }
    }
}








