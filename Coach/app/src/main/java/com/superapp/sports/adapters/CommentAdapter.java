package com.superapp.sports.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.superapp.sports.R;
import com.superapp.sports.model.InboxResponse;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final List<InboxResponse> mModelOrdersSummaries;
    private final Context mContext;
    private final String user_id = "";
    private final String senderId="";
    private final String receiverId="";
    private final String delId="";
    private final String userId="";
    private static final int ME_TYPE = 11;
    private static final int YOU_TYPE = 12;


    public CommentAdapter(Context context, List<InboxResponse> modelOrdersSummaries) {

        this.mContext = context;
        this.mModelOrdersSummaries = modelOrdersSummaries;

    }



    @Override
    public int getItemViewType(int position) {
        //InboxResponse md= mModelOrdersSummaries.get(position);
        /*senderId= String.valueOf(md.getSenderId());
        receiverId= String.valueOf(md.getReciverId());
        delId= String.valueOf(md.getDeliveryBoyId());
        userId=md.getUserId();*/


        if(position%2==1){
            return ME_TYPE;
        }

        else {
            return YOU_TYPE;
        }


    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ME_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_row_right, parent, false);
            return new MeViewHolder(view, mContext);
        }
        else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_row_left, parent, false);
            return new YouViewHolder(view, mContext);
        }

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ME_TYPE:
                initMeMessage((MeViewHolder) holder, position);
                break;
            case YOU_TYPE:
                initYouMessage((YouViewHolder) holder, position);
                break;

            default:
                break;
        }
    }

    private void initYouMessage(YouViewHolder holder, int position) {

    }

    private void initMeMessage(MeViewHolder holder, int position) {
        // InboxResponse md= mModelOrdersSummaries.get(position);

    }


    @Override
    public int getItemCount() {

        return 4;

    }


    public class MeViewHolder extends RecyclerView.ViewHolder {

        Context context;
        TextView msgTvMe, meTime, meUserName;
        Activity mActivity;
        Typeface app_font_heading;
        CircleImageView meUserPicture_right;

        public MeViewHolder(View view, Context context) {
            super(view);
            this.context = context;

            msgTvMe = itemView.findViewById(R.id.msgTv_MeIds);
            meTime = itemView.findViewById(R.id.meTimeIds);
            meUserPicture_right = itemView.findViewById(R.id.meUserPicture_right_ids);
            meUserName = itemView.findViewById(R.id.meUserName_ids);

        }
    }

    private class YouViewHolder extends RecyclerView.ViewHolder {
        Context context;

        TextView msgTvYou, youTime, youUserName;
        Typeface app_font_heading;
        CircleImageView youUserPicture_right;

        public YouViewHolder(View view, Context context) {
            super(view);
            this.context = context;

            msgTvYou = itemView.findViewById(R.id.msgTv_YouIds);
            youTime = itemView.findViewById(R.id.youTimeIds);
            youUserPicture_right = itemView.findViewById(R.id.userPicture_right_ids);
            // youUserName = itemView.findViewById(R.id.mUserName_ids);
        }
    }
}
