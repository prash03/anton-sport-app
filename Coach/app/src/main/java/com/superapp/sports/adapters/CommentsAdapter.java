package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.OtherProfile;
import com.superapp.sports.model.CommentListResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHoler> {

    List<CommentListResponse.CommentDatum> dummyModals;
    private final Context mcontext;
    private OnClickDataCart onClickDataCart;
    private OnClickDataReplyLike onClickDataReplyLike;
    private OnClickDataReplyComment onClickDataReplyComment;
    private OnLongClickComment onLongClickComment;
    private OnLongClickReply onLongClickReply;
    View view;
    ReplyAdapter replyAdapter;
    JSONArray commentArrayJson, replyJsonArray;
    //String commentid="";

    // interface

    ClickItem clickItem;

    public interface ClickItem{
        void clickLikebtn(String id,String userID);

        void unLikeClickbtn(String id,String userID);
    }

    public CommentsAdapter(Context mcontext, JSONArray commentArrayJson, ClickItem clickItem) {
        this.mcontext = mcontext;
        this.commentArrayJson = commentArrayJson;
        this.clickItem = clickItem;
    }

    public void onReplyLongClick(OnLongClickReply onLongClickReply) {
        this.onLongClickReply = onLongClickReply;
    }

    public void onItemClick(OnLongClickComment onLongClickComment) {
        this.onLongClickComment = onLongClickComment;
    }

    public void onItemClick(OnClickDataReplyLike onClickDataReplyLike) {
        this.onClickDataReplyLike = onClickDataReplyLike;
    }

    public void onItemClick(OnClickDataCart onClickDataCart) {
        this.onClickDataCart = onClickDataCart;
    }


    public void onItemLike(OnClickDataReplyComment onClickDataReplyComment) {
        this.onClickDataReplyComment = onClickDataReplyComment;
    }


    @NonNull
    @Override
    public CommentsAdapter.CommentViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mcontext).inflate(R.layout.item_comment_layout, parent, false);
        return new CommentViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsAdapter.CommentViewHoler holder, int position) {

        for (int i = 0; i < commentArrayJson.length(); i++) {
            try {
                replyJsonArray = commentArrayJson.optJSONObject(position).optJSONArray("reply_comment");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        //user_image
        String user_img = commentArrayJson.optJSONObject(position).optJSONObject("user").optString("user_image");
        if (user_img.equals("")) {
            holder.imgPic.setImageResource(R.drawable.dummy_user);
        } else {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgPic);
        }
        holder.tvName.setText(commentArrayJson.optJSONObject(position).optJSONObject("user").optString("user_name"));
        holder.tvMessage.setText(commentArrayJson.optJSONObject(position).optString("comment_text"));
        holder.tvComment.setText(commentArrayJson.optJSONObject(position).optString("total_count_of_likes"));
        holder.tvTimeComment.setText(commentArrayJson.optJSONObject(position).optJSONObject("user").optString("created_at"));

        holder.imgPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mcontext, OtherProfile.class);
                i.putExtra("userid", commentArrayJson.optJSONObject(position).optString("user_id"));
                mcontext.startActivity(i);
            }
        });

        holder.tvName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                String text = "comment";
                onLongClickComment.addNewClass(text, commentArrayJson.optJSONObject(position).optString("id"));

                return false;
            }
        });
        /*if (replyJsonArray.length()>0){
            replyAdapter.onItemLongClick(new ReplyAdapter.OnLongClickReply() {
                @Override
                public void addNewClass(String replyid) {
                    String replyId=replyid;
                    String text="reply";
                    onLongClickReply.addNewClass(text,replyId);
                }
            });
        }*/


        replyAdapter = new ReplyAdapter(mcontext, replyJsonArray);
        holder.reply_comment.setAdapter(replyAdapter);

        replyAdapter.onItemLike(new ReplyAdapter.OnClickDataLike() {
            @Override
            public void addNewClass(String replyid, String userid, String stat, String commentid) {

                String replyId = replyid;
                String userId = userid;
                String likeStat = stat;
                String CommId = commentid;
                onClickDataReplyLike.addNewClass(replyId, userId, likeStat, CommId);
            }
        });

        replyAdapter.onItemClick(new ReplyAdapter.OnClickDataCart() {
            @Override
            public void addNewClass(String message, String commentid) {

                String replyMsg = message;
                String commentId = commentid;
                onClickDataReplyComment.addNewClass(replyMsg, commentId);
            }
        });

        replyAdapter.onItemLongClick(new ReplyAdapter.OnLongClickReply() {
            @Override
            public void addNewClass(String replyid) {
                String replyId = replyid;
                onLongClickReply.addNewClass("reply", replyid);
            }
        });


        holder.tvReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.reply_lyt.setVisibility(View.VISIBLE);
                holder.reply_lyt.setVisibility((holder.reply_lyt.getVisibility() == View.VISIBLE)
                        ? View.GONE : View.VISIBLE);
            }
        });

        holder.send_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.ed_message.getText().toString().equals("")) {
                    Toast.makeText(mcontext, "Please enter reply text", Toast.LENGTH_LONG).show();
                } else {
                    //id
                    onClickDataCart.addNewClass(holder.ed_message.getText().toString(), commentArrayJson.optJSONObject(position).optString("id"));
                }
            }
        });


        holder.lllikelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (commentArrayJson.optJSONObject(position).optString("likes").equals("1")) {
                    clickItem.unLikeClickbtn(commentArrayJson.optJSONObject(position).optString("id"),
                            commentArrayJson.optJSONObject(position).optString("user_id"));
                } else {
                    clickItem.clickLikebtn(commentArrayJson.optJSONObject(position).optString("id"),
                            commentArrayJson.optJSONObject(position).optString("user_id"));
                }
            }
        });


        if (replyJsonArray.length() > 0) {
            holder.childview.setVisibility(View.VISIBLE);
        } else {
            holder.childview.setVisibility(View.GONE);
        }


        //likes
        if (commentArrayJson.optJSONObject(position).optString("likes").equals("0")) {
            holder.imLikeImg.setVisibility(View.GONE);
            holder.imUnlikeImg.setVisibility(View.VISIBLE);
        } else {
            holder.imLikeImg.setVisibility(View.VISIBLE);
            holder.imUnlikeImg.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return commentArrayJson.length();
    }

    public interface OnClickDataCart {

        void addNewClass(String message, String commentid);
    }

    public interface OnClickDataReplyLike {

        void addNewClass(String replyid, String userid, String likeStat, String commentid);
    }

    public interface OnClickDataReplyComment {

        void addNewClass(String msg, String commentid);
    }

    public interface OnLongClickComment {

        void addNewClass(String comment, String commentid);
    }

    public interface OnLongClickReply {

        void addNewClass(String reply, String replyid);
    }


    public class CommentViewHoler extends RecyclerView.ViewHolder {
        LinearLayout lllikelayout, reply_lyt;
        View childview;
        TextView tvName, tvMessage, tvTimeComment, tvComment, tvNameReply, tvMsgReply, tvReply;
        CircleImageView imgPic;
        EditText ed_message;
        ImageView send_reply, imLikeImg, imUnlikeImg;
        RecyclerView reply_comment;


        public CommentViewHoler(View itemview) {
            super(itemview);
            imLikeImg = itemview.findViewById(R.id.im_like);
            imUnlikeImg = itemview.findViewById(R.id.im_unlike);
            lllikelayout = itemview.findViewById(R.id.ll_likelayout);

            tvComment = itemview.findViewById(R.id.tvComment);
            reply_comment = itemview.findViewById(R.id.reply_comment);
            ed_message = itemview.findViewById(R.id.ed_message);
            childview = itemview.findViewById(R.id.child_view);
            tvName = itemview.findViewById(R.id.tvName);
            send_reply = itemview.findViewById(R.id.send_reply);
            tvMessage = itemview.findViewById(R.id.tvMessage);
            tvTimeComment = itemview.findViewById(R.id.tvTimeComment);
            imgPic = itemview.findViewById(R.id.imgPic);
            tvReply = itemview.findViewById(R.id.tvReply);
            reply_lyt = itemview.findViewById(R.id.reply_lyt);

            reply_comment.setLayoutManager(new LinearLayoutManager(mcontext, RecyclerView.VERTICAL, false));
        }
    }
}






/*
package com.ranoliaventures.coach.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ranoliaventures.coach.R;
import com.ranoliaventures.coach.model.CommentListResponse;
import com.ranoliaventures.coach.model.DummyModal;
import com.ranoliaventures.coach.model.MyPostResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHoler> {

    List<CommentListResponse.CommentDatum> dummyModals;
    private Context mcontext;
    private OnClickDataCart onClickDataCart;
    View view;
    ReplyAdapter replyAdapter;
    JSONArray commentArrayJson,replyJsonArray;

    public CommentsAdapter(Context mcontext, List<CommentListResponse.CommentDatum> dummyModals) {
        this.mcontext = mcontext;
        this.dummyModals = dummyModals;
    }

    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }



    @NonNull
    @Override
    public CommentsAdapter.CommentViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mcontext).inflate(R.layout.item_comment_layout, parent, false);
        return new CommentViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsAdapter.CommentViewHoler holder, int position) {

        String user_img = String.valueOf(dummyModals.get(position).getUser().getUserImage());
        if (user_img.equalsIgnoreCase(null)) {
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgPic);
        }
        holder.tvName.setText(dummyModals.get(position).getUser().getUserName());
        holder.tvMessage.setText(dummyModals.get(position).getCommentText());
        holder.tvTimeComment.setText(dummyModals.get(position).getCreatedAt());

        replyAdapter = new ReplyAdapter(mcontext,dummyModals);
        holder.reply_comment.setAdapter(replyAdapter);

        holder.tvReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.reply_lyt.setVisibility(View.VISIBLE);
                holder.reply_lyt.setVisibility((holder.reply_lyt.getVisibility() == View.VISIBLE)
                        ? View.GONE : View.VISIBLE);
            }
        });

        holder.send_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.ed_message.getText().toString().equals("")){
                    Toast.makeText(mcontext,"Please enter reply text",Toast.LENGTH_LONG).show();
                }
                else {
                    onClickDataCart.addNewClass(holder.ed_message.getText().toString());
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return dummyModals.size();
    }
    public interface OnClickDataCart {

        public  void  addNewClass(String message);
    }


    public class CommentViewHoler extends RecyclerView.ViewHolder {
        LinearLayout llchildlayout,reply_lyt;
        View childview;
        TextView tvName,tvMessage,tvTimeComment,tvTimeReply,tvNameReply,tvMsgReply,tvReply;
        CircleImageView imgPic,imgReply;
        EditText ed_message;
        ImageView send_reply;
        RecyclerView reply_comment;

        public CommentViewHoler(View itemview) {
            super(itemview);
            reply_comment = itemview.findViewById(R.id.reply_comment);
            ed_message = itemview.findViewById(R.id.ed_message);
            childview = itemview.findViewById(R.id.child_view);
            tvName = itemview.findViewById(R.id.tvName);
            send_reply = itemview.findViewById(R.id.send_reply);
            tvMessage = itemview.findViewById(R.id.tvMessage);
            tvTimeComment = itemview.findViewById(R.id.tvTimeComment);
            imgPic = itemview.findViewById(R.id.imgPic);
            tvReply = itemview.findViewById(R.id.tvReply);
            reply_lyt = itemview.findViewById(R.id.reply_lyt);

            reply_comment.setLayoutManager(new LinearLayoutManager(mcontext,RecyclerView.VERTICAL,false));
        }
    }
}
*/
