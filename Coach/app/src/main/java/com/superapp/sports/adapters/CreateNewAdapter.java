package com.superapp.sports.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.superapp.sports.R;

import java.util.ArrayList;

import static com.superapp.sports.activities.CreateNewPost.TAG;

/*public class CreateNewAdapter {
}*/
public class CreateNewAdapter extends RecyclerView.Adapter<CreateNewAdapter.ViewHolder> {
    ArrayList<Uri> mArrayUri;
    Context context;
    addImagePerItem addImagePerItem;


    public CreateNewAdapter(Context context, ArrayList<Uri> mArrayUri) {

        this.context = context;
        this.mArrayUri = mArrayUri;

    }

    public void clickItem(addImagePerItem addImagePerItem) {
        this.addImagePerItem = addImagePerItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_createpost, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final String media_pic = String.valueOf(mArrayUri.get(position));
        Log.i(TAG, "onBindViewHolder URi: " + media_pic);

        if (!media_pic.equalsIgnoreCase("")) {
            Glide.with(context).load(media_pic).placeholder(R.drawable.dummy_user).centerCrop().into(holder.selected_image);
        }


        holder.selected_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addImagePerItem.addImageOnClicked(media_pic);
            }
        });

        holder.category_cross_ids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addImagePerItem.deletImageOnClicked(holder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        Log.i(TAG, "getItemCountValue: " + mArrayUri.size());
        return mArrayUri.size();
    }

    public interface addImagePerItem {

        void addImageOnClicked(String media_pic);


        void deletImageOnClicked(int position);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView selected_image, category_cross_ids;

        public ViewHolder(View itemView) {
            super(itemView);
            selected_image = itemView.findViewById(R.id.selected_image);
            category_cross_ids = itemView.findViewById(R.id.category_cross_ids);

        }
    }
}
