package com.superapp.sports.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.FollowUserListResponse;
import com.squareup.picasso.Picasso;

import java.util.List;


public class FindFriendAdapter extends RecyclerView.Adapter<FindFriendAdapter.ViewHolder> {
    List<FollowUserListResponse.UserListData> orderDataList;
    Context context;
    private OnClickDataCart onClickDataCart;
    boolean followStat;



    // RecyclerView recyclerView;
    public FindFriendAdapter(Context context, List<FollowUserListResponse.UserListData> listdata) {

        this.orderDataList = listdata;
        this.context=context;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_find_friend, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String user_img = orderDataList.get(position).getUser_image();
        if (user_img.equals("")) {
            holder.imageView.setImageResource(R.drawable.dummy_user);
        }else {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imageView);

        }
        //holder.img.setImageResource(Integer.parseInt(((orderDataList.get(position).getImage()))));
        holder.tvName.setText(orderDataList.get(position).getUser_name());
        holder.tv_subhead.setText("Followed by "+orderDataList.get(position).getUser_name());

        followStat= Boolean.parseBoolean(orderDataList.get(position).getFollowing_status());


        holder.tv_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* holder.tv_follow.setBackgroundResource(R.drawable.blue_round_corner);
                holder.tv_follow.setTextColor(Color.parseColor("#ffffff"));
                holder.tv_follow.setText("Following");*/
                onClickDataCart.addNewClass(orderDataList.get(position).getUser_id(), Boolean.parseBoolean(orderDataList.get(position).getFollowing_status()));
            }
        });

        if(followStat==true){
            holder.tv_follow.setBackgroundResource(R.drawable.blue_round_corner);
            holder.tv_follow.setTextColor(Color.parseColor("#ffffff"));
            holder.tv_follow.setText("Following");
        }
        else {
            holder.tv_follow.setBackgroundResource(R.drawable.white_round);
            holder.tv_follow.setTextColor(Color.parseColor("#818181"));
            holder.tv_follow.setText("Follow");
        }

    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public interface OnClickDataCart {

        void  addNewClass(int userid, boolean followStat);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView tvName,tv_subhead,tv_follow;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imageView = itemView.findViewById(R.id.img_profile);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tv_subhead = itemView.findViewById(R.id.tv_subhead);
            this.tv_follow = itemView.findViewById(R.id.tv_follow);
        }
    }
}

