package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.OtherProfile;
import com.squareup.picasso.Picasso;
import com.superapp.sports.model.FollowingUserResponse;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.ViewHolder>implements Filterable {
    private final List<FollowingUserResponse.UserDatum> listdata;
    private final List<FollowingUserResponse.UserDatum> finallistdata;
    private final Context mcontext;
    private OnClickDataCart onClickDataCart;
    private OnClickBlock onClickBlock;


    public FollowingAdapter(Context context,List<FollowingUserResponse.UserDatum> listdata) {
        this.listdata = listdata;
        this.mcontext = context;
        finallistdata= new ArrayList<>(listdata);
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }
    public void onClickBlock(OnClickBlock onClickBlock){
        this.onClickBlock=onClickBlock;
    }

    @Override
    public FollowingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_following, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String user_img = String.valueOf(listdata.get(position).getUserImage());
        if (user_img.equals("")) {
            holder.imageView.setImageResource(R.mipmap.ic_logos_round);
        }
        else {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imageView);
        }
        holder.tvName.setText(listdata.get(position).getUserName());
        holder.tv_subhead.setText(listdata.get(position).getUserName());


        if(listdata.get(position).getIs_blocked().equals("yes")){
            holder.tv_status.setText("UnBlock");
        }
        else {
            holder.tv_status.setText("Block");

        }

        holder.tv_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickBlock.addNewClass(listdata.get(position).getUserId(),listdata.get(position).getIs_blocked());
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mcontext, OtherProfile.class);
                i.putExtra("userid",String.valueOf(listdata.get(position).getUserId()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(i);
            }
        });

        holder.tv_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* holder.tv_follow.setBackgroundResource(R.drawable.blue_round_corner);
                holder.tv_follow.setTextColor(Color.parseColor("#ffffff"));
                holder.tv_follow.setText("Following");*/
                onClickDataCart.addNewClass(listdata.get(position).getUserId());
            }
        });

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }
    private final Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<FollowingUserResponse.UserDatum> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(finallistdata);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (FollowingUserResponse.UserDatum item : finallistdata) {
                    if (item.getUserName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listdata.clear();
            listdata.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public interface OnClickDataCart {

        void  addNewClass(int userid);
    }
    public interface OnClickBlock{
        void addNewClass(int userid,String blockStatus);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imageView;
        public TextView tvName, tv_subhead, tv_follow,tv_status;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.img_profile);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tv_subhead = itemView.findViewById(R.id.tv_subhead);
            this.tv_follow = itemView.findViewById(R.id.tv_follow);
            this.tv_status = itemView.findViewById(R.id.tv_status);

        }
    }
}