package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.HelpDetail;
import com.superapp.sports.model.HelpResponse;

import java.util.List;

public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.ViewHolder> {
    List<HelpResponse.DataReview.Reviews> orderDataList;
    Context context;
    private OnClickDataCart onClickDataCart;
    boolean followStat;



    // RecyclerView recyclerView;
    public HelpAdapter(Context context, List<HelpResponse.DataReview.Reviews> listdata) {

        this.orderDataList = listdata;
        this.context=context;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_help, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.tvHelp.setText(orderDataList.get(position).getReviewTxt());


        holder.imNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, HelpDetail.class);
                i.putExtra("headStr",holder.tvHelp.getText());
                i.putExtra("DECP","");
                i.putExtra("ReportID","");
                i.putExtra("SubreportID","");
                context.startActivity(i);
            }
        });

        holder.rlROw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, HelpDetail.class);
                i.putExtra("headStr",holder.tvHelp.getText());
                i.putExtra("DECP","");
                i.putExtra("ReportID","");
                i.putExtra("SubreportID","");
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public interface OnClickDataCart {

        void  addNewClass(int userid, boolean followStat);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imNext;
        public TextView tvHelp;
        RelativeLayout rlROw;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imNext = itemView.findViewById(R.id.imNext);
            this.tvHelp = itemView.findViewById(R.id.tvHelp);
            this.rlROw = itemView.findViewById(R.id.rlROw);
        }
    }
}
/*public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.ViewHolder>{
    List<HelpResponse.DataReview.Reviews> orderDataList;
    private Context context;

    public HelpAdapter(Context context,List<HelpResponse.DataReview.Reviews> listdata) {
        this.orderDataList = listdata;
        this.context=context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_help, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String revievText=orderDataList.get(position).getReviewTxt();
        holder.tvHelp.setText(revievText);
        holder.imNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, HelpDetail.class);
                i.putExtra("headStr",holder.tvHelp.getText());
                context.startActivity(i);
            }
        });

        holder.rlROw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, HelpDetail.class);
                i.putExtra("headStr",holder.tvHelp.getText());
                context.startActivity(i);
            }
        });


    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imNext;
        public TextView tvHelp;
        RelativeLayout rlROw;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imNext = (ImageView) itemView.findViewById(R.id.imNext);
            this.tvHelp = (TextView) itemView.findViewById(R.id.tvHelp);
            this.rlROw = (RelativeLayout) itemView.findViewById(R.id.rlROw);

        }
    }
}*/
