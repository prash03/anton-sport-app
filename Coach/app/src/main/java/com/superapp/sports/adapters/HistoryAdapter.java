package com.superapp.sports.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>{
    //private FIndFriendResponse[] listdata;
    JSONArray jsonArray,updatedJsonArray;
    private OnClickDataCart onClickDataCart;
    Context mContext;

    public HistoryAdapter( Context mContext,JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        this.mContext = mContext;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_history, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //final FIndFriendResponse myListData = listdata[position];
        String user_img = jsonArray.optJSONObject(position).optString("user_image");
        if (user_img.equals("")) {
            holder.imageView.setImageResource(R.mipmap.ic_logos_round);

        }
        else {
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imageView);
        }
        holder.tvName.setText(jsonArray.optJSONObject(position).optString("user_name"));
        holder.tv_subhead.setText("Followed by "+jsonArray.optJSONObject(position).optString("user_name"));

        //holder.tv_subhead.setText(jsonArray.optJSONObject(position).optString("user_name"));
        //holder.imageView.setImageResource(listdata[position].getProfilePic());

        holder.imCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //jsonArray.optJSONObject(position).remove("user_name");
                onClickDataCart.addNewClass(position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }
    public void removeItem(int position) {
        jsonArray.remove(position);
        notifyItemRemoved(position);
        updatedJsonArray=jsonArray;
        SharedHelper.putKey(mContext,"search_Data",updatedJsonArray.toString());
    }
    public void removeAll(){
        SharedHelper.putKey(mContext,"search_Data","");
        notifyDataSetChanged();
    }

    public interface OnClickDataCart {

        void  addNewClass(int pos);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView,imCross;
        public TextView tvName,tv_subhead,tv_follow;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.img_profile);
            this.imCross = itemView.findViewById(R.id.imCross);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tv_subhead = itemView.findViewById(R.id.tv_subhead);

        }
    }
}