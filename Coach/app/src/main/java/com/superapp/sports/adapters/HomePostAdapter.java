package com.superapp.sports.adapters;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.superapp.sports.R;
import com.superapp.sports.activities.CommentActivity;
import com.superapp.sports.activities.OtherProfile;
import com.superapp.sports.model.HomeResponse;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.DeliverItToApplication.TAG;


public class HomePostAdapter extends RecyclerView.Adapter<HomePostAdapter.ViewHolder> {
    JSONArray bannerArray;
    private JSONArray jsonArray;
    private JSONArray suggestArr;
    Context context;
    boolean followStat;
    ArrayList<String> imageArr = new ArrayList<String>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    HorizontalRecyclerAdapter horizontalRecyclerAdapter;
    int likeStatus;
    int userId , uId, postUserId ,my_id;
    private ProgressDialog progressBar;
    String authKey = "";
    private OnClickMore onClickMore;
    private OnClickFollow onClickFollow;
    private OnClickSeeAll onClickSeeAll;
    private OnClickShare onClickShare;

    //    set interface
    ClickItem clickItem;

    public interface ClickItem {
        void clickLikebtn(String postID);
        void unLikeClickbtn(String postID);
    }


    public HomePostAdapter(Context context, JSONArray jsArray, JSONArray suggestList, int uid, ClickItem clickItem) {
        this.context = context;
        this.jsonArray = jsArray;
        this.suggestArr = suggestList;
        this.userId = uid;
        this.clickItem = clickItem;
        authKey = SharedHelper.getKey(context, AUTH_TOKEN);
    }


    public void onClickSeeAll(OnClickSeeAll onClickSeeAll) {
        this.onClickSeeAll = onClickSeeAll;
    }


    public void onClicShare(OnClickShare onClickShare) {
        this.onClickShare = onClickShare;
    }

    public void onItemClick(OnClickMore onClickMore) {
        this.onClickMore = onClickMore;
    }

    public void onClickFollow(OnClickFollow onClickFollow) {
        this.onClickFollow = onClickFollow;
    }

    @Override
    public HomePostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_home_post, parent, false);
        HomePostAdapter.ViewHolder viewHolder = new HomePostAdapter.ViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomePostAdapter.ViewHolder holder, int position) {

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                bannerArray = jsonArray.optJSONObject(position).optJSONArray("multiple_post_image");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        Log.d("imageSlider", bannerArray.toString());
        Log.d("userId : ", String.valueOf(userId));

        if (position == 0) {
            holder.horizontal_list.setVisibility(View.VISIBLE);
            holder.tvmsg.setVisibility(View.VISIBLE);
            holder.tvsuggest.setVisibility(View.VISIBLE);
            holder.imNext.setVisibility(View.VISIBLE);
            holder.tvseeAll.setVisibility(View.VISIBLE);
        }
        String banner_img = jsonArray.optJSONObject(position).optString("user_image");
        if (!banner_img.equals("")) {
            Picasso.get().load(banner_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgProfile);
        }

        likeStatus = Integer.parseInt((jsonArray.optJSONObject(position).optString("liked_status")));
        String commentCount = jsonArray.optJSONObject(position).optString("total_comment_count");
        String likeCount = jsonArray.optJSONObject(position).optString("total_like_count");
        holder.tvName.setText(jsonArray.optJSONObject(position).optString("user_name"));
        holder.tvLog.setText(jsonArray.optJSONObject(position).optString("posted_time"));
        if (!jsonArray.optJSONObject(position).optString("post_description").equalsIgnoreCase("")) {
            holder.tvmsg.setText(jsonArray.optJSONObject(position).optString("post_description"));
            holder.tvmsg.setVisibility(View.VISIBLE);
        } else {
            holder.tvmsg.setVisibility(View.GONE);
        }
        holder.tvLike.setText(likeCount);
        holder.tvComment.setText(commentCount);

        if (likeStatus == 1) {
            holder.imLike.setVisibility(View.VISIBLE);
            holder.imunlike.setVisibility(View.GONE);
        } else {
            holder.imLike.setVisibility(View.GONE);
            holder.imunlike.setVisibility(View.VISIBLE);
        }

        holder.tvseeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSeeAll.addNewClass(jsonArray.optJSONObject(position).optString("id"));
            }
        });

        holder.lllikelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (jsonArray.optJSONObject(position).optString("liked_status").equals("1")) {
                    System.out.println("call unlink metho");
                    clickItem.unLikeClickbtn(jsonArray.optJSONObject(position).optString("id"));
                }else {
                    System.out.println("call link metho");
                    clickItem.clickLikebtn(jsonArray.optJSONObject(position).optString("id"));
                }
            }
        });

        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postUserId = Integer.parseInt((jsonArray.optJSONObject(position).optString("user_id")));
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(postUserId));
                context.startActivity(i);
            }
        });
        holder.tvLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postUserId = Integer.parseInt((jsonArray.optJSONObject(position).optString("user_id")));
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(postUserId));
                context.startActivity(i);
            }
        });
        holder.imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postUserId = Integer.parseInt((jsonArray.optJSONObject(position).optString("user_id")));
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(postUserId));
                context.startActivity(i);
            }
        });

        holder.llcommentlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int postid = Integer.parseInt((jsonArray.optJSONObject(position).optString("id")));
                Intent i = new Intent(context, CommentActivity.class);
                i.putExtra("postId", postid);
                context.startActivity(i);
            }
        });

        holder.imNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent i = new Intent(context, FindFriend.class);
                context.startActivity(i);*/
            }
        });
        holder.tvseeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(context, FindFriend.class);
                context.startActivity(i);*/
                onClickSeeAll.addNewClass(jsonArray.optJSONObject(position).optString("user_id"));
            }
        });

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                postUserId = Integer.parseInt((jsonArray.optJSONObject(position).optString("user_id")));
                Log.i(TAG, "Pop Up onClick: " + postUserId + " , "+ userId);
                //creating a popup menu
                PopupMenu popup = new PopupMenu(context,holder.imgMore);
                //inflating menu from xml resource
                popup.inflate(R.menu.home_popup_menu);
                if(userId==postUserId){
                    popup.getMenu().findItem(R.id.delete_post).setVisible(true);
                    popup.getMenu().findItem(R.id.unfollow_post).setVisible(false);
                }
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete_post:
                                //handle menu1 click
                                onClickMore.addNewClass(jsonArray.optJSONObject(position).optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Delete");
                                break;
                            case R.id.hide_post:
                                //handle menu2 click
                                onClickMore.addNewClass(jsonArray.optJSONObject(position).optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Hide");
                                break;
                            case R.id.unfollow_post:
                                //handle menu3 click
                                onClickMore.addNewClass(jsonArray.optJSONObject(position).optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Unfollow");
                                break;

                                case R.id.report_post:
                                //handle menu3 click
                                    onClickMore.addNewClass(jsonArray.optJSONObject(position).optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Report");
                                break;
                                case R.id.block_post:
                                //handle menu3 click
                                    onClickMore.addNewClass(jsonArray.optJSONObject(position).optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Block");
                                break;

                                case R.id.share_post:
                                //handle menu3 click
                                    onClickMore.addNewClass(jsonArray.optJSONObject(position).optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Share");
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });

        holder.imgSharePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickShare.addNewClass(
                        jsonArray.optJSONObject(position).optString("user_name"),
                        bannerArray.optJSONObject(position).optString("posted_image"));
            }
        });


        holder.horizontal_list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        horizontalRecyclerAdapter = new HorizontalRecyclerAdapter(context, suggestArr);
        holder.horizontal_list.setAdapter(horizontalRecyclerAdapter);

        horizontalRecyclerAdapter.onItemClick(new HorizontalRecyclerAdapter.OnClickDataCart() {
            @Override
            public void addNewClass(int uid) {
                uId = uid;
                onClickFollow.addNewClass(String.valueOf(uId));

                //callFollowApi(uId);
            }
        });

        holder.mPager.setAdapter(new HomePostViewPagerAdapter(context, bannerArray));
        holder.indicator.setViewPager(holder.mPager);

        final float density = context.getResources().getDisplayMetrics().density;

        holder.indicator.setRadius(5 * density);

        NUM_PAGES = bannerArray.length();

        // Pager listener over indicator
        holder.indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public interface OnClickDataCart {

        void addNewClass(String postId);
    }

    public interface OnClickMore {

        void addNewClass(String userId, String postId , String val_type);
    }

    public interface OnClickFollow {

        void addNewClass(String userId);
    }

    public interface OnClickSeeAll {
        void addNewClass(String userId);
    }

    public interface OnClickShare {
        void addNewClass(String username,String postImage);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgProfile, imNext, imLike, imunlike, imgMore, imgSharePost;
        public TextView tvName, tvLog, tvLike, tvComment, tvmsg, tvsuggest, tvseeAll;
        ViewPager mPager;
        CirclePageIndicator indicator;
        RecyclerView horizontal_list;
        LinearLayout llcommentlayout,lllikelayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgSharePost = itemView.findViewById(R.id.imgSharePost);
            this.imgMore = itemView.findViewById(R.id.imgMore);
            this.imLike = itemView.findViewById(R.id.imLike);
            this.imunlike = itemView.findViewById(R.id.imunLike);
            this.imNext = itemView.findViewById(R.id.imNext);
            this.imgProfile = itemView.findViewById(R.id.imgProfile);
            this.tvmsg = itemView.findViewById(R.id.tvmsg);
            this.tvseeAll = itemView.findViewById(R.id.tvseeAll);
            this.tvsuggest = itemView.findViewById(R.id.tvsuggest);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tvLog = itemView.findViewById(R.id.tvLog);
            this.tvLike = itemView.findViewById(R.id.tvLike);
            this.tvComment = itemView.findViewById(R.id.tvComment);
            this.mPager = itemView.findViewById(R.id.pager);
            this.indicator = itemView.findViewById(R.id.indicator);
            this.horizontal_list = itemView.findViewById(R.id.horizontal_list);
            llcommentlayout = itemView.findViewById(R.id.ll_commentlayout);
            lllikelayout = itemView.findViewById(R.id.ll_likelayout);
        }
    }
}
