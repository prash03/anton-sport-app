package com.superapp.sports.adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.superapp.sports.R;
import com.superapp.sports.model.HomeResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.List;


public class HomeSliderAdapter extends RecyclerView.Adapter<HomeSliderAdapter.ViewHolder>{

    Context context;
    // RecyclerView recyclerView;
    JSONArray jsonArray;
    // creating a variable for exoplayer
    SimpleExoPlayer exoPlayer;

    public HomeSliderAdapter(Context context, JSONArray  listdata) {
        this.context = context;
        this.jsonArray = listdata;
    }

    /*public HomeSliderAdapter(Context context, List<HomeResponse.UserPost.HomePostImage> sliderList) {
        this.context = context;
        this.sliderList = sliderList;

    }
*/
    @Override
    public HomeSliderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.slider_item, parent, false);
        HomeSliderAdapter.ViewHolder viewHolder = new HomeSliderAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeSliderAdapter.ViewHolder holder, int position) {

        String user_img = (jsonArray.optJSONObject(position).optString("posted_image"));
        String post_type = (jsonArray.optJSONObject(position).optString("mime_type"));
        String thumb_img = (jsonArray.optJSONObject(position).optString("thumb_image"));

      /*  String user_img = (sliderList.get(position).getPostedImage());
        String post_type = (sliderList.get(position).getMimeType());
        String thumb_img = (sliderList.get(position).getThumbImage());*/
        Log.d("imageString:",user_img);


        if(post_type.equalsIgnoreCase("0")){
            if(!user_img.equals(null)){
                Picasso.get().load(thumb_img).placeholder(R.drawable.splash_image).error(R.drawable.splash_image).into(holder.imgCat);

            }
        }else {
            Picasso.get().load(thumb_img).placeholder(R.drawable.splash_image).error(R.drawable.splash_image).into(holder.imgCat);
        }


        holder.imgCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(user_img.contains("mp4")){
                    holder.video_layout.setVisibility(View.VISIBLE);
                    holder.imgCat.setVisibility(View.GONE);
                    try {

                        // bandwisthmeter is used for
                        // getting default bandwidth
                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

                        // track selector is used to navigate between
                        // video using a default seekbar.
                        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));

                        // we are adding our track selector to exoplayer.
                        exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

                        // we are parsing a video url
                        // and parsing its video uri.
                        Uri videouri = Uri.parse(user_img);

                        // we are creating a variable for datasource factory
                        // and setting its user agent as 'exoplayer_view'
                        DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");

                        // we are creating a variable for extractor factory
                        // and setting it to default extractor factory.
                        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                        // we are creating a media source with above variables
                        // and passing our event handler as null,
                        MediaSource mediaSource = new ExtractorMediaSource(videouri, dataSourceFactory, extractorsFactory, null, null);

                        // inside our exoplayer view
                        // we are setting our player
                        holder.exoPlayerView.setPlayer(exoPlayer);

                        // we are preparing our exoplayer
                        // with media source.
                        exoPlayer.prepare(mediaSource);

                        // we are setting our exoplayer
                        // when it is ready.
                        exoPlayer.setPlayWhenReady(true);

                    } catch (Exception e) {
                        // below line is used for
                        // handling our errors.
                        Log.e("TAG", "Error : " + e.toString());
                    }

                   /* try {
                        holder.videoPlayer_ids.setVideoPath(user_img);
                        // videoPlayer_ids.seekTo(500);
                        MediaController mediaController = new MediaController(context);
                        mediaController.setAnchorView(holder.videoPlayer_ids);
                        holder.videoPlayer_ids.setMediaController(mediaController);
                        holder.videoPlayer_ids.requestFocus();
                        holder.videoPlayer_ids.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.setLooping(true);
                                 holder.videoPlayer_ids.start();
                            }
                        });

                    }catch (Exception e){
                        e.printStackTrace();
                    }*/

                }else {
                    holder.video_layout.setVisibility(View.GONE);
                    holder.imgCat.setVisibility(View.VISIBLE);

                }

            }
        });


    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
       // return sliderList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgCat;
        public RelativeLayout video_layout;
        public VideoView videoPlayer_ids;
        // creating a variable for exoplayerview.
        SimpleExoPlayerView exoPlayerView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgCat = itemView.findViewById(R.id.image_s);
            this.video_layout = itemView.findViewById(R.id.video_layout);
            this.videoPlayer_ids = itemView.findViewById(R.id.videoPlayer_ids);
            this.exoPlayerView = itemView.findViewById(R.id.idExoPlayerVIew);

        }
    }
}
