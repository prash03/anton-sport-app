package com.superapp.sports.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import android.graphics.Color;

import com.squareup.picasso.Picasso;
import com.superapp.sports.model.HomeResponse;

import org.json.JSONArray;

import java.util.List;


public class HorizontalRecyclerAdapter extends RecyclerView.Adapter<HorizontalRecyclerAdapter.ViewHolder>{
    //private FIndFriendResponse[] listdata;
    //List<HomeResponse.SuggestedDatum> orderDataList;
    Context context;
    boolean followStat;
    JSONArray jsonArray;
    private OnClickDataCart onClickDataCart;
    private List<HomeResponse.HomeData.SuggestedDatum> suggestedData;

    // RecyclerView recyclerView;

    /*public HorizontalRecyclerAdapter(FIndFriendResponse[] listdata) {

        this.listdata = listdata;
    }*/

    public HorizontalRecyclerAdapter(Context context,JSONArray listdata) {
        this.context=context;
        this.jsonArray=listdata;
    }

   /* public HorizontalRecyclerAdapter(Context context, List<HomeResponse.HomeData.SuggestedDatum> suggestedData) {
        this.context=context;
        this.suggestedData=suggestedData;

    }*/

    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.courses_horizontal_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        followStat= Boolean.parseBoolean((jsonArray.optJSONObject(position).optString("following_status")));
        String banner_img = jsonArray.optJSONObject(position).optString("user_image");
        holder.tvHead.setText((jsonArray.optJSONObject(position).optString("user_name")));
        holder.tv_subhead.setText((jsonArray.optJSONObject(position).optString("matual_friends_count"))+" mutual friends");


       /* followStat= suggestedData.get(position).getFollowingStatus();
        String banner_img = suggestedData.get(position).getUserImage();
        holder.tvHead.setText(suggestedData.get(position).getUserName());
        holder.tv_subhead.setText(String.valueOf(suggestedData.get(position).getMatualFriendsCount())+" mutual friends");*/


        if (!banner_img.equals("")) {
            Picasso.get().load(banner_img).placeholder(R.color.grayColor).error(R.mipmap.ic_logos_round).into(holder.imgPic);
        }else {
            Picasso.get().load(R.drawable.dummy_user).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgPic);
        }

        holder.tvFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  onClickDataCart.addNewClass(suggestedData.get(position).getUserId());
                onClickDataCart.addNewClass(Integer.parseInt(jsonArray.optJSONObject(position).optString("user_id")));
            }
        });

        if(followStat==true){
            holder.tvFollow.setBackgroundResource(R.drawable.blue_round_corner);
            holder.tvFollow.setTextColor(Color.parseColor("#ffffff"));
            holder.tvFollow.setText("Followback");
        }
        else {
            holder.tvFollow.setBackgroundResource(R.drawable.white_round);
            holder.tvFollow.setTextColor(Color.parseColor("#818181"));
            holder.tvFollow.setText("Follow");
        }

    }

    public interface OnClickDataCart {

        void  addNewClass(int uid);
    }
    @Override
    public int getItemCount() {
        return 8;
        //return suggestedData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgPic;
        public TextView tvHead,tv_subhead,tvFollow;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgPic = itemView.findViewById(R.id.imgPic);
            this.tvHead = itemView.findViewById(R.id.tvHead);
            this.tv_subhead = itemView.findViewById(R.id.tv_subhead);
            this.tvFollow = itemView.findViewById(R.id.tvFollow);

        }
    }
}