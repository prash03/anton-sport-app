package com.superapp.sports.adapters;



import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.superapp.sports.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageViewPagerAdapter extends PagerAdapter {
    private final ArrayList<String> IMAGES;
    private final ArrayList<String> Quotes;
    private final ArrayList<String> SubQuotes;
    private final LayoutInflater inflater;
    private final Context context;


    public ImageViewPagerAdapter(Context context, ArrayList<String> IMAGES, ArrayList<String>Quotes, ArrayList<String> SubQuotes) {
        this.context = context;
        this.IMAGES=IMAGES;
        this.Quotes=Quotes;
        this.SubQuotes=SubQuotes;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return IMAGES.size();
       // return 3;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slider_viewlayout, view, false);

        assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.image_s);
        final TextView tvQuote = imageLayout.findViewById(R.id.tvQuote);
        final TextView tvSubQuote = imageLayout.findViewById(R.id.tvSubQuote);

        String banner_img = IMAGES.get(position);

        if (!banner_img.equalsIgnoreCase("")) {
            Picasso.get().load(banner_img).placeholder(R.mipmap.ic_logos).error(R.mipmap.ic_logos_round).into(imageView);
        }

        //imageView.setImageResource(Integer.parseInt(IMAGES.get(position)));
        tvQuote.setText(Quotes.get(position));
        tvSubQuote.setText(SubQuotes.get(position));


        view.addView(imageLayout, 0);

        return imageLayout;
    }
     @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}

