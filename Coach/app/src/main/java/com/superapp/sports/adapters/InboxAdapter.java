package com.superapp.sports.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.model.ChatListResponse;
import com.superapp.sports.model.InboxResponse;
import com.superapp.sports.model.TagSuggestionResponse;
import com.superapp.sports.utils.CommonUtils;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.superapp.sports.utils.Constants.USERID;

public class InboxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final List<ChatListResponse.Datum> mModelOrdersSummaries;
    private final Context mContext;
    private String senderId="",receiverId="",myId="",otherPersonId="";
    private static final int ME_TYPE = 11;
    private static final int YOU_TYPE = 12;
    OnClickImage onClickImage;


    public InboxAdapter(Context context, List<ChatListResponse.Datum> modelOrdersSummaries) {

        this.mContext = context;
        this.mModelOrdersSummaries = modelOrdersSummaries;

    }

    public void onImageViewListner(OnClickImage onClickImage){
        this.onClickImage=onClickImage;
    }


    @Override
    public int getItemViewType(int position) {
        ChatListResponse.Datum md= mModelOrdersSummaries.get(position);
        senderId= String.valueOf(md.getSenderId());
        receiverId= String.valueOf(md.getReciverId());
        myId= SharedHelper.getKey(mContext,USERID);
        //myId= String.valueOf(md.getUserId());
        otherPersonId= String.valueOf(md.getUserId());

        if(senderId.equals(myId)){
            return ME_TYPE;
        } else {
            return YOU_TYPE;
        }


       /* if(myId.equals(senderId)){
            return ME_TYPE;
        }

        else {
            return YOU_TYPE;
        }*/


    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ME_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row_right, parent, false);
            return new MeViewHolder(view, mContext);
        }
        else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row_left, parent, false);
            return new YouViewHolder(view, mContext);
        }

    }

    public interface OnClickImage {

        void addNewClass(String pic, String name, String time, String user_image);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ME_TYPE:
                initMeMessage((MeViewHolder) holder, position);
                break;
            case YOU_TYPE:
                initYouMessage((YouViewHolder) holder, position);
                break;

            default:
                break;
        }
    }

    private void initYouMessage(YouViewHolder holder, int position) {
        ChatListResponse.Datum md= mModelOrdersSummaries.get(position);
        String pic = md.getUserImage();

        String pic_chat = md.getImage();
        if (!pic_chat.equalsIgnoreCase("")) {
            holder.chat_img_recieve.setVisibility(View.VISIBLE);
            holder.rltext.setVisibility(View.GONE);
            Picasso.get().load(pic_chat).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.chat_img_recieve);
        } else {
            holder.chat_img_recieve.setVisibility(View.GONE);
        }

        if (!md.getMessage().equalsIgnoreCase("")) {
            holder.msgTvYou.setText(md.getMessage());
            holder.msgTvYou.setVisibility(View.VISIBLE);

        } else {
            holder.msgTvYou.setVisibility(View.GONE);
            holder.rltext.setVisibility(View.GONE);
        }

        String date_time=String.valueOf(md.getTime());
        if(date_time.equals(null)){
            CommonUtils.showToast(mContext,"time is null in api");
        }
        else {
            holder.youTime.setText(date_time);
        }

       /* holder.chat_img_recieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onClickImage.addNewClass(pic_chat,md.getUserName(),date_time,pic);
            }
        });*/
    }

    private void initMeMessage(MeViewHolder holder, int position) {
        ChatListResponse.Datum md= mModelOrdersSummaries.get(position);

        //holder.msgTvMe.setText(md.getMessage());
        holder.meUserName.setText(md.getUserName());
        String pic_chat = md.getImage();

        if (!pic_chat.equals("")) {
            holder.imgPic.setVisibility(View.VISIBLE);
            holder.rltextright.setVisibility(View.GONE);
            Picasso.get().load(pic_chat).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgPic);
        } else {
            holder.imgPic.setVisibility(View.GONE);
            holder.rltextright.setVisibility(View.VISIBLE);
        }
        if (!md.getMessage().equalsIgnoreCase("")) {
            holder.msgTvMe.setText(md.getMessage());




        } else {
            holder.msgTvMe.setVisibility(View.GONE);
            holder.rltextright.setVisibility(View.GONE);
        }

        String date_time=String.valueOf(md.getTime());
        if(date_time.equals(null)){
            CommonUtils.showToast(mContext,"time is null in api");
            holder.meTime.setText("");
        }
        else {
            holder.meTime.setText(date_time);
        }


       /* holder.chat_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onClickImage.addNewClass(pic_chat,md.getUserName(),date_time,"");
            }
        });*/

    }


    @Override
    public int getItemCount() {

        return mModelOrdersSummaries.size();

    }


    public class MeViewHolder extends RecyclerView.ViewHolder {

        Context context;
        TextView msgTvMe, meTime, meUserName;
        CircleImageView meUserPicture_right;
        ImageView imgPic;
        RelativeLayout rltextright;

        public MeViewHolder(View view, Context context) {
            super(view);
            this.context = context;

            msgTvMe = itemView.findViewById(R.id.msgTv_MeIds);
            meTime = itemView.findViewById(R.id.meTimeIds);
            meUserPicture_right = itemView.findViewById(R.id.meUserPicture_right_ids);
            meUserName = itemView.findViewById(R.id.meUserName_ids);
            imgPic = itemView.findViewById(R.id.chat_img);
            rltextright = itemView.findViewById(R.id.rltextright);

        }
    }

    private class YouViewHolder extends RecyclerView.ViewHolder {
        Context context;
        TextView msgTvYou, youTime;
        CircleImageView youUserPictureleft;
        ImageView chat_img_recieve;
        RelativeLayout rltext;


        public YouViewHolder(View view, Context context) {
            super(view);
            this.context = context;

            rltext = itemView.findViewById(R.id.rltext);
            msgTvYou = itemView.findViewById(R.id.msgTv_YouIds);
            youTime = itemView.findViewById(R.id.youTimeIds);
            youUserPictureleft = itemView.findViewById(R.id.userPicture_left_ids);
            chat_img_recieve = itemView.findViewById(R.id.chat_img_recieve);
        }
    }
}

