package com.superapp.sports.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.ChooseInterestResponse;

import de.hdodenhof.circleimageview.CircleImageView;

public class LinkAdapter extends RecyclerView.Adapter<LinkAdapter.ViewHolder>{
    private final ChooseInterestResponse[] listdata;
    private final Context context;

    // RecyclerView recyclerView;
    public LinkAdapter(Context context,ChooseInterestResponse[] listdata) {
        this.listdata = listdata;
        this.context=context;
    }
    @Override
    public LinkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_link, parent, false);
        LinkAdapter.ViewHolder viewHolder = new LinkAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LinkAdapter.ViewHolder holder, int position) {
        final ChooseInterestResponse myListData = listdata[position];
        /*holder.tvCat.setText(listdata[position].getPlayName());
        holder.imgCat.setImageResource(listdata[position].getPlayImage());*/
        int pos=position;

    }


    @Override
    public int getItemCount() {
        return 8;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgProfilePic;
        public TextView tvHead,tvSubHead,tvDate,tvCount;
        RelativeLayout rlmain;


        public ViewHolder(View itemView) {
            super(itemView);
            this.imgProfilePic = itemView.findViewById(R.id.imgProfilePic);
            this.tvHead = itemView.findViewById(R.id.tvHead);
            this.tvSubHead = itemView.findViewById(R.id.tvSubHead);
            this.tvDate = itemView.findViewById(R.id.tvDate);
            this.rlmain = itemView.findViewById(R.id.rlmain);

        }
    }
}
