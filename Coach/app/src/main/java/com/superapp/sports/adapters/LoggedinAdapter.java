package com.superapp.sports.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.LoginActivityResponse;

import java.util.List;

public class LoggedinAdapter extends RecyclerView.Adapter<LoggedinAdapter.ViewHolder>{
    private final List<LoginActivityResponse.Datum> listdata;
    Context mContext;


    public LoggedinAdapter(Context mContext,List<LoginActivityResponse.Datum> listdata) {
        this.listdata = listdata;
        this.mContext = mContext;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_loggedin_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvDate.setText(listdata.get(position).getDate());
        holder.tvDevice.setText(" "+listdata.get(position).getPhone_name());
        holder.tvAddress.setText(listdata.get(position).getLocationTitle());
       // holder.imageView.setImageResource(listdata[position].getProfilePic());

        if(listdata.get(position).getActive_status().equals("online")){
            holder.tvDate.setTextColor(Color.parseColor("#00C936"));
            holder.tvDate.setText("Active now ");
        }

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView tvDate,tvDevice,tvAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.imlocation);
            this.tvDate = itemView.findViewById(R.id.tvDate);
            this.tvDevice = itemView.findViewById(R.id.tvDevice);
            this.tvAddress = itemView.findViewById(R.id.tvAddress);

        }
    }
}