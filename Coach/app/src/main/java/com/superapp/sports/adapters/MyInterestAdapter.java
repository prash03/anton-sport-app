package com.superapp.sports.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.MyProfileResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyInterestAdapter extends RecyclerView.Adapter<MyInterestAdapter.ViewHolder>{
    private static final String TAG = "My Intrest";
    List<MyProfileResponse.UserData.MyInterest> orderDataList;
    Context context;


    public MyInterestAdapter(Context context,List<MyProfileResponse.UserData.MyInterest> listdata) {
        this.context = context;
        this.orderDataList = listdata;
    }
    @Override
    public MyInterestAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_interest, parent, false);
        MyInterestAdapter.ViewHolder viewHolder = new MyInterestAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyInterestAdapter.ViewHolder holder, int position) {
        String user_img = (orderDataList.get(position).getItemImage());
        Log.i(TAG, "onBindViewHolder image: " + user_img);
        //Log.d("imgString",user_img);
        if(!user_img.equals("")){
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgCat);

        }else {
            Picasso.get().load(R.drawable.dummy_user).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgCat);

        }

        holder.tvCat.setText(orderDataList.get(position).getItemName());

        /*if(position==orderDataList.size()+1){
            holder.imgCat.setImageResource(R.drawable.ic_add);
            holder.tvCat.setText("New");
        }*/
    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgCat;
        public TextView tvCat;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgCat = itemView.findViewById(R.id.imgCat);
            this.tvCat = itemView.findViewById(R.id.tvCat);

        }
    }
}
