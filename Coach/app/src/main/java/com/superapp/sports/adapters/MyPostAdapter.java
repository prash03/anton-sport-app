package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.superapp.sports.R;
import com.superapp.sports.activities.CommentActivity;
import com.superapp.sports.model.MyPostResponse;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.superapp.sports.utils.Constants.FNAME;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.LNAME;
import static com.superapp.sports.utils.DeliverItToApplication.TAG;

public class MyPostAdapter extends RecyclerView.Adapter<MyPostAdapter.ViewHolder> {
    List<MyPostResponse.MyPost> orderDataList;
    Context context;
    private OnClickDataCart onClickDataCart;
    private OnClickMore onClickMore;
    private OnClickShare onClickShare;
    boolean followStat;
    ArrayList<String> imageArr = new ArrayList<>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private List<MyPostResponse.MyPost.MultiplePostImage> sliderList;
    String location;
    JSONArray jsonArray,bannerArray;
    JSONObject jsonObject;
    int likeStatus;


    // RecyclerView recyclerView;
    public MyPostAdapter(Context context, JSONArray jsonArray,JSONObject jsonObject ,String location) {
        this.location=location;
        this.jsonArray = jsonArray;
        this.jsonObject = jsonObject;
        this.context=context;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }
    public void onClickShare(OnClickShare onClickShare){
        this.onClickShare=onClickShare;
    }
    public void onClick(OnClickMore onClickMore){
        this.onClickMore=onClickMore;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_my_post, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        for(int i=0;i<jsonArray.length();i++){
            try{
                bannerArray = jsonArray.optJSONObject(position).optJSONArray("multiple_post_image");
            }
            catch (Exception e){e.printStackTrace();}

        }

        String user_img = jsonArray.optJSONObject(position).optString("user_image");
        likeStatus= Integer.parseInt((jsonArray.optJSONObject(position).optString("liked_status")));
        //user_image
        if (!user_img.equalsIgnoreCase("")) {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgProfile);
        }else {
            Picasso.get().load(R.drawable.dummy_user).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgProfile);
        }
        //holder.img.setImageResource(Integer.parseInt(((orderDataList.get(position).getImage()))));
        holder.tvName.setText(jsonArray.optJSONObject(position).optString("name"));
        holder.tvmsg.setText(jsonArray.optJSONObject(position).optString("post_description"));
        String likeCount= jsonArray.optJSONObject(position).optString("total_like_count");
       String commentCount= jsonArray.optJSONObject(position).optString("total_comment_count");
        holder.tvComment.setText(commentCount);
        holder.tvLike.setText(likeCount);
        holder.tvLog.setText(jsonArray.optJSONObject(position).optString("posted_time"));

       /* for (int i = 0; i < orderDataList.size(); i++) {
            imageArr.add(orderDataList.get(position).getMultiplePostImage().get(position).getPostedImage());
        }*/

        holder.imgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int postid= Integer.parseInt((jsonArray.optJSONObject(position).optString("id")));
                Intent i = new Intent(context, CommentActivity.class);
                i.putExtra("postId",postid);
                context.startActivity(i);
            }
        });

        if(likeStatus==1){
            holder.imLike.setVisibility(View.VISIBLE);
            holder.imunLike.setVisibility(View.GONE);
        }
        else {
            holder.imLike.setVisibility(View.GONE);
            holder.imunLike.setVisibility(View.VISIBLE);
        }

        holder.ll_likelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(likeStatus==1){
                    onClickDataCart.addDislike(jsonArray.optJSONObject(position).optString("id"));

                }else {
                    onClickDataCart.addNewClass(jsonArray.optJSONObject(position).optString("id"));

                }

            }
        });

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(context,holder.imgMore);
                //inflating menu from xml resource
                popup.inflate(R.menu.home_popup_menu);
                if(jsonObject.optString("user_id") == jsonArray.optJSONObject(position).optString("user_id")){
                    popup.getMenu().findItem(R.id.delete_post).setVisible(true);
                    popup.getMenu().findItem(R.id.unfollow_post).setVisible(false);
                }
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.delete_post:
                                //handle menu1 click
                                onClickMore.addNewClass(jsonObject.optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Delete");
                                break;
                            case R.id.hide_post:
                                //handle menu2 click
                                onClickMore.addNewClass(jsonObject.optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Hide");
                                break;
                            case R.id.unfollow_post:
                                //handle menu3 click
                                onClickMore.addNewClass(jsonObject.optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Unfollow");
                                break;

                            case R.id.report_post:
                                //handle menu3 click
                                onClickMore.addNewClass(jsonObject.optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Report");
                                break;
                                case R.id.block_post:
                                //handle menu3 click
                                onClickMore.addNewClass(jsonObject.optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Block");
                                break;

                            case R.id.share_post:
                                //handle menu3 click
                                onClickMore.addNewClass(jsonObject.optString("user_id"), jsonArray.optJSONObject(position).optString("id"),"Share");
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

            }
        });

        holder.imgSharePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickShare.addNewClass(jsonArray.optJSONObject(position).optString("user_name"),jsonArray.optJSONObject(position).optString("user_image"), bannerArray.optJSONObject(position).optString("posted_image"));
            }
        });

        //imageArr.add(sliderList.get(position).getPostedImage());
        holder.mPager.setAdapter(new MyPostViewPagerAdapter(context,bannerArray));
        holder.indicator.setViewPager(holder.mPager);

        final float density = context.getResources().getDisplayMetrics().density;

        holder.indicator.setRadius(5 * density);

        NUM_PAGES =bannerArray.length();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                //holder.mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        holder.indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public interface OnClickDataCart {

        void  addNewClass(String postId);

        void  addDislike(String postId);
    }
    public interface OnClickMore {

        void  addNewClass(String userId, String postId ,String type);
    }
    public interface OnClickShare{
        void addNewClass(String username,String profilePic,String postImage);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgProfile,imgComment,imLike,imunLike,imgMore,imgSharePost;
        public TextView tvName,tvLog,tvLike,tvComment,tvmsg;
        ViewPager mPager;
        CirclePageIndicator indicator;
        RecyclerView horizontal_list;
        LinearLayout ll_likelayout;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imgSharePost = itemView.findViewById(R.id.imgSharePost);
            this.imgMore = itemView.findViewById(R.id.imgMore);
            this.imgComment = itemView.findViewById(R.id.imComment);
            this.imLike = itemView.findViewById(R.id.imLike);
            this.imunLike = itemView.findViewById(R.id.imunLike);
            this.imgProfile = itemView.findViewById(R.id.imgProfile);
            this.tvComment = itemView.findViewById(R.id.tvComment);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tvLog = itemView.findViewById(R.id.tvLog);
            this.tvLike = itemView.findViewById(R.id.tvLike);
            this.tvmsg = itemView.findViewById(R.id.tvmsg);
            this.mPager = itemView.findViewById(R.id.pager);
            this.indicator = itemView.findViewById(R.id.indicator);
            this.horizontal_list = itemView.findViewById(R.id.horizontal_list);
            this.ll_likelayout = itemView.findViewById(R.id.ll_likelayout);
        }
    }
}


