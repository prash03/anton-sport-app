package com.superapp.sports.adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.HomeResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.List;

public class MyPostSliderAdapter extends RecyclerView.Adapter<HomeSliderAdapter.ViewHolder>{
    Context context;
    // RecyclerView recyclerView;
    JSONArray jsonArray;
    public MyPostSliderAdapter(Context context, JSONArray  listdata) {
        this.context = context;
        this.jsonArray = listdata;
    }
    @Override
    public HomeSliderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.slider_item, parent, false);
        HomeSliderAdapter.ViewHolder viewHolder = new HomeSliderAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeSliderAdapter.ViewHolder holder, int position) {

        String user_img = (jsonArray.optJSONObject(position).optString("posted_image"));
        String post_type = (jsonArray.optJSONObject(position).optString("mime_type"));
        String thumb_img = (jsonArray.optJSONObject(position).optString("thumb_image"));
        Log.d("imageString:",user_img);

        if(post_type.equalsIgnoreCase("0")){
            if(!user_img.equals(null)){
                Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgCat);

            }
        }else {
            Picasso.get().load(thumb_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgCat);
        }

        holder.imgCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(user_img.contains("mp4")){
                    holder.video_layout.setVisibility(View.VISIBLE);
                    holder.imgCat.setVisibility(View.GONE);

                    /*try {
                        holder.videoPlayer_ids.setVideoPath(user_img);
                        // videoPlayer_ids.seekTo(500);
                        MediaController mediaController = new MediaController(context);
                        mediaController.setAnchorView(holder.videoPlayer_ids);
                        holder.videoPlayer_ids.setMediaController(mediaController);
                        holder.videoPlayer_ids.requestFocus();
                        holder.videoPlayer_ids.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.setLooping(true);
                                holder.videoPlayer_ids.start();
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/

                }else {
                    holder.video_layout.setVisibility(View.GONE);
                    holder.imgCat.setVisibility(View.VISIBLE);

                }

            }
        });

        /*if(!user_img.equals(null)){
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgCat);
        }
*/


    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgCat;
        public RelativeLayout video_layout;
        public VideoView videoPlayer_ids;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgCat = itemView.findViewById(R.id.image_s);
            this.video_layout = itemView.findViewById(R.id.video_layout);
            this.videoPlayer_ids = itemView.findViewById(R.id.videoPlayer_ids);
        }
    }
}
