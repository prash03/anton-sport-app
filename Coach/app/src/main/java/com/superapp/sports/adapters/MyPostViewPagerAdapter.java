package com.superapp.sports.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.squareup.picasso.Picasso;
import com.superapp.sports.R;

import org.json.JSONArray;

import java.util.ArrayList;

public class MyPostViewPagerAdapter extends PagerAdapter {
    private final LayoutInflater inflater;
    private final Context context;
    JSONArray jsonArray;
    SimpleExoPlayer exoPlayer;

    public MyPostViewPagerAdapter(Context context, JSONArray jsonArray ) {
        this.context = context;
        this.jsonArray=jsonArray;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return jsonArray.length();
        //return 3;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slider_item, view, false);

        assert imageLayout != null;


        ImageView image_s = imageLayout.findViewById(R.id.image_s);
        RelativeLayout video_layout = imageLayout.findViewById(R.id.video_layout);
        SimpleExoPlayerView exoPlayerView = imageLayout.findViewById(R.id.idExoPlayerVIew);


        String user_img = (jsonArray.optJSONObject(position).optString("posted_image"));
        String post_type = (jsonArray.optJSONObject(position).optString("mime_type"));
        String thumb_img = (jsonArray.optJSONObject(position).optString("thumb_image"));

     /*   String user_img = sliderList.get(position).getPostedImage();
        String post_type =  sliderList.get(position).getMimeType();
        String thumb_img =  sliderList.get(position).getThumbImage();*/

        Log.d("imageString:",user_img);


        if(post_type.equalsIgnoreCase("0")){
            if(!user_img.equals(null)){
                Picasso.get().load(thumb_img).placeholder(R.drawable.splash_image).error(R.drawable.splash_image).into(image_s);

            }
        }else {
            Picasso.get().load(thumb_img).placeholder(R.drawable.splash_image).error(R.drawable.splash_image).into(image_s);
        }

        image_s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(user_img.contains("mp4")){
                    video_layout.setVisibility(View.VISIBLE);
                    image_s.setVisibility(View.GONE);
                    try {

                        // bandwisthmeter is used for
                        // getting default bandwidth
                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();

                        // track selector is used to navigate between
                        // video using a default seekbar.
                        TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));

                        // we are adding our track selector to exoplayer.
                        exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

                        // we are parsing a video url
                        // and parsing its video uri.
                        Uri videouri = Uri.parse(user_img);

                        // we are creating a variable for datasource factory
                        // and setting its user agent as 'exoplayer_view'
                        DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");

                        // we are creating a variable for extractor factory
                        // and setting it to default extractor factory.
                        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                        // we are creating a media source with above variables
                        // and passing our event handler as null,
                        MediaSource mediaSource = new ExtractorMediaSource(videouri, dataSourceFactory, extractorsFactory, null, null);

                        // inside our exoplayer view
                        // we are setting our player
                        exoPlayerView.setPlayer(exoPlayer);

                        // we are preparing our exoplayer
                        // with media source.
                        exoPlayer.prepare(mediaSource);

                        // we are setting our exoplayer
                        // when it is ready.
                        exoPlayer.setPlayWhenReady(false);


                    } catch (Exception e) {
                        // below line is used for
                        // handling our errors.
                        Log.e("TAG", "Error : " + e.toString());
                    }

                }else {
                    video_layout.setVisibility(View.GONE);
                    image_s.setVisibility(View.VISIBLE);

                }
            }
        });

        view.addView(imageLayout, 0);

        return imageLayout;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
