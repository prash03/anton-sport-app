package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.ChatInbox;
import com.superapp.sports.activities.PaymentScreen;
import com.superapp.sports.activities.RequestPay;
import com.superapp.sports.model.MySponserResponse;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.superapp.sports.activities.ChatInbox.TAG;
import static com.superapp.sports.utils.Constants.ACCOUNTTYPE;

public class MySponsorsAdapter extends RecyclerView.Adapter<MySponsorsAdapter.CommentViewHoler> {
    //private ArrayList<DummyModal> dummyModals;
    private final List<MySponserResponse.Datum> datumList;
    private final Context mcontext;
    String accountType="";
    Month month;
    int day,reqStat;

    public MySponsorsAdapter(Context mcontext, List<MySponserResponse.Datum> datumList) {
        this.mcontext = mcontext;
        this.datumList = datumList;
        accountType= SharedHelper.getKey(mcontext,ACCOUNTTYPE);
    }


    View view;

    @NonNull
    @Override
    public MySponsorsAdapter.CommentViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mcontext).inflate(R.layout.item_sponsors_layout, parent, false);
        return new CommentViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MySponsorsAdapter.CommentViewHoler holder, int position) {

        String banner_img = datumList.get(position).getUserImage();
        if (banner_img.equals("")) {
            holder.imgPic.setImageResource(R.mipmap.ic_logos_round);
        }
        else {
            Picasso.get().load(banner_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgPic);
        }

        String fulldate =(datumList.get(position).getDate());
        //featching month name
        LocalDate currentDate
                = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            currentDate = LocalDate.parse(fulldate);
            day = currentDate.getDayOfMonth();
            month = currentDate.getMonth();
            int year = currentDate.getYear();
        }

        holder.tvName.setText(datumList.get(position).getUserName());
        holder.tvAmount.setText(datumList.get(position).getAmount()+" $");
        holder.tvDate.setText(month.toString().substring(0,3)+" "+ day);


        holder.imchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mcontext, ChatInbox.class);
                Log.i(TAG, "onClick SponserID: "+ datumList.get(position).getUserId().toString());
                i.putExtra("uid",datumList.get(position).getUserId().toString());
                i.putExtra("uname",datumList.get(position).getUserName());
                mcontext.startActivity(i);
            }
        });

        if(accountType.equals("Sponsor")){
            holder.llmain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(mcontext,RequestPay.class);
                    mcontext.startActivity(i);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }


    public class CommentViewHoler extends RecyclerView.ViewHolder {
            LinearLayout llmain;
        public ImageView imchat;
        public TextView tvName,tvAmount,tvDate;
        CircleImageView imgPic;

        public CommentViewHoler(View itemview) {

            super(itemview);
            imchat=itemview.findViewById(R.id.imchat);
            llmain=itemview.findViewById(R.id.llmain);

            this.imgPic = itemView.findViewById(R.id.imgPic);
            this.imchat = itemView.findViewById(R.id.imchat);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tvAmount = itemView.findViewById(R.id.tvAmount);
            this.tvDate = itemView.findViewById(R.id.tvDate);
        }
    }
}
