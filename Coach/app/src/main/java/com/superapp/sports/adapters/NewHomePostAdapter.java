package com.superapp.sports.adapters;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.activities.CommentActivity;
import com.superapp.sports.activities.OtherProfile;
import com.superapp.sports.model.HomeResponse;
import com.superapp.sports.utils.SharedHelper;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.DeliverItToApplication.TAG;


public class NewHomePostAdapter extends RecyclerView.Adapter<NewHomePostAdapter.ViewHolder> {

    Context context;
    boolean followStat;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private List<HomeResponse.HomeData.UserPost.HomePostImage> sliderList;
    HorizontalRecyclerAdapter horizontalRecyclerAdapter;

    boolean followStatus;
    int likeStatus;
    int userId=0,uId,postUserId;
    private ProgressDialog progressBar;
    String authKey="";
    private OnClickDataCart onClickDataCart;
    private OnClickMore onClickMore;
    private OnClickFollow onClickFollow;
    private OnClickSeeAll onClickSeeAll;
    private OnClickShare onClickShare;

    List<HomeResponse.HomeData.UserPost> user_post;
    List<HomeResponse.HomeData.SuggestedDatum> suggestedData;

  /*  public NewHomePostAdapter(Context context, List<HomeResponse.UserPost> user_post) {
        this.context = context;
        this.user_post = user_post;
    }*/

    public NewHomePostAdapter(Context context, List<HomeResponse.HomeData.UserPost> userPost, List<HomeResponse.HomeData.SuggestedDatum> suggestedData) {
        this.context = context;
        this.user_post = userPost;
        this.suggestedData = suggestedData;

    }

    public void onClickSeeAll(OnClickSeeAll onClickSeeAll){
        this.onClickSeeAll=onClickSeeAll;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }
    public void onClicShare(OnClickShare onClickShare){
        this.onClickShare=onClickShare;
    }
    public void onItemClick(OnClickMore onClickMore){
        this.onClickMore=onClickMore;
    }
    public void onClickFollow(OnClickFollow onClickFollow){
        this.onClickFollow=onClickFollow;
    }


    @Override
    public NewHomePostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_home_post, parent, false);
        NewHomePostAdapter.ViewHolder viewHolder = new NewHomePostAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewHomePostAdapter.ViewHolder holder, int position) {


        if(position==0){
            holder.horizontal_list.setVisibility(View.VISIBLE);
            holder.tvmsg.setVisibility(View.VISIBLE);
            holder.tvsuggest.setVisibility(View.VISIBLE);
            holder.imNext.setVisibility(View.VISIBLE);
            holder.tvseeAll.setVisibility(View.VISIBLE);
        }
        String banner_img = user_post.get(position).getUserImage();
        if (!banner_img.equals("")) {
            Picasso.get().load(banner_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgProfile);
        }

        likeStatus= user_post.get(position).getLikedStatus();
        String commentCount= String.valueOf(user_post.get(position).getTotalCommentCount());
        String likeCount= String.valueOf(user_post.get(position).getTotalLikeCount());
        holder.tvName.setText(user_post.get(position).getUserName());
        holder.tvLog.setText(user_post.get(position).getPostedTime());
        if(!user_post.get(position).getPostDescription().equalsIgnoreCase("")){
        holder.tvmsg.setText(user_post.get(position).getPostDescription());
        holder.tvmsg.setVisibility(View.VISIBLE);}
        else{ holder.tvmsg.setVisibility(View.GONE);}
        holder.tvLike.setText(likeCount);
        holder.tvComment.setText(commentCount);

        if(likeStatus==1){
            holder.imLike.setImageResource(R.drawable.ic_like);
        }
        else {
            holder.imLike.setImageResource(R.drawable.ic_unlike);
        }
        holder.tvseeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSeeAll.addNewClass(String.valueOf(user_post.get(position).getId()));
            }
        });

        holder.imLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickDataCart.addNewClass(String.valueOf(user_post.get(position).getId()));
            }
        });

        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postUserId= user_post.get(position).getUserId();
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(postUserId));
                context.startActivity(i);
            }
        });
        holder.tvLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postUserId= user_post.get(position).getUserId();
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(postUserId));
                context.startActivity(i);
            }
        });
        holder.imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postUserId= user_post.get(position).getUserId();
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(postUserId));
                context.startActivity(i);
            }
        });

        holder.llcommentlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int postid= user_post.get(position).getId();
                Intent i = new Intent(context, CommentActivity.class);
                i.putExtra("postId",postid);
                context.startActivity(i);
            }
        });

        holder.imNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent i = new Intent(context, FindFriend.class);
                context.startActivity(i);*/
            }
        });
        holder.tvseeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent i = new Intent(context, FindFriend.class);
                context.startActivity(i);*/
                onClickSeeAll.addNewClass(String.valueOf(user_post.get(position).getUserId()));
            }
        });

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickMore.addNewClass(String.valueOf(user_post.get(position).getUserId()), String.valueOf(user_post.get(position).getId()));
            }
        });

        holder.imgSharePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickShare.addNewClass(String.valueOf(user_post.get(position).getUserName()),user_post.get(position).getUserImage(), "");
            }
        });


       /* holder.horizontal_list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        horizontalRecyclerAdapter = new HorizontalRecyclerAdapter(context,suggestedData);
        holder.horizontal_list.setAdapter(horizontalRecyclerAdapter);

        horizontalRecyclerAdapter.onItemClick(new HorizontalRecyclerAdapter.OnClickDataCart() {
            @Override
            public void addNewClass(int uid) {
                followStatus=followStat;
                uId=uid;
                onClickFollow.addNewClass(String.valueOf(uId));

                //callFollowApi(uId);
            }
        });*/


        sliderList.addAll(user_post.get(position).getMultiplePostImage());

        Log.i(TAG, "onBindViewHolder: " + sliderList.size());

       /* holder.mPager.setAdapter(new HomePostViewPagerAdapter(context,sliderList));
        holder.indicator.setViewPager(holder.mPager);*/


    }

    @Override
    public int getItemCount() {
        return user_post.size();
    }

    public interface OnClickDataCart {

        void  addNewClass(String postId);
    }
    public interface OnClickMore {

        void  addNewClass(String userId, String postId);
    }
    public interface OnClickFollow {

        void  addNewClass(String userId);
    }
    public interface OnClickSeeAll {
        void  addNewClass(String userId);
    }
    public interface OnClickShare{
        void addNewClass(String username,String profilePic,String postImage);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgProfile,imNext,imLike,imgMore,imgSharePost;
        public TextView tvName, tvLog, tvLike,tvComment,tvmsg,tvsuggest,tvseeAll;
        ViewPager mPager;
        CirclePageIndicator indicator;
        RecyclerView horizontal_list;
        LinearLayout llcommentlayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgSharePost = itemView.findViewById(R.id.imgSharePost);
            this.imgMore = itemView.findViewById(R.id.imgMore);
            this.imLike = itemView.findViewById(R.id.imLike);
            this.imNext = itemView.findViewById(R.id.imNext);
            this.imgProfile = itemView.findViewById(R.id.imgProfile);
            this.tvmsg = itemView.findViewById(R.id.tvmsg);
            this.tvseeAll = itemView.findViewById(R.id.tvseeAll);
            this.tvsuggest = itemView.findViewById(R.id.tvsuggest);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tvLog = itemView.findViewById(R.id.tvLog);
            this.tvLike = itemView.findViewById(R.id.tvLike);
            this.tvComment = itemView.findViewById(R.id.tvComment);
            this.mPager = itemView.findViewById(R.id.pager);
            this.indicator = itemView.findViewById(R.id.indicator);
            this.horizontal_list = itemView.findViewById(R.id.horizontal_list);
            llcommentlayout = itemView.findViewById(R.id.ll_commentlayout);
        }
    }
}
