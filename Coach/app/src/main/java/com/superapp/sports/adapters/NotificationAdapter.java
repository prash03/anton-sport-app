package com.superapp.sports.adapters;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.MyNotificationResponse;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>{
    List<MyNotificationResponse.Datum> orderDataList;
    private final Context context;
    String currentDate="",currentTIme="";


    // RecyclerView recyclerView;
    public NotificationAdapter(Context context,List<MyNotificationResponse.Datum> orderDataList) {
        this.context = context;
        this.orderDataList = orderDataList;
    }
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_notification, parent, false);
        NotificationAdapter.ViewHolder viewHolder = new NotificationAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        CharSequence dayName = DateFormat.format("EEEE", c.getTime());

        holder.tvDate.setText("Today, " + formattedDate.substring(3, 6) + " " + formattedDate.substring(0, 2));
        //tvDay.setText(dayName + " Earned");
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dsf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar dc = Calendar.getInstance();
        currentDate = dsf.format(dc.getTime());
        currentTIme = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        System.out.println("Current date => " + currentDate + "Time-" + currentTIme);

        String user_img = String.valueOf(orderDataList.get(position).getImage());
        if (!user_img.equalsIgnoreCase("")) {
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgProfilePic);
        }
        holder.tvHead.setText(orderDataList.get(position).getTitle());
        holder.tvSubHead.setText(orderDataList.get(position).getMessage());
        holder.tvDate.setText(orderDataList.get(position).getDate());

    }
    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgProfilePic;
        public TextView tvHead,tvSubHead,tvDate;


        public ViewHolder(View itemView) {
            super(itemView);
            this.imgProfilePic = itemView.findViewById(R.id.imgProfilePic);
            this.tvHead = itemView.findViewById(R.id.tvHead);
            this.tvSubHead = itemView.findViewById(R.id.tvSubHead);
            this.tvDate = itemView.findViewById(R.id.tvDate);


        }
    }
}
