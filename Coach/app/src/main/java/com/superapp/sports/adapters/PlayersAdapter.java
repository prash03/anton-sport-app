package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.ChatInbox;
import com.superapp.sports.activities.PaymentScreen;
import com.superapp.sports.activities.RequestPay;
import com.superapp.sports.model.MyPlayerResponse;
import com.squareup.picasso.Picasso;
import com.superapp.sports.model.MySponserResponse;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.ViewHolder>{

    List<MySponserResponse.Datum> listdata;
    Context context;
    Month month;
    int day,reqStat;

    public PlayersAdapter(Context context,List<MySponserResponse.Datum> listdata) {
        this.listdata = listdata;
        this.context=context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_players, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String banner_img = String.valueOf(listdata.get(position).getUserImage());
        if (banner_img.equals("")) {
            holder.imageView.setImageResource(R.mipmap.ic_logos_round);
        }
        else {
            Picasso.get().load(banner_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imageView);
        }



        String fulldate =(listdata.get(position).getDate());
        //featching month name
        LocalDate currentDate
                = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            currentDate = LocalDate.parse(fulldate);
            day = currentDate.getDayOfMonth();
            month = currentDate.getMonth();
            int year = currentDate.getYear();
        }

        holder.tvName.setText(listdata.get(position).getUserName());
        holder.tvAmount.setText(listdata.get(position).getAmount()+" $");
        holder.tvDate.setText(month.toString().substring(0,3)+" "+ day);

        reqStat= listdata.get(position).getReqStatus();

        if (reqStat==0){
            holder.imchat.setVisibility(View.GONE);
            holder.impay.setVisibility(View.VISIBLE);
            Log.d("reStatValue", String.valueOf(reqStat));
            holder.tvStatus.setText("Pending");

        }
        else {
            holder.imchat.setVisibility(View.VISIBLE);
            holder.impay.setVisibility(View.GONE);
            Log.d("reqStatValue", String.valueOf(reqStat));
            holder.tvStatus.setText("Approved");

        }
        holder.impay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context, RequestPay.class);
                context.startActivity(i);
            }
        });

        holder.imchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ChatInbox.class);
                i.putExtra("uid",String.valueOf(listdata.get(position).getUserId()));
                i.putExtra("uname",listdata.get(position).getUserName());
                context.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView,imchat,impay;
        public TextView tvName,tvAmount,tvDate,tvStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.imgPic);
            this.imchat = itemView.findViewById(R.id.imchat);
            this.impay = itemView.findViewById(R.id.impay);
            this.tvStatus = itemView.findViewById(R.id.tvStatus);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tvAmount = itemView.findViewById(R.id.tvAmount);
            this.tvDate = itemView.findViewById(R.id.tvDate);

        }
    }
}