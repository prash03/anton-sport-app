package com.superapp.sports.adapters;


import com.bumptech.glide.Glide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.NewPostResponse;

import java.io.File;
import java.util.ArrayList;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder>{
    private static final String TAG = "PostImages";
    private NewPostResponse[] listdata;
    Animation zoomIn,zoomOut;
    Context mContext;
    boolean zoom=false;
    ArrayList<String> mArrayImages;
    ArrayList<File> listFiles;



    public PostAdapter(Context mContext, ArrayList<String> mArrayImages) {
        this.mArrayImages = mArrayImages;
        this.mContext=mContext;

    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_new_post, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        Glide.with(mContext).load(mArrayImages.get(position)).placeholder(R.drawable.dummy_user).centerCrop().into(holder.imageView);

        if(position==0){
            holder.imcheck.setVisibility(View.GONE);
        }
        else
        {
            holder.imcheck.setVisibility(View.VISIBLE);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
                View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_zoom, null);
                //View mView = getLayoutInflater().inflate(R.layout.dialog_zoom, null);
                PhotoView photoView = mView.findViewById(R.id.imageView);
                photoView.setImageURI(listdata[position].getProfilePic());
                mBuilder.setView(mView);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();*/
            }
        });

    }


    @Override
    public int getItemCount() {
        return mArrayImages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView tvName,tv_subhead,tv_follow;
        AppCompatCheckBox imcheck;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.imgProfile);
            this.imcheck = itemView.findViewById(R.id.imcheck);

        }
    }
}