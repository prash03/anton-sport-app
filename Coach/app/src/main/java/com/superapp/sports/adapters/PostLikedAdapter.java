package com.superapp.sports.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.PostYouLikeResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.superapp.sports.utils.DeliverItToApplication.TAG;

public class PostLikedAdapter extends RecyclerView.Adapter<PostLikedAdapter.ViewHolder>{
    //private ProfileResponse[] listdata;
    private final List<PostYouLikeResponse.Datum> listdata;
    private final Context context;



    public PostLikedAdapter(Context context,List<PostYouLikeResponse.Datum> listdata) {
        this.listdata = listdata;
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_post_liked, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //final ProfileResponse myListData = listdata[position];
        String user_img = String.valueOf(listdata.get(position).getImage());
        Log.i(TAG, "onBindViewHolder user_img : " + user_img);
        if (!user_img.equals(null)) {
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imageView);
        }

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.imgProfile);

        }
    }
}