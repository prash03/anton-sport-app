package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.MyProfilePost;
import com.superapp.sports.model.MyProfileResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProfileTagAdapter extends RecyclerView.Adapter<ProfileTagAdapter.ViewHolder>{

    List<MyProfileResponse.UserData.TagPost> listdata;
    private final Context context;
    int playerId;


    public ProfileTagAdapter(Context context,List<MyProfileResponse.UserData.TagPost> listdata, int playerId) {
        this.listdata = listdata;
        this.context = context;
        this.playerId = playerId;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_profile, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String banner_img = String.valueOf(listdata.get(position).getItemImage());
        if (!banner_img.equals("")) {
            Picasso.get().load(banner_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imageView);
        }


        holder.cardPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, MyProfilePost.class);
                i.putExtra("PlayerID",String.valueOf(playerId));
                i.putExtra("post_type","tag");
                context.startActivity(i);
            }
        });


    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView tvName,tv_subhead;
        CardView cardPost;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.imgProfile);
            this.cardPost = itemView.findViewById(R.id.cardPost);

        }
    }
}