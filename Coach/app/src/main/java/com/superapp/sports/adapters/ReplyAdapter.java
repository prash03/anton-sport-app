package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.squareup.picasso.Picasso;
import com.superapp.sports.activities.OtherProfile;

import org.json.JSONArray;

import de.hdodenhof.circleimageview.CircleImageView;


public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.CommentViewHoler> {
    private final Context mcontext;
    private OnClickDataCart onClickDataCart;
    private OnClickDataLike onClickDataLike;
    private OnLongClickReply onLongClickReply;
    View view;
    JSONArray replyArrayJson,replyToreplyJson;
    //ReplyToReplyAdapter replyToReplyAdapter;

    public ReplyAdapter(Context mcontext, JSONArray replyArrayJson) {
        this.mcontext = mcontext;
        this.replyArrayJson = replyArrayJson;
    }


    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }
    public void onItemLongClick(OnLongClickReply onLongClickReply){
        this.onLongClickReply=onLongClickReply;
    }

    public void onItemLike(OnClickDataLike onClickDataLike){
        this.onClickDataLike=onClickDataLike;
    }

    @NonNull
    @Override
    public ReplyAdapter.CommentViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mcontext).inflate(R.layout.reply_item, parent, false);
        return new CommentViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReplyAdapter.CommentViewHoler holder, int position) {

        for(int i=0;i<replyArrayJson.length();i++){
            try{
                replyToreplyJson = replyArrayJson.optJSONObject(position).optJSONArray("reply_comment");
            }
            catch (Exception e){e.printStackTrace();}

        }

        String user_img = replyArrayJson.optJSONObject(position).optString("user_image");

        if (user_img.equalsIgnoreCase("")) {
            holder.imgReply.setImageResource(R.drawable.dummy_user);
        }
        else {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgReply);
        }

        holder.tvTimeReply.setText(replyArrayJson.optJSONObject(position).optString("created_at"));
        holder.tvNameReply.setText(replyArrayJson.optJSONObject(position).optString("user_name"));
        holder.tvMsgReply.setText(replyArrayJson.optJSONObject(position).optString("comment_text"));

        holder.imgReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mcontext, OtherProfile.class);
                i.putExtra("userid", replyArrayJson.optJSONObject(position).optString("user_id"));
                mcontext.startActivity(i);
            }
        });
        //holder.tvLikeCount.setText(replyArrayJson.optJSONObject(position).optString("comment_text"));

        /*replyToReplyAdapter = new ReplyToReplyAdapter(mcontext,replyToreplyJson);
        holder.reply_toReplyRv.setAdapter(replyToReplyAdapter);*/

        if (replyArrayJson.length()>0){
            holder.tvMsgReply.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    onLongClickReply.addNewClass(replyArrayJson.optJSONObject(position).optString("id"));
                    return false;
                }
            });
        }



        holder.tvReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.reply_lyt.setVisibility(View.VISIBLE);
                holder.reply_Tolyt.setVisibility((holder.reply_Tolyt.getVisibility() == View.VISIBLE)
                        ? View.GONE : View.VISIBLE);
            }
        });

        holder.send_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.ed_message.getText().toString().equals("")){
                    Toast.makeText(mcontext,"Please enter reply text",Toast.LENGTH_LONG).show();
                }
                else {
                    //id
                    onClickDataCart.addNewClass(holder.ed_message.getText().toString(),replyArrayJson.optJSONObject(position).optString("comment_id"));
                }
            }
        });

        holder.imLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickDataLike.addNewClass(replyArrayJson.optJSONObject(position).optString("id"),replyArrayJson.optJSONObject(position).optString("user_id"),replyArrayJson.optJSONObject(position).optString("likes"),replyArrayJson.optJSONObject(position).optString("comment_id"));
            }
        });

        if(replyArrayJson.optJSONObject(position).optString("likes").equals("0")){
            holder.imLike.setImageResource(R.drawable.ic_unlike);
        }
        else {
            holder.imLike.setImageResource(R.drawable.ic_like);
        }

    }

    @Override
    public int getItemCount() {
        return replyArrayJson.length();
    }
    public interface OnClickDataCart {

        void  addNewClass(String message, String replyId);
    }
    public interface OnClickDataLike {

        void  addNewClass(String replyid, String userid, String stat, String commentid);
    }
    public interface OnLongClickReply {

        void  addNewClass(String replyid);
    }
    public class CommentViewHoler extends RecyclerView.ViewHolder {
        TextView tvTimeReply,tvNameReply,tvMsgReply,tvReply,tvLikeCount;
        CircleImageView imgReply;
        LinearLayout reply_Tolyt;
        ImageView send_reply,imLike;
        EditText ed_message;
        LinearLayout linearLayout;
        //RecyclerView reply_toReplyRv;

        public CommentViewHoler(View itemview) {
            super(itemview);
            //reply_toReplyRv = itemview.findViewById(R.id.reply_toReply);
            linearLayout = itemview.findViewById(R.id.ll_child_layout);
            ed_message = itemview.findViewById(R.id.ed_message);
            tvLikeCount = itemview.findViewById(R.id.tvLikeCount);
            send_reply = itemview.findViewById(R.id.send_reply);
            imLike = itemview.findViewById(R.id.imLike);
            reply_Tolyt = itemview.findViewById(R.id.reply_Tolyt);
            tvReply = itemview.findViewById(R.id.tvReply);
            imgReply = itemview.findViewById(R.id.imgReply);
            tvNameReply = itemview.findViewById(R.id.tvNameReply);
            tvMsgReply = itemview.findViewById(R.id.tvMsgReply);
            tvTimeReply = itemview.findViewById(R.id.tvTimeReply);


        }
    }
}


/*
package com.ranoliaventures.coach.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ranoliaventures.coach.R;
import com.ranoliaventures.coach.model.CommentListResponse;
import com.ranoliaventures.coach.utils.SharedHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.ranoliaventures.coach.utils.Constants.Key_profile_image;


public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.CommentViewHoler> {
    List<CommentListResponse.CommentDatum> dummyModals;
    private Context mcontext;
    private OnClickDataCart onClickDataCart;
    View view;
    JSONArray replyArrayJson;

    public ReplyAdapter(Context mcontext, JSONArray replyArrayJson) {
        this.mcontext = mcontext;
        this.replyArrayJson = replyArrayJson;
    }

    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }



    @NonNull
    @Override
    public ReplyAdapter.CommentViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mcontext).inflate(R.layout.reply_item, parent, false);
        return new CommentViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReplyAdapter.CommentViewHoler holder, int position) {

        String user_img = dummyModals.get(position).getReplyComment().get(position).getUser_image();

        if (user_img.equalsIgnoreCase(null)) {
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgReply);
        }

        holder.tvTimeReply.setText(dummyModals.get(position).getCreatedAt());
        holder.tvNameReply.setText(dummyModals.get(position).getReplyComment().get(position).getUser_name());
        holder.tvNameReply.setText(dummyModals.get(position).getCommentText());

    }

    @Override
    public int getItemCount() {
        return dummyModals.size();
    }
    public interface OnClickDataCart {

        public  void  addNewClass(String message);
    }


    public class CommentViewHoler extends RecyclerView.ViewHolder {
        TextView tvTimeReply,tvNameReply,tvMsgReply;
        CircleImageView imgReply;

        public CommentViewHoler(View itemview) {
            super(itemview);
            imgReply = itemview.findViewById(R.id.imgReply);
            tvNameReply = itemview.findViewById(R.id.tvNameReply);
            tvMsgReply = itemview.findViewById(R.id.tvMsgReply);
            tvTimeReply = itemview.findViewById(R.id.tvTimeReply);


        }
    }
}
*/
