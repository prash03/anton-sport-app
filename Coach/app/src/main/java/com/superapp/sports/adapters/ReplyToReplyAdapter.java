package com.superapp.sports.adapters;

/*
public class ReplyToReplyAdapter {
}
*/

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import de.hdodenhof.circleimageview.CircleImageView;


public class ReplyToReplyAdapter extends RecyclerView.Adapter<ReplyToReplyAdapter.CommentViewHoler> {
    private final Context mcontext;
    private OnClickDataCart onClickDataCart;
    View view;
    JSONArray replyArrayJson;

    public ReplyToReplyAdapter(Context mcontext, JSONArray replyArrayJson) {
        this.mcontext = mcontext;
        this.replyArrayJson = replyArrayJson;
    }

    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }



    @NonNull
    @Override
    public ReplyToReplyAdapter.CommentViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mcontext).inflate(R.layout.item_reply_to_reply, parent, false);
        return new CommentViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReplyToReplyAdapter.CommentViewHoler holder, int position) {

        String user_img = replyArrayJson.optJSONObject(position).optString("user_image");


        if (user_img.equalsIgnoreCase(null)) {
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.imgReply);
        }

        holder.tvTimeReply.setText(replyArrayJson.optJSONObject(position).optString("created_at"));
        holder.tvNameReply.setText(replyArrayJson.optJSONObject(position).optString("user_name"));
        holder.tvMsgReply.setText(replyArrayJson.optJSONObject(position).optString("comment_text"));


    }

    @Override
    public int getItemCount() {
        return replyArrayJson.length();
    }
    public interface OnClickDataCart {

        void  addNewClass(String message);
    }


    public class CommentViewHoler extends RecyclerView.ViewHolder {
        TextView tvTimeReply,tvNameReply,tvMsgReply;
        CircleImageView imgReply;

        public CommentViewHoler(View itemview) {
            super(itemview);
            imgReply = itemview.findViewById(R.id.imgReply);
            tvNameReply = itemview.findViewById(R.id.tvNameReply);
            tvMsgReply = itemview.findViewById(R.id.tvMsgReply);
            tvTimeReply = itemview.findViewById(R.id.tvTimeReply);


        }
    }
}

