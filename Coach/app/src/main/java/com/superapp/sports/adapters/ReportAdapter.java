package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.HelpDetail;
import com.superapp.sports.activities.ReportActivity;
import com.superapp.sports.model.HelpResponse;
import com.superapp.sports.model.ReportResponse;

import java.util.List;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> {

    List<ReportResponse.DataReview.Report> dataList;
    Context context;
    private OnClickDataCart onClickDataCart;
    boolean followStat;

    public ReportAdapter(Context context, List<ReportResponse.DataReview.Report> dataList) {
        this.context=context;
        this.dataList=dataList;
    }

    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_help, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.tvHelp.setText(dataList.get(position).getReport_title());

        holder.rlROw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent i=new Intent(context, HelpDetail.class);
                i.putExtra("headStr",dataList.get(position).getReport_title());
                context.startActivity(i);*/

                onClickDataCart.addNewClass(dataList.get(position).getItemId(),dataList.get(position).getReport_title());
            }
        });

    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public interface OnClickDataCart {

        void  addNewClass(int id , String report_name);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imNext;
        public TextView tvHelp;
        RelativeLayout rlROw;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imNext = itemView.findViewById(R.id.imNext);
            this.tvHelp = itemView.findViewById(R.id.tvHelp);
            this.rlROw = itemView.findViewById(R.id.rlROw);
        }
    }
}

