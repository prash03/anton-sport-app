package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.HelpDetail;
import com.superapp.sports.activities.ReportActivity;
import com.superapp.sports.model.ReportResponse;
import com.superapp.sports.model.ReportSubCatResponse;

import java.util.List;

public class ReportSubCatAdapter extends RecyclerView.Adapter<ReportSubCatAdapter.ViewHolder> {

    List<ReportSubCatResponse.SubCatData> sub_dataList;
    Context context;


    public ReportSubCatAdapter(Context context, List<ReportSubCatResponse.SubCatData> sub_dataList) {
        this.context=context;
        this.sub_dataList=sub_dataList;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_help, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        if(sub_dataList.get(position).getSubReportTitle().equalsIgnoreCase("")){
            Intent i=new Intent(context, HelpDetail.class);
            i.putExtra("headStr",sub_dataList.get(position).getReport_title());
            i.putExtra("DECP",sub_dataList.get(position).getSubReportDesc());
            i.putExtra("ReportID",sub_dataList.get(position).getReportId().toString());
            i.putExtra("SubreportID",sub_dataList.get(position).getId().toString());
            context.startActivity(i);
        }else {
            holder.tvHelp.setText(sub_dataList.get(position).getSubReportTitle());
        }

        holder.rlROw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, HelpDetail.class);
                i.putExtra("headStr",sub_dataList.get(position).getSubReportTitle());
                i.putExtra("DECP",sub_dataList.get(position).getSubReportDesc());
                i.putExtra("ReportID",sub_dataList.get(position).getReportId().toString());
                i.putExtra("SubreportID",sub_dataList.get(position).getId().toString());
                context.startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {
        return sub_dataList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imNext;
        public TextView tvHelp;
        RelativeLayout rlROw;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imNext = itemView.findViewById(R.id.imNext);
            this.tvHelp = itemView.findViewById(R.id.tvHelp);
            this.rlROw = itemView.findViewById(R.id.rlROw);
        }
    }
}

