package com.superapp.sports.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.activities.OtherProfile;
import com.superapp.sports.model.SearchUserResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.ViewHolder>implements Filterable {
    List<SearchUserResponse.TagSuggestion> orderDataList;
    List<SearchUserResponse.TagSuggestion> productModelListfull;
    Context context;
    boolean followStat;
    private OnClickDataCart onClickDataCart;


    public SearchUserAdapter(Context context, List<SearchUserResponse.TagSuggestion> orderDataList) {
        this.orderDataList = orderDataList;
        this.context=context;
        productModelListfull = new ArrayList<>(orderDataList);

    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_search_user, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String user_img = String.valueOf(orderDataList.get(position).getUserImage());
        if (!user_img.equals(null)) {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imageView);
        }else {
            Picasso.get().load(R.drawable.dummy_user).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imageView);

        }
        //holder.img.setImageResource(Integer.parseInt(((orderDataList.get(position).getImage()))));
        holder.tvName.setText(orderDataList.get(position).getUserName());
        holder.tv_subhead.setText("Followed by "+orderDataList.get(position).getUserName());

        followStat= Boolean.parseBoolean(orderDataList.get(position).getFollowingStatus());


        holder.tv_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onClickDataCart.addNewClass(orderDataList.get(position).getUserId(), Boolean.parseBoolean(orderDataList.get(position).getFollowingStatus()));
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(orderDataList.get(position).getUserId()));
                context.startActivity(i);
            }
        });

        holder.profile_item_lyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, OtherProfile.class);
                i.putExtra("userid", String.valueOf(orderDataList.get(position).getUserId()));
                context.startActivity(i);
            }
        });

        if(followStat==true){
            holder.tv_follow.setBackgroundResource(R.drawable.blue_round_corner);
            holder.tv_follow.setTextColor(Color.parseColor("#ffffff"));
            holder.tv_follow.setText("Following");
        }
        else {
            holder.tv_follow.setBackgroundResource(R.drawable.white_round);
            holder.tv_follow.setTextColor(Color.parseColor("#818181"));
            holder.tv_follow.setText("Follow");
        }

    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        //return null;
        return exampleFilter;
    }

    private final Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<SearchUserResponse.TagSuggestion> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(productModelListfull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (SearchUserResponse.TagSuggestion item : productModelListfull) {
                    if (item.getUserName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            orderDataList.clear();
            orderDataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public interface OnClickDataCart {

        void  addNewClass(int userid, boolean followStat);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView tvName,tv_subhead,tv_follow;
        LinearLayout profile_item_lyt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.img_profile);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.tv_subhead = itemView.findViewById(R.id.tv_subhead);
            this.tv_follow = itemView.findViewById(R.id.tv_follow);
            this.profile_item_lyt = itemView.findViewById(R.id.profile_item_lyt);

        }
    }
}