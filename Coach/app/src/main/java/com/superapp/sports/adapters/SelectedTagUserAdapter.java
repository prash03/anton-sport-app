package com.superapp.sports.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.superapp.sports.R;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.superapp.sports.activities.CreateNewPost.TAG;

/*public class SelectedTagUserAdapter {
}*/
public class SelectedTagUserAdapter extends RecyclerView.Adapter<SelectedTagUserAdapter.ViewHolder> {
    //ArrayList<Uri> mArrayUri;
    ArrayList<String> selectedNamelist = new ArrayList<String>();
    ArrayList<String> selectedImagelist = new ArrayList<String>();
    JSONObject jsonArray;
    Context context;
    addImagePerItem addImagePerItem;


    public SelectedTagUserAdapter(Context context, ArrayList<String> selectedNamelist,ArrayList<String> selectedImagelist) {

        this.context = context;
        this.selectedNamelist = selectedNamelist;
        this.selectedImagelist = selectedImagelist;

    }

    public void clickItem(addImagePerItem addImagePerItem) {
        this.addImagePerItem = addImagePerItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.tag_itemlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

       /* final String media_pic =(jsonArray.optString("tag_user_image"));
        Log.i(TAG, "onBindViewHolder URi: " + media_pic);

        if (media_pic.equals("")) {
            holder.selected_image.setImageResource(R.mipmap.ic_logos_round);
        }
        else {
            Glide.with(context).load(media_pic).placeholder(R.drawable.dummy_user).centerCrop().into(holder.selected_image);

        }

        holder.taggeduser.setText(jsonArray.optString("tag_user_name"));*/
        final String media_pic =(selectedImagelist.get(position));
        Log.i(TAG, "onBindViewHolder URi: " + media_pic);

        if (media_pic.equals("")) {
            holder.selected_image.setImageResource(R.mipmap.ic_logos_round);
        }
        else {
            Glide.with(context).load(media_pic).placeholder(R.drawable.dummy_user).centerCrop().into(holder.selected_image);

        }

        holder.taggeduser.setText(selectedNamelist.get(position));


        holder.selected_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addImagePerItem.addImageOnClicked(media_pic);
            }
        });

        holder.category_cross_ids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //addImagePerItem.deletImageOnClicked(holder.getAdapterPosition());
                selectedNamelist.remove(position);
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        //Log.i(TAG, "getItemCountValue: " + mArrayUri.size());
        return selectedNamelist.size();
    }

    public interface addImagePerItem {

        void addImageOnClicked(String media_pic);


        void deletImageOnClicked(int position);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView selected_image, category_cross_ids;
        TextView taggeduser;

        public ViewHolder(View itemView) {
            super(itemView);
            selected_image = itemView.findViewById(R.id.imgProfilePic);
            category_cross_ids = itemView.findViewById(R.id.category_cross_ids);
            taggeduser = itemView.findViewById(R.id.tagged_user);

        }
    }
}
