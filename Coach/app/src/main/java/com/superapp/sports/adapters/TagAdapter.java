package com.superapp.sports.adapters;


import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.superapp.sports.R;
import com.superapp.sports.model.TagSuggestionResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder> {
    List<TagSuggestionResponse.TagSuggestion> orderDataList;
    Context context;
    private OnClickDataCart onClickDataCart;


    int check = 0,selectedId=0,deletedId=0;
    String selectedName="",selectectedPic="",deletedName="",deletedPic="";
    boolean ifselected=false;
    ArrayList<Integer> selectedItemid = new ArrayList<>();
    ArrayList<Integer> deletedItemid = new ArrayList<>();
    ArrayList<String> selectectedNamelist = new ArrayList<String>();
    ArrayList<String> deletedNamelist = new ArrayList<String>();

    ArrayList<String> selectectedImagelist = new ArrayList<String>();
    ArrayList<String> deletedImagelist = new ArrayList<String>();
    JSONObject jsonObject;


    // RecyclerView recyclerView;
    public TagAdapter(Context context, List<TagSuggestionResponse.TagSuggestion> listdata) {

        this.orderDataList = listdata;
        this.context=context;
    }
    public void onItemClick(OnClickDataCart onClickDataCart){
        this.onClickDataCart=onClickDataCart;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_tag, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String user_img = String.valueOf(orderDataList.get(position).getTagUserImage());
        if (user_img.equalsIgnoreCase(null)||!user_img.equals(" ")) {
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgPic);
        }

        holder.tvName.setText(orderDataList.get(position).getTagUserName());
        holder.rdButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ifselected=true;
                if(holder.rdButton.isChecked()){
                    selectedId=orderDataList.get(position).getTagUserId();
                    selectedName=orderDataList.get(position).getTagUserName();
                    selectectedPic= String.valueOf(orderDataList.get(position).getTagUserImage());
                    check++;
                    selectedItemid.add(selectedId);
                    //selectectedlist.add(selectedId,selectedName,selectectedPic);
                    selectectedNamelist.add(selectedName);
                    selectectedImagelist.add(selectectedPic);



                /*HashSet<Integer> set = new HashSet<Integer>(selectedItemid);
                selectedItemid = new ArrayList<Integer>(set);
                System.out.println(selectedItemid);*/

                    onClickDataCart.addNewClass(check,selectedItemid,selectectedNamelist,selectectedImagelist);
                    Log.d("selectedidvalue:", String.valueOf(selectedId));
                    Log.d("selectedNamevalue:", String.valueOf(selectedId));
                }
                else{
                    check--;
                    deletedId=holder.rdButton.getImeActionId();
                    deletedName=orderDataList.get(deletedId).getTagUserName();
                    deletedPic= String.valueOf(orderDataList.get(deletedId).getTagUserImage());

                    selectedItemid.remove(deletedId);
                    selectectedNamelist.remove(deletedName);
                    selectectedImagelist.remove(deletedPic);

                    deletedItemid.addAll(selectedItemid);
                    deletedNamelist.addAll(selectectedNamelist);
                    deletedImagelist.addAll(selectectedImagelist);

                    onClickDataCart.addNewClass(check,deletedItemid,deletedNamelist,deletedImagelist);
                    Log.d("deselectedidvalue:", String.valueOf(deletedId));
                    Log.d("deselectedNamevalue:", String.valueOf(deletedName));
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public interface OnClickDataCart {

       // public  void  addNewClass(ArrayList<Integer> userIdSelected);
       void  addNewClass(int numSelected, ArrayList<Integer> playIdSelected, ArrayList<String> selectectedname, ArrayList<String> selectectedimage);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView imgPic;
        public TextView tvName;
        CheckBox rdButton;

        public ViewHolder(View itemView) {
            super(itemView);

            this.imgPic = itemView.findViewById(R.id.imgPic);
            this.tvName = itemView.findViewById(R.id.tvName);
            this.rdButton = itemView.findViewById(R.id.rdButton);
        }
    }
}

