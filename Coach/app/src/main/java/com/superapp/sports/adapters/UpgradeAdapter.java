package com.superapp.sports.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.model.UpgradeResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

/*
public class UpgradeAdapter {
}
*/
public class UpgradeAdapter extends RecyclerView.Adapter<UpgradeAdapter.ViewHolder>{
    List<UpgradeResponse.Datum> orderDataList;
    private final Context context;
    String currentDate="",currentTIme="";


    // RecyclerView recyclerView;
    public UpgradeAdapter(Context context,List<UpgradeResponse.Datum> orderDataList) {
        this.context = context;
        this.orderDataList = orderDataList;
    }
    @Override
    public UpgradeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_upgrade, parent, false);
        UpgradeAdapter.ViewHolder viewHolder = new UpgradeAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UpgradeAdapter.ViewHolder holder, int position) {

        String user_img = String.valueOf(orderDataList.get(position).getImage());
        String upgradeType = String.valueOf(orderDataList.get(position).getSubKey());
        if (!user_img.equals("")) {
            Picasso.get().load(user_img).placeholder(R.mipmap.ic_logos_round).error(R.mipmap.ic_logos_round).into(holder.img);
        }
        holder.tvchat.setText(orderDataList.get(position).getTitle());
        holder.tvpost.setText(orderDataList.get(position).getSubKey());
        holder.tvprice.setText("Starting at $"+orderDataList.get(position).getPrice());

        if(upgradeType.equals("Silver")){
            holder.rlItem.setBackgroundResource(R.drawable.silver_gradient);
            holder.img.setImageResource(R.drawable.silvertroffy);
        }
        else if(upgradeType.equals("Gold")){
            holder.rlItem.setBackgroundResource(R.drawable.gold_gradient);
            holder.img.setImageResource(R.drawable.goldtroffy);
        }
        else {
            holder.rlItem.setBackgroundResource(R.drawable.bronze_gradient);
            holder.img.setImageResource(R.drawable.bronzetroffy);
        }

    }
    @Override
    public int getItemCount() {
        return orderDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView tvchat,tvpost,tvprice,tvpay;
        RelativeLayout rlItem;


        public ViewHolder(View itemView) {
            super(itemView);
            this.img = itemView.findViewById(R.id.imgGold);
            this.tvchat = itemView.findViewById(R.id.tvchat);
            this.tvpost = itemView.findViewById(R.id.tvpost);
            this.tvprice = itemView.findViewById(R.id.tvprice);
            this.tvpay = itemView.findViewById(R.id.tvpay);
            this.rlItem = itemView.findViewById(R.id.rlItem);


        }
    }
}