package com.superapp.sports.appController.network;

import com.superapp.sports.model.AboutResponse;
import com.superapp.sports.model.ChangeNotificationResponse;
import com.superapp.sports.model.ChangePasswordResponse;
import com.superapp.sports.model.ChatListResponse;
import com.superapp.sports.model.CommentListResponse;
import com.superapp.sports.model.CommentPostResponse;
import com.superapp.sports.model.EditProfileResponse;
import com.superapp.sports.model.FollowUserListResponse;
import com.superapp.sports.model.FollowUserResponse;
import com.superapp.sports.model.FollowerUserResponse;
import com.superapp.sports.model.FollowingUserResponse;
import com.superapp.sports.model.ForgotResponse;
import com.superapp.sports.model.HelpResponse;
import com.superapp.sports.model.HomeResponse;
import com.superapp.sports.model.InterestResponse;
import com.superapp.sports.model.LikePostResponse;
import com.superapp.sports.model.LocationListResponse;
import com.superapp.sports.model.LoginActivityResponse;
import com.superapp.sports.model.LoginResponse;
import com.superapp.sports.model.MyNotificationResponse;
import com.superapp.sports.model.MyPlayerResponse;
import com.superapp.sports.model.MyPostResponse;
import com.superapp.sports.model.MyProfileResponse;
import com.superapp.sports.model.MySponserResponse;
import com.superapp.sports.model.PostStatusResponse;
import com.superapp.sports.model.PostYouLikeResponse;
import com.superapp.sports.model.PrivacyResponse;
import com.superapp.sports.model.RegisterResponse;
import com.superapp.sports.model.RemoveFollowerResponse;
import com.superapp.sports.model.ReportResponse;
import com.superapp.sports.model.ReportSubCatResponse;
import com.superapp.sports.model.RequestSponserResponse;
import com.superapp.sports.model.ResetPasswordResponse;
import com.superapp.sports.model.SearchUserResponse;
import com.superapp.sports.model.SendMessageResponse;
import com.superapp.sports.model.SubmitInterestResponse;
import com.superapp.sports.model.SubmitProblemResponse;
import com.superapp.sports.model.TagSuggestionResponse;
import com.superapp.sports.model.UpgradeResponse;
import com.superapp.sports.model.WelcomeResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface Api {


    @GET("api/home_page")
    Call<HomeResponse> getHomeApi(@Header("Authorization") String header);

    @GET("api/chat_list")
    Call<ChatListResponse> chatlist(@Header("Authorization")String authToken,
                                    @Query("user_id_2") String userId);

    @FormUrlEncoded
    @POST("api/post_you_like")
    Call<PostYouLikeResponse> postYouLikeApi(@Header("Authorization")String authToken,
                                             @Field("post_type")String post_type);

    @GET("api/upgrade_profile")
    Call<UpgradeResponse> upgradeListApi(@Header("Authorization")String authToken);



    @GET("api/login_activity_list")
    Call<LoginActivityResponse> loggedinListApi(@Header("Authorization")String authToken);

    @FormUrlEncoded
    @POST("api/post_status")
    Call<PostStatusResponse> statusPost(@Header("Authorization") String authToken,
                                              @Field("post_type")String comment,
                                              @Field("post_id")String postId,
                                              @Field("block_user_id")String block_user_id
                                        );
    @FormUrlEncoded
    @POST("api/comment_reply_post")
    Call<CommentPostResponse> commentPostReply(@Header("Authorization") String authToken,
                                          @Field("comment_text")String comment,
                                          @Field("post_id")int postId,
                                          @Field("comment_id")int comment_id);
    @FormUrlEncoded
    @POST("api/delete_post_own")
    Call<CommentPostResponse> deletePost(@Header("Authorization") String authToken,
                                          @Field("post_id")String comment_id);
    @FormUrlEncoded
    @POST("api/delete_reply")
    Call<CommentPostResponse> deletereply(@Header("Authorization") String authToken,
                                          @Field("reply_id")String comment_id);
    @FormUrlEncoded
    @POST("api/delete_comment")
    Call<CommentPostResponse> deleteComment(@Header("Authorization") String authToken,
                                          @Field("comment_id")String comment_id);
    @FormUrlEncoded
    @POST("api/sponsor_request")
    Call<RequestSponserResponse> sponserRequest(@Header("Authorization") String authToken,
                                                @Field("player_id")int palyerId,
                                                @Field("amount")int amount,
                                                @Field("title")String title,
                                                @Field("desc")String desc);

    @FormUrlEncoded
    @POST("api/other_profile")
    Call<MyProfileResponse> otherProfile(@Header("Authorization") String authToken,
                                          @Field("user_id")int postId);

    @FormUrlEncoded
    @POST("api/comment_post")
    Call<CommentPostResponse> commentPost(@Header("Authorization") String authToken,
                                          @Field("comment_text")String comment,
                                          @Field("post_id")int postId);

    @FormUrlEncoded
    @POST("api/change_notification_status")
    Call<ChangeNotificationResponse> changeNotificationApi(@Header("Authorization") String authToken,
                                                           @Field("notification_status")String notificationStat);

    @FormUrlEncoded
    @POST("api/remove_followers")
    Call<RemoveFollowerResponse> removeFollowerApi(@Header("Authorization") String authToken,
                                                   @Field("user_id")int userid);

    @FormUrlEncoded
    @POST("api/location_list")
    Call<LocationListResponse> locationListApi(@Header("Authorization") String authToken,
                                                @Field("country_name")String countryName,
                                                @Field("country_code")String countryCode,
                                                @Field("search_key")String searchKey);

    @FormUrlEncoded
    @POST("api/following_user_list")
    Call<FollowingUserResponse> followingUserListApi(@Header("Authorization") String header,
                                                     @Field("user_id")String userid);
    @FormUrlEncoded
    @POST("api/followers_user_list")
    Call<FollowerUserResponse> followerUserListApi(@Header("Authorization") String header,
                                                   @Field("user_id")String userid);

    @GET("api/notification_list")
    Call<MyNotificationResponse> notificationListApi(@Header("Authorization") String header);

    @GET("api/my_profile")
    Call<MyProfileResponse> myprofileApi(@Header("Authorization") String header);
    /*@GET("api/my_player")
    Call<MyPlayerResponse> myplayerApi(@Header("Authorization") String header);*/

    @GET("api/sponsor_list")
    Call<MySponserResponse> mysponserPlayerApi(@Header("Authorization") String header);

    @GET("api/help")
    Call<HelpResponse> helpApi(@Header("Authorization") String header);

    @GET("api/report_list")
    Call<ReportResponse> getReportCatApi(@Header("Authorization") String header);

    @FormUrlEncoded
    @POST("api/sub_report_list")
    Call<ReportSubCatResponse> getsubCatReport(@Header("Authorization") String authToken,
                                          @Field("report_id")int id);

    @GET("api/about")
    Call<AboutResponse> aboutApi(@Header("Authorization") String header);

    @GET("api/pravacy_policy")
    Call<PrivacyResponse> privacypolicyApi(@Header("Authorization") String header);

    @GET("api/home_page")
    Call<HomeResponse> homeApi(@Header("Authorization") String header);

    @GET("api/interest_list")
    Call<InterestResponse> interestApi(@Header("Authorization") String header);

    @FormUrlEncoded
    @POST("api/user_search")
    Call<SearchUserResponse> SearchUser(@Header("Authorization") String authToken,
                                        @Field("page_number")int pagenum,
                                        @Field("search_key")String searchKey);

    @FormUrlEncoded
    @POST("api/submit_interest")
    Call<SubmitInterestResponse> submitInterest(@Header("Authorization") String authToken,
                                                @Field("selected_item_id")String itemid);

    @FormUrlEncoded
    @POST("api/change_password")
    Call<ChangePasswordResponse> changePassword(@Header("Authorization") String authToken,
                                                @Field("current_password")String currentpassword,
                                                @Field("new_password")String newpassword);


    @FormUrlEncoded
    @POST("api/like_comment")
    Call<LikePostResponse> likeComment(@Header("Authorization") String authToken,
                                    @Field("comment_id")int commentid,
                                    @Field("user_id")String userid);

    @FormUrlEncoded
    @POST("api/like_reply")
    Call<LikePostResponse> likeReply(@Header("Authorization") String authToken,
                                    @Field("reply_id")String replyid,
                                    @Field("user_id")String userid,
                                    @Field("status")String status,
                                    @Field("comment_id")String commentid);
    @FormUrlEncoded
    @POST("api/unlike_comment")
    Call<LikePostResponse> UnlikeComment(@Header("Authorization") String authToken,
                                       @Field("comment_id")int commentid,
                                       @Field("user_id")String userid);
    @FormUrlEncoded
    @POST("api/like_post")
    Call<LikePostResponse> likePost(@Header("Authorization") String authToken,
                                    @Field("post_id")String postid,
                                    @Field("user_id")String userid);
    @FormUrlEncoded
    @POST("api/unlike_post")
    Call<LikePostResponse> unlikePost(@Header("Authorization") String authToken,
                                    @Field("post_id")String postid,
                                    @Field("user_id")String userid);
    @FormUrlEncoded
    @POST("api/tag_suggestion")
    Call<TagSuggestionResponse> tagSuggestion(@Header("Authorization") String authToken,
                                              @Field("page_number")String pageNum,
                                              @Field("search_key")String searchKey);

    @FormUrlEncoded
    @POST("api/my_post_list")
    Call<MyPostResponse> mypostHome(@Header("Authorization") String authToken,
                                    @Field("page_number")String pagenum);
    @FormUrlEncoded
    @POST("api/follow_user")
    Call<FollowUserResponse> followUser(@Header("Authorization") String authToken,
                                        @Field("user_id")int userid);
    @FormUrlEncoded
    @POST("api/unfollow_user")
    Call<FollowUserResponse> UnfollowUser(@Header("Authorization") String authToken,
                                        @Field("user_id")int userid);

    @FormUrlEncoded
    @POST("api/submit_problem")
    Call<SubmitProblemResponse> submitProblem(@Header("Authorization") String authToken,
                                              @Field("description")String description,
                                              @Field("review_text")String reviewText);


  @FormUrlEncoded
    @POST("api/report_complaint")
    Call<SubmitProblemResponse> submitReport(@Header("Authorization") String authToken,
                                              @Field("report_id")String report_id,
                                              @Field("sub_report_id")String sub_report_id);


    @FormUrlEncoded
    @POST("api/users_list")
    Call<FollowUserListResponse> followingUserList(@Header("Authorization") String authToken,
                                                   @Field("search_key") String searchKey,
                                                   @Field("page_number") String pageKey);

    @GET("api/comment_list")
    Call<CommentListResponse> CommentList(@Header("Authorization") String authToken);


    @GET("api/welcome_list")
    Call<WelcomeResponse> welcomeApi();

    @FormUrlEncoded
    @POST("api/register")
    Call<RegisterResponse> signupapi(@Field("user_types") String userType,
                                     @Field("type")String Logintype,
                                        @Field("first_name")String fname,
                                        @Field("last_name") String lname,
                                        @Field("email_id")String email,
                                        @Field("mobile_num")String mobile,
                                        @Field("password")String password,
                                        @Field("fcm_token")String fcmToken,
                                        @Field("device_type")String deviceType,
                                        @Field("country")String country,
                                        @Field("country_code")String country_code,
                                        @Field("zip")String zip,
                                        @Field("address")String address,
                                        @Field("state")String state,
                                        @Field("city")String city,
                                        @Field("profile_image")String profile_image);
    /*type_of_user , first_name , last_name , email_id , mobile_num , password , confirm_password  , fcm_token , device_type*/

    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponse> loginapi(@Field("email_id") String email,
                                 @Field("password")String password,
                                 @Field("fcm_token")String fcmToken,
                                 @Field("device_type")String deviceType,
                                 @Field("address")String address,
                                 @Field("lat")String lat,
                                 @Field("lon")String lon,
                                 @Field("date_time")String dateTIme);


    @FormUrlEncoded
    @POST("api/forgot_password")
    Call<ForgotResponse> forgotPasswordApi(@Field("email_id")String emailId,
                                           @Field("device_type") String deviceType);

    @FormUrlEncoded
    @POST("api/reset_password")
    Call<ResetPasswordResponse> resetPasswordApi(@Field("otp_code")String otpCode,
                                                 @Field("new_password")String newPassword,
                                                 @Field("confirm_password")String cPass);


   /* @Multipart
    @POST("api/edit_profile")
    Call<EditProfileResponse> editprofile(@Header("Authorization") String header,
                                          @Part("first_name") RequestBody fname,
                                          @Part("last_name") RequestBody lname,
                                          @Part("mobile_num") RequestBody mob,
                                          @Part("user_website") RequestBody website,
                                          @Part("user_bio") RequestBody bio,
                                          @Part("user_DOB") RequestBody dob,
                                          @Part("user_gender") RequestBody gender,
                                          @Part("user_marital_status") RequestBody marital,
                                          @Part("email") RequestBody email,
           //, , , , , , , , , , , , , , , ,                                @Part MultipartBody.Part file);*/
   @Multipart
   @POST("api/edit_profile")
   Call<EditProfileResponse> editprofile(@Header("Authorization") String header,
                                         @Part("first_name") RequestBody fname,
                                         @Part("last_name") RequestBody lname,
                                         @Part("dob") RequestBody dob,
                                         @Part("height") RequestBody height,
                                         @Part("weight") RequestBody weight,
                                         @Part("position") RequestBody position,
                                         @Part("ranking") RequestBody ranking,
                                         @Part("speed") RequestBody speed,
                                         @Part("vertical") RequestBody vertical,
                                         @Part("gpa") RequestBody gpa,
                                         @Part("home_town") RequestBody home_town,
                                         @Part("classification") RequestBody classification,
                                         @Part("school") RequestBody school,
                                         @Part("experience") RequestBody experience,
                                         @Part("sports_name") RequestBody sports_name,
                                         @Part("achievement") RequestBody achievement,
                                         @Part("sponser") RequestBody sponser,
                                         @Part("company_work") RequestBody company_work,
                                         @Part("speciality") RequestBody speciality,
                                         @Part("ppg") RequestBody ppg,
                                         @Part("rpg") RequestBody rpg,
                                         @Part("apg") RequestBody apg,
                                         @Part MultipartBody.Part file);



    @POST("api/create_post")
    Call<ResponseBody> createPost(@Header("Authorization") String header,
                                  @Body MultipartBody file);

  /*  @POST("s3_file_upload/s3_video_upload.php")
    Call<ResponseBody> createPost(@Body MultipartBody file);*/




}
