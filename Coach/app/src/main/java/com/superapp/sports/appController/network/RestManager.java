package com.superapp.sports.appController.network;



import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
    //private static final String baseUrl="http://antwon.marketingchord.com/";
    //private static final String baseUrl="https://apps.supersportselite.com/";
    private static final String baseUrl="https://app.supersportselite.com/";
    private static Retrofit retrofit = null;

    public static Api instanceOf(){
        if (retrofit==null){

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            //interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
           // interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(200, TimeUnit.SECONDS).readTimeout(200,TimeUnit.SECONDS).build();
            retrofit = new Retrofit.Builder().baseUrl(baseUrl).client(client).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit.create(Api.class);
    }

     public static Api instanceOfF(){
        if (retrofit==null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            //interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(200, TimeUnit.SECONDS).readTimeout(200,TimeUnit.SECONDS).build();
            retrofit = new Retrofit.Builder().baseUrl("https://marketingchord.com/").client(client).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit.create(Api.class);
    }




}

