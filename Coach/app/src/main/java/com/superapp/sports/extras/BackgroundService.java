package com.superapp.sports.extras;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.superapp.sports.R;

import java.io.File;

public class BackgroundService extends Service {

    public BackgroundService() {}

    private int DEFAULT_SYNC_INTERVAL = 3 * 5000;
    private Handler mHandler;
    private String timeMilli , videopath;


    @Override
    public void onCreate() {
        super.onCreate();

        mHandler = new Handler();


        //Intent extras = Getin

        // here i'm checking that if android version is grater that 8.0 then it'll start foreground service
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(62318, builtNotification());
        }
    }


    // here i have called handler that will run hit api after every 3 seconds
    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
           // getDataList();

            // Repeat this runnable code block again every ... min
            mHandler.postDelayed(runnableService, DEFAULT_SYNC_INTERVAL);
        }
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mHandler = new Handler();

        mHandler.post(runnableService);
        //  Toast.makeText(this, "Service Created Successfully ", Toast.LENGTH_SHORT).show();


        // here i have called start_sticy that will run continuously  background
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // upload video on the server

   /* private void uploadvideoAWS(){
        ClientConfiguration configuration = new ClientConfiguration();
        configuration.setMaxErrorRetry(3);
        configuration.setConnectionTimeout(501000);
        configuration.setSocketTimeout(501000);
        configuration.setProtocol(Protocol.HTTP);

        AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(
                NK_AWS_ACCESS_KEY, NK_AWS_SECRET_KEY), configuration);
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_2));



//            BasicAWSCredentials credentials = new BasicAWSCredentials(
//                    NK_AWS_ACCESS_KEY,
//                    NK_AWS_SECRET_KEY);
//            AmazonS3Client s3 = new AmazonS3Client(credentials);
//            s3.setRegion(Region.getRegion(Regions.US_EAST_2));

        TransferUtility transferUtility = new TransferUtility(s3Client, BackgroundService.this);

        //You have to pass your file path here.
        File file = new File(videopath);
        if (!file.exists()) {
            Toast.makeText(getApplicationContext(), "File Not Found!", Toast.LENGTH_SHORT).show();
            return;
        }
        TransferObserver observer = transferUtility.upload(
                NK_AWS_BUCKET_NAME,
                "uploads/"+timeMilli+"mp.4",
                file
        );


        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.COMPLETED.equals(observer.getState())) {
                    System.out.println(observer.getKey() + "........File Upload Complete");
                    String url = "https://"+NK_AWS_BUCKET_NAME+".s3.amazonaws.com/" + observer.getKey();
                    System.out.println(url + "........File Upload Complete");
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                System.out.println(bytesTotal + "........bytesTotal");
                System.out.println(bytesCurrent + "........bytesCurrent");
                System.out.println(id + "........id");
            }

            @Override
            public void onError(int id, Exception ex) {
                System.out.println(ex.getMessage() + "........message");
                Toast.makeText(getApplicationContext(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }*/


    /// i have called this coz if user close or kills app then it'll restart service
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
        super.onTaskRemoved(rootIntent);
    }


    // android 8.0 and above version have some restrictions so in these versions
    // does'nt allow to run any task in background
    // so i have created an custom notification that will keep application in foreground in these verions
    // and lower verions we don't need this

    public Notification builtNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;

        NotificationCompat.Builder builder = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("ID", "Name", importance);
            notificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(this, notificationChannel.getId());
        } else {
            builder = new NotificationCompat.Builder(this);
        }

        builder.setDefaults(Notification.DEFAULT_LIGHTS);
        String message = "Service is running in Coach";
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(false)
                .setPriority(Notification.PRIORITY_MAX)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setColor(Color.parseColor("#0f9595"))
                .setContentTitle("Coach")
                .setContentText(message);

        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, launchIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        Notification notification = builder.build();
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        return notification;
    }

}



