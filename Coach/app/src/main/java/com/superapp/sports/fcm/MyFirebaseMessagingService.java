package com.superapp.sports.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.superapp.sports.R;
import com.superapp.sports.activities.Dashboard;
import com.superapp.sports.utils.SharedHelper;

import org.jetbrains.annotations.NotNull;

import static com.superapp.sports.utils.Constants.FCMTOKEN;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(@NotNull String fcmtoken) {
        //SharedHelper.putFcmKey(getApplicationContext(), "fcm_token", fcmtoken);
        SharedHelper.putKey(getApplicationContext(),FCMTOKEN,fcmtoken);

        System.out.println(fcmtoken +".....fcmTOKEN");
        super.onNewToken(fcmtoken);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        System.out.println(remoteMessage.getData() + "........FirebaseNotification");

        String title = remoteMessage.getData().get("msg");


        Intent intent = new Intent(getApplicationContext(), Dashboard.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "default")
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(title)
                .setAutoCancel(true)
                .setSound(null)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManager notificationManager2 = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager2.notify(0, builder.build());//getting the title and the body

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "Default channel", NotificationManager.IMPORTANCE_HIGH);
            notificationManager2.createNotificationChannel(channel);
        }
    }
}

