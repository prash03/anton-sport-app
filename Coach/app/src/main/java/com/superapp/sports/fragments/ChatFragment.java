package com.superapp.sports.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.adapters.ChatListAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.TagSuggestionResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class ChatFragment extends Fragment implements View.OnClickListener{
    public static Fragment newInstance(){return new ChatFragment();}
    View rootView;
    RecyclerView recycleChat,recycleCall;

    ChatListAdapter chatListAdapter;
    TextView tv_chat,tv_call;
    private List<TagSuggestionResponse.TagSuggestion> dataList;
    String searchKey="",pageNum="",authKey="";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.chat_fragment,container,false);

        tv_chat=rootView.findViewById(R.id.tv_chat);
        tv_call=rootView.findViewById(R.id.tv_call);
        recycleChat=rootView.findViewById(R.id.recycleChat);
        recycleCall=rootView.findViewById(R.id.recycleCall);

        recycleChat.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recycleCall.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));

        authKey= SharedHelper.getKey(getActivity(),AUTH_TOKEN);

        /*ChooseInterestResponse[] myListData = new ChooseInterestResponse[] {
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top),
                new ChooseInterestResponse("Eliza Graza", R.drawable.home_top)

        };


        tv_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recycleChat.setVisibility(View.VISIBLE);
                recycleCall.setVisibility(View.GONE);
                tv_chat.setTextColor(Color.parseColor("#1C1D46"));
                tv_chat.setBackgroundResource(R.drawable.white_rect_filled);

                tv_call.setTextColor(Color.parseColor("#ffffff"));
                tv_call.setBackgroundResource(R.drawable.blue_rect_filled);

            }
        });
        tv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recycleChat.setVisibility(View.GONE);
                recycleCall.setVisibility(View.VISIBLE);
                tv_call.setTextColor(Color.parseColor("#1C1D46"));
                tv_call.setBackgroundResource(R.drawable.white_rect_filled);

                tv_chat.setTextColor(Color.parseColor("#ffffff"));
                tv_chat.setBackgroundResource(R.drawable.blue_rect_filled);

            }
        });*/
        calltagListApi();
        return rootView;
    }

    private void calltagListApi() {
        Api api = RestManager.instanceOf();
        Call<TagSuggestionResponse> responseCall = api.tagSuggestion(authKey,pageNum,searchKey);
        responseCall.enqueue(new Callback<TagSuggestionResponse>() {
            @Override
            public void onResponse(Call<TagSuggestionResponse> call, Response<TagSuggestionResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getTagSuggestion();

                        //Toast.makeText(FindFriend.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        chatListAdapter = new ChatListAdapter(getActivity(),dataList);
                        recycleChat.setAdapter(chatListAdapter);

                       /* callAdapter = new CallAdapter(dataList,getActivity());
                        recycleCall.setAdapter(callAdapter);*/

                        /*tagAdapter.onItemClick(new TagAdapter.OnClickDataCart() {
                            @Override
                            public void addNewClass(int numSelected, ArrayList<Integer> playIdSelected) {
                                selectedItemid=playIdSelected;
                                Log.d("selectedItemid:",selectedItemid.toString());
                            }
                        });*/

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<TagSuggestionResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
