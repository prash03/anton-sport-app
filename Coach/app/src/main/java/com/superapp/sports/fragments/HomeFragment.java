package com.superapp.sports.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.superapp.sports.R;
import com.superapp.sports.activities.FindFriend;
import com.superapp.sports.activities.CreateNewPost;
import com.superapp.sports.activities.FollwerActivity;
import com.superapp.sports.activities.ReportActivity;
import com.superapp.sports.adapters.HomePostAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.CommentPostResponse;
import com.superapp.sports.model.FollowUserResponse;
import com.superapp.sports.model.HomeCatAdapter;
import com.superapp.sports.model.HomeResponse;
import com.superapp.sports.model.LikePostResponse;
import com.superapp.sports.model.PostStatusResponse;
import com.superapp.sports.model.RemoveFollowerResponse;
import com.superapp.sports.utils.Constants;
import com.superapp.sports.utils.DeliverItToApplication;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.NK_PAGE_NUM;
import static com.superapp.sports.utils.Constants.NK_PLAYSTORE_URL;
import static com.superapp.sports.utils.Constants.USERID;
import static com.superapp.sports.utils.DeliverItToApplication.trimMessage;

//import org.linphone.core.Core;
//import org.linphone.core.Factory;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private static final int FINDFRIEND = 200;
    View rootView;
    RecyclerView recycleHomeTop, recyclePost;
    HomeCatAdapter homeCatAdapter;
    HomePostAdapter homePostAdapter;
    TextView tv_heading;
    ImageView imgPost;
    CircleImageView imgProfilePic;
    RelativeLayout create_post_lyt, ll_top;
    String authKey = "", postid = "";
    String profilePic = "", userid = "";
    ArrayList<String> imageArr = new ArrayList<>();
    JSONArray otherUser, SuggestArray;
    private ProgressDialog progressBar;
    private SwipeRefreshLayout swipe_layout;
    JSONArray userpostsArr = new JSONArray();
    JSONArray newuserPostArr = new JSONArray();
    NestedScrollView nscroll;
    int page = 1, pagelimit=10;


    public static Fragment newInstance() {
        return new HomeFragment();
    }


    @SuppressLint("WrongViewCast")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.home_fragment, container, false);

        recycleHomeTop = rootView.findViewById(R.id.recycleHomeTop);
        nscroll = rootView.findViewById(R.id.nested_scrollview);
        swipe_layout = rootView.findViewById(R.id.swipe_layout);
        recyclePost = rootView.findViewById(R.id.rvpostlist);
        imgProfilePic = rootView.findViewById(R.id.imgProfilePic);
        imgPost = rootView.findViewById(R.id.imgPost);
        create_post_lyt = rootView.findViewById(R.id.create_post_lyt);
        ll_top = rootView.findViewById(R.id.ll_top);
        tv_heading = rootView.findViewById(R.id.tv_heading);

        recycleHomeTop.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        recycleHomeTop.setHasFixedSize(true);
        recycleHomeTop.setNestedScrollingEnabled(false);

        recyclePost.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recyclePost.setHasFixedSize(true);
        recyclePost.setNestedScrollingEnabled(false);


        imgPost.setOnClickListener(this::onClick);
        create_post_lyt.setOnClickListener(this::onClick);
        authKey = SharedHelper.getKey(getActivity(), AUTH_TOKEN);
        userid = SharedHelper.getKey(getActivity(), USERID);
        tv_heading.setText("Home");

        Log.d("userIdLogin", userid);


       /* Factory.instance().setDebugMode(true, "Linphone");
        Core core = Factory.instance().createCore(null, null, this.getActivity());
        core.start();*/
        profilePic = SharedHelper.getKey(getActivity(), Key_profile_image);
        if (!profilePic.equalsIgnoreCase("")) {
            Picasso.get().load(profilePic).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(imgProfilePic);
        }

        //callHomeApi(authKey);

        swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                userpostsArr = new JSONArray();
                newuserPostArr = new JSONArray();
                page = 1;
                pagelimit = 10;
                getHomeDataApi(0, 0);
            }
        });


        // calling a method to load our api.
        getHomeDataApi(page, pagelimit);


        // adding on scroll change listener method for our nested scroll view.
        nscroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                // on scroll change we are checking when users scroll as bottom.
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    // in this method we are incrementing page number,
                    // making progress bar visible and calling get data method.
                    page++;
                    getHomeDataApi(page, pagelimit);
                }
            }
        });


        return rootView;
    }


    public void getHomeDataApi(int page, int limit) {

        if (page > limit) {
            // checking if the page number is greater than limit.
            // displaying toast message in this case when page>limit.
            Toast.makeText(getActivity(), "That's all the data..", Toast.LENGTH_SHORT).show();
            return;
        }
        // creating a string variable for url .
        String url = Constants.RV_DASHBOARD_PAGE_URL + NK_PAGE_NUM + page;

        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading ........");
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response != null && response.length() > 0) {
                            if (response.optString("status").equalsIgnoreCase("1")) {
                                System.out.println(response.toString() + "....homedata");
                                JSONObject jsonObject = response.optJSONObject("data");
                                progressDialog.dismiss();
                                swipe_layout.setRefreshing(false);

                                userpostsArr = jsonObject.optJSONArray("user_post");
                                otherUser = jsonObject.optJSONArray("other_users");
                                SuggestArray = jsonObject.optJSONArray("suggested_data");
                                pagelimit = response.optInt("No_of_page");

                                try {
                                    for (int i = 0; i < userpostsArr.length(); i++) {
                                        JSONObject jjj = userpostsArr.getJSONObject(i);
                                        newuserPostArr.put(jjj);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                               // System.out.println(newuserPostArr.length() + ".......nwLength");
                                homeCatAdapter = new HomeCatAdapter(getActivity(), otherUser);
                                recycleHomeTop.setAdapter(homeCatAdapter);

                                homePostAdapter = new HomePostAdapter(getActivity(), newuserPostArr, SuggestArray, Integer.parseInt(userid),
                                        new HomePostAdapter.ClickItem() {
                                            @Override
                                            public void clickLikebtn(String postID) {
                                                postid = postID;
                                                likepostApi(authKey, postID, userid);
                                            }

                                            @Override
                                            public void unLikeClickbtn(String postID) {
                                                postid = postID;
                                                unlikepostApi(authKey, postID, userid);
                                            }
                                        });


                                recyclePost.setAdapter(homePostAdapter);
                                //recyclePost.clearFocus();

                                homePostAdapter.onClickSeeAll(new HomePostAdapter.OnClickSeeAll() {
                                    @Override
                                    public void addNewClass(String userId) {
                                        Intent i = new Intent(getActivity(), FindFriend.class);
                                        i.putExtra("ISFROM", "home");
                                        startActivityForResult(i, FINDFRIEND);
                                    }
                                });

                                homePostAdapter.onClickFollow(new HomePostAdapter.OnClickFollow() {
                                    @Override
                                    public void addNewClass(String userId) {
                                        String USERID = userId;
                                        callFollowApi(Integer.parseInt(USERID));
                                    }
                                });

                                homePostAdapter.onClicShare(new HomePostAdapter.OnClickShare() {
                                    @Override
                                    public void addNewClass(String username, String postImage) {

                                        System.out.println(postImage.trim() + ".....post");
                                        System.out.println(username + ".....post");

                                        try {
                                            Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                            shareIntent.setType("text/plain");
                                            shareIntent.putExtra(Intent.EXTRA_TEXT, username + "\n" + postImage.trim() +"\n"+ NK_PLAYSTORE_URL+ "\n\n");
                                            startActivity(Intent.createChooser(shareIntent, "Share via"));
                                        } catch (Exception e) {
                                            //e.toString();
                                        }

                                    }
                                });

                                homePostAdapter.onItemClick(new HomePostAdapter.OnClickMore() {
                                    @Override
                                    public void addNewClass(String deleteUserId, String postid ,String click_type) {


                                        Log.d("valueOfUserId:=>", deleteUserId + "," + postid);


                                        if(click_type.equalsIgnoreCase("Delete")){
                                            DeletePost(authKey, postid);
                                        }else if(click_type.equalsIgnoreCase("Hide")){
                                            ChangePostStatus(authKey, "hide", postid, deleteUserId);
                                        }else if(click_type.equalsIgnoreCase("Unfollow")){
                                            callRemoveFollower(Integer.parseInt(deleteUserId));
                                        }else if(click_type.equalsIgnoreCase("Report")){
                                            Intent intent = new Intent(getActivity(), ReportActivity.class);
                                            startActivity(intent);
                                        }else if(click_type.equalsIgnoreCase("Block")){
                                            ChangePostStatus(authKey, "block", postid, deleteUserId);
                                        }else if(click_type.equalsIgnoreCase("Share")){
                                            try {
                                                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                                shareIntent.setType("text/plain");
                                                //shareIntent.putExtra(Intent.EXTRA_TEXT, "Super Sports is recognized as the growing startup in India. We are a mobile marketplace for local services. " + "\n" + "https://play.google.com/store/apps/details?id=com.anbshopping" + "\n");
                                                shareIntent.putExtra(Intent.EXTRA_TEXT, "Super Sports is recognized as the growing startup . We are a Sport college organization +\n"+ NK_PLAYSTORE_URL+"\n\n");
                                                startActivity(Intent.createChooser(shareIntent, "Share via"));
                                            } catch (Exception e) {
                                                //e.toString();
                                            }
                                        }

                                        //final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.DialogSlideAnim);

                                        /*final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(), R.style.AppBottomSheetDialogTheme);
                                        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        View view1 = inflater.inflate(R.layout.dialog_more, null);
                                        getActivity().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                        bottomSheetDialog.setContentView(view1);
                                        bottomSheetDialog.setCancelable(true);
                                        bottomSheetDialog.show();
                                        bottomSheetDialog.setDismissWithAnimation(true);
                                        LinearLayout llSheet = view1.findViewById(R.id.llSheet);
                                        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(llSheet);
                                        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                                            @Override
                                            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                                                if(bottomSheetDialog.isShowing()){
                                                    bottomSheetDialog.dismiss();
                                                }
                                            }

                                            @Override
                                            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                                                if(bottomSheetDialog.isShowing()){
                                                    bottomSheetDialog.dismiss();
                                                }
                                            }
                                        });

                                        TextView tvDeletePost = view1.findViewById(R.id.tvDeletePost);
                                        TextView tvShare = view1.findViewById(R.id.tvShare);
                                        TextView tvBlock = view1.findViewById(R.id.tvBlock);
                                        TextView tvReport = view1.findViewById(R.id.tvReport);
                                        View vv = view1.findViewById(R.id.vv);
                                        View viewBlock = view1.findViewById(R.id.viewBlock);
                                        View viewhide = view1.findViewById(R.id.viewhide);
                                        tvBlock.setVisibility(View.GONE);
                                        viewBlock.setVisibility(View.GONE);
                                        TextView tvhide = view1.findViewById(R.id.tvhide);

                                        if (deleteUserId.equals(userid)) {
                                            tvDeletePost.setVisibility(View.VISIBLE);
                                            tvBlock.setVisibility(View.GONE);
                                            tvhide.setVisibility(View.GONE);
                                            viewhide.setVisibility(View.GONE);
                                            viewBlock.setVisibility(View.GONE);
                                            vv.setVisibility(View.VISIBLE);
                                        } else {
                                            tvDeletePost.setVisibility(View.GONE);
                                            tvBlock.setVisibility(View.VISIBLE);
                                            tvhide.setVisibility(View.VISIBLE);
                                            viewhide.setVisibility(View.VISIBLE);
                                            viewBlock.setVisibility(View.VISIBLE);
                                            vv.setVisibility(View.GONE);
                                        }
                                        tvShare.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                try {
                                                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                                    shareIntent.setType("text/plain");
                                                    //shareIntent.putExtra(Intent.EXTRA_TEXT, "Super Sports is recognized as the growing startup in India. We are a mobile marketplace for local services. " + "\n" + "https://play.google.com/store/apps/details?id=com.anbshopping" + "\n");
                                                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Super Sports is recognized as the growing startup . We are a Sport college organization +\n"+ NK_PLAYSTORE_URL+"\n\n");
                                                    startActivity(Intent.createChooser(shareIntent, "Share via"));
                                                } catch (Exception e) {
                                                    //e.toString();
                                                }
                                            }
                                        });
                                        tvDeletePost.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                DeletePost(authKey, postDeleteId);
                                                bottomSheetDialog.dismiss();
                                            }
                                        });
                                        tvhide.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                ChangePostStatus(authKey, "hide", postDeleteId, deleteUserId);
                                                bottomSheetDialog.dismiss();
                                            }
                                        });
                                        tvBlock.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                               // ChangePostStatus(authKey, "block", postDeleteId, deleteUserId);

                                                callRemoveFollower(Integer.parseInt(deleteUserId));
                                                bottomSheetDialog.dismiss();
                                            }
                                        });
                                        tvReport.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                *//*open Report Activity*//*
                                                Intent intent = new Intent(getActivity(), ReportActivity.class);
                                                startActivity(intent);
                                                bottomSheetDialog.dismiss();
                                            }
                                        });*/
                                    }
                                });
                                //sliderMethod();
                            } else {
                                displayMessage(response.optString("Message"));
                                progressDialog.dismiss();
                            }

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            displayMessage(getString(R.string.something_went_wrong));
                            progressDialog.dismiss();
                        } else if (response.statusCode == 401) {

                        } else if (response.statusCode == 422) {

                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                displayMessage(json);
                                progressDialog.dismiss();
                            } else {
                                displayMessage(getString(R.string.please_try_again));
                                progressDialog.dismiss();
                            }

                        } else {
                            displayMessage(getString(R.string.please_try_again));
                            progressDialog.dismiss();
                        }

                    } catch (Exception e) {
                        displayMessage(getString(R.string.something_went_wrong));
                        progressDialog.dismiss();
                    }

                } else {
                    progressDialog.dismiss();
                    if (error instanceof NoConnectionError) {
                        displayMessage(getString(R.string.oops_connect_your_internet));
                    } else if (error instanceof NetworkError) {
                        displayMessage(getString(R.string.oops_connect_your_internet));
                    } else if (error instanceof TimeoutError) {

                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", authKey);
                System.out.println(headers + "...headers");
                return headers;
            }
        };

        DeliverItToApplication.getInstance().addToRequestQueue(jsonObjectRequest);
        Log.i(TAG, "home api url : " + jsonObjectRequest.getUrl());

    }


    @Override
    public void onResume() {
        super.onResume();
        //getHomeDataApi();
    }

    public void displayMessage(String toastString) {
        Snackbar.make(getActivity().findViewById(android.R.id.content), toastString, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgPost:
            case R.id.create_post_lyt:
                Intent i = new Intent(getActivity(), CreateNewPost.class);
                startActivity(i);
                break;
        }
    }


    private void likepostApi(String authToken, String postid, String userid) {
        progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.likePost(authToken, postid, userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                progressBar.dismiss();

                if (response != null) {
                    if (response.body().getStatus() == 1) {
                        page = 1;
                        pagelimit = 10;
                        userpostsArr = new JSONArray();
                        newuserPostArr = new JSONArray();
                        getHomeDataApi(page, pagelimit);
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                progressBar.dismiss();
            }
        });
    }


    private void unlikepostApi(String authToken, String postid, String userid) {
        progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.unlikePost(authToken, postid, userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                progressBar.dismiss();
                System.out.println(response.body().getStatus() + ".....sssss");
                System.out.println(response.body() + ".....sssss");

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        page = 1;
                        pagelimit = 10;
                        newuserPostArr = new JSONArray();
                        getHomeDataApi(page, pagelimit);
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                progressBar.dismiss();
            }
        });
    }

    private void callFollowApi(int uid) {
        progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<FollowUserResponse> responseCall = api.followUser(authKey, uid);
        responseCall.enqueue(new Callback<FollowUserResponse>() {
            @Override
            public void onResponse(Call<FollowUserResponse> call, Response<FollowUserResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        page = 1;
                        pagelimit = 10;
                        newuserPostArr = new JSONArray();
                        getHomeDataApi(page, pagelimit);

                    } else if (response.body().getStatus() == 0) {

                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    private void DeletePost(String authKey, String postid) {

        Api api = RestManager.instanceOf();
        Call<CommentPostResponse> responseCall = api.deletePost(authKey, postid);
        responseCall.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        page = 1;
                        pagelimit = 10;
                        newuserPostArr = new JSONArray();
                        getHomeDataApi(page, pagelimit);
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void ChangePostStatus(String authKey, String postType, String postid, String userid) {

        Api api = RestManager.instanceOf();
        Call<PostStatusResponse> responseCall = api.statusPost(authKey, postType, postid, userid);
        responseCall.enqueue(new Callback<PostStatusResponse>() {
            @Override
            public void onResponse(Call<PostStatusResponse> call, Response<PostStatusResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        page = 1;
                        pagelimit = 10;
                        newuserPostArr = new JSONArray();
                        getHomeDataApi(page, pagelimit);
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<PostStatusResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void callRemoveFollower(int userid) {
        progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();
        Api api = RestManager.instanceOf();
        Call<RemoveFollowerResponse> responseCall = api.removeFollowerApi(authKey,userid);
        responseCall.enqueue(new Callback<RemoveFollowerResponse>() {
            @Override
            public void onResponse(Call<RemoveFollowerResponse> call, Response<RemoveFollowerResponse> response) {
                progressBar.dismiss();

                if (response != null) {
                    if (response.body().getStatus()==1) {
                        page = 1;
                        pagelimit = 10;
                        userpostsArr = new JSONArray();
                        newuserPostArr = new JSONArray();
                        getHomeDataApi(page, pagelimit);
                    } else if (response.body().getStatus()==0) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<RemoveFollowerResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                progressBar.dismiss();
            }
        });
    }

}


