package com.superapp.sports.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.superapp.sports.R;
import com.superapp.sports.activities.CreateNewPost;
import com.superapp.sports.activities.FollwerActivity;
import com.superapp.sports.adapters.FollwerAdapter;
import com.superapp.sports.adapters.HomePostAdapter;
import com.superapp.sports.adapters.NewHomePostAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.CommentPostResponse;
import com.superapp.sports.model.FollowUserResponse;
import com.superapp.sports.model.FollowerUserResponse;
import com.superapp.sports.model.HomeCatAdapter;
import com.superapp.sports.model.HomeResponse;
import com.superapp.sports.model.LikePostResponse;
import com.superapp.sports.model.PostStatusResponse;
import com.superapp.sports.utils.SharedHelper;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.USERID;
import static com.superapp.sports.utils.DeliverItToApplication.TAG;

public class NewHomeFragment extends Fragment implements View.OnClickListener {

    View rootView;
    RecyclerView recycleHomeTop, recyclePost;
    HomeCatAdapter homeCatAdapter;
    HomePostAdapter homePostAdapter;
    TextView tv_heading;
    ImageView imgPost;
    CircleImageView imgProfilePic;
    RelativeLayout create_post_lyt,ll_top;
    String authKey = "", postid = "";
    int userId;
    String profilePic = "", userid = "";
    private ProgressDialog progressBar;
    private SwipeRefreshLayout swipe_layout;
    NewHomePostAdapter newHomePostAdapter;

    public static Fragment newInstance() {
        return new NewHomeFragment();
    }

    @SuppressLint("WrongViewCast")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.home_fragment, container, false);
        recycleHomeTop = rootView.findViewById(R.id.recycleHomeTop);
        //nscroll = rootView.findViewById(R.id.nscroll);
        swipe_layout = rootView.findViewById(R.id.swipe_layout);
        recyclePost = rootView.findViewById(R.id.rvpostlist);
        imgProfilePic = rootView.findViewById(R.id.imgProfilePic);
        imgPost = rootView.findViewById(R.id.imgPost);
        create_post_lyt = rootView.findViewById(R.id.create_post_lyt);
        ll_top = rootView.findViewById(R.id.ll_top);
        tv_heading = rootView.findViewById(R.id.tv_heading);

        recycleHomeTop.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        recycleHomeTop.setHasFixedSize(true);
        recycleHomeTop.setNestedScrollingEnabled(false);

        recyclePost.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recyclePost.setHasFixedSize(true);
        recyclePost.setNestedScrollingEnabled(false);


        imgPost.setOnClickListener(this::onClick);
        create_post_lyt.setOnClickListener(this::onClick);
        authKey = SharedHelper.getKey(getActivity(), AUTH_TOKEN);
        userid = SharedHelper.getKey(getActivity(), USERID);
        tv_heading.setText("Home");

        Log.d("userIdLogin",userid);
        Log.d("authKey Token:",authKey);


        profilePic = SharedHelper.getKey(getActivity(), Key_profile_image);
        if (!profilePic.equalsIgnoreCase("")) {
            Picasso.get().load(profilePic).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(imgProfilePic);
        }


        swipe_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHomeDataApi();
            }
        });


        getHomeDataApi();


        return rootView;
    }


    public void getHomeDataApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Loading ........");
        progressDialog.show();

        Api api = RestManager.instanceOf();
        Call<HomeResponse> responseCall = api.getHomeApi(authKey);
        responseCall.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                if (response != null) {
                    progressDialog.dismiss();
                    if (response.body().getStatus()==1) {

                        swipe_layout.setRefreshing(false);

                      /*  homeCatAdapter = new HomeCatAdapter(getActivity(), response.body().getData().getOtherUsers());
                        recycleHomeTop.setAdapter(homeCatAdapter);

                        Log.i(TAG, "onResponse post Size: " +response.body().getData().getUserPost().size() );
                        newHomePostAdapter = new NewHomePostAdapter(getActivity(), response.body().getData().getUserPost(),response.body().getData().getSuggestedData());
                        recyclePost.setAdapter(homePostAdapter);*/

                    } else if (response.body().getStatus()==0) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.i(TAG, "Home api Failure: "+ t.getMessage());
            }
        });

    }

    public void displayMessage(String toastString) {
        Snackbar.make(getActivity().findViewById(android.R.id.content), toastString, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgPost:
                break;
            case R.id.create_post_lyt:
                Intent i = new Intent(getActivity(), CreateNewPost.class);
                startActivity(i);
                break;
        }
    }



    private void likepostApi(String authToken, String postid, String userid) {
        /*progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.likePost(authToken, postid, userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        getHomeDataApi();
                        /*Intent i=new Intent(getActivity(), SecurityScreen.class);
                        startActivity(i);*/

                    } else if (response.body().getStatus() == 0) {

                        //Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        unlikepostApi(authKey, postid, userid);
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void unlikepostApi(String authToken, String postid, String userid) {
       /* progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<LikePostResponse> responseCall = api.unlikePost(authToken, postid, userid);
        responseCall.enqueue(new Callback<LikePostResponse>() {
            @Override
            public void onResponse(Call<LikePostResponse> call, Response<LikePostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        getHomeDataApi();
                        /*Intent i=new Intent(getActivity(), SecurityScreen.class);
                        startActivity(i);*/

                    } else if (response.body().getStatus() == 0) {

                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<LikePostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                progressBar.dismiss();
            }
        });
    }

    private void callFollowApi(int uid) {
        progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<FollowUserResponse> responseCall = api.followUser(authKey, uid);
        responseCall.enqueue(new Callback<FollowUserResponse>() {
            @Override
            public void onResponse(Call<FollowUserResponse> call, Response<FollowUserResponse> response) {
                progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        getHomeDataApi();

                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserResponse> call, Throwable t) {
                progressBar.dismiss();
            }
        });
    }

    private void DeletePost(String authKey, String postid) {

        Api api = RestManager.instanceOf();
        Call<CommentPostResponse> responseCall = api.deletePost(authKey, postid);
        responseCall.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, Response<CommentPostResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getHomeDataApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

    private void ChangePostStatus(String authKey,String postType, String postid,String userid) {

        Api api = RestManager.instanceOf();
        Call<PostStatusResponse> responseCall = api.statusPost(authKey, postType,postid,userid);
        responseCall.enqueue(new Callback<PostStatusResponse>() {
            @Override
            public void onResponse(Call<PostStatusResponse> call, Response<PostStatusResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus() == 1) {
                        //Toast.makeText(CommentActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        getHomeDataApi();
                    } else if (response.body().getStatus() == 0) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<PostStatusResponse> call, Throwable t) {
                Log.d("errorRes:", t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

}


