package com.superapp.sports.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.superapp.sports.adapters.NotificationAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.MyNotificationResponse;
import com.superapp.sports.utils.SharedHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.AUTH_TOKEN;

public class NotificationFragment extends Fragment {
    View rootView;
    NotificationAdapter notificationAdapter;
    RecyclerView recycleNotification;
    String authKey="";
    private List<MyNotificationResponse.Datum> dataList;
    RelativeLayout errorLayout;

    public static Fragment newInstance(){return new NotificationFragment(); }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       rootView=inflater.inflate(R.layout.fragment_notification,container,false);

        authKey= SharedHelper.getKey(getActivity(),AUTH_TOKEN);

        errorLayout=rootView.findViewById(R.id.errorLayout);
        recycleNotification=rootView.findViewById(R.id.recycleNotification);
        recycleNotification.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));


        /*NotificationResponse[] notifList=new NotificationResponse[]{
                new NotificationResponse(R.drawable.notification,"Laura Marvel (Admin)", "Lorem Ipsum is simply dummy text....","Feb 09"),
                new NotificationResponse(R.drawable.notification,"Laura Marvel (Admin)", "Lorem Ipsum is simply dummy text....","Feb 09"),
                new NotificationResponse(R.drawable.notification,"Laura Marvel (Admin)", "Lorem Ipsum is simply dummy text....","Feb 09"),
                new NotificationResponse(R.drawable.notification,"Laura Marvel (Admin)", "Lorem Ipsum is simply dummy text....","Feb 09"),
                new NotificationResponse(R.drawable.notification,"Laura Marvel (Admin)", "Lorem Ipsum is simply dummy text....","Feb 09")
        };
        notificationAdapter=new NotificationAdapter(getActivity(),notifList);
        recycleNotification.setAdapter(notificationAdapter);*/
        callNotificationApi();

        return rootView;
    }

    private void callNotificationApi() {
        /*progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<MyNotificationResponse> responseCall = api.notificationListApi(authKey);
        responseCall.enqueue(new Callback<MyNotificationResponse>() {
            @Override
            public void onResponse(Call<MyNotificationResponse> call, Response<MyNotificationResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        dataList = response.body().getData();

                        /*
                        notificationAdapter = new NotificationAdapter(getActivity(), dataList);
                        recycleNotification.setAdapter(notificationAdapter);*/

                        if(dataList.size()>0){
                            notificationAdapter = new NotificationAdapter(getActivity(), dataList);
                            recycleNotification.setAdapter(notificationAdapter);

                            errorLayout.setVisibility(View.GONE);
                        }
                        else {
                            recycleNotification.setVisibility(View.GONE);
                            errorLayout.setVisibility(View.VISIBLE);
                        }


                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<MyNotificationResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }
}
