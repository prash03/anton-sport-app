package com.superapp.sports.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.superapp.sports.R;
import com.superapp.sports.adapters.SearchUserAdapter;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.FollowUserResponse;
import com.superapp.sports.model.SearchUserResponse;
import com.superapp.sports.utils.DeliverItToApplication;
import com.superapp.sports.utils.SharedHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.DeliverItToApplication.trimMessage;

public class SearchFragment extends Fragment {
    RecyclerView recycleSearch;
    SearchUserAdapter searchUserAdapter;
    String authKey="",searchKey="";
    int pageNum=1;
    int userId=0;
    boolean followStatus;
    private List<SearchUserResponse.TagSuggestion> dataList = new ArrayList<>();
    EditText ed_search;
    private static final int PAGE_START = 0;
    private final int currentPage = PAGE_START;
    JSONArray UserDataArray;
    boolean flag = false;
    private boolean isLastpage = false;
    //NestedScrollView nestedScroll;
    LinearLayoutManager linearLayoutManager;

    public static Fragment newInstance() {
        return new SearchFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        authKey= SharedHelper.getKey(getActivity(),AUTH_TOKEN);

        //nestedScroll=root.findViewById(R.id.nestedScroll);
        ed_search=root.findViewById(R.id.ed_search);
        recycleSearch=root.findViewById(R.id.recycleFind);
        linearLayoutManager=new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
        recycleSearch.setLayoutManager(linearLayoutManager);
        //recycleSearch.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recycleSearch.setHasFixedSize(true);
        ed_search.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //searchUserAdapter.getFilter().filter(editable);
                String search_query= ed_search.getText().toString().trim();
                if(editable.length()>=3){
                callSearchUserApi(authKey,currentPage,search_query);
                }
            }
        });


        /*nestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    int currentItems = linearLayoutManager.getChildCount();
                    int scrolledItems = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                    int totalItems = linearLayoutManager.getItemCount();
                    if(currentItems + scrolledItems == totalItems)
                    {
                        //isLastpage = false;
                        isLoading = true;
                        currentPage += 1;

                        callSearchUserApi(authKey,currentPage,ed_search.getText().toString()); }
                    else{
                        // isLastpage = true;
                        currentPage -= 1;
                        isLoading = false;
                        Toast.makeText(getActivity(), "That's all the data..", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });*/




        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        callSearchUserApi(authKey,currentPage,ed_search.getText().toString().trim());
    }

    private void callSearchUserApi(String authkey, int page, String searchKey) {
        ProgressDialog progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();

        Api api = RestManager.instanceOf();
        Call<SearchUserResponse> responseCall = api.SearchUser(authkey,page,searchKey);
        responseCall.enqueue(new Callback<SearchUserResponse>() {
            @Override
            public void onResponse(Call<SearchUserResponse> call, Response<SearchUserResponse> response) {

                if (response != null) {
                    dataList.clear();
                    progressBar.dismiss();
                    if (response.body().getStatus()==1) {
                        dataList = response.body().getTagSuggestion();
                        Log.i(TAG, "onResponsedataList: " + dataList.size());


                        try {
                            JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                           // SharedHelper.putKey(getActivity(), "search_Data",jsonObject.getJSONArray("tag_suggestion").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try
                        {
                            if (dataList.size() > 0) {
                                searchUserAdapter = new SearchUserAdapter(getActivity(), dataList);
                                recycleSearch.setAdapter(searchUserAdapter);
                                searchUserAdapter.notifyDataSetChanged();

                            } else {
                               // Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }

                            if (flag) {
                                if (dataList.size() < 10) {
                                    isLastpage = true;
                                }
                            }

                            searchUserAdapter.onItemClick(new SearchUserAdapter.OnClickDataCart() {
                                @Override
                                public void addNewClass(int userid, boolean followStat) {
                                    userId=userid;
                                    followStatus=followStat;

                                    if(followStatus==false){
                                        callFollowApi();
                                    }
                                    else {
                                        callUnFollowApi();
                                    }
                                }
                            });
                        }
                        catch (IllegalStateException | JsonSyntaxException exception)
                        {

                            progressBar.dismiss();
                            exception.printStackTrace();
                            Log.i(TAG, "onResponse exception: "+exception.getMessage());
                        }

                    } else if (response.body().getStatus()==0) {
                        progressBar.dismiss();
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        progressBar.dismiss();
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<SearchUserResponse> call, Throwable t) {
                progressBar.dismiss();
                Log.i(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    // Follow Uer api
    private void callFollowApi() {
       /* progressBar = new ProgressDialog(getActivity());
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<FollowUserResponse> responseCall = api.followUser(authKey,userId);
        responseCall.enqueue(new Callback<FollowUserResponse>() {
            @Override
            public void onResponse(Call<FollowUserResponse> call, Response<FollowUserResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        callSearchUserApi(authKey,currentPage,ed_search.getText().toString());
                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }

    // Unfollow user
    private void callUnFollowApi() {
        /*progressBar = new ProgressDialog(FindFriend.this);
        progressBar.setMessage("Please Wait...");
        progressBar.show();*/

        Api api = RestManager.instanceOf();
        Call<FollowUserResponse> responseCall = api.UnfollowUser(authKey,userId);
        responseCall.enqueue(new Callback<FollowUserResponse>() {
            @Override
            public void onResponse(Call<FollowUserResponse> call, Response<FollowUserResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        callSearchUserApi(authKey,currentPage,ed_search.getText().toString());
                    } else if (response.body().getStatus()==0) {

                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<FollowUserResponse> call, Throwable t) {
                //progressBar.dismiss();
            }
        });
    }
}
