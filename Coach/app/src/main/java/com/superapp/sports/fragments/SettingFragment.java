package com.superapp.sports.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.FacebookSdk;
import com.superapp.sports.R;
import com.superapp.sports.activities.AboutActivity;
import com.superapp.sports.activities.AccountScreen;
import com.superapp.sports.activities.HelpActivity;
import com.superapp.sports.activities.LoginActivity;
import com.superapp.sports.activities.MessageScreen;
import com.superapp.sports.activities.MyPlayer;
import com.superapp.sports.activities.MyProfile;
import com.superapp.sports.activities.MyProfilePost;
import com.superapp.sports.activities.PrivacyPolicy;
import com.superapp.sports.activities.RequestPay;
import com.superapp.sports.activities.SecurityScreen;
import com.superapp.sports.activities.SponsorsAct;
import com.superapp.sports.appController.network.Api;
import com.superapp.sports.appController.network.RestManager;
import com.superapp.sports.model.ChangeNotificationResponse;
import com.superapp.sports.utils.SharedHelper;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.superapp.sports.utils.Constants.ACCOUNTTYPE;
import static com.superapp.sports.utils.Constants.AUTH_TOKEN;
import static com.superapp.sports.utils.Constants.FNAME;
import static com.superapp.sports.utils.Constants.Key_profile_image;
import static com.superapp.sports.utils.Constants.LNAME;
import static com.superapp.sports.utils.Constants.NOTIF_STATUS;
import static com.superapp.sports.utils.Constants.USERID;

public class SettingFragment extends Fragment implements View.OnClickListener {
    View rootView,lineMessages,linePost,lineMyPlayer,linesponsor,linereqsponsor;
    RelativeLayout rl_requestsponsor,rlSecurity,rlAccount,rlMessages,rlMyPlayer,rlMyPost,ll_search, rlAbout, rlLogout, rlPrivacy, rlHelp, rlsponsor,rlNotification;
    String accountType="",lname="",fname="",profilePic="",noteStatus="",authkey="";
    TextView tvName,user_type_tv;
    CircleImageView imgProfile;
    ImageView imgNotif;
    boolean notifCheck;
    FrameLayout main_frame;


    public static Fragment newInstance() {
        return new SettingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.setting_fragment, container, false);


        main_frame = rootView.findViewById(R.id.main_frame);
        imgNotif = rootView.findViewById(R.id.imgNotif);
        tvName = rootView.findViewById(R.id.tvName);
        ll_search = rootView.findViewById(R.id.ll_search);
        lineMessages = rootView.findViewById(R.id.lineMessages);
        linereqsponsor = rootView.findViewById(R.id.linereqsponsor);
        lineMyPlayer = rootView.findViewById(R.id.lineMyPlayer);
        linesponsor = rootView.findViewById(R.id.linesponsor);
        linePost = rootView.findViewById(R.id.linePost);
        rlMyPost = rootView.findViewById(R.id.rlMyPost);
        rlMyPlayer = rootView.findViewById(R.id.rlMyPlayer);
        rlMessages = rootView.findViewById(R.id.rlMessages);
        rlAccount = rootView.findViewById(R.id.rlAccount);
        rlSecurity = rootView.findViewById(R.id.rlSecurity);
        rl_requestsponsor = rootView.findViewById(R.id.rl_requestsponsor);
        rlAbout = rootView.findViewById(R.id.rlAbout);
        rlLogout = rootView.findViewById(R.id.rlLogout);
        rlPrivacy = rootView.findViewById(R.id.rlPrivacy);
        rlHelp = rootView.findViewById(R.id.rlHelp);
        rlNotification = rootView.findViewById(R.id.rlNotification);
        rlsponsor = rootView.findViewById(R.id.rl_mysponsorlayout);
        imgProfile = rootView.findViewById(R.id.imgProfile);
        user_type_tv = rootView.findViewById(R.id.user_type_tv);

        fname= SharedHelper.getKey(getActivity(),FNAME);
        lname= SharedHelper.getKey(getActivity(),LNAME);
        profilePic= SharedHelper.getKey(getActivity(),Key_profile_image);
        accountType= SharedHelper.getKey(getActivity(),ACCOUNTTYPE);
        authkey= SharedHelper.getKey(getActivity(),AUTH_TOKEN);
        noteStatus= SharedHelper.getKey(getActivity(),NOTIF_STATUS);

        rlMessages.setVisibility(View.GONE);

        if (!profilePic.equalsIgnoreCase("")) {
            Picasso.get().load(profilePic).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(imgProfile);
        }

        tvName.setText(fname+" "+lname);
        user_type_tv.setText(accountType);

        if(accountType.equals("Coach")){
            rlsponsor.setVisibility(View.GONE);
            linesponsor.setVisibility(View.GONE);

            rlMyPlayer.setVisibility(View.GONE);
            lineMyPlayer.setVisibility(View.GONE);

            rl_requestsponsor.setVisibility(View.GONE);
            linereqsponsor.setVisibility(View.GONE);
            rlsponsor.setVisibility(View.GONE);
            linesponsor.setVisibility(View.GONE);
        }
        else if(accountType.equals("Player")){
            rlsponsor.setVisibility(View.VISIBLE);
            linesponsor.setVisibility(View.VISIBLE);
            rlMessages.setVisibility(View.GONE);
            //rlMyPost.setVisibility(View.GONE);
            rlMyPlayer.setVisibility(View.GONE);
            lineMyPlayer.setVisibility(View.GONE);
            rl_requestsponsor.setVisibility(View.GONE);

            linereqsponsor.setVisibility(View.GONE);
            lineMessages.setVisibility(View.GONE);
            //linePost.setVisibility(View.GONE);
            lineMyPlayer.setVisibility(View.GONE);


        }
        else if(accountType.equals("Sponser")){
            rlMyPlayer.setVisibility(View.VISIBLE);
            lineMyPlayer.setVisibility(View.VISIBLE);
            rlsponsor.setVisibility(View.GONE);
            linesponsor.setVisibility(View.GONE);


        }
        /*else{
            rl_requestsponsor.setVisibility(View.GONE);
            linereqsponsor.setVisibility(View.GONE);
        }*/


        rlLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                        .setTitle("Exit")
                        .setMessage("Are you sure you want to exit?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int which) {
                                logout();
                                /*Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);*/
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
        imgNotif.setOnClickListener(this);
        rlPrivacy.setOnClickListener(this);
        rlHelp.setOnClickListener(this);
        ll_search.setOnClickListener(this);
        rlAbout.setOnClickListener(this);
        rlsponsor.setOnClickListener(this);
        rlMyPost.setOnClickListener(this);
        rlMyPlayer.setOnClickListener(this);
        rlMessages.setOnClickListener(this);
        rlAccount.setOnClickListener(this);
        rlSecurity.setOnClickListener(this);
        rl_requestsponsor.setOnClickListener(this);

        if(noteStatus.equals("on")){
            imgNotif.setImageResource(R.drawable.ic_notif_on);

        }
        else if(noteStatus.equals("off")){
            imgNotif.setImageResource(R.drawable.ic_notification_button);
        }

        return rootView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgNotif:
                /*Intent inot = new Intent(getActivity(), PrivacyPolicy.class);
                startActivity(inot);*/
                //notifCheck=true;
                if(notifCheck==false){
                    noteStatus="on";
                    changeNotificationStatus(authkey,noteStatus);
                    //imgNotif.setImageResource(R.drawable.ic_notification_button);
                }
                else if(notifCheck==true){
                    //notifCheck=true;
                    noteStatus="off";
                    changeNotificationStatus(authkey,noteStatus);
                    //imgNotif.setImageResource(R.drawable.ic_notif_on);
                }

                break;


                case R.id.rlPrivacy:
                Intent i = new Intent(getActivity(), PrivacyPolicy.class);
                startActivity(i);
                break;

            case R.id.rlHelp:
                Intent in = new Intent(getActivity(), HelpActivity.class);
                startActivity(in);
                break;

            case R.id.ll_search:
                Intent i2 = new Intent(getActivity(), MyProfile.class);
                i2.putExtra("comingFrom", "setting");
                startActivity(i2);
                break;

            case R.id.rlAbout:
                Intent ia = new Intent(getActivity(), AboutActivity.class);
                startActivity(ia);
                break;

            case R.id.rl_mysponsorlayout:
                Intent ias = new Intent(getActivity(), SponsorsAct.class);
                startActivity(ias);
                break;

                /*case R.id.rl_requestsponsor:
                Intent irq = new Intent(getActivity(), RequestPay.class);
                startActivity(irq);
                break;*/



                case R.id.rlMessages:
                /*Intent im = new Intent(getActivity(), MessageScreen.class);
                startActivity(im);*/
                    ChatFragment chatFragment= new ChatFragment();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.main_frame, chatFragment, "chatFragment")
                            // .add(R.id.home_frame, searchFragment, "searchFragment")
                            .addToBackStack(null)
                            .commit();
                break;

                case R.id.rlMyPost:
                Intent ip = new Intent(getActivity(), MyProfilePost.class);
                    ip.putExtra("PlayerID",String.valueOf(SharedHelper.getKey(getActivity(),USERID)));
                    ip.putExtra("post_type","self");
                startActivity(ip);
                    /*main_frame.setVisibility(View.VISIBLE);
                    Fragment childFragment=new SearchFragment();
                    FragmentTransaction ft=getChildFragmentManager().beginTransaction();
                    ft.replace(R.id.main_frame,childFragment);
                    ft.commit();*/

                break;

            case R.id.rlAccount:
                Intent ic = new Intent(getActivity(), AccountScreen.class);
                startActivity(ic);
                break;

                case R.id.rlSecurity:
                Intent ins = new Intent(getActivity(), SecurityScreen.class);
                startActivity(ins);
                break;

            case R.id.rlMyPlayer:
                Intent ipl=new Intent(getActivity(), MyPlayer.class);
                startActivity(ipl);
                break;

        }
    }
    public void logout() {

        FacebookSdk.setAdvertiserIDCollectionEnabled(false);
        SharedHelper.putKey(getActivity(), AUTH_TOKEN, "");
        SharedPreferences preferences = getActivity().getSharedPreferences("Cache", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        Intent goToLogin = new Intent(getActivity(), LoginActivity.class);
        goToLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(goToLogin);
        getActivity().finish();
    }

    private void changeNotificationStatus(String authToken,String notestatus) {

        Api api = RestManager.instanceOf();
        Call<ChangeNotificationResponse> responseCall = api.changeNotificationApi(authToken,notestatus);
        responseCall.enqueue(new Callback<ChangeNotificationResponse>() {
            @Override
            public void onResponse(Call<ChangeNotificationResponse> call, Response<ChangeNotificationResponse> response) {
                //progressBar.dismiss();

                if (response != null) {

                    if (response.body().getStatus()==1) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        String notes= response.body().getChange_status();
                        SharedHelper.putKey(getActivity(),NOTIF_STATUS,notes);
                        if(response.body().getChange_status().equals("on")){
                            notifCheck=true;
                            imgNotif.setImageResource(R.drawable.ic_notif_on);

                        }
                        else if(response.body().getChange_status().equals("off")){
                            notifCheck=false;
                            imgNotif.setImageResource(R.drawable.ic_notification_button);
                        }

                    } else if (response.body().getStatus()==0) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<ChangeNotificationResponse> call, Throwable t) {
                Log.d("errorRes:",t.getMessage());
                //progressBar.dismiss();
            }
        });
    }

}
