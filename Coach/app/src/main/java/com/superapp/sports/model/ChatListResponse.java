package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatListResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("reciver_user_id")
        @Expose
        private Integer reciverUserId;
        @SerializedName("time")
        @Expose
        private String time;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("user_image")
        @Expose
        private String userImage;
        @SerializedName("reciver_user_image")
        @Expose
        private String reciverUserImage;
        @SerializedName("sender_id")
        @Expose
        private Integer senderId;
        @SerializedName("reciver_id")
        @Expose
        private Integer reciverId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("reciver_user_name")
        @Expose
        private String reciverUserName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getReciverUserId() {
            return reciverUserId;
        }

        public void setReciverUserId(Integer reciverUserId) {
            this.reciverUserId = reciverUserId;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public String getReciverUserImage() {
            return reciverUserImage;
        }

        public void setReciverUserImage(String reciverUserImage) {
            this.reciverUserImage = reciverUserImage;
        }

        public Integer getSenderId() {
            return senderId;
        }

        public void setSenderId(Integer senderId) {
            this.senderId = senderId;
        }

        public Integer getReciverId() {
            return reciverId;
        }

        public void setReciverId(Integer reciverId) {
            this.reciverId = reciverId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getReciverUserName() {
            return reciverUserName;
        }

        public void setReciverUserName(String reciverUserName) {
            this.reciverUserName = reciverUserName;
        }

    }
}
