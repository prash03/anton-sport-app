package com.superapp.sports.model;

public class ChooseInterestResponse {

   private String playName;
   private int playImage;


    public ChooseInterestResponse(String playName, int playImage) {
        this.playName = playName;
        this.playImage = playImage;
    }

    public String getPlayName() {
        return playName;
    }

    public void setPlayName(String playName) {
        this.playName = playName;
    }

    public int getPlayImage() {
        return playImage;
    }

    public void setPlayImage(int playImage) {
        this.playImage = playImage;
    }
}
