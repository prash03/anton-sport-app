package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentListResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("comment_data")
    @Expose
    private List<CommentDatum> commentData = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CommentDatum> getCommentData() {
        return commentData;
    }

    public void setCommentData(List<CommentDatum> commentData) {
        this.commentData = commentData;
    }

    public class CommentDatum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("post_id")
        @Expose
        private Integer postId;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("comment_text")
        @Expose
        private String commentText;
        @SerializedName("likes")
        @Expose
        private Integer likes;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("total_count_of_likes")
        @Expose
        private Integer totalCountOfLikes;
        @SerializedName("user")
        @Expose
        private User user;

        @SerializedName("reply_comment")
        @Expose
        private List<ReplyComment> replyComment = null;


        public List<ReplyComment> getReplyComment() {
            return replyComment;
        }

        public void setReplyComment(List<ReplyComment> replyComment) {
            this.replyComment = replyComment;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPostId() {
            return postId;
        }

        public void setPostId(Integer postId) {
            this.postId = postId;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getCommentText() {
            return commentText;
        }

        public void setCommentText(String commentText) {
            this.commentText = commentText;
        }

        public Integer getLikes() {
            return likes;
        }

        public void setLikes(Integer likes) {
            this.likes = likes;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getTotalCountOfLikes() {
            return totalCountOfLikes;
        }

        public void setTotalCountOfLikes(Integer totalCountOfLikes) {
            this.totalCountOfLikes = totalCountOfLikes;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public class ReplyComment {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("comment_id")
            @Expose
            private Integer commentId;
            @SerializedName("post_id")
            @Expose
            private Integer postId;
            @SerializedName("user_id")
            @Expose
            private Integer userId;
            @SerializedName("comment_text")
            @Expose
            private String commentText;
            @SerializedName("likes")
            @Expose
            private Integer likes;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;

            @SerializedName("user_name")
            @Expose
            private String user_name;

            @SerializedName("user_image")
            @Expose
            private String user_image;

            public String getUser_image() {
                return user_image;
            }

            public void setUser_image(String user_image) {
                this.user_image = user_image;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getCommentId() {
                return commentId;
            }

            public void setCommentId(Integer commentId) {
                this.commentId = commentId;
            }

            public Integer getPostId() {
                return postId;
            }

            public void setPostId(Integer postId) {
                this.postId = postId;
            }

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public String getCommentText() {
                return commentText;
            }

            public void setCommentText(String commentText) {
                this.commentText = commentText;
            }

            public Integer getLikes() {
                return likes;
            }

            public void setLikes(Integer likes) {
                this.likes = likes;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

        }

    }

    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("user_image")
        @Expose
        private Object userImage;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Object getUserImage() {
            return userImage;
        }

        public void setUserImage(Object userImage) {
            this.userImage = userImage;
        }

    }
}
