package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DBUserPostModal {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("posted_time")
    @Expose
    private String postedTime;
    @SerializedName("item_type")
    @Expose
    private String itemType;
    @SerializedName("multiple_post_image")
    @Expose
    private List<MultiplePostImage> multiplePostImage = null;
    @SerializedName("multiple_post_thumb_image")
    @Expose
    private String multiplePostThumbImage;
    @SerializedName("post_description")
    @Expose
    private String postDescription;
    @SerializedName("total_like_count")
    @Expose
    private Integer totalLikeCount;
    @SerializedName("total_comment_count")
    @Expose
    private Integer totalCommentCount;
    @SerializedName("liked_status")
    @Expose
    private Integer likedStatus;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_image")
    @Expose
    private String userImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(String postedTime) {
        this.postedTime = postedTime;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public List<MultiplePostImage> getMultiplePostImage() {
        return multiplePostImage;
    }

    public void setMultiplePostImage(List<MultiplePostImage> multiplePostImage) {
        this.multiplePostImage = multiplePostImage;
    }

    public String getMultiplePostThumbImage() {
        return multiplePostThumbImage;
    }

    public void setMultiplePostThumbImage(String multiplePostThumbImage) {
        this.multiplePostThumbImage = multiplePostThumbImage;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public Integer getTotalLikeCount() {
        return totalLikeCount;
    }

    public void setTotalLikeCount(Integer totalLikeCount) {
        this.totalLikeCount = totalLikeCount;
    }

    public Integer getTotalCommentCount() {
        return totalCommentCount;
    }

    public void setTotalCommentCount(Integer totalCommentCount) {
        this.totalCommentCount = totalCommentCount;
    }

    public Integer getLikedStatus() {
        return likedStatus;
    }

    public void setLikedStatus(Integer likedStatus) {
        this.likedStatus = likedStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }


    public class MultiplePostImage {
        @SerializedName("mime_type")
        @Expose
        private String mimeType;
        @SerializedName("thumb_image")
        @Expose
        private String thumbImage;
        @SerializedName("posted_image")
        @Expose
        private String postedImage;

        public String getMimeType() {
            return mimeType;
        }

        public void setMimeType(String mimeType) {
            this.mimeType = mimeType;
        }

        public String getThumbImage() {
            return thumbImage;
        }

        public void setThumbImage(String thumbImage) {
            this.thumbImage = thumbImage;
        }

        public String getPostedImage() {
            return postedImage;
        }

        public void setPostedImage(String postedImage) {
            this.postedImage = postedImage;
        }
    }
}
