package com.superapp.sports.model;

public class FIndFriendResponse {
    private String personName;
    private String subHead;
    private String profilePic;

    public FIndFriendResponse(String personName, String subHead, String profilePic) {
        this.personName = personName;
        this.subHead = subHead;
        this.profilePic = profilePic;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getSubHead() {
        return subHead;
    }

    public void setSubHead(String subHead) {
        this.subHead = subHead;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }
}
