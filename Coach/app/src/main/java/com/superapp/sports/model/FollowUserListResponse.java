package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowUserListResponse {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String msg;
    @SerializedName("user_data")
    @Expose
    private List<UserListData> UserData = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<UserListData> getUserData() {
        return UserData;
    }

    public void setUserData(List<UserListData> userData) {
        UserData = userData;
    }

    public class UserListData{

        @SerializedName("user_id")
        @Expose
        private int user_id;

        @SerializedName("user_image")
        @Expose
        private String user_image;

        @SerializedName("following_status")
        @Expose
        private String following_status;

        @SerializedName("user_name")
        @Expose
        private String user_name;

        @SerializedName("matual_friends_count")
        @Expose
        private int matual_friends_count;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }


        public String getUser_image() {
            return user_image;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public String getFollowing_status() {
            return following_status;
        }

        public void setFollowing_status(String following_status) {
            this.following_status = following_status;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public int getMatual_friends_count() {
            return matual_friends_count;
        }

        public void setMatual_friends_count(int matual_friends_count) {
            this.matual_friends_count = matual_friends_count;
        }
    }
}
