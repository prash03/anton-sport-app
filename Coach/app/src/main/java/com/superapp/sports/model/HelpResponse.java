package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HelpResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataReview data;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataReview getData() {
        return data;
    }

    public void setData(DataReview data) {
        this.data = data;
    }

    public class DataReview {

        @SerializedName("reviews_list")
        @Expose
        private List<Reviews> reviewsList = null;

        public List<Reviews> getReviewsList() {
            return reviewsList;
        }

        public void setReviewsList(List<Reviews> reviewsList) {
            this.reviewsList = reviewsList;
        }

        public class Reviews {

            @SerializedName("item_id")
            @Expose
            private Integer itemId;
            @SerializedName("review_text")
            @Expose
            private String reviewTxt;

            public Integer getItemId() {
                return itemId;
            }

            public void setItemId(Integer itemId) {
                this.itemId = itemId;
            }

            public String getReviewTxt() {
                return reviewTxt;
            }

            public void setReviewTxt(String reviewTxt) {
                this.reviewTxt = reviewTxt;
            }

        }

    }

}
