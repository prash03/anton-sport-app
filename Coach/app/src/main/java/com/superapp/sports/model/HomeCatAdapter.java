package com.superapp.sports.model;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.superapp.sports.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeCatAdapter extends RecyclerView.Adapter<HomeCatAdapter.ViewHolder>{

    Context context;
    JSONArray jsonArray;
    List<HomeResponse.HomeData.OtherUser> otherUsers;
    // RecyclerView recyclerView;


    public HomeCatAdapter(Context context,JSONArray listdata) {
        this.context = context;
        this.jsonArray = listdata;
    }

/*
    public HomeCatAdapter(Context context, List<HomeResponse.HomeData.OtherUser> otherUsers) {
           this.context = context;
           this.otherUsers = otherUsers;
       }*/


    @Override
    public HomeCatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_home_top, parent, false);
        HomeCatAdapter.ViewHolder viewHolder = new HomeCatAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeCatAdapter.ViewHolder holder, int position) {
        String user_img = (jsonArray.optJSONObject(position).optString("user_image"));
      //  String user_img = (otherUsers.get(position).getUserImage());
        if(!user_img.equals("")||user_img.equalsIgnoreCase(null)){
            Picasso.get().load(user_img).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgCat);
        }else {
            Picasso.get().load(R.drawable.dummy_user).placeholder(R.drawable.dummy_user).error(R.drawable.dummy_user).into(holder.imgCat);

        }

        holder.tvCat.setText(jsonArray.optJSONObject(position).optString("user_name"));
       // holder.tvCat.setText(otherUsers.get(position).getUserName());
    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
        //return otherUsers.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgCat;
        public TextView tvCat;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgCat = itemView.findViewById(R.id.imgCat);
            this.tvCat = itemView.findViewById(R.id.tvCat);

        }
    }
}

