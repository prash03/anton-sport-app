package com.superapp.sports.model;

import java.util.List;

public class HomePostResponse {

    private int profileImage;
    private String profileName;
    private String loginTime;
    //private List<Integer> data = null;
    private String likeCount;
    private String commentCount;


    public HomePostResponse(int profileImage, String profileName, String loginTime, List<Integer> data, String likeCount, String commentCount) {
        this.profileImage = profileImage;
        this.profileName = profileName;
        this.loginTime = loginTime;
        //this.data = data;
        this.likeCount = likeCount;
        this.commentCount = commentCount;
    }

    public HomePostResponse(String russell_hayes, String profileName,int sliding_banner, String likeCount, String commentCount) {

    }

    /*public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }*/

    public int getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(int profileImage) {
        this.profileImage = profileImage;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }



    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

   /* public class PostList{
        private int postImage;

        public int getPostImage() {
            return postImage;
        }

        public void setPostImage(int postImage) {
            this.postImage = postImage;
        }
    }*/
}
