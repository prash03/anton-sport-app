package com.superapp.sports.model;

public class InboxResponse {
    private String msg;

    public InboxResponse(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
