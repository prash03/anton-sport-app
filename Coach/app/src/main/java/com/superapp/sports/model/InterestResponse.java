package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InterestResponse {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("Message")
    @Expose
    private String msg;

    @SerializedName("data")
    @Expose
    private InterestData data;

    public InterestData getData() {
        return data;
    }

    public void setData(InterestData data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }



    public class InterestData{
        @SerializedName("slider_images")
        @Expose
        private List<SliderImage> sliderImages = null;
        @SerializedName("interest_data")
        @Expose
        private List<InterestDatum> interestData = null;

        public List<SliderImage> getSliderImages() {
            return sliderImages;
        }

        public void setSliderImages(List<SliderImage> sliderImages) {
            this.sliderImages = sliderImages;
        }

        public List<InterestDatum> getInterestData() {
            return interestData;
        }

        public void setInterestData(List<InterestDatum> interestData) {
            this.interestData = interestData;
        }


    }

    public class SliderImage {

        @SerializedName("slider_title")
        @Expose
        private String sliderTitle;

        @SerializedName("slider_sub_title")
        @Expose
        private String slider_sub_title;

        @SerializedName("slider_image")
        @Expose
        private String sliderImage;

        public String getSlider_sub_title() {
            return slider_sub_title;
        }

        public void setSlider_sub_title(String slider_sub_title) {
            this.slider_sub_title = slider_sub_title;
        }

        public String getSliderTitle() {
            return sliderTitle;
        }

        public void setSliderTitle(String sliderTitle) {
            this.sliderTitle = sliderTitle;
        }

        public String getSliderImage() {
            return sliderImage;
        }

        public void setSliderImage(String sliderImage) {
            this.sliderImage = sliderImage;
        }

    }
    public class InterestDatum {

        @SerializedName("item_id")
        @Expose
        private Integer itemId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("is_intrest")
        @Expose
        private Integer is_intrest ;

        public Integer getItemId() {
            return itemId;
        }

        public void setItemId(Integer itemId) {
            this.itemId = itemId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getIs_intrest() {
            return is_intrest;
        }

        public void setIs_intrest(Integer is_intrest) {
            this.is_intrest = is_intrest;
        }
    }
}