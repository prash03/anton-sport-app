package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocationListResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public class Datum {

        @SerializedName("location_title")
        @Expose
        private String locationTitle;

        @SerializedName("active_status")
        @Expose
        private String active_status;

        @SerializedName("phone_name")
        @Expose
        private String phone_name;
        @SerializedName("date")
        @Expose
        private String date;

        public String getActive_status() {
            return active_status;
        }

        public void setActive_status(String active_status) {
            this.active_status = active_status;
        }

        public String getPhone_name() {
            return phone_name;
        }

        public void setPhone_name(String phone_name) {
            this.phone_name = phone_name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getLocationTitle() {
            return locationTitle;
        }

        public void setLocationTitle(String locationTitle) {
            this.locationTitle = locationTitle;
        }

    }
}
