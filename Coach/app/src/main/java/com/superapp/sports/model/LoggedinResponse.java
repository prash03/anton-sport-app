package com.superapp.sports.model;

public class LoggedinResponse {
    private String address;
    private String date;
    private String device;
    private int pic;

    public LoggedinResponse(String address, String date, String device, int pic) {
        this.address = address;
        this.date = date;
        this.device = device;
        this.pic = pic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }
}
