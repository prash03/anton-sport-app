package com.superapp.sports.model;

/*public class LoginResponse {
}*/
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("Message")
    @Expose
    private String msg;

    @SerializedName("auth_key")
    @Expose
    private String authKey;

    @SerializedName("data")
    @Expose
    private RequestDetail rq_detail;


    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestDetail getRq_detail() {
        return rq_detail;
    }

    public void setRq_detail(RequestDetail rq_detail) {
        this.rq_detail = rq_detail;
    }

    public class RequestDetail{

        @SerializedName("id")
        @Expose
        private String userId;

        @SerializedName("first_name")
        @Expose
        private String fname;

        @SerializedName("last_name")
        @Expose
        private String lname;

        @SerializedName("email_id")
        @Expose
        private String email;

        @SerializedName("mobile_number")
        @Expose
        private String mob;

       /* @SerializedName("user_image")
        @Expose
        private String userImage;*/

        @SerializedName("auth_key")
        @Expose
        private String authToken;

        @SerializedName("user_type")
        @Expose
        private String userType;

        @SerializedName("type")
        @Expose
        private String loginType;


        public String getLoginType() {
            return loginType;
        }

        public void setLoginType(String loginType) {
            this.loginType = loginType;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMob() {
            return mob;
        }

        public void setMob(String mob) {
            this.mob = mob;
        }

        /*public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }*/

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }
    }
}

