package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyPostResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_image")
    @Expose
    private Object userImage;
    @SerializedName("my_post")
    @Expose
    private List<MyPost> myPost = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Object getUserImage() {
        return userImage;
    }

    public void setUserImage(Object userImage) {
        this.userImage = userImage;
    }

    public List<MyPost> getMyPost() {
        return myPost;
    }

    public void setMyPost(List<MyPost> myPost) {
        this.myPost = myPost;
    }

    public class MyPost {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("posted_time")
        @Expose
        private String postedTime;
        @SerializedName("multiple_post_image")
        @Expose
        private List<MultiplePostImage> multiplePostImage = null;
        @SerializedName("post_description")
        @Expose
        private String postDescription;
        @SerializedName("total_like_count")
        @Expose
        private Integer totalLikeCount;
        @SerializedName("total_comment_count")
        @Expose
        private Integer totalCommentCount;
        @SerializedName("liked_status")
        @Expose
        private Integer likedStatus;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPostedTime() {
            return postedTime;
        }

        public void setPostedTime(String postedTime) {
            this.postedTime = postedTime;
        }

        public List<MultiplePostImage> getMultiplePostImage() {
            return multiplePostImage;
        }

        public void setMultiplePostImage(List<MultiplePostImage> multiplePostImage) {
            this.multiplePostImage = multiplePostImage;
        }

        public String getPostDescription() {
            return postDescription;
        }

        public void setPostDescription(String postDescription) {
            this.postDescription = postDescription;
        }

        public Integer getTotalLikeCount() {
            return totalLikeCount;
        }

        public void setTotalLikeCount(Integer totalLikeCount) {
            this.totalLikeCount = totalLikeCount;
        }

        public Integer getTotalCommentCount() {
            return totalCommentCount;
        }

        public void setTotalCommentCount(Integer totalCommentCount) {
            this.totalCommentCount = totalCommentCount;
        }

        public Integer getLikedStatus() {
            return likedStatus;
        }

        public void setLikedStatus(Integer likedStatus) {
            this.likedStatus = likedStatus;
        }

        public class MultiplePostImage {

            @SerializedName("posted_image")
            @Expose
            private String postedImage;

            public String getPostedImage() {
                return postedImage;
            }

            public void setPostedImage(String postedImage) {
                this.postedImage = postedImage;
            }

        }

    }

}


