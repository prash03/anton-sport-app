package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyProfileResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_data")
    @Expose
    private UserData userData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public class UserData {

        @SerializedName("user_image")
        @Expose
        private Object userImage;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("user_type")
        @Expose
        private String user_type;
        @SerializedName("user_email")
        @Expose
        private String userEmail;

        @SerializedName("height")
        @Expose
        private String height;
        @SerializedName("weight")
        @Expose
        private String weight;

        @SerializedName("position")
        @Expose
        private String position;
        @SerializedName("ranking")
        @Expose
        private String ranking;
        @SerializedName("speed")
        @Expose
        private String speed;
        @SerializedName("vertical")
        @Expose
        private String vertical;

        @SerializedName("gpa")
        @Expose
        private String gpa;
        @SerializedName("home_town")
        @Expose
        private String home_town;
        @SerializedName("classification")
        @Expose
        private String classification;
        @SerializedName("school")
        @Expose
        private String school;
        @SerializedName("experience")
        @Expose
        private String experience;
        @SerializedName("sports_name")
        @Expose
        private String sports_name;
        @SerializedName("achievement")
        @Expose
        private String achievement;
        @SerializedName("sponser")
        @Expose
        private String sponser;
        @SerializedName("company_work")
        @Expose
        private String company_work;
        @SerializedName("speciality")
        @Expose
        private String speciality;
        @SerializedName("user_mobile_num")
        @Expose
        private Object userMobileNum;
        @SerializedName("user_website")
        @Expose
        private Object userWebsite;
        @SerializedName("user_bio")
        @Expose
        private Object userBio;
        @SerializedName("user_dob")
        @Expose
        private Object userDob;
        @SerializedName("user_gender")
        @Expose
        private Object userGender;
        @SerializedName("user_maital_status")
        @Expose
        private Object userMaitalStatus;
        @SerializedName("status_text")
        @Expose
        private Object statusText;
        @SerializedName("following_count")
        @Expose
        private Integer followingCount;
        @SerializedName("followers_count")
        @Expose
        private Integer followersCount;
        @SerializedName("posts_count")
        @Expose
        private Integer postsCount;

        @SerializedName("ppg")
        @Expose
        private String ppg;

        @SerializedName("rpg")
        @Expose
        private String rpg;

        @SerializedName("apg")
        @Expose
        private String apg;

        @SerializedName("followed_by")
        @Expose
        private List<FollowedBy> followedBy = null;
        /*@SerializedName("my_interest")
        @Expose
        private List<Object> myInterest = null;*/
        @SerializedName("my_interest")
        @Expose
        private List<MyInterest> myInterest = null;
        @SerializedName("self_post")
        @Expose
        private List<SelfPost> selfPost = null;
        @SerializedName("tag_post")
        @Expose
        private List<TagPost> tagPost = null;

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getRanking() {
            return ranking;
        }

        public void setRanking(String ranking) {
            this.ranking = ranking;
        }

        public String getSpeed() {
            return speed;
        }

        public void setSpeed(String speed) {
            this.speed = speed;
        }

        public String getVertical() {
            return vertical;
        }

        public void setVertical(String vertical) {
            this.vertical = vertical;
        }

        public String getGpa() {
            return gpa;
        }

        public void setGpa(String gpa) {
            this.gpa = gpa;
        }

        public String getHome_town() {
            return home_town;
        }

        public void setHome_town(String home_town) {
            this.home_town = home_town;
        }

        public String getClassification() {
            return classification;
        }

        public void setClassification(String classification) {
            this.classification = classification;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getSchool() {
            return school;
        }

        public void setSchool(String school) {
            this.school = school;
        }

        public String getExperience() {
            return experience;
        }

        public void setExperience(String experience) {
            this.experience = experience;
        }

        public String getSports_name() {
            return sports_name;
        }

        public void setSports_name(String sports_name) {
            this.sports_name = sports_name;
        }

        public String getAchievement() {
            return achievement;
        }

        public void setAchievement(String achievement) {
            this.achievement = achievement;
        }

        public String getSponser() {
            return sponser;
        }

        public void setSponser(String sponser) {
            this.sponser = sponser;
        }

        public String getCompany_work() {
            return company_work;
        }

        public void setCompany_work(String company_work) {
            this.company_work = company_work;
        }

        public String getSpeciality() {
            return speciality;
        }

        public void setSpeciality(String speciality) {
            this.speciality = speciality;
        }

        public Object getUserImage() {
            return userImage;
        }

        public void setUserImage(Object userImage) {
            this.userImage = userImage;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public Object getUserMobileNum() {
            return userMobileNum;
        }

        public void setUserMobileNum(Object userMobileNum) {
            this.userMobileNum = userMobileNum;
        }

        public Object getUserWebsite() {
            return userWebsite;
        }

        public void setUserWebsite(Object userWebsite) {
            this.userWebsite = userWebsite;
        }

        public Object getUserBio() {
            return userBio;
        }

        public void setUserBio(Object userBio) {
            this.userBio = userBio;
        }

        public Object getUserDob() {
            return userDob;
        }

        public void setUserDob(Object userDob) {
            this.userDob = userDob;
        }

        public Object getUserGender() {
            return userGender;
        }

        public void setUserGender(Object userGender) {
            this.userGender = userGender;
        }

        public Object getUserMaitalStatus() {
            return userMaitalStatus;
        }

        public void setUserMaitalStatus(Object userMaitalStatus) {
            this.userMaitalStatus = userMaitalStatus;
        }

        public Object getStatusText() {
            return statusText;
        }

        public void setStatusText(Object statusText) {
            this.statusText = statusText;
        }

        public Integer getFollowingCount() {
            return followingCount;
        }

        public void setFollowingCount(Integer followingCount) {
            this.followingCount = followingCount;
        }

        public Integer getFollowersCount() {
            return followersCount;
        }

        public void setFollowersCount(Integer followersCount) {
            this.followersCount = followersCount;
        }

        public Integer getPostsCount() {
            return postsCount;
        }

        public void setPostsCount(Integer postsCount) {
            this.postsCount = postsCount;
        }

        public List<FollowedBy> getFollowedBy() {
            return followedBy;
        }

        public void setFollowedBy(List<FollowedBy> followedBy) {
            this.followedBy = followedBy;
        }

        public List<MyInterest> getMyInterest() {
            return myInterest;
        }

        public void setMyInterest(List<MyInterest> myInterest) {
            this.myInterest = myInterest;
        }

        public List<SelfPost> getSelfPost() {
            return selfPost;
        }

        public void setSelfPost(List<SelfPost> selfPost) {
            this.selfPost = selfPost;
        }

        public List<TagPost> getTagPost() {
            return tagPost;
        }

        public void setTagPost(List<TagPost> tagPost) {
            this.tagPost = tagPost; }

        public String getPpg() {
            return ppg;
        }

        public void setPpg(String ppg) {
            this.ppg = ppg;
        }

        public String getRpg() {
            return rpg;
        }

        public void setRpg(String rpg) {
            this.rpg = rpg;
        }

        public String getApg() {
            return apg;
        }

        public void setApg(String apg) {
            this.apg = apg;
        }

        public class FollowedBy {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("following_id")
            @Expose
            private Integer followingId;
            @SerializedName("follower_id")
            @Expose
            private Integer followerId;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getFollowingId() {
                return followingId;
            }

            public void setFollowingId(Integer followingId) {
                this.followingId = followingId;
            }

            public Integer getFollowerId() {
                return followerId;
            }

            public void setFollowerId(Integer followerId) {
                this.followerId = followerId;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

        }
        public class MyInterest {

            @SerializedName("item_image")
            @Expose
            private String itemImage;
            @SerializedName("item_id")
            @Expose
            private Integer itemId;
            @SerializedName("item_name")
            @Expose
            private String itemName;

            public String getItemImage() {
                return itemImage;
            }

            public void setItemImage(String itemImage) {
                this.itemImage = itemImage;
            }

            public Integer getItemId() {
                return itemId;
            }

            public void setItemId(Integer itemId) {
                this.itemId = itemId;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

        }
        public class SelfPost {

            @SerializedName("item_id")
            @Expose
            private Integer itemId;
            @SerializedName("item_image")
            @Expose
            private String itemImage;
            @SerializedName("mime_type")
            @Expose
            private String itemType;
            @SerializedName("thumb_image")
            @Expose
            private String thumbnail;


            public Integer getItemId() {
                return itemId;
            }

            public void setItemId(Integer itemId) {
                this.itemId = itemId;
            }

            public String getItemImage() {
                return itemImage;
            }

            public void setItemImage(String itemImage) {
                this.itemImage = itemImage;
            }

            public String getItemType() {
                return itemType;
            }

            public void setItemType(String itemType) {
                this.itemType = itemType;
            }

            public String getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
            }
        }
        public class TagPost {

            @SerializedName("item_id")
            @Expose
            private Integer itemId;
            @SerializedName("item_image")
            @Expose
            private String itemImage;
            @SerializedName("item_type")
            @Expose
            private Object itemType;

            public Integer getItemId() {
                return itemId;
            }

            public void setItemId(Integer itemId) {
                this.itemId = itemId;
            }

            public String getItemImage() {
                return itemImage;
            }

            public void setItemImage(String itemImage) {
                this.itemImage = itemImage;
            }

            public Object getItemType() {
                return itemType;
            }

            public void setItemType(Object itemType) {
                this.itemType = itemType;
            }

        }

    }
}
