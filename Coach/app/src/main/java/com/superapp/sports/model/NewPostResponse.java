package com.superapp.sports.model;

import android.net.Uri;

public class NewPostResponse{
    private Uri profilePic;

    public NewPostResponse(Uri profilePic) {
        this.profilePic = profilePic;
    }

    public Uri getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Uri profilePic) {
        this.profilePic = profilePic;
    }
}
