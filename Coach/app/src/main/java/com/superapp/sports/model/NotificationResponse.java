package com.superapp.sports.model;

public class NotificationResponse {
    private int profileImage;
    private String profileName;
    private String description;
    private String dateString;

    public NotificationResponse(int profileImage, String profileName, String description, String dateString) {
        this.profileImage = profileImage;
        this.profileName = profileName;
        this.description = description;
        this.dateString = dateString;
    }

    public int getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(int profileImage) {
        this.profileImage = profileImage;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }
}
