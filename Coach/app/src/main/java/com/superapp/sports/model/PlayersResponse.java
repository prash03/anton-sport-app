package com.superapp.sports.model;

public class PlayersResponse {
    private String name;
    private String amount;
    private String date;
    private int pic;

    public PlayersResponse(String name, String amount, String date, int pic) {
        this.name = name;
        this.amount = amount;
        this.date = date;
        this.pic = pic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }
}
