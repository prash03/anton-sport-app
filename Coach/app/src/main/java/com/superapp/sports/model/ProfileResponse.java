package com.superapp.sports.model;

public class ProfileResponse {
    private int profilePic;

    public ProfileResponse(int profilePic) {
        this.profilePic = profilePic;
    }

    public int getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(int profilePic) {
        this.profilePic = profilePic;
    }
}
