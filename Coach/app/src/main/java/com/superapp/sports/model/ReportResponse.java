package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataReview data;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataReview getData() {
        return data;
    }

    public void setData(DataReview data) {
        this.data = data;
    }

    public class DataReview {

        @SerializedName("report_list")
        @Expose
        private List<Report> reportList = null;

        public List<Report> getReportList() {
            return reportList;
        }

        public void setReportList(List<Report> reportList) {
            this.reportList = reportList;
        }

        public class Report {

            @SerializedName("id")
            @Expose
            private Integer itemId;
            @SerializedName("report_title")
            @Expose
            private String report_title;

            public Integer getItemId() {
                return itemId;
            }

            public void setItemId(Integer itemId) {
                this.itemId = itemId;
            }

            public String getReport_title() {
                return report_title;
            }

            public void setReport_title(String report_title) {
                this.report_title = report_title;
            }
        }

    }

}
