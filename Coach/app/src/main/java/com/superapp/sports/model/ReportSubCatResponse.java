package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReportSubCatResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<SubCatData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubCatData> getData() {
        return data;
    }

    public void setData(List<SubCatData> data) {
        this.data = data;
    }

    public class SubCatData {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("report_id")
        @Expose
        private Integer reportId;
        @SerializedName("sub_report_title")
        @Expose
        private String subReportTitle;
        @SerializedName("sub_report_desc")
        @Expose
        private String subReportDesc;
        @SerializedName("report_title")
        @Expose
        private String report_title;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getReportId() {
            return reportId;
        }

        public void setReportId(Integer reportId) {
            this.reportId = reportId;
        }

        public String getSubReportTitle() {
            return subReportTitle;
        }

        public void setSubReportTitle(String subReportTitle) {
            this.subReportTitle = subReportTitle;
        }

        public String getSubReportDesc() {
            return subReportDesc;
        }

        public void setSubReportDesc(String subReportDesc) {
            this.subReportDesc = subReportDesc;
        }

        public String getReport_title() {
            return report_title;
        }

        public void setReport_title(String report_title) {
            this.report_title = report_title;
        }
    }
}
