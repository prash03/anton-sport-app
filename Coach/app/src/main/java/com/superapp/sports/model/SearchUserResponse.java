package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchUserResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("tag_suggestion")
    @Expose
    private List<TagSuggestion> tagSuggestion = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<TagSuggestion> getTagSuggestion() {
        return tagSuggestion;
    }

    public void setTagSuggestion(List<TagSuggestion> tagSuggestion) {
        this.tagSuggestion = tagSuggestion;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public class TagSuggestion {

        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("user_image")
        @Expose
        private String userImage;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("following_status")
        @Expose
        private String followingStatus;
        @SerializedName("followed_by")
        @Expose
        private List<FollowedBy> followedBy = null;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getFollowingStatus() {
            return followingStatus;
        }

        public void setFollowingStatus(String followingStatus) {
            this.followingStatus = followingStatus;
        }

        public List<FollowedBy> getFollowedBy() {
            return followedBy;
        }

        public void setFollowedBy(List<FollowedBy> followedBy) {
            this.followedBy = followedBy;
        }

        public class FollowedBy {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("following_id")
            @Expose
            private Integer followingId;
            @SerializedName("follower_id")
            @Expose
            private Integer followerId;
            @SerializedName("user_name")
            @Expose
            private String user_name;

            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getFollowingId() {
                return followingId;
            }

            public void setFollowingId(Integer followingId) {
                this.followingId = followingId;
            }

            public Integer getFollowerId() {
                return followerId;
            }

            public void setFollowerId(Integer followerId) {
                this.followerId = followerId;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

        }

    }
}
