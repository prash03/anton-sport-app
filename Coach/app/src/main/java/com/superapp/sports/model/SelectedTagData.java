package com.superapp.sports.model;


public class SelectedTagData {

    private String tagUserName;

    private String tagUserImage;

    private Integer tagUserId;

    public SelectedTagData(String tagUserName, String tagUserImage, Integer tagUserId) {
        this.tagUserName = tagUserName;
        this.tagUserImage = tagUserImage;
        this.tagUserId = tagUserId;
    }

    public String getTagUserName() {
        return tagUserName;
    }

    public void setTagUserName(String tagUserName) {
        this.tagUserName = tagUserName;
    }

    public String getTagUserImage() {
        return tagUserImage;
    }

    public void setTagUserImage(String tagUserImage) {
        this.tagUserImage = tagUserImage;
    }

    public Integer getTagUserId() {
        return tagUserId;
    }

    public void setTagUserId(Integer tagUserId) {
        this.tagUserId = tagUserId;
    }

}

