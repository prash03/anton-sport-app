package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TagSuggestionResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("tag_suggestion")
    @Expose
    private List<TagSuggestion> tagSuggestion = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<TagSuggestion> getTagSuggestion() {
        return tagSuggestion;
    }

    public void setTagSuggestion(List<TagSuggestion> tagSuggestion) {
        this.tagSuggestion = tagSuggestion;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class TagSuggestion {

        @SerializedName("tag_user_name")
        @Expose
        private String tagUserName;
        @SerializedName("tag_user_image")
        @Expose
        private Object tagUserImage;
        @SerializedName("tag_user_id ")
        @Expose
        private Integer tagUserId;

        public String getTagUserName() {
            return tagUserName;
        }

        public void setTagUserName(String tagUserName) {
            this.tagUserName = tagUserName;
        }

        public Object getTagUserImage() {
            return tagUserImage;
        }

        public void setTagUserImage(Object tagUserImage) {
            this.tagUserImage = tagUserImage;
        }

        public Integer getTagUserId() {
            return tagUserId;
        }

        public void setTagUserId(Integer tagUserId) {
            this.tagUserId = tagUserId;
        }

    }
}
