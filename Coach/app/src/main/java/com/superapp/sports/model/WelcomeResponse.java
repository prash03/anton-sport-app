package com.superapp.sports.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WelcomeResponse {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("Message")
    @Expose
    private String msg;

    @SerializedName("data")
    @Expose
    private WelcomeData data;

    public WelcomeData getData() {
        return data;
    }

    public void setData(WelcomeData data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class WelcomeData{
        @SerializedName("slider_images")
        @Expose
        private List<SliderImage> sliderImages = null;

        public List<SliderImage> getSliderImages() {
            return sliderImages;
        }

        public void setSliderImages(List<SliderImage> sliderImages) {
            this.sliderImages = sliderImages;
        }
    }
    public class SliderImage {

        @SerializedName("slider_title")
        @Expose
        private String sliderTitle;

        @SerializedName("slider_sub_title")
        @Expose
        private String slider_sub_title;

        @SerializedName("slider_image")
        @Expose
        private String sliderImage;

        public String getSlider_sub_title() {
            return slider_sub_title;
        }

        public void setSlider_sub_title(String slider_sub_title) {
            this.slider_sub_title = slider_sub_title;
        }

        public String getSliderTitle() {
            return sliderTitle;
        }

        public void setSliderTitle(String sliderTitle) {
            this.sliderTitle = sliderTitle;
        }

        public String getSliderImage() {
            return sliderImage;
        }

        public void setSliderImage(String sliderImage) {
            this.sliderImage = sliderImage;
        }

    }
}
