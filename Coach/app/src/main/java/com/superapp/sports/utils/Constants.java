package com.superapp.sports.utils;

public class Constants {
    public static String ACCOUNTTYPE = "accountType";
    public static String FCMTOKEN = "fcm_token";
    public static String AUTH_TOKEN = "auth_token";
    public static String FNAME = "fname";
    public static String LNAME = "lname";
    public static String USERMOBILE = "mobile";
    public static String WEBSITE = "website";
    public static String BIO = "bio";
    public static String DOB = "dob";
    public static String GENDER = "gender";
    public static String MARITAL = "marital";
    public static String USEREMAIL = "email";
    public static String USERID = "userId";
    public static String LOGINTYPE = "logintype";
    public static String NOTIF_STATUS = "notifstat";
    public static String Key_profile_image = "profilepic";
    public static String HEIGHT = "height";
    public static String WEIGHT = "weight";
    public static String POSITION = "position";
    public static String RANK = "ranking";
    public static String SPEED = "speed";
    public static String VERTICLE = "verticle";
    public static String GPA = "gpa";
    public static String HOME = "home_town";
    public static String CLASSIFICATION = "classification";
    public static String SCHOOLNAME = "school";
    public static String EXPERIENCE = "experience";
    public static String SPORTNAME = "sports_name";
    public static String ACHIEVEMENT = "achievement";
    public static String SPONSERNAME = "sponser";
    public static String COMPANYNAME = "company_work";
    public static String SPECIALITY = "speciality";
    public static String PPG = "PPG";
    public static String RPG = "RPG";
    public static String APG = "APG";


    //public static String BASE_URL = "https://antwon.marketingchord.com/";
    // public static String BASE_URL = "https://apps.supersportselite.com/";
    public static String BASE_URL = "https://app.supersportselite.com/";
    public static String TERM_URL = "https://marketingchord.com/pabor-informetive-page";


    public static String SENDMSG = BASE_URL + "api/send_message";
    public static String RV_LOGIN_URL = BASE_URL + "api/login";
    public static String RV_COMMENTLIST_URL = BASE_URL + "api/comment_list";
    public static String RV_POSTLIST_URL = BASE_URL + "api/my_post_list";
    public static String RV_DASBOARD_URL = BASE_URL + "api/home_page";
    public static String RV_DASHBOARD_PAGE_URL = BASE_URL + "api/home_page_pagination";
    public static String RV_REGISTER_URL = BASE_URL + "api/registration";
    public static String RV_WRITEUS_URL = BASE_URL + "api/write_to_us";
    public static String RV_PICKDROP_URL = BASE_URL + "api/pickup_dropoff";
    public static String RV_SUBCAT_URL = BASE_URL + "api/sub_category_list";
    public static String RV_PRODUCT_URL = BASE_URL + "api/productlist";
    public static String RV_ADDTOCART_URL = BASE_URL + "api/addtocartproduct";

    public static String NK_PAGE_NUM = "?page_number=";

    public static String NK_PLAYSTORE_URL="https://play.google.com/store/apps/details?id=com.superapp.sports";
}
