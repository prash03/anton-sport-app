package com.superapp.sports.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedHelper {


    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    public static SharedPreferences fcmsharedPreferences;
    public static SharedPreferences.Editor fcmeditor;

    public static void putKey(Context context, String Key, String Value) {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(Key, Value);
        editor.commit();

    }

    public static String getKey(Context contextGetKey, String Key) {
        sharedPreferences = contextGetKey.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        String Value = sharedPreferences.getString(Key, "");
        return Value;

    }

    public static void putFcmKey(Context context, String Key, String Value) {
        fcmsharedPreferences = context.getSharedPreferences("fcmdata", Context.MODE_PRIVATE);
        fcmeditor = fcmsharedPreferences.edit();
        fcmeditor.putString(Key, Value);
        fcmeditor.commit();

    }

    public static String getFcmKey(Context contextGetKey, String Key) {
        fcmsharedPreferences = contextGetKey.getSharedPreferences("fcmdata", Context.MODE_PRIVATE);
        String Value = fcmsharedPreferences.getString(Key, "");
        return Value;

    }

    public static void clearSharedPreferences(Context context)
    {
        sharedPreferences = context.getSharedPreferences("Cache", Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().commit();
    }



}
