package com.superapp.sports.utils;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface UpdateImageInterface {
    @PUT("ImageName")
    Call<Void> updateProfilePhoto(
            @Query(value="X-Amz-Security-Token", encoded = true)
            @Body MultipartBody file);
}
